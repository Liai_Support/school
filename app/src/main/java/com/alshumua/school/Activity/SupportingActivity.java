//package com.alshumua.school.Activity;
//
//import androidx.appcompat.app.AppCompatActivity;
//import androidx.recyclerview.widget.LinearLayoutManager;
//import androidx.recyclerview.widget.RecyclerView;
//
//import android.content.Intent;
//import android.os.Bundle;
//import android.util.Log;
//import android.view.View;
//import android.widget.Button;
//import android.widget.ImageView;
//import android.widget.TextView;
//import android.widget.Toast;
//
//import com.alshumua.school.Adapter.BriefAdapter;
//import com.alshumua.school.Adapter.SupportingAdapter;
//import com.alshumua.school.Model.InitialScreenModel;
//import com.alshumua.school.R;
//import com.alshumua.school.Std.Constants;
//import com.alshumua.school.Std.SessionManager;
//import com.alshumua.school.Std.Urls;
//import com.android.volley.AuthFailureError;
//import com.android.volley.DefaultRetryPolicy;
//import com.android.volley.Request;
//import com.android.volley.RequestQueue;
//import com.android.volley.Response;
//import com.android.volley.VolleyError;
//import com.android.volley.VolleyLog;
//import com.android.volley.toolbox.StringRequest;
//import com.android.volley.toolbox.Volley;
//
//import org.json.JSONException;
//import org.json.JSONObject;
//
//import java.io.UnsupportedEncodingException;
//import java.util.ArrayList;
//import java.util.HashMap;
//import java.util.Map;
//
//public class SupportingActivity extends AppCompatActivity {
//    ImageView back;
//    SessionManager sessionManager;
//    View view;
//    Button goback;
//
//    private static ArrayList<InitialScreenModel> initialScreenModelArrayList;
//    private static RecyclerView.Adapter supportingAdapter;
//    private static RecyclerView supportingRecyclerView;
//TextView toolbartxt;
//    @Override
//    protected void onCreate(Bundle savedInstanceState) {
//        super.onCreate(savedInstanceState);
//        setContentView(R.layout.activity_supporting);
//        back = (ImageView) findViewById(R.id.back_from_support);
//        goback = (Button) findViewById(R.id.gobacksupport);
//        toolbartxt = (TextView) findViewById(R.id.toolbartxt);
//        sessionManager = new SessionManager(this,this,view);
//
//        supportingRecyclerView = (RecyclerView) findViewById(R.id.supportingrecyclerview);
//        supportingRecyclerView.setLayoutManager(new LinearLayoutManager(this));
//
//        if (sessionManager.getlang().equals(Constants.ARABICKEY)){
//            back.setRotationY(180);
//            sessionManager.arabicfont(toolbartxt,null,null,Constants.BOLD,"textV");
//        }
//        back.setOnClickListener(new View.OnClickListener() {
//            @Override
//            public void onClick(View v) {
//
//                Intent intent = new Intent(SupportingActivity.this, ThreeBtnScreenActivity.class);
//                startActivity(intent);
//                finish();
//            }
//        });
//        goback.setOnClickListener(new View.OnClickListener() {
//            @Override
//            public void onClick(View v) {
//
//                Intent intent = new Intent(SupportingActivity.this, ThreeBtnScreenActivity.class);
//                startActivity(intent);
//                finish();
//            }
//        });
//
//        sessionManager.progressdialogshow();
//        JSONObject signinreqBody = new JSONObject();
//        try {
//            signinreqBody.put("", "");
//
//            supportingJSON(signinreqBody);
//        } catch (
//                JSONException e) {
//            e.printStackTrace();
//        }
//    }
//
//    private void supportingJSON(JSONObject response) {
//        RequestQueue requestQueue = Volley.newRequestQueue(this);
//        final String requestBody = response.toString();
//        StringRequest stringRequest = new StringRequest(Request.Method.POST, Urls.initialscreens, new Response.Listener<String>() {
//            @Override
//            public void onResponse(String response) {
//                sessionManager.progressdialogdismiss();
//                Log.d("support_response", ">>" + response);
//                try {
//                    JSONObject obj = new JSONObject(response);
//                    if (obj.getBoolean("error") == false) {
//                        initialScreenModelArrayList = new ArrayList<InitialScreenModel>();
//                        String data = obj.getString("data");
//                        JSONObject maindata = new JSONObject(data);
//
//                        InitialScreenModel datamodel = new InitialScreenModel(
//                                SupportingActivity.this,
//                                maindata.getString("basic"),
//                                maindata.getString("supporting"));
//                        initialScreenModelArrayList.add(datamodel);
//                        supportingAdapter = new SupportingAdapter(SupportingActivity.this,initialScreenModelArrayList);
//                        supportingRecyclerView.setAdapter(supportingAdapter);
//                    } else {
//                        //sessionManager.snackbarToast(getString(R.string.nodataavailable),view);
//                    }
//                } catch (Exception e) {
//                    e.printStackTrace();
//                }
//            }
//        }, new Response.ErrorListener() {
//            @Override
//            public void onErrorResponse(VolleyError error) {
//                Log.e("VOLLEY", error.toString());
//                sessionManager.progressdialogdismiss();
//                //  sessionManager.volleyerror(error);
//                Toast.makeText(SupportingActivity.this, error.toString(), Toast.LENGTH_SHORT).show();
//            }
//        }) {
//            @Override
//            public Map<String, String> getHeaders() throws AuthFailureError {
////                int id = sessionManager.getUserId();
//                Map<String, String> params = new HashMap<String, String>();
//                params.put("lang",sessionManager.getlang());
////                    params.put("Authorization", "Bearer "+"eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJpZCI6IjEyIiwidXNlcm5hbWUiOiJTbmVoYSBzZWx2YW0ifQ.Lo7r-ebR4O89j-nYaDlr6W_Rweo4f07CWYCWktspd2o");
//                Log.d("paramList", "" + params);
////                Log.d("id", "" + id);
//                return params;
//            }
//
//            @Override
//            public String getBodyContentType() {
//                return "application/json; charset=utf-8";
//            }
//
//            @Override
//            public byte[] getBody() throws AuthFailureError {
//                try {
//                    return requestBody == null ? null : requestBody.getBytes("utf-8");
//                } catch (UnsupportedEncodingException uee) {
//                    VolleyLog.wtf("Unsupported Encoding while trying to get the bytes of %s using %s", requestBody, "utf-8");
//                    return null;
//                }
//            }
//        };
//        stringRequest.setRetryPolicy(new DefaultRetryPolicy(10000, DefaultRetryPolicy.DEFAULT_MAX_RETRIES, DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
//        Log.d("asdsdjkxdk", "" + stringRequest);
//        requestQueue.add(stringRequest);
//    }
//
//    @Override
//    public void onBackPressed() {
//        super.onBackPressed();
//        Intent intent = new Intent(SupportingActivity.this, ThreeBtnScreenActivity.class);
//        startActivity(intent);
//        finish();
//    }
//}