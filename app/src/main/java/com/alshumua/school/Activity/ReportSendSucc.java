package com.alshumua.school.Activity;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.os.Handler;
import android.view.View;
import android.widget.ImageView;

import androidx.appcompat.app.AppCompatActivity;

import com.alshumua.school.Std.Constants;
import com.alshumua.school.Std.SessionManager;
import com.alshumua.school.R;

public class ReportSendSucc extends AppCompatActivity {
    private static int SPLASHSCREENVISIBLE = 2000; //    4 sec
    ImageView back;
    SessionManager sessionManager;
    View view;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.reportsentlayout);

        back = (ImageView) findViewById(R.id.back_from_Requestsent);
        sessionManager = new SessionManager(this, this, view);

        if (sessionManager.getlang().equals(Constants.ARABICKEY)){
            back.setRotationY(180);
        }

        back.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
               /* Intent intent = new Intent(RequestSentActivity.this, DashboardActivity.class);
                startActivity(intent);
                finish();*/

                Intent i = new Intent(ReportSendSucc.this, DashboardActivity.class);
                i.putExtra("sendreport","sendreport");
                startActivity(i);
                ((Activity) ReportSendSucc.this).overridePendingTransition(0, 0);
                ((Activity) ReportSendSucc.this).finish();
                finish();
            }
        });


//            DELAY METHOD
 /*       new Handler().postDelayed(new Runnable() {
            @Override
            public void run() {
//                    TO MOVE MAIN ACTIVITY
                Intent i = new Intent(ReportSendSucc.this, DashboardActivity.class);
                i.putExtra("sendreport","sendreport");
                startActivity(i);
                ((Activity) ReportSendSucc.this).overridePendingTransition(0, 0);
                ((Activity) ReportSendSucc.this).finish();


                finish();
            }
        }, SPLASHSCREENVISIBLE);*/
    }

    @Override
    public void onBackPressed() {
        Intent i = new Intent(ReportSendSucc.this, DashboardActivity.class);
        i.putExtra("sendreport","sendreport");
        startActivity(i);
        ((Activity) ReportSendSucc.this).overridePendingTransition(0, 0);
        ((Activity) ReportSendSucc.this).finish();
       // finish();
    }
}
