package com.alshumua.school.Activity;

import static android.widget.Toast.LENGTH_SHORT;

import androidx.appcompat.app.AppCompatActivity;

import android.graphics.Color;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.Toast;

import com.alshumua.school.R;

public class SampleActivity extends AppCompatActivity {
    RadioButton rb,r0,r1,r2,r3;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.sample_design);

        RadioGroup radioGroup= (RadioGroup)findViewById(R.id.radiogroup);
        RadioButton r0= (RadioButton)findViewById(R.id.radiobrief);
        RadioButton r1= (RadioButton)findViewById(R.id.radiovid);
        RadioButton r2= (RadioButton)findViewById(R.id.radioadd);
        RadioButton r3= (RadioButton)findViewById(R.id.radiolocation);
        radioGroup.setOnCheckedChangeListener(new RadioGroup.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(RadioGroup group, int checkedId) {
                rb = (RadioButton)findViewById(checkedId);
                Log.d("TAG", "onCheckedChanged: "+rb.getText());
                if (rb.getText().equals("Location")){
                    r3.setTextColor(Color.WHITE);
                    r0.setTextColor(Color.BLACK);
                    r1.setTextColor(Color.BLACK);
                    r2.setTextColor(Color.BLACK);
                }
               else if (rb.getText().equals("Brief")){
                    r0.setTextColor(Color.WHITE);
                    r1.setTextColor(Color.BLACK);
                    r2.setTextColor(Color.BLACK);
                    r3.setTextColor(Color.BLACK);
                }
                else if (rb.getText().equals("Video")){
                    r1.setTextColor(Color.WHITE);
                    r2.setTextColor(Color.BLACK);
                    r3.setTextColor(Color.BLACK);
                    r0.setTextColor(Color.BLACK);
                }
                else if (rb.getText().equals("Address")){
                    r2.setTextColor(Color.WHITE);
                    r0.setTextColor(Color.BLACK);
                    r1.setTextColor(Color.BLACK);
                    r3.setTextColor(Color.BLACK);
                }

                Log.d("TAG", "id: "+checkedId);

            }
        });


    }
}