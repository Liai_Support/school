package com.alshumua.school.Activity;

import androidx.annotation.RequiresApi;
import androidx.appcompat.app.AppCompatActivity;
import androidx.core.content.ContextCompat;
import androidx.fragment.app.Fragment;

import android.content.Intent;
import android.graphics.Color;
import android.os.Build;
import android.os.Bundle;
import android.view.View;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.alshumua.school.Fragment.BriefFragment;
import com.alshumua.school.Fragment.ContactUsFragment;
import com.alshumua.school.R;
import com.alshumua.school.Std.Constants;
import com.alshumua.school.Std.Dialog;
import com.alshumua.school.Std.SessionManager;

public class ThreeBtnScreenActivity extends AppCompatActivity {
//    varialbes
    ImageView support,service,brief,back;
    LinearLayout servicelinear,brieflinear,supportlinear;
    SessionManager sessionManager;
    View view;
TextView toolbartxt,brieftxt,servicetxt,supporttxt;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_three_btn_screen);

//        hooks
        support = (ImageView) findViewById(R.id.supporting);
        service = (ImageView) findViewById(R.id.service);
        brief = (ImageView) findViewById(R.id.brief);
        back = (ImageView) findViewById(R.id.back_from_threebtn);
        servicelinear = (LinearLayout) findViewById(R.id.servicelinear);
        supportlinear = (LinearLayout) findViewById(R.id.supportinglinear);
        brieflinear = (LinearLayout) findViewById(R.id.brieflinear);
        sessionManager = new SessionManager(this,this,view);

        brieftxt = (TextView) findViewById(R.id.brieftxt);
        supporttxt = (TextView) findViewById(R.id.supporttxt);
        servicetxt = (TextView) findViewById(R.id.servicetxt);
        toolbartxt = (TextView) findViewById(R.id.toolbartxt);

//  ROTATE ARROWS ON ARABIC
        if (sessionManager.getlang().equals(Constants.ARABICKEY)){
            back.setRotationY(180);
            sessionManager.arabicfont(brieftxt,null,null,Constants.REGULAR,"textV");
            sessionManager.arabicfont(supporttxt,null,null,Constants.REGULAR,"textV");
            sessionManager.arabicfont(servicetxt,null,null,Constants.REGULAR,"textV");
            sessionManager.arabicfont(servicetxt,null,null,Constants.REGULAR,"textV");
            sessionManager.arabicfont(toolbartxt,null,null,Constants.BOLD,"textV");

            final int sdk = android.os.Build.VERSION.SDK_INT;
            if(sdk < android.os.Build.VERSION_CODES.JELLY_BEAN) {
                support.setBackgroundDrawable(ContextCompat.getDrawable(this, R.drawable.onsidecorner_left) );
                brief.setBackgroundDrawable(ContextCompat.getDrawable(this, R.drawable.onsidecorner_left) );
                service.setBackgroundDrawable(ContextCompat.getDrawable(this, R.drawable.onsidecorner_left) );
            } else {
                support.setBackgroundDrawable(ContextCompat.getDrawable(this, R.drawable.onsidecorner_left) );
                brief.setBackgroundDrawable(ContextCompat.getDrawable(this, R.drawable.onsidecorner_left) );
                service.setBackgroundDrawable(ContextCompat.getDrawable(this, R.drawable.onsidecorner_left) );
            }
        }

//        no network fragment
        if (!sessionManager.isNetwork() == true) {
            Dialog dialog=new Dialog();
            dialog.show(getSupportFragmentManager(), Constants.NONETWORK); }

//        clicks
        supportlinear.setOnClickListener(new View.OnClickListener() {
            @RequiresApi(api = Build.VERSION_CODES.LOLLIPOP)
            @Override
            public void onClick(View v) {
                supportlinear.setBackgroundTintList(ContextCompat.getColorStateList(ThreeBtnScreenActivity.this, R.color.white));
                /*if (!sessionManager.gettokenin().equals("")&&
                        sessionManager.isNetwork() == true) {*/
                    Intent intent = new Intent(ThreeBtnScreenActivity.this, ThreeBtnScreenActivity.class);
                    startActivity(intent);
//                    finish();
            /*    }
                else {
                    Intent intent = new Intent(ThreeBtnScreenActivity.this, NewSignin.class);
                    intent.putExtra("fromsupport","fromsupport");
                    startActivity(intent);
                }*/

            }
        });

        brieflinear.setOnClickListener(new View.OnClickListener() {
            @RequiresApi(api = Build.VERSION_CODES.LOLLIPOP)
            @Override
            public void onClick(View v) {
                brieflinear.setBackgroundTintList(ContextCompat.getColorStateList(ThreeBtnScreenActivity.this, R.color.white));
                /*Intent intent = new Intent(ThreeBtnScreenActivity.this, BriefActivity.class);
                startActivity(intent);
                finish();*/

                Fragment fragment = new BriefFragment();
              getSupportFragmentManager().beginTransaction().replace(R.id.fragment_container, fragment).addToBackStack(null).commit();
            }
        });

        servicelinear.setOnClickListener(new View.OnClickListener() {
            @RequiresApi(api = Build.VERSION_CODES.LOLLIPOP)
            @Override
            public void onClick(View v) {
                servicelinear.setBackgroundTintList(ContextCompat.getColorStateList(ThreeBtnScreenActivity.this, R.color.white));

               /* if (!sessionManager.gettokenin().equals("")&&
                        sessionManager.isNetwork() == true) {*/
                    Intent intent = new Intent(ThreeBtnScreenActivity.this, DashboardActivity.class);
                    startActivity(intent);
                    finish();



            }
        });

        back.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(ThreeBtnScreenActivity.this, LanguageActivity.class);
                startActivity(intent);
                finish();
            }
        });
    }

//    back press
    @Override
    public void onBackPressed() {
        super.onBackPressed();
        Intent intent = new Intent(ThreeBtnScreenActivity.this, LanguageActivity.class);
        startActivity(intent);
        finish();
    }
}