package com.alshumua.school.Activity;

import androidx.appcompat.app.AppCompatActivity;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.os.Handler;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.alshumua.school.R;
import com.alshumua.school.Std.Constants;
import com.alshumua.school.Std.SessionManager;

public class RequestSentActivity extends AppCompatActivity {
    private static int SPLASHSCREENVISIBLE = 1000; //    4 sec
    ImageView back;
    SessionManager sessionManager;
    Button homell;
    View view;
    TextView reqsenttxt;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.requestsent);

        back = (ImageView) findViewById(R.id.back_from_Requestsent);
        homell = (Button) findViewById(R.id.homelinear);
        reqsenttxt = (TextView) findViewById(R.id.reqsent);
        sessionManager = new SessionManager(this, this, view);

        if (sessionManager.getlang().equals("ar")){
            back.setRotationY(180);
            sessionManager.arabicfont(reqsenttxt,null, null, Constants.REGULAR,"textV");
        }

        back.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(RequestSentActivity.this, DashboardActivity.class);
                startActivity(intent);
                finish();


//                my request page
                /*Intent i = new Intent(RequestSentActivity.this, DashboardActivity.class);
                i.putExtra("send","send");
                startActivity(i);
                ((Activity) RequestSentActivity.this).overridePendingTransition(0, 0);
                ((Activity) RequestSentActivity.this).finish();*/
            }
        });
        homell.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(RequestSentActivity.this, DashboardActivity.class);
                startActivity(intent);
                finish();
            }
        });


//            DELAY METHOD
       /* new Handler().postDelayed(new Runnable() {
            @Override
            public void run() {
//                    TO MOVE MAIN ACTIVITY
                Intent i = new Intent(RequestSentActivity.this, DashboardActivity.class);
                i.putExtra("send","send");
                startActivity(i);
                ((Activity) RequestSentActivity.this).overridePendingTransition(0, 0);
                ((Activity) RequestSentActivity.this).finish();
            }
        }, SPLASHSCREENVISIBLE);*/
    }

    @Override
    public void onBackPressed() {
        Intent intent = new Intent(RequestSentActivity.this, DashboardActivity.class);
        startActivity(intent);
        finish();

    }
}