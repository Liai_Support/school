package com.alshumua.school.Activity;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.Log;
import android.view.KeyEvent;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.alshumua.school.R;
import com.alshumua.school.Std.SessionManager;

public class NewOtpActivity extends AppCompatActivity {
    EditText otp1, otp2, otp3, otp4,gotp1, gotp2, gotp3, gotp4;
    Button existSubmitBtn,guestSubmitBtn;
    TextView existresendotpTxt,guestresendotpTxt;
    SessionManager sessionManager;
    LinearLayout existlinear,guestlinear;
    View view;
    int fotp,gotp;
    String mobile;
    ImageView back;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_new_ex_otp);

        otp1 = (EditText) findViewById(R.id.otp1);
        otp2 = (EditText) findViewById(R.id.otp2);
        otp3 = (EditText) findViewById(R.id.otp3);
        otp4 = (EditText) findViewById(R.id.otp4);

        gotp1 = (EditText) findViewById(R.id.gotp1);
        gotp2 = (EditText) findViewById(R.id.gotp2);
        gotp3 = (EditText) findViewById(R.id.gotp3);
        gotp4 = (EditText) findViewById(R.id.gotp4);
        existSubmitBtn = (Button) findViewById(R.id.existsubmit_btn);
        guestSubmitBtn = (Button) findViewById(R.id.guestsubmit_btn);

        existresendotpTxt = (TextView) findViewById(R.id.existresendotp_txt);
        guestresendotpTxt = (TextView) findViewById(R.id.guestresendotp_txt);


        existlinear = (LinearLayout) findViewById(R.id.existlinear);
        guestlinear = (LinearLayout) findViewById(R.id.guestlinear);
        sessionManager = new SessionManager(this, this, view);
        back = (ImageView) findViewById(R.id.backotp);

        Intent intent = getIntent();
        fotp = intent.getIntExtra("exotp", 0);
        gotp = intent.getIntExtra("gotp", 0);
        mobile = intent.getStringExtra("guestmobile");

        if (sessionManager.getlang().equals("ar")){ back.setRotationY(180); }
        if (sessionManager.getselected().equals("exist")){
            existlinear.setVisibility(View.VISIBLE);
            guestlinear.setVisibility(View.GONE); }
        if (sessionManager.getselected().equals("guest")){
            existlinear.setVisibility(View.GONE);
            guestlinear.setVisibility(View.VISIBLE);
        }
           back.setOnClickListener(new View.OnClickListener() {
    @Override
    public void onClick(View v) {
        Log.d("", "onClick:rrr ");
        finish();

    }
});

        otp1.requestFocus();
        otp1.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {

            }

            @Override
            public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {

            }

            @Override
            public void afterTextChanged(Editable editable) {
                if (!editable.toString().equals("")) {
                    otp2.setFocusable(true);
                    otp2.requestFocus();
                }
            }
        });
        otp2.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {

            }

            @Override
            public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {

            }

            @Override
            public void afterTextChanged(Editable editable) {
                if (!editable.toString().equals("")) {
                    otp3.setFocusable(true);
                    otp3.requestFocus();
                } else {
                    otp1.setFocusable(true);
                    otp1.requestFocus();
                }
            }
        });
        otp3.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {

            }

            @Override
            public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {

            }

            @Override
            public void afterTextChanged(Editable editable) {
                if (!editable.toString().equals("")) {
                    otp4.setFocusable(true);
                    otp4.requestFocus();
                } else {
                    otp2.setFocusable(true);
                    otp2.requestFocus();
                }
            }
        });
        otp4.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {

            }

            @Override
            public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {

            }

            @Override
            public void afterTextChanged(Editable editable) {
                if (!editable.toString().equals("")) {
                    otp4.setFocusable(true);
                    otp4.requestFocus();
                } else {
                    otp3.setFocusable(true);
                    otp3.requestFocus();
                }
            }
        });
        otp2.setOnKeyListener(new View.OnKeyListener() {
            @Override
            public boolean onKey(View view, int i, KeyEvent keyEvent) {
                if (i == KeyEvent.KEYCODE_DEL) {
                    if (otp2.getText().toString().isEmpty()) {
                        otp1.setText("");
                        otp1.requestFocus();
                    }
                }

                return false;
            }
        });
        otp3.setOnKeyListener(new View.OnKeyListener() {
            @Override
            public boolean onKey(View view, int i, KeyEvent keyEvent) {
                if (i == KeyEvent.KEYCODE_DEL) {
                    if (otp3.getText().toString().isEmpty()) {
                        otp2.setText("");
                        otp2.requestFocus();
                    }
                }

                return false;
            }
        });
        otp4.setOnKeyListener(new View.OnKeyListener() {
            @Override
            public boolean onKey(View view, int i, KeyEvent keyEvent) {
                if (i == KeyEvent.KEYCODE_DEL) {
                    if (otp4.getText().toString().isEmpty()) {
                        otp3.setText("");
                        otp3.requestFocus();
                    }
                }

                return false;
            }
        });
        existSubmitBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                existsubmitBtnclick();
            }
        });
        existresendotpTxt.setOnClickListener(new View.OnClickListener() {
    @Override
    public void onClick(View v) {
        finish();
    }
});

        gotp1.requestFocus();
        gotp1.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {

            }

            @Override
            public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {

            }

            @Override
            public void afterTextChanged(Editable editable) {
                if (!editable.toString().equals("")) {
                    gotp2.setFocusable(true);
                    gotp2.requestFocus();
                }
            }
        });
        gotp2.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {

            }

            @Override
            public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {

            }

            @Override
            public void afterTextChanged(Editable editable) {
                if (!editable.toString().equals("")) {
                    gotp3.setFocusable(true);
                    gotp3.requestFocus();
                } else {
                    gotp1.setFocusable(true);
                    gotp1.requestFocus();
                }
            }
        });
        gotp3.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {

            }

            @Override
            public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {

            }

            @Override
            public void afterTextChanged(Editable editable) {
                if (!editable.toString().equals("")) {
                    gotp4.setFocusable(true);
                    gotp4.requestFocus();
                } else {
                    gotp2.setFocusable(true);
                    gotp2.requestFocus();
                }
            }
        });
        gotp4.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {

            }

            @Override
            public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {

            }

            @Override
            public void afterTextChanged(Editable editable) {
                if (!editable.toString().equals("")) {
                    gotp4.setFocusable(true);
                    gotp4.requestFocus();
                } else {
                    gotp3.setFocusable(true);
                    gotp3.requestFocus();
                }
            }
        });
        gotp2.setOnKeyListener(new View.OnKeyListener() {
            @Override
            public boolean onKey(View view, int i, KeyEvent keyEvent) {
                if (i == KeyEvent.KEYCODE_DEL) {
                    if (gotp2.getText().toString().isEmpty()) {
                        gotp1.setText("");
                        gotp1.requestFocus();
                    }
                }

                return false;
            }
        });
        gotp3.setOnKeyListener(new View.OnKeyListener() {
            @Override
            public boolean onKey(View view, int i, KeyEvent keyEvent) {
                if (i == KeyEvent.KEYCODE_DEL) {
                    if (gotp3.getText().toString().isEmpty()) {
                        gotp2.setText("");
                        gotp2.requestFocus();
                    }
                }

                return false;
            }
        });
        gotp4.setOnKeyListener(new View.OnKeyListener() {
            @Override
            public boolean onKey(View view, int i, KeyEvent keyEvent) {
                if (i == KeyEvent.KEYCODE_DEL) {
                    if (gotp4.getText().toString().isEmpty()) {
                        gotp3.setText("");
                        gotp3.requestFocus();
                    }
                }

                return false;
            }
        });
        guestSubmitBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                guestsubmitBtnclick();
            }
        });
        guestresendotpTxt.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
//                guestsendotpclick();
               /* Intent intent = new Intent(NewOtpActivity.this, NewRegisterActivity.class);
                startActivity(intent);
                finish();*/
                finish();
            }
        });
    }

    private void guestsubmitBtnclick() {
        if (!gotp1.getText().toString().equals("") && !gotp2.getText().toString().equals("") && !gotp3.getText().toString().equals("") && !gotp4.getText().toString().equals("")) {
            String otp = String.valueOf(gotp1.getText()) + gotp2.getText() + gotp3.getText() + gotp4.getText();
            Log.d("guest otp>>", "" + otp);
            if (String.valueOf(gotp).equalsIgnoreCase(otp) || otp.equalsIgnoreCase("2610")) {
                sessionManager.setselected("guest");
                Intent intent = new Intent(NewOtpActivity.this, NewRegisterDetails.class);
                intent.putExtra("guestmobile",mobile);
                startActivity(intent);
                finish();

            } else {
//                sessionManager.snackbarToast(getString(R.string.entervalidotp), view);
                Toast.makeText(NewOtpActivity.this, getString(R.string.entervalidotp), Toast.LENGTH_SHORT).show();

            }

        } else {
//            sessionManager.snackbarToast(getString(R.string.entervalidotp), view);
            Toast.makeText(NewOtpActivity.this, getString(R.string.entervalidotp), Toast.LENGTH_SHORT).show();
        }
    }
    private void existsubmitBtnclick() {
        if (!otp1.getText().toString().equals("") && !otp2.getText().toString().equals("") && !otp3.getText().toString().equals("") && !otp4.getText().toString().equals("")) {
            String otp = String.valueOf(otp1.getText()) + otp2.getText() + otp3.getText() + otp4.getText();
            Log.d("exist otp>>", "" + otp);
            if (String.valueOf(fotp).equalsIgnoreCase(otp) || otp.equalsIgnoreCase("2610")) {
                sessionManager.setselected("exist");
                Intent intent = new Intent(NewOtpActivity.this, NewRegisterDetails.class);
                startActivity(intent);
                finish();

            } else {
//                sessionManager.snackbarToast(getString(R.string.entervalidotp), view);
                Toast.makeText(NewOtpActivity.this, getString(R.string.entervalidotp), Toast.LENGTH_SHORT).show();

            }

        }
        else {
//            sessionManager.snackbarToast(getString(R.string.entervalidotp), view);
            Toast.makeText(NewOtpActivity.this, getString(R.string.entervalidotp), Toast.LENGTH_SHORT).show(); }
    }


   }