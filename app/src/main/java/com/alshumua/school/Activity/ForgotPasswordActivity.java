package com.alshumua.school.Activity;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.Toast;

import com.alshumua.school.R;
import com.alshumua.school.Std.Constants;
import com.alshumua.school.Std.SessionManager;
import com.alshumua.school.Std.Urls;
import com.android.volley.AuthFailureError;
import com.android.volley.DefaultRetryPolicy;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.VolleyLog;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;


import org.json.JSONException;
import org.json.JSONObject;

import java.io.UnsupportedEncodingException;

public class ForgotPasswordActivity extends AppCompatActivity {
    SessionManager sessionManager;
    View view;
    LinearLayout existlinear,guestlinear;
    EditText guestMobile,existMobile;
    String gmobile,exmobile;
    Button guestSubmit,existSubmit;
    ImageView back;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_forgot_password);

        existlinear = (LinearLayout) findViewById(R.id.forgotLinearExist);
        guestlinear = (LinearLayout) findViewById(R.id.forgotLinearGuest);
        guestMobile = (EditText) findViewById(R.id.mobilenumberGuest);
        existMobile = (EditText) findViewById(R.id.mobilenumberExist);
        guestSubmit = (Button) findViewById(R.id.guestsubmit_btn);
        existSubmit = (Button) findViewById(R.id.existsubmit_btn);
        back = (ImageView) findViewById(R.id.back_from_forgotpass);
        sessionManager = new SessionManager(this, this, view);

        if (sessionManager.getlang().equals(Constants.ARABICKEY)){
            back.setRotationY(180);
        }
        back.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(ForgotPasswordActivity.this, NewSignin.class);
                startActivity(intent);
                finish();
            }
        });
        if (sessionManager.gettype().equals("exist")){
            existlinear.setVisibility(View.VISIBLE);
            guestlinear.setVisibility(View.GONE);

            existSubmit.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    existSubmitClick();
                }
            });
        }
        else {
            existlinear.setVisibility(View.GONE);
            guestlinear.setVisibility(View.VISIBLE);
            guestSubmit.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    guestSubmitClick();
                }
            });
        }
    }
    private void existSubmitClick() {
        exmobile=existMobile.getText().toString();
        if (exmobile.isEmpty()){
            sessionManager.snackbarToast(getString(R.string.pleaseentermobilenumber), existSubmit);
        }
        else if (!isValidPhone(exmobile)){
            sessionManager.snackbarToast(getString(R.string.pleaseentervalidmobilenumber), existSubmit);
        }
        else {

            sessionManager.progressdialogshow();
            JSONObject existForgotBody = new JSONObject();
            try {
                existForgotBody.put("mobile_number", exmobile);
                exForgotpasswordJSON(existForgotBody);
            } catch (
                    JSONException e) {
                e.printStackTrace();
            }
        }
    }
    private void guestSubmitClick() {
        gmobile=guestMobile.getText().toString();
        if (gmobile.isEmpty()){
            sessionManager.snackbarToast(getString(R.string.pleaseentermobilenumber), existSubmit);
        }
        else if (!isValidPhone(gmobile)){
            sessionManager.snackbarToast(getString(R.string.pleaseentervalidmobilenumber), existSubmit);
        }
        else {

            sessionManager.progressdialogshow();
            JSONObject guestForgotBody = new JSONObject();
            try {
                guestForgotBody.put("mobile_number", gmobile);
                exForgotpasswordJSON(guestForgotBody);
            } catch (
                    JSONException e) {
                e.printStackTrace();
            }
        }
    }
    private void exForgotpasswordJSON(JSONObject create) {
        RequestQueue requestQueue = Volley.newRequestQueue(this);
        final String requestBody = create.toString();
        Log.d("exForgotpasswordJSON", String.valueOf(create));
        StringRequest stringRequest = new StringRequest(Request.Method.POST, Urls.forgotpassword, new Response.Listener<String>() {
            @Override
            public void onResponse(String response) {
                Log.d("exForgotpasswordRespons", ">>" + response);
                sessionManager.progressdialogdismiss();
                try {
                    JSONObject obj = new JSONObject(response);
                    if (obj.getBoolean("error") == true) {
                        String msg = obj.getString("message");
//                        sessionManager.snackbarToast(msg, view);
                        Toast.makeText(ForgotPasswordActivity.this, msg, Toast.LENGTH_SHORT).show();


                    } else {
                        String data = obj.getString("data");
                        JSONObject maindata = new JSONObject(data);
                        String msg = obj.getString("message");
//                        sessionManager.snackbarToast(msg, view);
                        Toast.makeText(ForgotPasswordActivity.this, msg, Toast.LENGTH_SHORT).show();

                        int newpassword = maindata.getInt("newpassword");
                        if (sessionManager.gettype().equals("exist")) {
                            sessionManager.setexnewpass(newpassword);
                            Log.d("", "getexnewpass: "+sessionManager.getexnewpass());

                        }
                        if (sessionManager.gettype().equals("guest")) {
                            sessionManager.setgunewpass(newpassword);
                            Log.d("", "getgunewpass: "+sessionManager.getgunewpass());
                        }
                        finish();
                        startActivity(new Intent(ForgotPasswordActivity.this, NewSignin.class));
                        finish();
                    }
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                sessionManager.progressdialogdismiss();
                Log.e("VOLLEY", error.toString());
                Toast.makeText(ForgotPasswordActivity.this, error.toString(), Toast.LENGTH_SHORT).show();
            }
        }) {
            @Override
            public String getBodyContentType() {
                return "application/json; charset=utf-8";
            }

            @Override
            public byte[] getBody() throws AuthFailureError {
                try {
                    return requestBody == null ? null : requestBody.getBytes("utf-8");
                } catch (UnsupportedEncodingException uee) {
                    VolleyLog.wtf("Unsupported Encoding while trying to get the bytes of %s using %s", requestBody, "utf-8");
                    return null;
                }
            }
        };
        stringRequest.setRetryPolicy(new DefaultRetryPolicy(10000, DefaultRetryPolicy.DEFAULT_MAX_RETRIES, DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
        Log.d("asdsdjkxdk", "" + stringRequest);
        requestQueue.add(stringRequest);
    }
    private boolean isValidPhone(String phone) {
        return android.util.Patterns.PHONE.matcher(phone).matches();
    }
    @Override
    public void onBackPressed() {
        super.onBackPressed();
        Intent intent = new Intent(ForgotPasswordActivity.this, NewSignin.class);
        startActivity(intent);
        finish();
    }
}