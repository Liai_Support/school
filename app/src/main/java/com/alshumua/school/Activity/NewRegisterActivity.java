package com.alshumua.school.Activity;

import androidx.annotation.RequiresApi;
import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.content.res.ColorStateList;
import android.content.res.Configuration;
import android.os.Build;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.Toast;

import com.alshumua.school.Std.Dialog;
import com.android.volley.AuthFailureError;
import com.android.volley.DefaultRetryPolicy;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.VolleyLog;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;
import com.alshumua.school.R;
import com.alshumua.school.Std.SessionManager;
import com.alshumua.school.Std.Urls;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.UnsupportedEncodingException;

public class NewRegisterActivity extends AppCompatActivity {
    Button existingparent_btn,guest_btn,existSendotp,guestSendotp;
    EditText studentidEt,mobilenumberEt;
    SessionManager sessionManager;
    View view;
    LinearLayout existlinear,guestlinear;
    String stuid,mobile;
    int finalValue;
    String parent_name,name,mobile_number;
    int exotp,gotp;
    ImageView back;

    @RequiresApi(api = Build.VERSION_CODES.LOLLIPOP)
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_new_ex_register);

        //        HOOK
        existingparent_btn = (Button) findViewById(R.id.existingparent_btn);
        guest_btn = (Button) findViewById(R.id.guest_btn);
        existSendotp = (Button) findViewById(R.id.existsendotp);
        guestSendotp = (Button) findViewById(R.id.guestsendotp);

        existlinear = (LinearLayout) findViewById(R.id.existlinear);
        guestlinear = (LinearLayout) findViewById(R.id.guestlinear);
        studentidEt = (EditText) findViewById(R.id.studentid);
        mobilenumberEt = (EditText) findViewById(R.id.mobilenumber);
        studentidEt.setRawInputType(Configuration.KEYBOARD_12KEY);
        mobilenumberEt.setRawInputType(Configuration.KEYBOARD_12KEY);
        back = (ImageView) findViewById(R.id.back_from_register);
        sessionManager = new SessionManager(this, this, view);

        if (!sessionManager.isNetwork() == true) {
            Dialog dialog = new Dialog();
            dialog.show(getSupportFragmentManager(), "nonetwork");
        }

        if (sessionManager.getlang().equals("ar")){
            back.setRotationY(180);
        }
        back.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(NewRegisterActivity.this, NewSignin.class);
                startActivity(intent);
                finish();
            }
        });

if (sessionManager.getselected().equals("exist")){
    guestlinear.setVisibility(View.GONE);
    existlinear.setVisibility(View.VISIBLE);
    sessionManager.setfcmtok("");
    existingparent_btnClick();
}
if (sessionManager.getselected().equals("guest")){
    existlinear.setVisibility(View.GONE);
    guestlinear.setVisibility(View.VISIBLE);
    sessionManager.setfcmtok("");
    guest_btnClick();
}
//        BUTTON CLICK
        existingparent_btn.setOnClickListener(new View.OnClickListener() {
            @RequiresApi(api = Build.VERSION_CODES.LOLLIPOP)
            @Override
            public void onClick(View v) {
                existingparent_btnClick();
            }
        });

        guest_btn.setOnClickListener(new View.OnClickListener() {
            @RequiresApi(api = Build.VERSION_CODES.LOLLIPOP)
            @Override
            public void onClick(View v) {
                guest_btnClick();
            }
        });

        existSendotp.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                existsendotpclick();
            }
        });

        guestSendotp.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                guestsendotpclick();
            }
        });
    }
    @RequiresApi(api = Build.VERSION_CODES.LOLLIPOP)
    private void guest_btnClick() {
        existlinear.setVisibility(View.GONE);
        guestlinear.setVisibility(View.VISIBLE);
        sessionManager.setfcmtok("");
        guest_btn.setBackgroundTintList(ColorStateList.valueOf(getResources().getColor(R.color.gray_l2)));
        existingparent_btn.setBackgroundTintList(ColorStateList.valueOf(getResources().getColor(R.color.gray)));
        guest_btn.setTextColor(this.getResources().getColor(R.color.white));
        existingparent_btn.setTextColor(this.getResources().getColor(R.color.black));
        sessionManager.setselected("guest");
    }
    @RequiresApi(api = Build.VERSION_CODES.LOLLIPOP)
    private void existingparent_btnClick() {
        guestlinear.setVisibility(View.GONE);
        existlinear.setVisibility(View.VISIBLE);
        sessionManager.setfcmtok("");
        guest_btn.setBackgroundTintList(ColorStateList.valueOf(getResources().getColor(R.color.gray)));
        existingparent_btn.setBackgroundTintList(ColorStateList.valueOf(getResources().getColor(R.color.gray_l2)));
        guest_btn.setTextColor(this.getResources().getColor(R.color.black));
        existingparent_btn.setTextColor(this.getResources().getColor(R.color.white));
        sessionManager.setselected("exist");
    }
    private void existsendotpclick() {

        if (sessionManager.getselected().equals("exist")) {
            stuid = studentidEt.getText().toString();
            if (!stuid.isEmpty()) {
                finalValue = Integer.parseInt(stuid);
            }
            if (stuid.equals("")) {
                sessionManager.snackbarToast(getString(R.string.pleaseenterstudentid), existSendotp);
            } else {
                sessionManager.progressdialogshow();
             /*   Intent intent = getIntent();
                resendexist = intent.getStringExtra("resendexist");


                if (resendexist.equals("resendexist")){
                    existsendotpclick();
                }
*/

                JSONObject otpreqBody = new JSONObject();
                try {
                    otpreqBody.put("student_id", finalValue);
                    getExistingparentloginrequestJSON(otpreqBody);
                } catch (
                        JSONException e) {
                    e.printStackTrace();
                }
            }

        }
    }
    private void guestsendotpclick() {


        if (sessionManager.getselected().equals("guest")){
            mobile = mobilenumberEt.getText().toString();
            if (mobile.equals("")) {
                sessionManager.snackbarToast(getString(R.string.pleaseentermobilenumber), guestSendotp);
            }
            else if (!isValidPhone(mobile)){
                sessionManager.snackbarToast(getString(R.string.pleaseentervalidmobilenumber), guestSendotp);
            }

            else {
                sessionManager.progressdialogshow();

               /* Intent intent = getIntent();
                resendguest = intent.getStringExtra("resendguest");
                if (resendguest.equals("resendguest")){
                    guestsendotpclick();
                }*/
                JSONObject otpreqBody = new JSONObject();
                try {
                    otpreqBody.put("mobile_number", mobile);
                    getGuestOtpJSON(otpreqBody);
                } catch (
                        JSONException e) {
                    e.printStackTrace();
                }
            }
        }
    }
    private boolean isValidPhone(String phone) {
        return android.util.Patterns.PHONE.matcher(phone).matches();
    }
    private void getExistingparentloginrequestJSON(JSONObject response) {
        RequestQueue requestQueue = Volley.newRequestQueue(this);
        Log.d("getExistingparentlogin", String.valueOf(response));
        StringRequest stringRequest = new StringRequest(Request.Method.POST, Urls.existingparentlogin, new Response.Listener<String>() {
            @Override
            public void onResponse(String response) {
                Log.d("Existingparentresponse", ">>" + response);
                try {
                    sessionManager.progressdialogdismiss();
                    JSONObject obj = new JSONObject(response);
                    sessionManager.setselected("exist");

                    if (obj.getBoolean("error") == true) {
                        String msg = obj.getString("message");
//                        sessionManager.snackbarToast(msg, existSendotp);
                        Toast.makeText(NewRegisterActivity.this, msg, Toast.LENGTH_SHORT).show();

                        sessionManager.setverified("no");
                        Log.d("exerror", "" + msg);
                    } else {
                        String data = obj.getString("data");
                        JSONObject maindata = new JSONObject(data);
                        exotp = maindata.getInt("otp");

                        String studentData = maindata.getString("studentData");
                        JSONObject mainstudentData = new JSONObject(studentData);
                        parent_name = mainstudentData.getString("parent_name");
                        mobile_number = mainstudentData.getString("mobile_number");
                        name = mainstudentData.getString("name");



                        if (response.toString().trim().length() != 0) {
                            sessionManager.setparent_name(parent_name);
                            sessionManager.setmobile_number(mobile_number);
                            sessionManager.setname(name);
                            sessionManager.setstudent_id(finalValue);
                            sessionManager.setverified("yes");
                            Toast.makeText(NewRegisterActivity.this, R.string.enteryourotp, Toast.LENGTH_SHORT).show();
                            if (!mobile_number.equals("")||!mobile_number.equals(null)){

                                Intent intent = new Intent(NewRegisterActivity.this, NewOtpActivity.class);
                                intent.putExtra("exotp", exotp);
                                intent.putExtra("student_id", finalValue);
                                startActivity(intent);

                            }
                            else{
//                                sessionManager.snackbarToast(getString(R.string.thereisnomobilenumber),existSendotp);
                                Toast.makeText(NewRegisterActivity.this, getString(R.string.thereisnomobilenumber), Toast.LENGTH_SHORT).show();
                            }

                        }


                    }
                }
                catch (Exception e) {
                    e.printStackTrace();
                }
            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                sessionManager.progressdialogdismiss();
                Log.e("VOLLEY", error.toString());
//                 sessionManager.volleyerror(error);
                Toast.makeText(NewRegisterActivity.this, error.toString(), Toast.LENGTH_SHORT).show();
            }
        }) {
            @Override
            public String getBodyContentType() {
                return "application/json; charset=utf-8";
            }
            @Override
            public byte[] getBody() throws com.android.volley.AuthFailureError {
                String str = "{\"student_id\":"+finalValue+"}";
                Log.d("TAG", "getBody: "+str);//{"student_id":1116025}
                Log.d("TAG", "getBody1: "+str.getBytes());
                return str.getBytes();
            };
        };
        stringRequest.setRetryPolicy(new DefaultRetryPolicy(10000, DefaultRetryPolicy.DEFAULT_MAX_RETRIES, DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
        Log.d("asdsdjkxdk", "" + stringRequest);
        requestQueue.add(stringRequest);
    }
    private void getGuestOtpJSON(JSONObject response) {
        RequestQueue requestQueue = Volley.newRequestQueue(this);
        final String requestBody = response.toString();
        Log.d("guestsend otp", String.valueOf(response));
        StringRequest stringRequest = new StringRequest(Request.Method.POST, Urls.guestusersendotp, new Response.Listener<String>() {
            @Override
            public void onResponse(String response) {
                Log.d("guestsend otp response", ">>" + response);
                try {
                    sessionManager.progressdialogdismiss();
                    JSONObject obj = new JSONObject(response);
                    sessionManager.setselected("guest");

                    if (obj.getBoolean("error") == true) {
                        String msg = obj.getString("message");
//                        sessionManager.snackbarToast(msg, guestSendotp);
                        Toast.makeText(NewRegisterActivity.this, msg, Toast.LENGTH_SHORT).show();

                        sessionManager.setverified("no");
                        Log.d("exerror", "" + msg);
                    } else {
                        String data = obj.getString("data");
                        JSONObject maindata = new JSONObject(data);
                        gotp = maindata.getInt("otp");




                        if (response.toString().trim().length() != 0) {
                            sessionManager.setverified("yes");
                            Toast.makeText(NewRegisterActivity.this, R.string.enteryourotp, Toast.LENGTH_SHORT).show();
//                            startActivity(new Intent(ExistingParentdata.this, DashboardActivity.class));

                                if (!mobile.equals("")||!mobile.equals(null)) {
                                Intent intent = new Intent(NewRegisterActivity.this, NewOtpActivity.class);
                                intent.putExtra("gotp", gotp);
                                intent.putExtra("guestmobile", mobile);
                                startActivity(intent);
                            }
                                else{
//                                sessionManager.snackbarToast(getString(R.string.thereisnomobilenumber),guestSendotp);
                                    Toast.makeText(NewRegisterActivity.this, getString(R.string.thereisnomobilenumber), Toast.LENGTH_SHORT).show();
                                }
                        }
                    }
                }
                catch (Exception e) {
                    e.printStackTrace();
                }
            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                sessionManager.progressdialogdismiss();
                Log.e("VOLLEY", error.toString());
//                 sessionManager.volleyerror(error);
                Toast.makeText(NewRegisterActivity.this, error.toString(), Toast.LENGTH_SHORT).show();

            }
        }) {
            @Override
            public String getBodyContentType() {
                return "application/json; charset=utf-8";
            }
              @Override
            public byte[] getBody() throws AuthFailureError {
                try {
                    return requestBody == null ? null : requestBody.getBytes("utf-8");
                } catch (UnsupportedEncodingException uee) {
                    VolleyLog.wtf("Unsupported Encoding while trying to get the bytes of %s using %s", requestBody, "utf-8");
                    return null;
                }

            };
        };
        stringRequest.setRetryPolicy(new DefaultRetryPolicy(10000, DefaultRetryPolicy.DEFAULT_MAX_RETRIES, DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
        Log.d("asdsdjkxdk", "" + stringRequest);
        requestQueue.add(stringRequest);
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
        Intent intent = new Intent(NewRegisterActivity.this, NewSignin.class);
        startActivity(intent);
        finish();
    }
}