package com.alshumua.school.Activity;

import android.content.Intent;
import android.os.Bundle;
import android.os.Handler;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.RelativeLayout;

import androidx.appcompat.app.AppCompatActivity;

import com.alshumua.school.Std.Constants;
import com.alshumua.school.Std.Dialog;
import com.alshumua.school.R;
import com.alshumua.school.Std.SessionManager;
import com.alshumua.school.Std.Urls;

public class SplashActivity extends AppCompatActivity {
    //    variables
    Animation animationfadein;
    ImageView imageView;
    private Window mWindow;
    SessionManager sessionManager;
    View view;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_splash);
        view = (RelativeLayout) findViewById(R.id.mysplashrelativeLayout);
        sessionManager = new SessionManager(this, this, view);

        if (!sessionManager.isNetwork() == true) {
            Dialog dialog = new Dialog();
            dialog.show(getSupportFragmentManager(), Constants.NONETWORK);
        }

//        HIDING STATUS BAR(manifest,style)
        mWindow = getWindow();
        mWindow.getDecorView().setSystemUiVisibility(View.SYSTEM_UI_FLAG_LAYOUT_STABLE | View.SYSTEM_UI_FLAG_LAYOUT_FULLSCREEN);

//            LOAD THE ANIMATION
        animationfadein = AnimationUtils.loadAnimation(this, R.anim.animation_fade);

//           HOOKS
        imageView = findViewById(R.id.splash_logo_img);
        sessionManager = new SessionManager(this, this, view);

//            SET IMG WITH ANIMATION
        imageView.setAnimation(animationfadein);

//            DELAY METHOD
        new Handler().postDelayed(new Runnable() {
            @Override
            public void run() {
//                    TO MOVE MAIN ACTIVITY
                Intent intent = new Intent(SplashActivity.this, LanguageActivity.class);
                startActivity(intent);
                finish();
            }
        }, Constants.SPLASHSCREENVISIBLE);
    }

    //-------------------FOR BACK PRESS-----------------------------------------------------------
    @Override
    public void onBackPressed() {
        this.finish();
        super.onBackPressed();
    }
}




