package com.alshumua.school.Activity;

import static android.content.ContentValues.TAG;

import androidx.annotation.NonNull;
import androidx.annotation.RequiresApi;
import androidx.appcompat.app.AppCompatActivity;
import androidx.core.content.res.ResourcesCompat;

import android.content.Intent;
import android.content.SharedPreferences;
import android.content.res.ColorStateList;
import android.content.res.Configuration;
import android.content.res.Resources;
import android.graphics.Color;
import android.os.Build;
import android.os.Bundle;
import android.util.DisplayMetrics;
import android.util.Log;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.view.WindowManager;
import android.widget.AdapterView;
import android.widget.Button;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import com.alshumua.school.FCM.MyFCMtoken;
import com.alshumua.school.LanguageSpinnerPackage.CountryAdapter;
import com.alshumua.school.LanguageSpinnerPackage.CountryItem;
import com.alshumua.school.R;
import com.alshumua.school.Std.Constants;
import com.alshumua.school.Std.Dialog;
import com.alshumua.school.Std.SessionManager;
import com.alshumua.school.Std.Urls;
import com.android.volley.AuthFailureError;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.VolleyLog;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;
import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.messaging.FirebaseMessaging;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.UnsupportedEncodingException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Locale;
import java.util.Map;


public class LanguageActivity extends AppCompatActivity implements View.OnClickListener {
//    variables
    private ArrayList<CountryItem> mCountryList;
    private CountryAdapter mAdapter;
    String langcode= Constants.ENGLISHKEY;
    SessionManager sessionManager;
    Button continuenext;
    private Toast backToast;
    private long backPressedTime;
    View view;

    private TextView arabic, english;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_language);

        MyFCMtoken.getToken(this);
        Log.d("", "tockk: "+ MyFCMtoken.getToken(this));
//        hooks
        continuenext=(Button)findViewById(R.id.language_btn);
        view = (RelativeLayout) findViewById(R.id.root);
        sessionManager=new SessionManager(this,this,view);

//        check camera and storage permission for profile image
        isCheck();

//        no network fragment
        if (!sessionManager.isNetwork() == true) {
            Dialog dialog=new Dialog();
            dialog.show(getSupportFragmentManager(),Constants.NONETWORK); }

//        programmatically set status bar color as green
      /*  if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
            Window window = getWindow();
            window.addFlags(WindowManager.LayoutParams.FLAG_DRAWS_SYSTEM_BAR_BACKGROUNDS);
            window.setStatusBarColor(getResources().getColor(R.color.white)); }*/

        continuenext.setOnClickListener(this);

        /*------------------new lang------------------*/
        arabic = (TextView) findViewById(R.id.arabic);
        english = (TextView) findViewById(R.id.english);
        arabic.setOnClickListener(new View.OnClickListener() {
            @RequiresApi(api = Build.VERSION_CODES.LOLLIPOP)
            @Override
            public void onClick(View v) {
//                if(isCheck() == true){
//               arabic.setBackgroundColor(Color.parseColor("#3BB54A"));
                arabic.getBackground().setTint(getResources().getColor(R.color.linear_green));
                    arabic.setEnabled(false);
                    //sessionManager.progressdialogshow();
                    sessionManager.setAppLocale("ar");
                    sessionManager.setlang(Constants.ARABICKEY);
                    Intent n5 = new Intent(LanguageActivity.this, DashboardActivity.class);
                    startActivity(n5);
                    finish();
                    //sessionManager.progressdialogdismiss();

            }
        });
        english.setOnClickListener(new View.OnClickListener() {
            @RequiresApi(api = Build.VERSION_CODES.LOLLIPOP)
            @Override
            public void onClick(View v) {
//                if(isCheck() == true){
                english.getBackground().setTint(getResources().getColor(R.color.linear_green));
                    english.setEnabled(false);
                    //sessionManager.progressdialogshow();
                    sessionManager.setAppLocale("eng");
                    sessionManager.setlang(Constants.ENGLISHKEY);
                    Intent n5 = new Intent(LanguageActivity.this, DashboardActivity.class);
                    startActivity(n5);
                    finish();

            }
        });

        if(!sessionManager.gettoken().equals("")){
            FirebaseMessaging.getInstance().subscribeToTopic("allDevices");
            FirebaseMessaging.getInstance().getToken()
                    .addOnCompleteListener(new OnCompleteListener<String>() {
                        @Override
                        public void onComplete(@NonNull Task<String> task) {
                            if (!task.isSuccessful()) {
                                Log.w(TAG, "Fetching FCM registration token failed", task.getException());
                                return;
                            }
                            else {
                                // Get new FCM registration token
                                String token = task.getResult();
                                Log.d(TAG, "token:lang "+token);
                                JSONObject requestbody = new JSONObject();
                                try {
                                    requestbody.put("userid", sessionManager.getid());
                                    requestbody.put("token", token);
                                    requestbody.put("device", "Android");
                                    updatedevicetoken(requestbody);
                                    sessionManager.progressdialogshow();
                                } catch (JSONException e) {
                                    e.printStackTrace();
                                }
                            }
                        }
                    });
        }




        /*------------------new lang------------------*/
//        DROPDOWN TO SELECT LANGUAGE
//        adding dropdown elements on list
        initList();
        Spinner spinnerCountries = findViewById(R.id.spinner_countries);
        mAdapter = new CountryAdapter(this, mCountryList);
        spinnerCountries.setAdapter(mAdapter);
        spinnerCountries.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                CountryItem clickedItem = (CountryItem) parent.getItemAtPosition(position);
                String clickedCountryName = clickedItem.getCountryName();

//                if select arabic on dropdown
                if (clickedCountryName.equals(getString(R.string.arabic))){
                    langcode = Constants.ARABICKEY;
                    sessionManager.setlang(Constants.ARABICKEY);
                    continuenext.setText(getString(R.string.continue_arabic));
                }
//                if select english on dropdown
                if (clickedCountryName.equals(getString(R.string.english))){
                    langcode = Constants.ENGLISHKEY;
                    sessionManager.setlang(Constants.ENGLISHKEY);
                    continuenext.setText(getString(R.string.continue_english));
                }
//                if nothing select on dropdown
                if (clickedCountryName.equals(getString(R.string.ChooseYourLanguage))){
                    langcode = "";
                }
            }
            @Override
            public void onNothingSelected(AdapterView<?> parent) {
            }
        });
    }
    private void initList() {
        mCountryList = new ArrayList<>();
        mCountryList.add(new CountryItem(getString(R.string.ChooseYourLanguage)));
        mCountryList.add(new CountryItem(getString(R.string.arabic)));
        mCountryList.add(new CountryItem(getString(R.string.english)));
    }

    @Override
    public void onClick(View v) {

//        button click
        if (v == continuenext) {
            if (!sessionManager.isNetwork() == true) {
                Dialog dialog = new Dialog();
                dialog.show(getSupportFragmentManager(), Constants.NONETWORK);
            }
            if (langcode.equals(Constants.ARABICKEY)) {
                setAppLocale(langcode);
            }
            if (langcode.equals(Constants.ENGLISHKEY)) {
                setAppLocale(langcode);
            }
            if (langcode.equals("")) {
                sessionManager.snackbarToast(getString(R.string.PleaseselectLanguage), continuenext);
            }
        }

    }
    private void setAppLocale(String localeCode) {
        Resources res=getResources();
        DisplayMetrics dm=res.getDisplayMetrics();
        Configuration conf=res.getConfiguration();
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.JELLY_BEAN_MR1){
            conf.setLocale(new Locale(localeCode.toLowerCase()));
        }
        else{
            conf.locale=new Locale(localeCode.toLowerCase());
        }
        SharedPreferences.Editor editor=getSharedPreferences("Settings",MODE_PRIVATE).edit();
        editor.putString("My_Lang",localeCode);
        editor.apply();
        res.updateConfiguration(conf,dm);
    }

//  back press twise to exit

    @Override
    public void onBackPressed() {
        if (backPressedTime+Constants.BACKPRESS_TIME>System.currentTimeMillis()) {
            backToast.cancel();
            super.onBackPressed();
            return;
        }
        else {
            backToast=Toast.makeText(getBaseContext(),getString(R.string.PressBackAgaintoExit),Toast.LENGTH_SHORT);
            backToast.show();
        }
        backPressedTime=System.currentTimeMillis();
    }
    @Override
    protected void onStart() {
        super.onStart();
        continuenext.setEnabled(true);
    }
    public Boolean isCheck(){
        if(!sessionManager.isPermissionsEnabled()){
            sessionManager.permissionsEnableRequest();
            return false;
        }
        return true;
    }
    private void updatedevicetoken(JSONObject response) {
        RequestQueue requestQueue = Volley.newRequestQueue(this);
        JSONObject jsonBody = response;
        final String requestcartBody = jsonBody.toString();
        Log.d("requesttoken", "" + requestcartBody);
        StringRequest stringRequest = new StringRequest(Request.Method.POST, Urls.updateusertoken, response1 -> {
            sessionManager.progressdialogdismiss();
            Log.d("requesttokenresponse", "" + response1);
            try {
                JSONObject obj = new JSONObject(response1);
                JSONObject tok = new JSONObject(obj.getString("data"));
                if(obj.getBoolean("error") == false){
                    sessionManager.snackbarToast(obj.getString("message"),view);
                }
                else {
                    sessionManager.snackbarToast(obj.getString("message"),view);
                }

            } catch (JSONException e) {
                e.printStackTrace();
            }

        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                sessionManager.progressdialogdismiss();
                Log.e("VOLLEY", error.toString());
                sessionManager.volleyerror(error);
            }
        }) {
            @Override
            public Map<String, String> getHeaders() throws AuthFailureError {
                int id = sessionManager.getid();
                Map<String, String> params = new HashMap<String, String>();
                //params.put("Content-Type","application/json" );
                params.put("Authorization", "Bearer " + sessionManager.gettoken());
                Log.d("param", "" + params);
                Log.d("id", "" + id);
                return params;
            }

            @Override
            public String getBodyContentType() {
                return "application/json; charset=utf-8";
            }

            @Override
            public byte[] getBody() throws AuthFailureError {
                try {
                    return requestcartBody == null ? null : requestcartBody.getBytes("utf-8");
                } catch (UnsupportedEncodingException uee) {
                    VolleyLog.wtf("Unsupported Encoding while trying to get the bytes of %s using %s", requestcartBody, "utf-8");
                    return null;
                }
            }
        };
        requestQueue.add(stringRequest);
    }
}
