package com.alshumua.school.Activity;


import android.content.Context;
import android.content.Intent;
import android.graphics.Color;
import android.graphics.PorterDuff;
import android.os.Build;
import android.os.Bundle;
import android.text.Html;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;

import androidx.appcompat.app.AppCompatActivity;
import androidx.viewpager.widget.PagerAdapter;
import androidx.viewpager.widget.ViewPager;

import com.alshumua.school.R;
import com.alshumua.school.Std.SessionManager;
import com.google.android.material.tabs.TabLayout;

public class WelcomeActivity extends AppCompatActivity {

    private ViewPager viewPager;
    private MyViewPagerAdapter myViewPagerAdapter;
    private LinearLayout dotsLayout;
    private TextView[] dots;
    private int[] layouts;
    private Button lastslide;
    TextView btnSkip,next_txt,next_txt2;
    LinearLayout linearLayout_welcome;
    public boolean processClick = true;
    View view;
    SessionManager sessionManager;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_welcome);
        view = (RelativeLayout) findViewById(R.id.root);
        sessionManager = new SessionManager(this,this,view);
        // Making notification bar transparent
        if (Build.VERSION.SDK_INT >= 21) {
            getWindow().getDecorView().setSystemUiVisibility(View.SYSTEM_UI_FLAG_LAYOUT_STABLE | View.SYSTEM_UI_FLAG_LAYOUT_FULLSCREEN);
        }

        viewPager = (ViewPager) findViewById(R.id.view_pager);
//        dotsLayout = (LinearLayout) findViewById(R.id.layoutDots);
        linearLayout_welcome = (LinearLayout) findViewById(R.id.linear_welcome);
        btnSkip = (TextView) findViewById(R.id.btn_skip);
        next_txt = (TextView) findViewById(R.id.next_welcome);
        next_txt2 = (TextView) findViewById(R.id.next_welcome1);
        lastslide = (Button) findViewById(R.id.lastslide);



        if (!(sessionManager.getwelcomeslidedone() ==0) && !sessionManager.gettokenin().equals("")&&
                sessionManager.isNetwork() == true) {
            Intent intent = new Intent(WelcomeActivity.this, DashboardActivity.class);
            startActivity(intent);
            finish();
        }

        next_txt.setVisibility(View.VISIBLE);
        next_txt2.setVisibility(View.GONE);
        lastslide.setVisibility(View.GONE);

        linearLayout_welcome.getBackground().setColorFilter(Color.parseColor("#F5F4F4"), PorterDuff.Mode.SRC_ATOP);
        btnSkip.setTextColor(Color.BLACK);

        layouts = new int[]{
                R.layout.welcome_slide1,
                R.layout.welcome_slide2,
                R.layout.welcome_slide3,

        };
        addBottomDots(0);
        changeStatusBarColor();

        myViewPagerAdapter = new MyViewPagerAdapter();
        viewPager.setAdapter(myViewPagerAdapter);
        viewPager.addOnPageChangeListener(viewPagerPageChangeListener);

        TabLayout tabLayout = (TabLayout) findViewById(R.id.tabLayout);
        tabLayout.setupWithViewPager(viewPager, true);

        lastslide.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if(processClick) {
                    btnSkip.setEnabled(false);
                    btnSkip.setClickable(false);
                    sessionManager.setwelcomeslidedone(1);
                    Intent intent=new Intent(WelcomeActivity.this, NewSignin.class);
                    startActivity(intent);
                    finish();
                }
            }
        });
        btnSkip.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if(processClick) {
                    btnSkip.setEnabled(false);
                    btnSkip.setClickable(false);
                    sessionManager.setwelcomeslidedone(1);
                    Intent intent=new Intent(WelcomeActivity.this, NewSignin.class);
                    startActivity(intent);
                    finish();
                }
            }
        });
        next_txt.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                int current = getItem(+1);
                if (current < layouts.length) {
                    viewPager.setCurrentItem(current);
                }
            }
        });
        next_txt2.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                int current = getItem(+1);
                if (current < layouts.length) {
                    viewPager.setCurrentItem(current);
                }
            }
        });

    }


    private void addBottomDots(int currentPage) {
        dots = new TextView[layouts.length];

        int[] colorsActive = getResources().getIntArray(R.array.array_dot_active);
        int[] colorsInactive = getResources().getIntArray(R.array.array_dot_inactive);

        dotsLayout.removeAllViews();
        for (int i = 0; i < dots.length; i++) {
            dots[i] = new TextView(this);
            dots[i].setText(Html.fromHtml("&#x2B2C;"));
            dots[i].setTextSize(35);
            dots[i].setTextColor(colorsInactive[currentPage]);
            dotsLayout.addView(dots[i]);
        }

        if (dots.length > 0)
            dots[currentPage].setTextColor(colorsActive[currentPage]);
    }

    private int getItem(int i) {
        return viewPager.getCurrentItem() + i;
    }

    ViewPager.OnPageChangeListener viewPagerPageChangeListener = new ViewPager.OnPageChangeListener() {

        @Override
        public void onPageSelected(int position) {
            addBottomDots(position);

            Log.d("", "onPageSelected: 2"+layouts.length);
            if (position == layouts.length - 1) {
           next_txt.setVisibility(View.GONE);
           next_txt2.setVisibility(View.GONE);
           lastslide.setVisibility(View.VISIBLE);
                btnSkip.setVisibility(View.GONE);

            } else {
                next_txt2.setText(R.string.next_caps);
                btnSkip.setVisibility(View.VISIBLE);
            }
            if (position==0){
                next_txt.setVisibility(View.VISIBLE);
                next_txt2.setVisibility(View.GONE);
                lastslide.setVisibility(View.GONE);
                linearLayout_welcome.getBackground().setColorFilter(Color.parseColor("#F5F4F4"), PorterDuff.Mode.SRC_ATOP);
                btnSkip.setTextColor(Color.BLACK);
                next_txt.setText(R.string.next_caps);
                btnSkip.setVisibility(View.VISIBLE);
            }
            if (position==1){
                next_txt.setVisibility(View.GONE);
                next_txt2.setVisibility(View.VISIBLE);
                lastslide.setVisibility(View.GONE);
                linearLayout_welcome.getBackground().setColorFilter(Color.parseColor("#F5F4F4"), PorterDuff.Mode.SRC_ATOP);
                btnSkip.setTextColor(Color.BLACK);
                next_txt.setText(R.string.next_caps);
                btnSkip.setVisibility(View.VISIBLE);
            }
            if (position==layouts.length - 1){
                next_txt.setVisibility(View.GONE);
                next_txt2.setVisibility(View.GONE);
                lastslide.setVisibility(View.VISIBLE);
            }
        }

        @Override
        public void onPageScrolled(int arg0, float arg1, int arg2) {

        }

        @Override
        public void onPageScrollStateChanged(int arg0) {

        }
    };

    private void changeStatusBarColor() {
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
            Window window = getWindow();
            window.clearFlags(WindowManager.LayoutParams.FLAG_TRANSLUCENT_STATUS);
            window.addFlags(WindowManager.LayoutParams.FLAG_DRAWS_SYSTEM_BAR_BACKGROUNDS);
            window.setStatusBarColor(Color.TRANSPARENT);
        }
    }

    /**
     * View pager adapter
     */
    public class MyViewPagerAdapter extends PagerAdapter {
        private LayoutInflater layoutInflater;
        public MyViewPagerAdapter() {
        }

        @Override
        public Object instantiateItem(ViewGroup container, int position) {
            layoutInflater = (LayoutInflater) getSystemService(Context.LAYOUT_INFLATER_SERVICE);

            View view = layoutInflater.inflate(layouts[position], container, false);
            container.addView(view);

            return view;
        }

        @Override
        public int getCount() {
            return layouts.length;
        }

        @Override
        public boolean isViewFromObject(View view, Object obj) {
            return view == obj;
        }


        @Override
        public void destroyItem(ViewGroup container, int position, Object object) {
            View view = (View) object;
            container.removeView(view);
        }
    }
    @Override
    protected void onStart() {
        super.onStart();
        btnSkip.setEnabled(true);
        btnSkip.setClickable(true);
    }

}