package com.alshumua.school.Activity;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.graphics.Color;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.Toast;

import com.alshumua.school.Std.Dialog;
import com.alshumua.school.Std.SessionManager;
import com.alshumua.school.Std.Urls;
import com.android.volley.AuthFailureError;
import com.android.volley.DefaultRetryPolicy;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.VolleyLog;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;
import com.alshumua.school.R;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.UnsupportedEncodingException;

public class NewRegisterDetails extends AppCompatActivity {
    LinearLayout existlinear,guestlinear;
    Button existRegBtn,guestRegBtn;
    EditText studentnameEt,parentnameEt,exEmail,exUsername,exPass,exRepass,exMobile;
    EditText lastnameEt,firstnameEt,guestEmail,guestUsername,guestPass,guestRepass,guestMobile,guestParentname;
    SessionManager sessionManager;
   View view;
    String mobile;
    String pname,stuname,eemail,emobile,epass,erepass,eusername;
    String fname,lname,gemail,gmobile,gpass,grepass,gusername,gparent;
String stuid;
ImageView back;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_new_register_details);

        back = (ImageView) findViewById(R.id.back_from_registerdetail);
        existlinear = (LinearLayout) findViewById(R.id.exist_linear);
        guestlinear = (LinearLayout) findViewById(R.id.guest_linear);

        existRegBtn = (Button) findViewById(R.id.exregisterbtn);
        guestRegBtn = (Button) findViewById(R.id.guestregisterbtn);

        studentnameEt = (EditText) findViewById(R.id.studentname);
        parentnameEt = (EditText) findViewById(R.id.parentname);
        exEmail = (EditText) findViewById(R.id.exemail);
        exUsername = (EditText) findViewById(R.id.exusername);
        exPass = (EditText) findViewById(R.id.expassword);
        exRepass = (EditText) findViewById(R.id.exrepass);
        exMobile = (EditText) findViewById(R.id.exmobile);

        firstnameEt = (EditText) findViewById(R.id.firstname);
        lastnameEt = (EditText) findViewById(R.id.lastname);
        guestUsername = (EditText) findViewById(R.id.guestusername);
        guestEmail = (EditText) findViewById(R.id.guestemail);
        guestMobile = (EditText) findViewById(R.id.guestmobile);
        guestPass = (EditText) findViewById(R.id.guestpassword);
        guestRepass = (EditText) findViewById(R.id.guestrepass);
        guestParentname = (EditText) findViewById(R.id.guestparentname);

        sessionManager = new SessionManager(this, this, view);

        Intent intent = getIntent();
        mobile = intent.getStringExtra("guestmobile");

        stuid= String.valueOf(sessionManager.getstudent_id());

        if (!sessionManager.isNetwork() == true) {
            Dialog dialog = new Dialog();
            dialog.show(getSupportFragmentManager(), "nonetwork");
        }

        if (sessionManager.getlang().equals("ar")){
            back.setRotationY(180);
        }
        back.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(NewRegisterDetails.this, NewSignin.class);
                startActivity(intent);
                finish();
            }
        });

        if (sessionManager.getselected().equals("exist")){
            existlinear.setVisibility(View.VISIBLE);
            guestlinear.setVisibility(View.GONE);
            if (!sessionManager.getparent_name().isEmpty()){
                parentnameEt.setText(sessionManager.getparent_name());
                parentnameEt.setFocusable(false);
                parentnameEt.setEnabled(false);
                parentnameEt.setTextColor(Color.parseColor("#B8B8B8"));
            }
            else {
                parentnameEt.setFocusable(true);
                parentnameEt.setEnabled(true);
            }
            if (!stuid.isEmpty()){
                exUsername.setText(stuid);
                exUsername.setFocusable(false);
                exUsername.setEnabled(false);
                exUsername.setTextColor(Color.parseColor("#B8B8B8"));
            }
            else {
                exUsername.setFocusable(true);
                exUsername.setEnabled(true);
            }
            if (!sessionManager.getname().isEmpty()){
                studentnameEt.setText(sessionManager.getname());
                studentnameEt.setFocusable(false);
                studentnameEt.setEnabled(false);
                studentnameEt.setTextColor(Color.parseColor("#B8B8B8"));
            }
            else {
                studentnameEt.setFocusable(true);
                studentnameEt.setEnabled(true);
            }
            if (!sessionManager.getmobile_number().isEmpty()){
                exMobile.setText(sessionManager.getmobile_number());
                exMobile.setFocusable(false);
                exMobile.setEnabled(false);
                exMobile.setTextColor(Color.parseColor("#B8B8B8"));
            }
            else {
                exMobile.setFocusable(true);
                exMobile.setEnabled(true);
            }

        }
        if (sessionManager.getselected().equals("guest")){
            existlinear.setVisibility(View.GONE);
            guestlinear.setVisibility(View.VISIBLE);
            if (!mobile.isEmpty()){
                guestMobile.setText(mobile);
                guestMobile.setFocusable(false);
                guestMobile.setEnabled(false);
                guestMobile.setTextColor(Color.parseColor("#B8B8B8"));
            }
            else {
                guestMobile.setFocusable(true);
                guestMobile.setEnabled(true);
            }
        }
        existRegBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                pname=parentnameEt.getText().toString().trim();
                stuname=studentnameEt.getText().toString().trim();
                eemail=exEmail.getText().toString().trim();
                epass=exPass.getText().toString().trim();
                erepass=exRepass.getText().toString().trim();
                emobile=exMobile.getText().toString().trim();
                eusername=exUsername.getText().toString().trim();

               /* exUsername.setEnabled(false);
                exMobile.setEnabled(false);
                studentnameEt.setEnabled(false);
                parentnameEt.setEnabled(false);
                exUsername.setTextColor(Color.parseColor("#B8B8B8"));
                exMobile.setTextColor(Color.parseColor("#B8B8B8"));
                studentnameEt.setTextColor(Color.parseColor("#B8B8B8"));
                parentnameEt.setTextColor(Color.parseColor("#B8B8B8"));*/

                if (!pname.isEmpty()&&!stuname.isEmpty()&&!eemail.isEmpty()&&
                        !epass.isEmpty()&&!erepass.isEmpty()&&!emobile.isEmpty()){
                    if (!epass.equals(erepass)){
//                        sessionManager.snackbarToast(getString(R.string.passdoesnotmatch),view);
                            Toast.makeText(NewRegisterDetails.this, getString(R.string.passdoesnotmatch), Toast.LENGTH_SHORT).show();
                    }
                    else if (!isValidMail(eemail)){
                        Toast.makeText(NewRegisterDetails.this, getString(R.string.entervalidemail), Toast.LENGTH_SHORT).show();
                    }
                    else{

                        sessionManager.progressdialogshow();
                        JSONObject existRegBody = new JSONObject();
                        try {
                            existRegBody.put("username",eusername);
                            existRegBody.put("password", epass);
                            existRegBody.put("first_name", stuname);
                            existRegBody.put("last_name", pname);
                            existRegBody.put("email", eemail);
                            existRegBody.put("mobile_number", emobile);
                            existRegBody.put("type", "exist");
                            existRegBody.put("parent_name", pname);
                            exRegisterJSON(existRegBody);
                        } catch (
                                JSONException e) {
                            e.printStackTrace();
                        }
                    }

                }
                else {
//                    sessionManager.snackbarToast(getString(R.string.enterallfields),view);
                    Toast.makeText(NewRegisterDetails.this, getString(R.string.enterallfields), Toast.LENGTH_SHORT).show();

                }

            }
        });

        guestRegBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                fname=firstnameEt.getText().toString().trim();
                lname=lastnameEt.getText().toString().trim();
                gemail=guestEmail.getText().toString().trim();
                gmobile=guestMobile.getText().toString().trim();
                gpass=guestPass.getText().toString().trim();
                grepass=guestRepass.getText().toString().trim();
                gusername=guestUsername.getText().toString().trim();
                gparent=guestParentname.getText().toString().trim();

                if (!fname.isEmpty()&&!lname.isEmpty()&&!gemail.isEmpty()&&
                        !gmobile.isEmpty()&&!gpass.isEmpty()&&!grepass.isEmpty()&&
                        !gusername.isEmpty()){
                    if (!gpass.equals(grepass)){
//                        sessionManager.snackbarToast(getString(R.string.passdoesnotmatch),view);
                            Toast.makeText(NewRegisterDetails.this, getString(R.string.passdoesnotmatch), Toast.LENGTH_SHORT).show();

                    }
                    else if (!isValidMail(gemail)){
                        Toast.makeText(NewRegisterDetails.this, getString(R.string.entervalidemail), Toast.LENGTH_SHORT).show();
                    }
                    else{
                        sessionManager.progressdialogshow();
                        JSONObject existRegBody = new JSONObject();
                        try {
                            existRegBody.put("username",gusername);
                            existRegBody.put("password", gpass);
                            existRegBody.put("first_name", fname);
                            existRegBody.put("last_name", lname);
                            existRegBody.put("email", gemail);
                            existRegBody.put("mobile_number", gmobile);
                            existRegBody.put("type", "guest");
                            existRegBody.put("parent_name", gparent);
                            exRegisterJSON(existRegBody);
                        } catch (
                                JSONException e) {
                            e.printStackTrace();
                        }
                    }

                }
                else {
//                    sessionManager.snackbarToast(getString(R.string.enterallfields),view);
                    Toast.makeText(NewRegisterDetails.this, getString(R.string.enterallfields), Toast.LENGTH_SHORT).show();


                }

            }
        });

    }

    private void exRegisterJSON(JSONObject create) {
        RequestQueue requestQueue = Volley.newRequestQueue(this);
        final String requestBody = create.toString();
        Log.d("ExRegrequestJSON", String.valueOf(create));
        StringRequest stringRequest = new StringRequest(Request.Method.POST, Urls.Signup, new Response.Listener<String>() {
            @Override
            public void onResponse(String response) {
                Log.d("ExRegrespons", ">>" + response);
                sessionManager.progressdialogdismiss();
                try {
                    JSONObject obj = new JSONObject(response);
                    if (obj.getBoolean("error") == true) {
                        String msg = obj.getString("message");
//                        sessionManager.snackbarToast(msg, view);
                        Toast.makeText(NewRegisterDetails.this, msg, Toast.LENGTH_SHORT).show();


                    } else {
                        JSONObject tok = new JSONObject(obj.getString("data"));
                        String msg = obj.getString("message");
                        sessionManager.settoken(tok.getString("accesstoken"));
//                        sessionManager.setguest(1);
                        sessionManager.setid(tok.getInt("id"));
                        sessionManager.setusernamedisplay(tok.getString("username"));
                        Toast.makeText(NewRegisterDetails.this, msg, Toast.LENGTH_SHORT).show();
//                        sessionManager.snackbarToast(msg, view);
                        Toast.makeText(NewRegisterDetails.this,msg, Toast.LENGTH_SHORT).show();


                        Intent intent = new Intent(NewRegisterDetails.this, NewSignin.class);
                        startActivity(intent);
                        finish();
                    }
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                sessionManager.progressdialogdismiss();
                Log.e("VOLLEY", error.toString());
                Toast.makeText(NewRegisterDetails.this, error.toString(), Toast.LENGTH_SHORT).show();
            }
        }) {
            @Override
            public String getBodyContentType() {
                return "application/json; charset=utf-8";
            }

            @Override
            public byte[] getBody() throws AuthFailureError {
                try {
                    return requestBody == null ? null : requestBody.getBytes("utf-8");
                } catch (UnsupportedEncodingException uee) {
                    VolleyLog.wtf("Unsupported Encoding while trying to get the bytes of %s using %s", requestBody, "utf-8");
                    return null;
                }
            }
        };
        stringRequest.setRetryPolicy(new DefaultRetryPolicy(10000, DefaultRetryPolicy.DEFAULT_MAX_RETRIES, DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
        Log.d("asdsdjkxdk", "" + stringRequest);
        requestQueue.add(stringRequest);
    }

    private boolean isValidMail(String email) {
        return android.util.Patterns.EMAIL_ADDRESS.matcher(email).matches();
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
        Intent intent = new Intent(NewRegisterDetails.this, NewRegisterActivity.class);
        startActivity(intent);
        finish();
    }
}