package com.alshumua.school.Activity;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;
import androidx.fragment.app.Fragment;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.os.Handler;
import android.util.Log;
import android.view.MenuItem;
import android.view.View;
import android.widget.Toast;

import com.alshumua.school.FCM.MyFCMtoken;
import com.alshumua.school.Fragment.BriefFragment;
import com.alshumua.school.Fragment.HomeFragment;
import com.alshumua.school.Fragment.Language_ProfileFragment;
import com.alshumua.school.Fragment.MyRequestFragment;
import com.alshumua.school.Fragment.ProfileFragment;
import com.alshumua.school.Fragment.ServiceDetailsFragment;
import com.alshumua.school.Fragment.ThreeBtnFragment;
import com.alshumua.school.Fragment.briefNewFragment;
import com.alshumua.school.R;
import com.alshumua.school.Std.SessionManager;

import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.Task;
import com.google.android.material.bottomnavigation.BottomNavigationView;
import com.google.firebase.FirebaseApp;
import com.google.firebase.FirebaseOptions;
import com.google.firebase.messaging.FirebaseMessaging;
import com.google.firebase.messaging.RemoteMessage;

import static android.content.ContentValues.TAG;

public class DashboardActivity extends AppCompatActivity {

    Fragment selectedFragment = null;
    Activity selectedActivity = null;
    String langfrom = "prrrr";
    String myrequest = "prrrr";
    String myprofile = "prrrr";
    String home = "prrrr";
    String profilesetting = "prrrr";
    String form = "prrrr";
    String send="prrrr";
    String sendreport="prrrr";
    String aboutus="prrrr";
    BottomNavigationView bottomNav;
  public   SessionManager sessionManager;

    View view;
    private Toast backToast;
    private long backPressedTime;




    private boolean isFirstBackPressed = false;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        langfrom = getIntent().getStringExtra("langfrom");
        myrequest = getIntent().getStringExtra("mypro");
        myprofile = getIntent().getStringExtra("pro");
        form = getIntent().getStringExtra("form");
        home = getIntent().getStringExtra("home");
        send = getIntent().getStringExtra("send");
        sendreport = getIntent().getStringExtra("sendreport");
        aboutus = getIntent().getStringExtra("aboutus");
        sessionManager = new SessionManager(this, this, view);


        profilesetting = getIntent().getStringExtra("profilesetting");

        Log.d(TAG, "langfrom" + langfrom);
        Log.d(TAG, "mypro" + myrequest);
        Log.d(TAG, "pro" + myprofile);
        Log.d(TAG, "profilesetting" + profilesetting);
        bottomNav = findViewById(R.id.bottom_navigation);
        bottomNav.setOnNavigationItemSelectedListener(navListener);


//        firebase
        MyFCMtoken.getToken(this);
        Log.d("", "tockk: "+ MyFCMtoken.getToken(this));

        if (getIntent().getBooleanExtra("LOGOUT", false)) {
            if (getSupportFragmentManager().getBackStackEntryCount() != 0) {
                super.onBackPressed();
            } else {
                if (isFirstBackPressed) {
                    super.onBackPressed();
                } else {
                    isFirstBackPressed = true;
                    backToast = Toast.makeText(getBaseContext(), "Press Back Again to Exit", Toast.LENGTH_SHORT);
                    backToast.show();
                    new Handler().postDelayed(new Runnable() {
                        @Override
                        public void run() {
                            isFirstBackPressed = false;
                        }
                    }, 1500);
                }
            }
        }





        //I added this if statement to keep the selected fragment when rotating the device
        if (savedInstanceState == null) {
            getSupportFragmentManager().beginTransaction().replace(R.id.fragment_container,
//                    new briefNewFragment()).commit();
                    new ThreeBtnFragment()).commit();

        }


        if (langfrom != null && !langfrom.equals("")) {
            if (langfrom.equals("profile")) {
                profilelangfragmentload();
            }
        }
        if (form != null && !form.equals("")) {
            if (form.equals("form")) {
                serviceDetailfragmentload();
            }
        }
        if (send != null && !send.equals("")) {
            if (send.equals("send")) {
                myreqfragmentload();
            }
        }
        if (profilesetting != null && !profilesetting.equals("")) {
            if (profilesetting.equals("profilesetting")) {
                profilefragmentload();
            }
        }
        if (myrequest != null && !myrequest.equals("")) {
            if (myrequest.equals("mypro")) {
                homefragmentload();
            }
        }
        if (sendreport != null && !sendreport.equals("")) {
            if (sendreport.equals("sendreport")) {
                homefragmentload();
            }
        }
        if (myprofile != null && !myprofile.equals("")) {
            if (myprofile.equals("pro")) {
                homefragmentload();
            }
        }
        if (home != null && !home.equals("")) {
            if (home.equals("home")) {

            }
        }
        if (aboutus != null && !aboutus.equals("")) {
            if (aboutus.equals("aboutus")) {
               brieffragmentload();
            }
        }

    }

    //    myrequest back//profile back
    public void homefragmentload() {
        navigationchangefragment1(new ThreeBtnFragment(), 0, 0);
    }
    public void brieffragmentload() {
        navigationchangefragmentbrief(new BriefFragment(), 0, 0);
    }

    public void navigationchangefragment1(Fragment fragment, int bnavid, int snavid) {
        for (int i = 0; i < bottomNav.getMenu().size(); i++) {
            bottomNav.getMenu().getItem(i).setCheckable(true);
        }
        getSupportFragmentManager().beginTransaction().replace(R.id.fragment_container, fragment, fragment.getClass().getSimpleName()).addToBackStack(null).commit();

        if (bnavid != -1) {
            bottomNav.getMenu().getItem(bnavid).setCheckable(true);
            bottomNav.getMenu().getItem(bnavid).setChecked(true);

        }

    }
    public void navigationchangefragmentbrief(Fragment fragment, int bnavid, int snavid) {
        for (int i = 0; i < bottomNav.getMenu().size(); i++) {
            bottomNav.getMenu().getItem(i).setCheckable(true);
        }
        getSupportFragmentManager().beginTransaction().replace(R.id.fragment_container, fragment, fragment.getClass().getSimpleName()).addToBackStack(null).commit();

        if (bnavid != -1) {
            bottomNav.getMenu().getItem(bnavid).setCheckable(true);
            bottomNav.getMenu().getItem(bnavid).setChecked(true);

        }

    }
    //    lang profile back
    public void profilelangfragmentload() {
        navigationchangefragment(new Language_ProfileFragment(), 2, 0);
    }

    public void navigationchangefragment(Fragment fragment, int bnavid, int snavid) {
        for (int i = 0; i < bottomNav.getMenu().size(); i++) {
            bottomNav.getMenu().getItem(i).setCheckable(true);
        }

        getSupportFragmentManager().beginTransaction().replace(R.id.fragment_container, fragment, fragment.getClass().getSimpleName()).addToBackStack(null).commit();

        if (bnavid != -1) {
            bottomNav.getMenu().getItem(bnavid).setCheckable(true);
            bottomNav.getMenu().getItem(bnavid).setChecked(true);

        }

    }

    public void myreqfragmentload() {
        navigationchangetomyreq(new MyRequestFragment(), 1, 0);
    }

    public void profilefragmentload() {
        navigationchangetoprofile(new ProfileFragment(), 2, 0);
    }

    public void navigationchangetoprofile(Fragment fragment, int bnavid, int snavid) {
        for (int i = 0; i < bottomNav.getMenu().size(); i++) {
            bottomNav.getMenu().getItem(i).setCheckable(true);
        }

        getSupportFragmentManager().beginTransaction().replace(R.id.fragment_container, fragment, fragment.getClass().getSimpleName()).addToBackStack(null).commit();

        if (bnavid != -1) {
            bottomNav.getMenu().getItem(bnavid).setCheckable(true);
            bottomNav.getMenu().getItem(bnavid).setChecked(true);

        }

    }

    public void navigationchangetomyreq(Fragment fragment, int bnavid, int snavid) {
        for (int i = 0; i < bottomNav.getMenu().size(); i++) {
            bottomNav.getMenu().getItem(i).setCheckable(true);
        }

        getSupportFragmentManager().beginTransaction().replace(R.id.fragment_container, fragment, fragment.getClass().getSimpleName()).addToBackStack(null).commit();

        if (bnavid != -1) {
            bottomNav.getMenu().getItem(bnavid).setCheckable(true);
            bottomNav.getMenu().getItem(bnavid).setChecked(true);

        }

    }

//    for form back
public void serviceDetailfragmentload() {
    navigationchangetohome(new ServiceDetailsFragment(), 0, 0);
}

    public void navigationchangetohome(Fragment fragment, int bnavid, int snavid) {
        for (int i = 0; i < bottomNav.getMenu().size(); i++) {
            bottomNav.getMenu().getItem(i).setCheckable(true);
        }

        getSupportFragmentManager().beginTransaction().replace(R.id.fragment_container, fragment, fragment.getClass().getSimpleName()).addToBackStack(null).commit();

        if (bnavid != -1) {
            bottomNav.getMenu().getItem(bnavid).setCheckable(true);
            bottomNav.getMenu().getItem(bnavid).setChecked(true);
        }
    }
    private BottomNavigationView.OnNavigationItemSelectedListener navListener =
            new BottomNavigationView.OnNavigationItemSelectedListener() {
                @Override
                public boolean onNavigationItemSelected(@NonNull MenuItem item) {
                    switch (item.getItemId()) {
                        case R.id.home:
                            selectedFragment = new ThreeBtnFragment();
                            break;
                        case R.id.myrequest:
                            selectedFragment = new MyRequestFragment();
                            break;
                        case R.id.profile:
                            selectedFragment = new ProfileFragment();
                            break;
                    }
                    getSupportFragmentManager().beginTransaction().replace(R.id.fragment_container,
                            selectedFragment).commit();

                    return true;
                }
            };



}
