package com.alshumua.school.Activity;

import androidx.annotation.RequiresApi;
import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.content.res.ColorStateList;
import android.content.res.Configuration;
import android.os.Build;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.alshumua.school.Std.Constants;
import com.android.volley.AuthFailureError;
import com.android.volley.DefaultRetryPolicy;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.VolleyLog;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;

import com.alshumua.school.R;
import com.alshumua.school.Std.Dialog;
import com.alshumua.school.Std.SessionManager;
import com.alshumua.school.Std.Urls;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.UnsupportedEncodingException;
import java.util.HashMap;
import java.util.Map;

public class NewSignin extends AppCompatActivity {
//    VAR
    Button existingparent_btn,guest_btn,signinBtnguest,signinBtnexist;
    TextView registerTxtguest,registerTxtexist,forgotpassGuest,forgotpassExist;
    EditText usernameGuestEt,studentidExistEt,passwordExistEt,passwordGuestEt;
    SessionManager sessionManager;
    ImageView back;
    View view;
    private String stuid,pass,uname,upass;
    int finalValue,uid;
    CheckBox rememberMeGuest,rememberMeExist;
    LinearLayout existLinear,guestLinear;
    String value;
    private Toast backToast;
    private long backPressedTime;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.newflowlogin);
        
//        HOOK
        existingparent_btn = (Button) findViewById(R.id.existingparent_btn);
        guest_btn = (Button) findViewById(R.id.guest_btn);
        signinBtnexist = (Button) findViewById(R.id.signinexist);
        signinBtnguest = (Button) findViewById(R.id.signinguest);
        usernameGuestEt = (EditText) findViewById(R.id.usernameguest);
        passwordGuestEt = (EditText) findViewById(R.id.passwordguest);
        studentidExistEt = (EditText) findViewById(R.id.studentidexist);
        passwordExistEt = (EditText) findViewById(R.id.passwordexist);
        registerTxtexist = (TextView) findViewById(R.id.register_txtexist);
        registerTxtguest = (TextView) findViewById(R.id.register_txtguest);

        forgotpassExist = (TextView) findViewById(R.id.forgot_passwordexist);
        forgotpassGuest = (TextView) findViewById(R.id.forgot_passwordguest);

        rememberMeExist = (CheckBox) findViewById(R.id.remembermeexixt);
        rememberMeGuest = (CheckBox) findViewById(R.id.remembermeguest);

        existLinear = (LinearLayout) findViewById(R.id.existlinear);
        guestLinear = (LinearLayout) findViewById(R.id.guestlinear);
        back = (ImageView) findViewById(R.id.back_from_signin);


        sessionManager = new SessionManager(this, this, view);

        existLinear.setVisibility(View.VISIBLE);
        guestLinear.setVisibility(View.GONE);

//        get intent data from supporting org screen
        Intent support=getIntent();
        if (!support.equals(null)){
            value = support.getStringExtra("fromsupport");
        }

        Log.d("TAG", "fromsupport"+value);
//        if user already signed in --> directly go to home screen
        if (!sessionManager.gettokenin().equals("") && sessionManager.isNetwork() == true) {
            Intent intent = new Intent(NewSignin.this, DashboardActivity.class);
            startActivity(intent);
            finish();
        }

//        set keypad with only number for enter student id
        studentidExistEt.setRawInputType(Configuration.KEYBOARD_12KEY);

        if (sessionManager.getremembermeExpass().isEmpty()&&
                sessionManager.getremembermeExuname().isEmpty()){
            studentidExistEt.setText(sessionManager.getremembermeExuname());
            passwordExistEt.setText(sessionManager.getremembermeExpass());
        }

//        no network check
        if(!sessionManager.isNetwork()==true){
            Dialog dialog = new Dialog();
            dialog.show(getSupportFragmentManager(), Constants.NONETWORK);
        }

//        BUTTON CLICK
        existingparent_btn.setOnClickListener(new View.OnClickListener() {
            @RequiresApi(api = Build.VERSION_CODES.LOLLIPOP)
            @Override
            public void onClick(View v) {
                existingparent_btnClick();
            }
        });
        guest_btn.setOnClickListener(new View.OnClickListener() {
            @RequiresApi(api = Build.VERSION_CODES.LOLLIPOP)
            @Override
            public void onClick(View v) {
                guest_btnClick();
            }
        });
        registerTxtguest.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                sessionManager.setselected(Constants.GUEST);
                Intent intent = new Intent(NewSignin.this, NewRegisterActivity.class);
                startActivity(intent);
                finish();
            }
        });
        registerTxtexist.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                sessionManager.setselected(Constants.EXIST);
                Intent intent = new Intent(NewSignin.this, NewRegisterActivity.class);
                startActivity(intent);
                finish();
            }
        });
        signinBtnexist.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                    existSigninclick();
            }
        });
        signinBtnguest.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                guestSigninclick();
            }
        });
        forgotpassGuest.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                sessionManager.settype(Constants.GUEST);
                Intent intent = new Intent(NewSignin.this, ForgotPasswordActivity.class);
                startActivity(intent);
                finish();
            }
        });
        forgotpassExist.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                sessionManager.settype(Constants.EXIST);
                Intent intent = new Intent(NewSignin.this, ForgotPasswordActivity.class);
                startActivity(intent);
                finish();
            }
        });
        back.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(NewSignin.this, DashboardActivity.class);
                startActivity(intent);
                finish();
            }
        });

    }
//    methods
    private void existSigninclick() {
        sessionManager.setselected(Constants.EXIST);
        guestLinear.setVisibility(View.GONE);
        stuid = studentidExistEt.getText().toString();
        if (!stuid.isEmpty()) {
            finalValue = Integer.parseInt(stuid);
        }
        pass = passwordExistEt.getText().toString();
//check box exist
        if (rememberMeExist.isChecked()) {
            sessionManager.setremembermeExuname(stuid);
            sessionManager.setremembermeExpass(pass);
        } else {
            sessionManager.setremembermeExuname("");
            sessionManager.setremembermeExpass("");
        }

        if (stuid.equals("") && pass.equals("")) {
            sessionManager.snackbarToast(getString(R.string.enterallfields), signinBtnexist);
        } else if (stuid.equals("")) {
            sessionManager.snackbarToast(getString(R.string.pleaseenterstudentid), signinBtnexist);
        } else if (pass.equals("")) {
            sessionManager.snackbarToast(getString( R.string.enterpass), signinBtnexist);
        }
        else if(!sessionManager.isNetwork()==true){
            Dialog dialog = new Dialog();
            dialog.show(getSupportFragmentManager(), Constants.NONETWORK);
        }
        else {
            sessionManager.progressdialogshow();
            JSONObject signinreqBody = new JSONObject();
            try {
                signinreqBody.put("username", stuid);
                signinreqBody.put("password", pass);
                signinrequestJSON(signinreqBody);
            } catch (
                    JSONException e) {
                e.printStackTrace();
            }
        }

    }
    private void guestSigninclick() {
        sessionManager.setselected("guest");
        existLinear.setVisibility(View.GONE);
        uname = usernameGuestEt.getText().toString();
        upass = passwordGuestEt.getText().toString();
//        check box guest
        if (rememberMeGuest.isChecked()) {
            sessionManager.setremembermeGuuname(stuid);
            sessionManager.setremembermeGupass(pass);
        } else {
            sessionManager.setremembermeGuuname("");
            sessionManager.setremembermeGupass("");
        }

        if (uname.equals("") && upass.equals("")) {
            sessionManager.snackbarToast(getString(R.string.enterallfields), signinBtnguest);
        } else if (uname.equals("")) {
            sessionManager.snackbarToast(getString(R.string.enterusername), signinBtnguest);
        } else if (upass.equals("")) {
            sessionManager.snackbarToast(getString( R.string.enterpass), signinBtnguest);
        }
        else if(!sessionManager.isNetwork()==true){
            Dialog dialog = new Dialog();
            dialog.show(getSupportFragmentManager(), Constants.NONETWORK);
        }
        else {
            sessionManager.progressdialogshow();
            JSONObject signinreqBody = new JSONObject();
            try {
                signinreqBody.put("username", uname);
                signinreqBody.put("password", upass);
                signinrequestJSON(signinreqBody);
            } catch (
                    JSONException e) {
                e.printStackTrace();
            }
        }


    }
    private void signinrequestJSON(JSONObject response) {
        RequestQueue requestQueue = Volley.newRequestQueue(this);
        final String requestBody = response.toString();
        Log.d("signinrequest", String.valueOf(response));
        StringRequest stringRequest = new StringRequest(Request.Method.POST, Urls.Signin, new Response.Listener<String>() {
            @Override
            public void onResponse(String response) {
                Log.d("signinresponse", ">>" + response);
                try {
                    sessionManager.progressdialogdismiss();
                    JSONObject obj = new JSONObject(response);
                    if (obj.getBoolean("error") == true) {
                        String msg = obj.getString("message");
                        Toast.makeText(NewSignin.this, msg, Toast.LENGTH_SHORT).show();
                        Log.d("loginerror", "" + msg);
                    } else {
                        String msg = obj.getString("data");
                        JSONObject tok = new JSONObject(msg);

                        String token = tok.getString("accessToken");
                        String username = tok.getString("username");
                        String type = tok.getString("type");
                        uid = tok.getInt("id");

                        if (response.toString().trim().length() != 0) {
                            sessionManager.settokenin(token);
                            sessionManager.setusernamedisplay(username);
                            sessionManager.setid(uid);
                            sessionManager.settype(type);
//                            Toast.makeText(NewSignin.this, R.string.loginsucc, Toast.LENGTH_SHORT).show();
                         /* if (value!=null) {
                              if (value.equals("fromsupport")) {
                                  Intent intent = new Intent(NewSignin.this, SupportingActivity.class);
                                  startActivity(intent);
                                  finish();
                              }
                          }
                            if (value==null) {*/
                                startActivity(new Intent(NewSignin.this, DashboardActivity.class));
                                finish();

                        }

                    }
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                sessionManager.progressdialogdismiss();
                Log.e("VOLLEY", error.toString());
//                 sessionManager.volleyerror(error);
                Toast.makeText(NewSignin.this, error.toString(), Toast.LENGTH_SHORT).show();
            }
        }) {
            @Override
            public String getBodyContentType() {
                return "application/json; charset=utf-8";
            }

            @Override
            public Map<String, String> getHeaders() throws AuthFailureError {
                Map<String, String> params = new HashMap<String, String>();
                params.put("lang",sessionManager.getlang() );
                Log.d("param", "" + params);
                return params;
            }

            @Override
            public byte[] getBody() throws AuthFailureError {
                try {
                    return requestBody == null ? null : requestBody.getBytes("utf-8");
                } catch (UnsupportedEncodingException uee) {
                    VolleyLog.wtf("Unsupported Encoding while trying to get the bytes of %s using %s", requestBody, "utf-8");
                    return null;
                }
            }
        };
        stringRequest.setRetryPolicy(new DefaultRetryPolicy(10000, DefaultRetryPolicy.DEFAULT_MAX_RETRIES, DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
        Log.d("asdsdjkxdk", "" + stringRequest);
        requestQueue.add(stringRequest);
    }
    @RequiresApi(api = Build.VERSION_CODES.LOLLIPOP)
    private void guest_btnClick() {
        guestLinear.setVisibility(View.VISIBLE);
        existLinear.setVisibility(View.GONE);
        guest_btn.setBackgroundTintList(ColorStateList.valueOf(getResources().getColor(R.color.gray_l2)));
        existingparent_btn.setBackgroundTintList(ColorStateList.valueOf(getResources().getColor(R.color.gray)));
        guest_btn.setTextColor(this.getResources().getColor(R.color.white));
        existingparent_btn.setTextColor(this.getResources().getColor(R.color.black));
        sessionManager.setselected(Constants.GUEST);
        sessionManager.setfcmtok("");

        if (usernameGuestEt.equals("")&&
                passwordGuestEt.equals("")){
            usernameGuestEt.setText(sessionManager.getremembermeGuuname());
            passwordGuestEt.setText(sessionManager.getremembermeGupass());
        }


    }

    @RequiresApi(api = Build.VERSION_CODES.LOLLIPOP)
    private void existingparent_btnClick() {
        guestLinear.setVisibility(View.GONE);
        existLinear.setVisibility(View.VISIBLE);
        sessionManager.setfcmtok("");
        guest_btn.setBackgroundTintList(ColorStateList.valueOf(getResources().getColor(R.color.gray)));
        existingparent_btn.setBackgroundTintList(ColorStateList.valueOf(getResources().getColor(R.color.gray_l2)));
        guest_btn.setTextColor(this.getResources().getColor(R.color.black));
        existingparent_btn.setTextColor(this.getResources().getColor(R.color.white));
        sessionManager.setselected(Constants.EXIST);

        if (studentidExistEt.equals("")&&
                passwordExistEt.equals("")){

        }
        studentidExistEt.setText(sessionManager.getremembermeExuname());
        passwordExistEt.setText(sessionManager.getremembermeExpass());


    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
        Intent intent = new Intent(NewSignin.this, DashboardActivity.class);
        startActivity(intent);
        finish();
    }

    @Override
    protected void onStart() {
        super.onStart();
    }
}