package com.alshumua.school.Model;

import android.content.Context;

public class SupportingorgModel {
    Context context;
    String title, icons;

    public SupportingorgModel(Context context, String title, String icons) {
        this.context = context;
        this.title = title;
        this.icons = icons;
    }

    public Context getContext() {
        return context;
    }

    public void setContext(Context context) {
        this.context = context;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getIcons() {
        return icons;
    }

    public void setIcons(String icons) {
        this.icons = icons;
    }
}
