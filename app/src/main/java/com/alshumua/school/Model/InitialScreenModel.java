package com.alshumua.school.Model;

import android.content.Context;

public class InitialScreenModel {
    Context context;
    String basic, supporting;

    public InitialScreenModel(Context context, String basic, String supporting) {
        this.context = context;
        this.basic = basic;
        this.supporting = supporting;
    }

    public Context getContext() {
        return context;
    }

    public String getBasic() {
        return basic;
    }

    public String getSupporting() {
        return supporting;
    }
}
