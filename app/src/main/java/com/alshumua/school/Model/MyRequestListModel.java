package com.alshumua.school.Model;

import android.content.Context;

import org.json.JSONArray;

public class MyRequestListModel {
    Context context;
    String serviceName, status, remarks, date,serviceIcon;
    int serviceId,requestId;

    public MyRequestListModel(Context context,
                              String serviceName,
                              String status,
                              String remarks,
                              String date,
                              String serviceIcon,
                              int serviceId,
                              int requestId
                              ) {
        this.context = context;
        this.serviceName = serviceName;
        this.status = status;
        this.remarks = remarks;
        this.date = date;
        this.serviceIcon = serviceIcon;
        this.serviceId = serviceId;
        this.requestId = requestId;

    }



    public String getServiceName() {
        return serviceName;
    }



    public String getDate() {
        return date;
    }

    public String getServiceIcon() {
        return serviceIcon;
    }


    public int getRequestId() {
        return requestId;
    }

}