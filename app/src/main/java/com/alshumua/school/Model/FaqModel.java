package com.alshumua.school.Model;

import android.content.Context;

public class FaqModel {
    Context context;
    String id, question,answer;

    public FaqModel(Context context, String id, String question, String answer) {
        this.context = context;
        this.id = id;
        this.question = question;
        this.answer = answer;
    }

    public Context getContext() {
        return context;
    }

    public String getId() {
        return id;
    }

    public String getQuestion() {
        return question;
    }

    public String getAnswer() {
        return answer;
    }
}
