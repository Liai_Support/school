package com.alshumua.school.Model;

import android.content.Context;

public class NotificationModel {
    Context context;
    String id,reqId,title,message,created_at;

    public NotificationModel(Context context,
                             String id,
                             String reqId,
                             String title,
                             String message,
                             String created_at) {
        this.context = context;
        this.id = id;
        this.reqId = reqId;
        this.title = title;
        this.message = message;
        this.created_at = created_at;
    }

    public Context getContext() {
        return context;
    }

    public String getId() {
        return id;
    }

    public String getReqId() {
        return reqId;
    }

    public String getTitle() {
        return title;
    }

    public String getMessage() {
        return message;
    }

    public String getCreated_at() {
        return created_at;
    }
}
