package com.alshumua.school.Model;

import android.content.Context;

public class ServiceModel {
    Context context;
    int id;
    String serviceName, description, serviceIcon,loginRequired,initial,symptoms;

    public ServiceModel(Context context,
                        int id,
                        String serviceName,
                        String description,
                        String serviceIcon,
                        String loginRequired,
                        String initial,
                        String symptoms) {
        this.context = context;
        this.id = id;
        this.serviceName = serviceName;
        this.description = description;
        this.serviceIcon = serviceIcon;
        this.loginRequired = loginRequired;
        this.initial = initial;
        this.symptoms = symptoms;
    }

    public Context getContext() {
        return context;
    }

    public int getId() {
        return id;
    }

    public String getServiceName() {
        return serviceName;
    }

    public String getDescription() {
        return description;
    }

    public String getServiceIcon() {
        return serviceIcon;
    }

    public String getLoginRequired() {
        return loginRequired;
    }

    public String getInitial() {
        return initial;
    }

    public String getSymptoms() {
        return symptoms;
    }
}