package com.alshumua.school.Model;

import android.content.Context;

import org.json.JSONArray;

import java.util.ArrayList;

public class ServiceRequestModel {
     Context context;
     int id, noOfFields,noOfQuestions;
     String subHead, question, fieldType;
     String answerFields, mandatory,inputType,description,autofill,ifAnswered;
//     ArrayList selectedStrings;


     public ServiceRequestModel(Context context,
                                int id,
                                int noOfFields,
                                int noOfQuestions,
                                String subHead,
                                String question,
                                String fieldType,
                                String answerFields,
                                String mandatory,
                                String inputType,
                                String description,
                                String autofill,
                                String ifAnswered) {
          this.context = context;
          this.id = id;
          this.noOfFields = noOfFields;
          this.noOfQuestions = noOfQuestions;
          this.subHead = subHead;
          this.question = question;
          this.fieldType = fieldType;
          this.answerFields = answerFields;
          this.mandatory = mandatory;
          this.inputType = inputType;
          this.description = description;
          this.autofill = autofill;
          this.ifAnswered = ifAnswered;
     }

     public Context getContext() {
          return context;
     }

     public int getId() {
          return id;
     }

     public int getNoOfFields() {
          return noOfFields;
     }

     public int getNoOfQuestions() {
          return noOfQuestions;
     }

     public String getSubHead() {
          return subHead;
     }

     public String getQuestion() {
          return question;
     }

     public String getFieldType() {
          return fieldType;
     }

     public String getAnswerFields() {
          return answerFields;
     }

     public String getMandatory() {
          return mandatory;
     }

     public String getInputType() {
          return inputType;
     }

     public String getDescription() {
          return description;
     }

     public String getAutofill() {
          return autofill;
     }

     public String getIfAnswered() {
          return ifAnswered;
     }
}