package com.alshumua.school.Model;

import org.json.JSONArray;

public class RequestDetailsModel {
    String serviceName,status,remarks,date,questionAnswers;

    public RequestDetailsModel(String serviceName,
                               String status,
                               String remarks,
                               String date,
                               String questionAnswers) {
        this.serviceName = serviceName;
        this.status = status;
        this.remarks = remarks;
        this.date = date;
        this.questionAnswers = questionAnswers;
    }

    public String getServiceName() {
        return serviceName;
    }

    public String getStatus() {
        return status;
    }

    public String getRemarks() {
        return remarks;
    }

    public String getDate() {
        return date;
    }

    public String getQuestionAnswers() {
        return questionAnswers;
    }
}
