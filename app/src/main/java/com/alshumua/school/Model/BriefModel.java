package com.alshumua.school.Model;

import android.content.Context;

import org.json.JSONArray;

public class BriefModel {
    Context context;
   String address,video,latitute,longitute,title,icon,content,phone,mail;

    public BriefModel(Context context,
                      String address,
                      String video,
                      String latitute,
                      String longitute,
                      String title,
                      String icon,
                      String content,
                      String phone,
                      String mail) {
        this.context = context;
        this.address = address;
        this.video = video;
        this.latitute = latitute;
        this.longitute = longitute;
        this.title = title;
        this.icon = icon;
        this.content = content;
        this.phone = phone;
        this.mail = mail;
    }

    public Context getContext() {
        return context;
    }

    public void setContext(Context context) {
        this.context = context;
    }

    public String getAddress() {
        return address;
    }

    public void setAddress(String address) {
        this.address = address;
    }

    public String getVideo() {
        return video;
    }

    public void setVideo(String video) {
        this.video = video;
    }

    public String getLatitute() {
        return latitute;
    }

    public void setLatitute(String latitute) {
        this.latitute = latitute;
    }

    public String getLongitute() {
        return longitute;
    }

    public void setLongitute(String longitute) {
        this.longitute = longitute;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getIcon() {
        return icon;
    }

    public void setIcon(String icon) {
        this.icon = icon;
    }

    public String getContent() {
        return content;
    }

    public void setContent(String content) {
        this.content = content;
    }

    public String getPhone() {
        return phone;
    }

    public void setPhone(String phone) {
        this.phone = phone;
    }

    public String getMail() {
        return mail;
    }

    public void setMail(String mail) {
        this.mail = mail;
    }
}
