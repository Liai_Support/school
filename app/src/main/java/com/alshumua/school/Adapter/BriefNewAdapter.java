package com.alshumua.school.Adapter;

import android.annotation.SuppressLint;
import android.content.Context;
import android.media.MediaPlayer;
import android.text.Html;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.VideoView;

import androidx.annotation.NonNull;
import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.RecyclerView;

import com.alshumua.school.Activity.DashboardActivity;
import com.alshumua.school.Fragment.FormFragment;
import com.alshumua.school.Fragment.briefNewFragment;
import com.alshumua.school.Model.BriefNewModel;
import com.alshumua.school.Model.SupportingorgModel;
import com.alshumua.school.R;
import com.alshumua.school.Std.SessionManager;
import com.bumptech.glide.Glide;
import com.google.android.gms.maps.MapFragment;

import org.json.JSONArray;
import org.json.JSONException;

import java.util.List;

public class BriefNewAdapter extends RecyclerView.Adapter<BriefNewAdapter.MyViewHolder> {
    private Context mcontext;
    private List<BriefNewModel> dataSet;
    private DashboardActivity dashboardActivity;
    private briefNewFragment briefFragment;
    Fragment fragment = null;
    SessionManager sessionManager;

    public BriefNewAdapter(Context mcontext, List<BriefNewModel> dataSet, briefNewFragment briefFragment) {
        this.mcontext = mcontext;
        this.dataSet = dataSet;
        this.dashboardActivity = dashboardActivity;
        this.briefFragment = briefFragment;
    }

    public class MyViewHolder extends RecyclerView.ViewHolder {

        ImageView imageView;

        LinearLayout homelinear,addresslinear,videolinear;
        MapFragment mapfragment;

        TextView hometitle,homecontent,address;
        VideoView videoView;

        public MyViewHolder(@NonNull View itemView) {
            super(itemView);

            this.homelinear = (LinearLayout) itemView.findViewById(R.id.homelinear);
            this.addresslinear = (LinearLayout) itemView.findViewById(R.id.addresslinear);
            this.videolinear = (LinearLayout) itemView.findViewById(R.id.videolinear);

            this.hometitle = (TextView) itemView.findViewById(R.id.hometitle);
            this.homecontent = (TextView) itemView.findViewById(R.id.homecontent);
            this.address = (TextView) itemView.findViewById(R.id.addresstxt);

            this.videoView = (VideoView) itemView.findViewById(R.id.videoview);

        }
    }
    @NonNull
    @Override
    public BriefNewAdapter.MyViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.card_briefnew, parent, false);
        BriefNewAdapter.MyViewHolder viewHolder  = new BriefNewAdapter.MyViewHolder(view);


        return viewHolder;
    }
    @Override
    public void onBindViewHolder(@NonNull BriefNewAdapter.MyViewHolder holder, @SuppressLint("RecyclerView") final int listPosition) {
//        holder.hometitle.setText(Html.fromHtml(dataSet.get(listPosition).getTitle()));
//        holder.homecontent.setText(Html.fromHtml(dataSet.get(listPosition).getContent()));
//        holder.address.setText(Html.fromHtml(dataSet.get(listPosition).getAddress()));


        Log.d("TAG", "onBindViewHolder: "+briefFragment.tab);
        if (!briefFragment.tab.equals("")){
            if (briefFragment.tab.equals("Video")){
                holder.videoView.setVideoPath(dataSet.get(listPosition).getVideo());
                holder.videoView.start();
                holder.videoView.setOnPreparedListener(new MediaPlayer.OnPreparedListener() {
                    @Override
                    public void onPrepared(MediaPlayer mp) {
                        mp.setLooping(true);
                    }
                });
                holder.homelinear.setVisibility(View.GONE);
                holder.addresslinear.setVisibility(View.GONE);
            }
            else {

                holder.videolinear.setVisibility(View.GONE);
            }

        }
    }

    @Override
    public int getItemCount() {
        return dataSet.size();
    }
}

