package com.alshumua.school.Adapter;

import android.annotation.SuppressLint;
import android.app.DatePickerDialog;
import android.content.Context;
import android.content.Intent;
import android.content.res.Configuration;
import android.graphics.Color;
import android.graphics.Typeface;
import android.os.Build;
import android.text.Editable;
import android.text.InputType;
import android.text.TextWatcher;
import android.util.Log;
import android.util.TypedValue;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.inputmethod.EditorInfo;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.CheckBox;
import android.widget.CompoundButton;
import android.widget.DatePicker;
import android.widget.EditText;
import android.widget.HorizontalScrollView;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.ScrollView;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.annotation.RequiresApi;
import androidx.cardview.widget.CardView;
import androidx.core.content.ContextCompat;
import androidx.core.content.res.ResourcesCompat;
import androidx.recyclerview.widget.RecyclerView;

import com.alshumua.school.Activity.DashboardActivity;
import com.alshumua.school.Fragment.FormFragment;
import com.alshumua.school.Model.ServiceRequestModel;
import com.alshumua.school.R;
import com.alshumua.school.Std.Constants;

import org.json.JSONArray;
import org.json.JSONException;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.List;
import java.util.Locale;

import static android.content.ContentValues.TAG;
import static java.lang.Double.parseDouble;
import static java.lang.String.valueOf;

public class ServiceRequestAdapter extends RecyclerView.Adapter<ServiceRequestAdapter.MyViewHolder> {
    private FormFragment formFragment;
    private Context mcontext;
    private List<ServiceRequestModel> dataSet;
    private DashboardActivity dashboardActivity;
    String ageS="";
    View view;
    ArrayList<String> selectedStrings = new ArrayList<String>();
    int count=0;
    private int CurrentProgress=0;
    private Double percentTxt=0.0;
    private Double singleper=0.0;

    int percent=0;
    public ServiceRequestAdapter(FormFragment formFragment, Context mcontext, List<ServiceRequestModel> dataSet,DashboardActivity dashboardActivity) {
        this.formFragment = formFragment;
        this.mcontext = mcontext;
        this.dataSet = dataSet;
        this.dashboardActivity = dashboardActivity;
    }

    public class MyViewHolder extends RecyclerView.ViewHolder {
        LinearLayout linearLayout;
        public MyViewHolder(@NonNull View itemView) {
            super(itemView);
            this.linearLayout = (LinearLayout) itemView.findViewById(R.id.linearlayout); }}
    @NonNull
    @Override
    public MyViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        view = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.form_items, parent, false);

        MyViewHolder viewHolder = new MyViewHolder(view);
        viewHolder.setIsRecyclable(false);
        return viewHolder; }

    @SuppressLint("ResourceAsColor")
    @RequiresApi(api = Build.VERSION_CODES.M)
    @Override
    public void onBindViewHolder(@NonNull MyViewHolder holder, @SuppressLint("RecyclerView") final int listPosition) {
        holder.setIsRecyclable(false);

//        FOR SUBHEAD
        formFragment.progressBar.setMax(dataSet.get(listPosition).getNoOfQuestions());
        singleper=parseDouble(String.valueOf(dataSet.get(listPosition).getNoOfQuestions())) / 100.00 ;

        if (listPosition == 0) {
            TextView textView = new TextView(mcontext);
            textView.setText(dataSet.get(listPosition).getSubHead());
            setAttributes(textView,"subHead",null,null,null,null);
            holder.linearLayout.addView(textView);
        } else {
            if (dataSet.get(listPosition).getSubHead().equals(dataSet.get(listPosition - 1).getSubHead())) { }
            else {
                TextView textView = new TextView(mcontext);
                textView.setText(dataSet.get(listPosition).getSubHead());
                setAttributes(textView,"subHead",null,null,null,null);
                holder.linearLayout.addView(textView);
            }
        }
//        FOR QUESTION
        TextView textView = new TextView(mcontext);
        textView.setText(dataSet.get(listPosition).getQuestion());
        setAttributes(textView,"question",null,null,null,null);
        if (dataSet.get(listPosition).getMandatory().equals("on")) {
            textView.setCompoundDrawablesRelativeWithIntrinsicBounds(0, 0, R.drawable.ic_baseline_star_rate_24, 0);
        }
        holder.linearLayout.addView(textView);
        String input = dataSet.get(listPosition).getAnswerFields();
        JSONArray jsonArray = null;
        try {
            jsonArray = new JSONArray(input);
        } catch (JSONException e) {
            e.printStackTrace();
        }

formFragment.percentagetxt.setText(String.valueOf(percentTxt+"%"));
//        Log.d(TAG, "loggetprogress "+formFragment.progressBar.getProgress()+" "+"%");
        if (dataSet.get(listPosition).getFieldType().equals("textbox")) {

            for (int i = 0; i < jsonArray.length(); i++) {
                if (dataSet.get(listPosition).getInputType().equals("date")) {
                    for (int j = 0; j < jsonArray.length(); j++) {
                        CardView datecard = new CardView(mcontext);
                        EditText tvd = new EditText(mcontext);
                        Calendar myCalendar = Calendar.getInstance();
                        datecard.addView(tvd);
                        try {
                            if (dashboardActivity.sessionManager.getlang().equals("ar")){
                                tvd.setHint(jsonArray.getJSONObject(i).getString("ar_display"));
                                dashboardActivity.sessionManager.arabicfont(null,tvd, null, Constants.REGULAR,"editV");
                            }
                            else {
                                tvd.setHint(jsonArray.getJSONObject(i).getString("display"));
                            }

                            if (!formFragment.emptyArr.getJSONObject(listPosition).getString("value").equals("default")) {
                                if (!formFragment.emptyArr.getJSONObject(listPosition).getString("value").equals("")) {
                                    tvd.setText("" + formFragment.emptyArr.getJSONObject(listPosition).getString("value"));

                                }
                            }
                        } catch (JSONException e) {
                            e.printStackTrace();
                        }
                        setAttributes(null,"dateEditText",tvd,datecard,null,null);
                        DatePickerDialog.OnDateSetListener date = new DatePickerDialog.OnDateSetListener() {
                            @Override
                            public void onDateSet(DatePicker view, int year, int monthOfYear,
                                                  int dayOfMonth) {
                                // TODO Auto-generated method stub
                                myCalendar.set(Calendar.YEAR, year);
                                myCalendar.set(Calendar.MONTH, monthOfYear);
                                myCalendar.set(Calendar.DAY_OF_MONTH, dayOfMonth);
                                String myFormat = "dd/MM/yyyy"; //In which you need put here
                                SimpleDateFormat sdf = new SimpleDateFormat(myFormat, Locale.US);
                                tvd.setText(sdf.format(myCalendar.getTime()));
                                if (dataSet.get(listPosition).getAutofill().equals("dob")){ getAge(year,(monthOfYear+1),dayOfMonth); }
                                notifyDataSetChanged();
                            }

                        };
                        tvd.setOnClickListener(new View.OnClickListener() {
                            @Override
                            public void onClick(View view) {
                                new DatePickerDialog(mcontext, date, myCalendar
                                        .get(Calendar.YEAR), myCalendar.get(Calendar.MONTH),
                                        myCalendar.get(Calendar.DAY_OF_MONTH)).show();
                            }
                        });
                        tvd.addTextChangedListener(new TextWatcher() {
                            @Override
                            public void beforeTextChanged(CharSequence s, int start, int count, int after) {
                            }
                            @Override
                            public void onTextChanged(CharSequence s, int start, int before, int count) {
                            }
                            @Override
                            public void afterTextChanged(Editable s) {
                                try {
                                    if(tvd.getText().toString().trim().equals("")){
                                        percent = (formFragment.progressBar.getProgress() * 100 / dataSet.get(listPosition).getNoOfQuestions());

                                        CurrentProgress-=1;
                                        percentTxt -= singleper;
                                        formFragment.progressBar.setProgress(CurrentProgress);
                                        formFragment.emptyArr2.getJSONObject(listPosition).put("ifAnswered","no");
                                        formFragment.percentagetxt.setText(String.valueOf(percentTxt+"%"));
//                                        formFragment.percentagetxt.setText(formFragment.progressBar.getProgress()+" "+"%");
                                    }else{
                                        if(formFragment.emptyArr2.getJSONObject(listPosition).get("ifAnswered").equals("no")) {

                                            percent = (formFragment.progressBar.getProgress() * 100 / dataSet.get(listPosition).getNoOfQuestions());

                                            CurrentProgress += 1;
                                            formFragment.progressBar.setProgress(CurrentProgress);
                                            percent = (formFragment.progressBar.getProgress() * 100 / dataSet.get(listPosition).getNoOfQuestions());
                                            formFragment.percentagetxt.setText(String.valueOf(percentTxt+"%"));
                                            formFragment.emptyArr2.getJSONObject(listPosition).put("ifAnswered", "yes");

                                        }
                                    }


                                    formFragment.emptyArr.getJSONObject(listPosition).put("value", tvd.getText().toString());
                                    formFragment.emptyArr2.getJSONObject(listPosition).put("value", tvd.getText().toString());
                                    formFragment.emptyArr2.getJSONObject(listPosition).put("manval", tvd.getText().toString());
                                } catch (JSONException e) {
                                    e.printStackTrace();
                                }

                            }
                        });
                        holder.linearLayout.addView(datecard);
                    }
                }
                else {
                    CardView card = new CardView(mcontext);
                    EditText tv = new EditText(mcontext);
                    setAttributes(null,"textbox",tv,card,null,null);
                    card.addView(tv);
//                    AUTOFILLED FIELDS
                        if (dataSet.get(listPosition).getAutofill().equals("phone")){
                            tv.setText(dashboardActivity.sessionManager.getmobile_number_common());
                            tv.setEnabled(false);
//                            tv.setTextColor(Color.parseColor("#B8B8B8"));
                            try {
                                formFragment.emptyArr.getJSONObject(listPosition).put("value", tv.getText().toString());
                                formFragment.emptyArr2.getJSONObject(listPosition).put("value", tv.getText().toString());
                                formFragment.emptyArr2.getJSONObject(listPosition).put("manval", tv.getText().toString());

                                if(tv.getText().toString().trim().equals("")){
                                    CurrentProgress-=1;
                                    percentTxt -= singleper;
                                    formFragment.progressBar.setProgress(CurrentProgress);
                                    formFragment.emptyArr2.getJSONObject(listPosition).put("ifAnswered","no");
                                    formFragment.percentagetxt.setText(String.valueOf(percentTxt+"%"));

                                }else{
                                    if(formFragment.emptyArr2.getJSONObject(listPosition).get("ifAnswered").equals("no")) {
                                        CurrentProgress += 1;
                                        percentTxt += singleper;
                                        formFragment.progressBar.setProgress(CurrentProgress);

                                        formFragment.emptyArr2.getJSONObject(listPosition).put("ifAnswered", "yes");

                                        formFragment.percentagetxt.setText(String.valueOf(percentTxt+"%"));                                    }
                                }



                            } catch (JSONException e) {
                                e.printStackTrace();
                            }

                        }
                        else if (dataSet.get(listPosition).getAutofill().equals("email")){
                            tv.setText(dashboardActivity.sessionManager.getemail_common());
                            tv.setEnabled(false);
                            try {
                                formFragment.emptyArr.getJSONObject(listPosition).put("value", tv.getText().toString());
                                formFragment.emptyArr2.getJSONObject(listPosition).put("value", tv.getText().toString());
                                formFragment.emptyArr2.getJSONObject(listPosition).put("manval", tv.getText().toString());
                                if(tv.getText().toString().trim().equals("")){
                                    CurrentProgress-=1;
                                    percentTxt -= singleper;
                                    formFragment.percentagetxt.setText(String.valueOf(percentTxt+"%"));

                                    formFragment.progressBar.setProgress(CurrentProgress);
                                    formFragment.emptyArr2.getJSONObject(listPosition).put("ifAnswered","no");

                                }else{
                                    if(formFragment.emptyArr2.getJSONObject(listPosition).get("ifAnswered").equals("no")) {
                                        CurrentProgress += 1;
                                        percentTxt += singleper;
                                        formFragment.percentagetxt.setText(String.valueOf(percentTxt+"%"));
                                        formFragment.progressBar.setProgress(CurrentProgress);

                                        formFragment.emptyArr2.getJSONObject(listPosition).put("ifAnswered", "yes");

                                    }
                                }
                            } catch (JSONException e) {
                                e.printStackTrace();
                            }
                        }
                        else if (dataSet.get(listPosition).getAutofill().equals("name")){
                            tv.setText(dashboardActivity.sessionManager.getfirst_name_common());
                            try {
                                formFragment.emptyArr.getJSONObject(listPosition).put("value", tv.getText().toString());
                                formFragment.emptyArr2.getJSONObject(listPosition).put("value", tv.getText().toString());
                                formFragment.emptyArr2.getJSONObject(listPosition).put("manval", tv.getText().toString());
                                if(tv.getText().toString().trim().equals("")){
                                    CurrentProgress-=1;
                                    formFragment.progressBar.setProgress(CurrentProgress);
                                    formFragment.emptyArr2.getJSONObject(listPosition).put("ifAnswered","no");
                                    percentTxt -= singleper;
                                    formFragment.percentagetxt.setText(String.valueOf(percentTxt+"%"));                                }else{
                                    if(formFragment.emptyArr2.getJSONObject(listPosition).get("ifAnswered").equals("no")) {
                                        CurrentProgress += 1;

                                        formFragment.progressBar.setProgress(CurrentProgress);

                                        formFragment.emptyArr2.getJSONObject(listPosition).put("ifAnswered", "yes");
                                        percentTxt += singleper;
                                        formFragment.percentagetxt.setText(String.valueOf(percentTxt+"%"));                                    }
                                }
                            } catch (JSONException e) {
                                e.printStackTrace();
                            }
                        }
                        else if (dataSet.get(listPosition).getInputType().equals("age")){
                            if (!ageS.equals("")||!ageS.isEmpty()){
                                tv.setText(ageS);
                                try {
                                    formFragment.emptyArr.getJSONObject(listPosition).put("value", tv.getText().toString());
                                    formFragment.emptyArr2.getJSONObject(listPosition).put("value", tv.getText().toString());
                                    formFragment.emptyArr2.getJSONObject(listPosition).put("manval", tv.getText().toString());
                                    if(tv.getText().toString().trim().equals("")){
                                        CurrentProgress-=1;
                                        formFragment.progressBar.setProgress(CurrentProgress);
                                        formFragment.emptyArr2.getJSONObject(listPosition).put("ifAnswered","no");
                                        percentTxt -= singleper;
                                        formFragment.percentagetxt.setText(String.valueOf(percentTxt+"%"));                                    }else{
                                        if(formFragment.emptyArr2.getJSONObject(listPosition).get("ifAnswered").equals("no")) {
                                            CurrentProgress += 1;
                                            formFragment.progressBar.setProgress(CurrentProgress);

                                            formFragment.emptyArr2.getJSONObject(listPosition).put("ifAnswered", "yes");
                                            percentTxt += singleper;
                                            formFragment.percentagetxt.setText(String.valueOf(percentTxt+"%"));                                        }
                                    }
                                } catch (JSONException e) {
                                    e.printStackTrace();
                                }
                            }

                        }

                    try {
                        if (dashboardActivity.sessionManager.getlang().equals("ar")){
                            tv.setHint(jsonArray.getJSONObject(i).getString("ar_display"));
                            dashboardActivity.sessionManager.arabicfont(null,tv, null, Constants.REGULAR,"editV");
                        }
                        else {
                            tv.setHint(jsonArray.getJSONObject(i).getString("display"));
                        }


                        if (!formFragment.emptyArr.getJSONObject(listPosition).getString("value").equals("default")) {
                            if (!formFragment.emptyArr.getJSONObject(listPosition).getString("value").equals("")) {
                                tv.setText("" + formFragment.emptyArr.getJSONObject(listPosition).getString("value"));
                            }
                        }
                    } catch (JSONException e) {
                        e.printStackTrace();
                    }
                    if (dataSet.get(listPosition).getInputType().equals("numeric") || dataSet.get(listPosition).getInputType().equals("phone")
                            || dataSet.get(listPosition).getInputType().equals("age")) {
                        tv.setRawInputType(Configuration.KEYBOARD_12KEY);
                    }
//                    close keypad when this was a last field of the form
                    if (dataSet.get(dataSet.size() - 1).equals(dataSet.get(listPosition))){
                        tv.setImeOptions(EditorInfo.IME_ACTION_GO);
                    }

                    tv.addTextChangedListener(new TextWatcher() {
                        @Override
                        public void beforeTextChanged(CharSequence s, int start, int count, int after) { }

                        @Override
                        public void onTextChanged(CharSequence s, int start, int before, int count) {
                        }

                        @Override
                        public void afterTextChanged(Editable s) {
                            try {
                                    if(tv.getText().toString().trim().equals("")){
                                        CurrentProgress-=1;
                                        formFragment.progressBar.setProgress(CurrentProgress);
                                        formFragment.emptyArr2.getJSONObject(listPosition).put("ifAnswered","no");
                                        percentTxt -= singleper;
                                        formFragment.percentagetxt.setText(String.valueOf(percentTxt+"%"));                                    }else{
                                        if(formFragment.emptyArr2.getJSONObject(listPosition).get("ifAnswered").equals("no")) {
                                            CurrentProgress += 1;
                                            formFragment.progressBar.setProgress(CurrentProgress);

                                            formFragment.emptyArr2.getJSONObject(listPosition).put("ifAnswered", "yes");
                                            percentTxt += singleper;
                                            formFragment.percentagetxt.setText(String.valueOf(percentTxt+"%"));                                        }
                                    }
//                                }
                                formFragment.emptyArr.getJSONObject(listPosition).put("value", tv.getText().toString());
                                formFragment.emptyArr2.getJSONObject(listPosition).put("value", tv.getText().toString());
                                formFragment.emptyArr2.getJSONObject(listPosition).put("manval", tv.getText().toString());
                                formFragment.emptyArr2.getJSONObject(listPosition).put("answered", tv.getText().toString());
                            } catch (JSONException e) {
                                e.printStackTrace();
                            }

                        }
                    });
                    holder.linearLayout.addView(card);

                }

            }
        }
        else if (dataSet.get(listPosition).getFieldType().equals("textarea")) {
            for (int i = 0; i < jsonArray.length(); i++) {
                CardView card = new CardView(mcontext);
                LinearLayout.LayoutParams layoutParams = new LinearLayout.LayoutParams(
                        LinearLayout.LayoutParams.MATCH_PARENT,
                        LinearLayout.LayoutParams.WRAP_CONTENT
                );
//                cardAppearence(card);
                EditText tv = new EditText(mcontext);
                tv.setLayoutParams(layoutParams);
                try {
                    if (dashboardActivity.sessionManager.getlang().equals("ar")){
                        tv.setHint(jsonArray.getJSONObject(i).getString("ar_display"));
                        dashboardActivity.sessionManager.arabicfont(null,tv, null, Constants.REGULAR,"editV");
                    }
                    else {
                        tv.setHint(jsonArray.getJSONObject(i).getString("display"));
                    }

                    if (!formFragment.emptyArr.getJSONObject(listPosition).getString("value").equals("default")) {
                        if (!formFragment.emptyArr.getJSONObject(listPosition).getString("value").equals("")) {
                            tv.setText("" + formFragment.emptyArr.getJSONObject(listPosition).getString("value"));
                            Log.d(TAG, "vall"+formFragment.emptyArr.getJSONObject(listPosition).getString("value"));
                        }
                    }
                } catch (JSONException e) {
                    e.printStackTrace();
                }
                setAttributes(null,"textarea",tv,card,null,null);
//                textAreaText(tv);
                card.addView(tv);

                tv.addTextChangedListener(new TextWatcher() {
                    @Override
                    public void beforeTextChanged(CharSequence s, int start, int count, int after) {
                    }

                    @Override
                    public void onTextChanged(CharSequence s, int start, int before, int count) {
                    }

                    @Override
                    public void afterTextChanged(Editable s) {
                        try {
                            if(tv.getText().toString().trim().equals("")){
                                CurrentProgress-=1;
                                formFragment.progressBar.setProgress(CurrentProgress);
                                formFragment.emptyArr2.getJSONObject(listPosition).put("ifAnswered","no");
                                percentTxt -= singleper;
                                formFragment.percentagetxt.setText(String.valueOf(percentTxt+"%"));
                            }else{
                                if(formFragment.emptyArr2.getJSONObject(listPosition).get("ifAnswered").equals("no")) {
                                    CurrentProgress += 1;
                                    formFragment.progressBar.setProgress(CurrentProgress);

                                    formFragment.emptyArr2.getJSONObject(listPosition).put("ifAnswered", "yes");
                                    percentTxt += singleper;
                                    formFragment.percentagetxt.setText(String.valueOf(percentTxt+"%"));                                }
                            }
                            if (!s.toString().equals("")){
                                formFragment.emptyArr.getJSONObject(listPosition).put("value", tv.getText().toString());
                                formFragment.emptyArr2.getJSONObject(listPosition).put("value", tv.getText().toString());
                                formFragment.emptyArr2.getJSONObject(listPosition).put("manval", tv.getText().toString());
                            }

                        } catch (JSONException e) {
                            e.printStackTrace();
                        }
                    }
                });
                holder.linearLayout.addView(card);


            }
        }
        else if (dataSet.get(listPosition).getFieldType().equals("radio")) {
            final int sdk = android.os.Build.VERSION.SDK_INT;
            RadioGroup radioGroup = new RadioGroup(mcontext);
            radioGroup.setOrientation(LinearLayout.HORIZONTAL);
            holder.linearLayout.addView(radioGroup);
            for (int i = 0; i < jsonArray.length(); i++) {
                RadioButton radioButton = new RadioButton(mcontext);

                if(sdk < android.os.Build.VERSION_CODES.JELLY_BEAN) {
                    radioButton.setBackgroundDrawable(ContextCompat.getDrawable(mcontext, R.drawable.customradibtn_background) );

                } else {
                    radioButton.setBackgroundDrawable(ContextCompat.getDrawable(mcontext, R.drawable.customradibtn_background) );

                }

                try {
                    if (dashboardActivity.sessionManager.getlang().equals("ar")){
                        radioButton.setText("   " + jsonArray.getJSONObject(i).getString("ar_display") + "   ");
                    }
                    else {
                        radioButton.setText("   " + jsonArray.getJSONObject(i).getString("display") + "   ");
                    }


                    if (!formFragment.emptyArr.getJSONObject(listPosition).getString("value").equals("default")) {
                        if (!formFragment.emptyArr.getJSONObject(listPosition).getString("value").equals("")) {
                            if (radioButton.getText().toString().equalsIgnoreCase(formFragment.emptyArr.getJSONObject(listPosition).getString("value"))) {
                                radioButton.setChecked(true);
                            }
                        }
                    }
                } catch (JSONException e) {
                    e.printStackTrace();
                }
                radioGroup.addView(radioButton);
                setAttributes(null,"radiobutton",null,null,null,radioButton);
                radioButton.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
                    @Override
                    public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                        if (radioButton.isChecked()) {

                            if (radioButton.isChecked()==true) {
                                try {
                                    radioButton.setTextColor(Color.parseColor("#FFFFFF"));
                                    formFragment.emptyArr.getJSONObject(listPosition).put("value", radioButton.getText().toString());
                                    formFragment.emptyArr2.getJSONObject(listPosition).put("value", radioButton.getText().toString());
                                    formFragment.emptyArr2.getJSONObject(listPosition).put("manval", radioButton.getText().toString());
                                } catch (JSONException e) {
                                    e.printStackTrace();
                                }
                            }
                        }
                        else {
                            radioButton.setChecked(false);
                            radioButton.setTextColor(Color.parseColor("#515151"));
                        }
                    }

                });

            }
        }
        else if (dataSet.get(listPosition).getFieldType().equals("dropdown")) {
            CardView    cardall = new CardView(mcontext);
            CardView    card2 = new CardView(mcontext);
            EditText Etall=new EditText(mcontext);

            ArrayList<String> spinnerArray = new ArrayList<String>();
            ArrayList<String> spinnerArray2 = new ArrayList<String>();
            spinnerArray.add(0, "");

            for (int i = 0; i < jsonArray.length(); i++) {
                try {
                    if (dashboardActivity.sessionManager.getlang().equals("ar")){
                        spinnerArray.add(jsonArray.getJSONObject(i).getString("ar_display")); }
                    else { spinnerArray.add(jsonArray.getJSONObject(i).getString("display")); }

                    spinnerArray2.add(jsonArray.getJSONObject(i).getString("value"));
                } catch (JSONException e) { e.printStackTrace(); }
            }
            CardView card = new CardView(mcontext);
            setAttributes(null,null,null,card,null,null);
            Spinner spinner = new Spinner(mcontext);
            ArrayAdapter<String> spinnerArrayAdapter = new ArrayAdapter<String>(mcontext, R.layout.customspinner_form, spinnerArray);
            spinner.setAdapter(spinnerArrayAdapter);
            card.addView(spinner);
            holder.linearLayout.addView(card);
            try {
                if (!formFragment.emptyArr.getJSONObject(listPosition).getString("value").equals("default")) {
                    if (!formFragment.emptyArr.getJSONObject(listPosition).getString("value").equals("")) {
                        if (spinnerArray.contains(formFragment.emptyArr.getJSONObject(listPosition).getString("value"))){
                            spinnerArray.remove(formFragment.emptyArr.getJSONObject(listPosition).getString("value"));
                        }
                        spinnerArray.add(0, formFragment.emptyArr.getJSONObject(listPosition).getString("value"));
                        spinnerArray.remove(1);
                        if (!formFragment.emptyArr2.getJSONObject(listPosition).getString("descvalue").equals("default")) {
                            if (!formFragment.emptyArr.getJSONObject(listPosition).getString("descvalue").equals("")) {
                                spinnerArray.remove(1);
                                Etall.setVisibility(View.VISIBLE);
                                Etall.setText("" + formFragment.emptyArr2.getJSONObject(listPosition).getString("descvalue"));
                            }
                        }
                    }
                }
            } catch (JSONException e) {
                e.printStackTrace();
            }

            spinner.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {

                @Override
                public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                    Log.d("positiondrop log",""+position);
                    Log.d("getProgress",">>"+formFragment.progressBar.getProgress());

                    if (parent.getItemAtPosition(position).equals("")) {
                        cardall.setVisibility(View.GONE);
                        Log.d("getProgress]]]",">>"+formFragment.progressBar.getProgress());
                          try {
                        Log.d("positiondrop", "" + formFragment.emptyArr2.getJSONObject(listPosition).get("ifAnswered"));

                              if(formFragment.emptyArr2.getJSONObject(listPosition).get("ifAnswered").equals("yes")){
                                  CurrentProgress -= 1;
                                  percentTxt -= singleper;
                                  formFragment.percentagetxt.setText(String.valueOf(percentTxt+"%"));
                                  formFragment.progressBar.setProgress(CurrentProgress);
                              formFragment.emptyArr2.getJSONObject(listPosition).put("ifAnswered", "no");
                              }

                    } catch (JSONException e) {
                        e.printStackTrace();
                    }

                    }
                    else {
                        Log.d("getProgresselse",">>"+formFragment.progressBar.getProgress());
                    try {

                            Log.d("positiondrop log",""+position);
                            if (position==0) {
                                CurrentProgress -= 1;
                                percentTxt -= singleper;
                                formFragment.percentagetxt.setText(String.valueOf(percentTxt+"%"));
                                formFragment.progressBar.setProgress(CurrentProgress);
                                formFragment.emptyArr2.getJSONObject(listPosition).put("ifAnswered", "no");
                            } else {
                                if(formFragment.emptyArr2.getJSONObject(listPosition).get("ifAnswered").equals("no")){
                                    CurrentProgress += 1;
                                    percentTxt += singleper;
                                    formFragment.percentagetxt.setText(String.valueOf(percentTxt+"%"));
                                    formFragment.progressBar.setProgress(CurrentProgress);
                                    formFragment.emptyArr2.getJSONObject(listPosition).put("ifAnswered", "yes");
                                }

                        } }catch (JSONException e) {
                        e.printStackTrace();
                    }
                        if (dataSet.get(listPosition).getDescription().equals("all")) {
                            cardall.setVisibility(View.VISIBLE);
                            count = 0;
                            card2.setVisibility(View.GONE);
                            holder.linearLayout.removeView(card2);


                            try {
                                if (formFragment.emptyArr2.getJSONObject(listPosition).get("ifAnswered") == "no" || formFragment.emptyArr2.getJSONObject(listPosition).get("ifAnswered").equals("no")) {
                                Log.d("position log",""+position);
                                    if (position==0) {
                                        CurrentProgress -= 1;
                                        percentTxt -= singleper;
                                        formFragment.progressBar.setProgress(CurrentProgress);
                                        formFragment.emptyArr2.getJSONObject(listPosition).put("ifAnswered", "no");
                                    } else {
                                        CurrentProgress += 1;
                                        percentTxt += singleper;
                                        formFragment.progressBar.setProgress(CurrentProgress);
                                        formFragment.emptyArr2.getJSONObject(listPosition).put("ifAnswered", "yes");
                                    }
                                } }catch (JSONException e) {
                                    e.printStackTrace();
                                }

                        }
                       else if (dataSet.get(listPosition).getDescription().equals("other")){
                            //                          other methods
                            cardall.setVisibility(View.GONE);
                            holder.linearLayout.removeView(cardall);
                            if (spinnerArray.get(position).equals("Other")||spinnerArray.get(position).equals("Others")){
                                count=count+1;
                                if (count==1){
                                    EditText Et=new EditText(mcontext);
                                    setAttributes(null,"textbox",Et,card2,null,null);
                                    card2.addView(Et);
                                    holder.linearLayout.addView(card2);
                                    card2.setVisibility(View.VISIBLE);
                                    Et.addTextChangedListener(new TextWatcher() {
                                        @Override
                                        public void beforeTextChanged(CharSequence s, int start, int count, int after) { }
                                        @Override
                                        public void onTextChanged(CharSequence s, int start, int before, int count) { }
                                        @Override
                                        public void afterTextChanged(Editable s) {
                                            try {
                                                formFragment.emptyArr.getJSONObject(listPosition).put("value", Et.getText().toString());
                                                formFragment.emptyArr2.getJSONObject(listPosition).put("value", Et.getText().toString());
                                                formFragment.emptyArr2.getJSONObject(listPosition).put("manval", Et.getText().toString());
                                            } catch (JSONException e) {
                                                e.printStackTrace();
                                            }

                                        }
                                    });
                                }
                                else {
                                    holder.linearLayout.removeView(card2);
                                    holder.linearLayout.removeView(cardall);
                                    cardall.setVisibility(View.GONE);
                                    card2.setVisibility(View.GONE);
                                }
                            }
                            else {
                                count=0;
                                card2.setVisibility(View.GONE);
                                holder.linearLayout.removeView(card2);

                                try {
                                    formFragment.emptyArr.getJSONObject(listPosition).put("value", spinnerArray.get(position));
                                    formFragment.emptyArr2.getJSONObject(listPosition).put("value", spinnerArray.get(position));
                                    formFragment.emptyArr2.getJSONObject(listPosition).put("manval", spinnerArray.get(position));

                                } catch (JSONException e) {
                                    e.printStackTrace();
                                }
                            }
                        }
//                       Description "" empty
                        else {
                            count=0;
                            holder.linearLayout.removeView(card2);
                            holder.linearLayout.removeView(cardall);
                            cardall.setVisibility(View.GONE);
                            card2.setVisibility(View.GONE);
                            try {

                                if (formFragment.emptyArr2.getJSONObject(listPosition).get("ifAnswered") == "no" || formFragment.emptyArr2.getJSONObject(listPosition).get("ifAnswered").equals("no")) {
                                    Log.d("position log",""+position);
                                    if (position==0) {
                                        CurrentProgress -= 1;
                                        percentTxt -= singleper;
                                        formFragment.progressBar.setProgress(CurrentProgress);
                                        formFragment.emptyArr2.getJSONObject(listPosition).put("ifAnswered", "no");
                                    } else {
                                        CurrentProgress += 1;
                                        percentTxt += singleper;
                                        formFragment.progressBar.setProgress(CurrentProgress);
                                        formFragment.emptyArr2.getJSONObject(listPosition).put("ifAnswered", "yes");
                                    }
                                } }catch (JSONException e) {
                                e.printStackTrace();
                            }
                            try {
                                formFragment.emptyArr.getJSONObject(listPosition).put("value", spinnerArray.get(position));
                                formFragment.emptyArr2.getJSONObject(listPosition).put("value", spinnerArray.get(position));
                                formFragment.emptyArr2.getJSONObject(listPosition).put("manval", spinnerArray.get(position));

                            } catch (JSONException e) {
                                e.printStackTrace();
                            }
                        }
                    }//main else
                }
                @Override
                public void onNothingSelected(AdapterView<?> parent) { }
            });
            Etall.setHint(R.string.EnterHere);
            setAttributes(null,"textbox",Etall,cardall,null,null);
            cardall.addView(Etall);
            Etall.addTextChangedListener(new TextWatcher() {
                @Override
                public void beforeTextChanged(CharSequence s, int start, int count, int after) {
                }
                @Override
                public void onTextChanged(CharSequence s, int start, int before, int count) {
                }

                @Override
                public void afterTextChanged(Editable s) {
                    try {
                        formFragment.emptyArr.getJSONObject(listPosition).put("descvalue", Etall.getText().toString());
                        formFragment.emptyArr2.getJSONObject(listPosition).put("descvalue", Etall.getText().toString());
                        formFragment.emptyArr2.getJSONObject(listPosition).put("manval", Etall.getText().toString());
                    } catch (JSONException e) {
                        e.printStackTrace();
                    }
                }
            });
            holder.linearLayout.addView(cardall);
        }
        else if (dataSet.get(listPosition).getFieldType().equals("file")) {
            TextView tv = new TextView(mcontext);
            CardView card = new CardView(mcontext);
            setAttributes(null,null,null,card,null,null);
            LinearLayout parent = new LinearLayout(mcontext);
            parent.setLayoutParams(new LinearLayout.LayoutParams
                    (LinearLayout.LayoutParams.MATCH_PARENT, LinearLayout.LayoutParams.WRAP_CONTENT));
            parent.setOrientation(LinearLayout.VERTICAL);
            ImageView iv = new ImageView(mcontext);
            iv.setImageResource(R.drawable.ic_upload_cloud);
            TextView complete = new TextView(mcontext);
            TextView filename = new TextView(mcontext);
            setAttributes(complete,"complete",null,null,null,null);
            setAttributes(filename,"filename",null,null,null,null);
            setAttributes(tv,"uploaddocument",null,null,null,null);
            complete.setVisibility(View.GONE);
            filename.setVisibility(View.GONE);
            parent.addView(tv);
            parent.addView(iv);
            card.addView(parent);
            holder.linearLayout.addView(card);
            formFragment.uploadTxtt=complete;
            formFragment.Filename=filename;
            parent.addView(filename);
            parent.addView(complete);
            try {
                if (!formFragment.emptyArr.getJSONObject(listPosition).getString("value").equals("default")) {
                    if (!formFragment.emptyArr.getJSONObject(listPosition).getString("value").equals("")) {
                        filename.setText("" + formFragment.emptyArr.getJSONObject(listPosition).getString("value"));
                        complete.setVisibility(View.VISIBLE);
                        filename.setVisibility(View.VISIBLE);
                            CurrentProgress=CurrentProgress+1;
                            formFragment.progressBar.setProgress(CurrentProgress);

                    }
                }
            } catch (JSONException e) {
                e.printStackTrace();
            }
            card.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    formFragment.imagevalueposition = listPosition;
                    Intent intent = new Intent(Intent.ACTION_GET_CONTENT);
                    intent.setType("*/*");
                    intent.addCategory(Intent.CATEGORY_OPENABLE);

                    try {
                        formFragment.startActivityForResult(
                                Intent.createChooser(intent, "Select a File to Upload"),
                                19);

                    } catch (android.content.ActivityNotFoundException ex) {
                        // Potentially direct the user to the Market with a Dialog
                        Toast.makeText(mcontext, "Please install a File Manager.",
                                Toast.LENGTH_SHORT).show();
                    }
                }


            });
        }
        else if (dataSet.get(listPosition).getFieldType().equals("checkbox")) {
            ArrayList<String> allitems = new ArrayList<String>();
            for (int i = 0; i < jsonArray.length(); i++) {
                HorizontalScrollView scroll = new HorizontalScrollView(mcontext);
                scroll.setMeasureAllChildren(false);
//                scroll.setScrollBar(null);
                scroll.setBackgroundColor(android.R.color.transparent);
                scroll.setLayoutParams(new ViewGroup.LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT,
                        ViewGroup.LayoutParams.WRAP_CONTENT));
//scroll.setFillViewport(true);
                LinearLayout checkBoxLayout = new LinearLayout(mcontext);
                checkBoxLayout.setOrientation(LinearLayout.HORIZONTAL);

                CheckBox checkBox = new CheckBox(mcontext);
checkBox.setBackgroundResource(R.drawable.custom_checkbox_bg);
                try {
                    if (dashboardActivity.sessionManager.getlang().equals("ar")){
                        checkBox.setText("   " + jsonArray.getJSONObject(i).getString("ar_display") + "   ");
                        allitems.add(jsonArray.getJSONObject(i).getString("ar_display") );
                    }
                    else {
                        checkBox.setText("   " + jsonArray.getJSONObject(i).getString("display") + "   ");
                        allitems.add(jsonArray.getJSONObject(i).getString("display") );
                    }


                    if (!formFragment.emptyArr.getJSONObject(listPosition).getString("value").equals("default")) {
                        if (!formFragment.emptyArr.getJSONObject(listPosition).getString("value").equals("")) {
                            for (int i1 =0;i1 < selectedStrings.size(); i1++){
                                if(checkBox.getText().toString().equalsIgnoreCase(selectedStrings.get(i1))){
                                    checkBox.setChecked(true);
                                }
                            }
                        }
                    }
                } catch (JSONException e) {
                    e.printStackTrace();
                }
                checkBox.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
                    @Override
                    public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                        if (isChecked){
                            checkBox.setTextColor(Color.parseColor("#FFFFFF"));
                        }
                        if (!isChecked){
                            checkBox.setTextColor(Color.parseColor("#515151"));
                        }
                        int value = ischecked_checkbox(selectedStrings,checkBox.getText().toString());
                        if(value != -1){
                            selectedStrings.remove(value);
                        }
                        else {
                            selectedStrings.add(checkBox.getText().toString());
                        }
                        if(selectedStrings.size() == 0){
                            try {
                                formFragment.emptyArr.getJSONObject(listPosition).put("value", "default");
                                formFragment.emptyArr2.getJSONObject(listPosition).put("value", "default");
                                formFragment.emptyArr2.getJSONObject(listPosition).put("manval", "default");
                            } catch (JSONException e) {
                                e.printStackTrace();
                            }
                        }
                        else {
                            try {
                                formFragment.emptyArr.getJSONObject(listPosition).put("value", selectedStrings.toString());
                                formFragment.emptyArr2.getJSONObject(listPosition).put("value", selectedStrings.toString());
                                formFragment.emptyArr2.getJSONObject(listPosition).put("manval", selectedStrings.toString());
                            } catch (JSONException e) {
                                e.printStackTrace();
                            }
                        }
                    }
                });
                setAttributes(null,"checkbox",null,null,checkBox,null);
                checkBoxLayout.addView(checkBox);

//                holder.linearLayout.addView(checkBoxLayout);
                scroll.addView(checkBoxLayout);
                holder.linearLayout.addView(scroll);
            }
        }
        else if (dataSet.get(listPosition).getFieldType().equals("dropdown")) {
            ArrayList<String> spinnerArray = new ArrayList<String>();
            if (dataSet.get(listPosition).getInputType().equals("nationality")){
                spinnerArray.add(0, "");
                for (int i = 0; i < jsonArray.length(); i++) {
                    try {
                        if (dashboardActivity.sessionManager.getlang().equals("ar")){
                            spinnerArray.add(jsonArray.getJSONObject(i).getString("ar_display"));
                        }
                        else {
                            spinnerArray.add(jsonArray.getJSONObject(i).getString("display"));
                        }
                    } catch (JSONException e) {
                        e.printStackTrace();
                    }
                }
            }

            CardView card = new CardView(mcontext);
            setAttributes(null,null,null,card,null,null);
            Spinner spinner = new Spinner(mcontext);
            ArrayAdapter<String> spinnerArrayAdapter = new ArrayAdapter<String>(mcontext, R.layout.customspinner_form, spinnerArray);
            spinner.setAdapter(spinnerArrayAdapter);
            card.addView(spinner);
            holder.linearLayout.addView(card);
            spinner.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
                @Override
                public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                    if (parent.getItemAtPosition(position).equals("")) {
                        try {
                            Log.d("positiondrop", "" + formFragment.emptyArr2.getJSONObject(listPosition).get("ifAnswered"));

                            if(formFragment.emptyArr2.getJSONObject(listPosition).get("ifAnswered")=="yes"||formFragment.emptyArr2.getJSONObject(listPosition).get("ifAnswered").equals("yes")){
                                CurrentProgress -= 1;
                                percentTxt -= singleper;
                                formFragment.percentagetxt.setText(String.valueOf(percentTxt+"%"));
                                formFragment.progressBar.setProgress(CurrentProgress);
                                formFragment.emptyArr2.getJSONObject(listPosition).put("ifAnswered", "no");
                            }

                        } catch (JSONException e) {
                            e.printStackTrace();
                        }
                    }
                    else {

                        try {

                            Log.d("positiondrop log",""+position);
                            if (position==0) {
                                CurrentProgress -= 1;
                                percentTxt -= singleper;
                                formFragment.percentagetxt.setText(String.valueOf(percentTxt+"%"));
                                formFragment.progressBar.setProgress(CurrentProgress);
                                formFragment.emptyArr2.getJSONObject(listPosition).put("ifAnswered", "no");
                            } else {
                                CurrentProgress += 1;
                                percentTxt += singleper;
                                formFragment.percentagetxt.setText(String.valueOf(percentTxt+"%"));
                                formFragment.progressBar.setProgress(CurrentProgress);
                                formFragment.emptyArr2.getJSONObject(listPosition).put("ifAnswered", "yes");
                            } }catch (JSONException e) {
                            e.printStackTrace();
                        }

                                try {
                                    if (dashboardActivity.sessionManager.getlang().equals("ar")){
                                        formFragment.emptyArr.getJSONObject(listPosition).put("ar_display", spinnerArray.get(position));
                                        formFragment.emptyArr2.getJSONObject(listPosition).put("ar_display", spinnerArray.get(position));

                                    }
                                    else {
                                        formFragment.emptyArr.getJSONObject(listPosition).put("display", spinnerArray.get(position));
                                        formFragment.emptyArr2.getJSONObject(listPosition).put("display", spinnerArray.get(position));
                                    }

                                    formFragment.emptyArr2.getJSONObject(listPosition).put("manval", spinnerArray.get(position));

                                } catch (JSONException e) { e.printStackTrace(); }
                    }
                }
                @Override
                public void onNothingSelected(AdapterView<?> parent) { }
            });
        }
    }
    private int ischecked_checkbox(ArrayList<String> selectedstrings,String text){
        for (int j = 0; j < selectedstrings.size(); j++) {
            if (text.equalsIgnoreCase(selectedstrings.get(j))) { return j; } }
        return -1; }
    private void setAttributes(TextView textView,String type,EditText editText,CardView cardView,CheckBox checkBox,RadioButton radioButton) {
        LinearLayout.LayoutParams params = new LinearLayout.LayoutParams(LinearLayout.LayoutParams.MATCH_PARENT, LinearLayout.LayoutParams.WRAP_CONTENT);
        Typeface typeface = ResourcesCompat.getFont(mcontext, R.font.poppins_medium);
        Typeface typeface_ar = ResourcesCompat.getFont(mcontext, R.font.droidkufiregular);
        if (textView != null){
            if (dashboardActivity.sessionManager.getlang().equals(Constants.ARABICKEY)){
                textView.setTypeface(typeface_ar);
            }
            else {
                textView.setTypeface(typeface);
            }

            textView.setTextAlignment(View.TEXT_ALIGNMENT_VIEW_START);
            textView.setTextColor(ContextCompat.getColor(mcontext, R.color.textcolor));
            textView.setLayoutParams(params);
            textView.setPadding(5, 5, 5, 5);
            if (type.equals("subHead")){
                textView.setTextSize(19);
                params.setMargins(0, 20, 0, 20);
            }
            else if (type.equals("question")){
                textView.setTextSize(12);
                params.setMargins(0, 35, 0, 35);
            }
            else if (type.equals("complete")){
        params.setMargins(0, 10, 0, 10);
                textView.setTextSize(15);
        textView.setTextAlignment(View.TEXT_ALIGNMENT_CENTER);
                textView.setText(R.string.UploadDocuments);
                textView.setTextColor(Color.parseColor("#3BB54A"));
//                textView.setTypeface(typeface);
                if (dashboardActivity.sessionManager.getlang().equals(Constants.ARABICKEY)){
                    textView.setTypeface(typeface_ar);
                }
                else {
                    textView.setTypeface(typeface);
                }
            }
            else if (type.equals("filename")){

        params.setMargins(0, 10, 0, 10);
        textView.setTextSize(11);
                textView.setTextAlignment(View.TEXT_ALIGNMENT_CENTER);
                textView.setText(R.string.File);
                textView.setTextColor(Color.parseColor("#707070"));
//                textView.setTypeface(typeface);
                if (dashboardActivity.sessionManager.getlang().equals(Constants.ARABICKEY)){
                    textView.setTypeface(typeface_ar);
                }
                else {
                    textView.setTypeface(typeface);
                }
                textView.setLayoutParams(params);
            }
            else if (type.equals("uploaddocument")){
        params.setMargins(0, 10, 0, 10);
        textView.setTextSize(12);
                textView.setTextAlignment(View.TEXT_ALIGNMENT_CENTER);
                textView.setText(R.string.UploadDocuments);
                textView.setTextColor(Color.parseColor("#515151"));
//                textView.setTypeface(typeface);
                if (dashboardActivity.sessionManager.getlang().equals(Constants.ARABICKEY)){
                    textView.setTypeface(typeface_ar);
                }
                else {
                    textView.setTypeface(typeface);
                }
                textView.setLayoutParams(params);
            }
        }
        if (editText != null){
//            editText.setTypeface(typeface);
            if (dashboardActivity.sessionManager.getlang().equals(Constants.ARABICKEY)){
                editText.setTypeface(typeface_ar);
            }
            else {
                editText.setTypeface(typeface);
            }
            editText.setTextAlignment(View.TEXT_ALIGNMENT_VIEW_START);
            editText.setBackground(null);
            editText.setBackgroundResource(android.R.color.transparent);
            editText.setTextSize(TypedValue.COMPLEX_UNIT_DIP, 12);
            editText.setImeOptions(EditorInfo.IME_FLAG_NO_ENTER_ACTION);
            editText.setPadding(15, 15, 15, 15);
            editText.setTextColor(Color.parseColor("#515151"));
            if (type.equals("dateEditText")){
                editText.setFocusable(false);
                editText.setClickable(true);
                editText.setCompoundDrawablesRelativeWithIntrinsicBounds(0, 0, R.drawable.ic_baseline_calendar_today_24, 0);
            }
            if (type.equals("textbox")){
//                editText.setImeOptions(EditorInfo.IME_ACTION_NEXT);
                editText.setImeOptions(EditorInfo.IME_ACTION_GO);
                editText.setSingleLine(true);
            }
            if (type.equals("textarea")){
                /*editText.setHeight(300);
                cardView.setMinimumHeight(300);*/
                editText.setOverScrollMode(View.OVER_SCROLL_NEVER);
                editText.setScrollBarStyle(View.SCROLLBARS_INSIDE_INSET);
                editText.setVerticalScrollBarEnabled(true);
                editText.setGravity(Gravity.CLIP_VERTICAL);
                editText.setMaxLines(6);
                editText.setSingleLine(false);
//                editText.setImeOptions(EditorInfo.IME_FLAG_NO_ENTER_ACTION);
                editText.setImeOptions(EditorInfo.IME_ACTION_GO);
            }
        }
        if (cardView !=null){
            cardView.setLayoutParams(params);
            cardView.setUseCompatPadding(true);
            cardView.setCardBackgroundColor(Color.parseColor("#F5F4F4"));//gray
            cardView.requestLayout();
        }
    /*    if (checkBox !=null){
            checkBox.setTextColor(Color.parseColor("#515151"));
            params.setMargins(20, 20, 0, 0);
            checkBox.setLayoutParams(params);
            checkBox.setTextAppearance(mcontext, R.style.poppin_medium);
//            checkBox.setTypeface(typeface);
            if (dashboardActivity.sessionManager.getlang().equals(Constants.ARABICKEY)){
                checkBox.setTypeface(typeface_ar);
            }
            else {
                checkBox.setTypeface(typeface);
            }
            checkBox.setTextSize(12);
            checkBox.setTextAlignment(View.TEXT_ALIGNMENT_VIEW_START);
        }*/
        if (checkBox !=null){
            checkBox.setTextColor(Color.parseColor("#515151"));
            params.setMargins(20, 20, 0, 0);
            checkBox.setLayoutParams(params);
            checkBox.setTextAppearance(mcontext, R.style.poppin_medium);
            checkBox.setButtonDrawable(R.drawable.nullselector);
//            checkBox.setTypeface(typeface);
            if (dashboardActivity.sessionManager.getlang().equals(Constants.ARABICKEY)){
                checkBox.setTypeface(typeface_ar);
            }
            else {
                checkBox.setTypeface(typeface);
            }
            checkBox.setTextSize(12);
            checkBox.setTextAlignment(View.TEXT_ALIGNMENT_VIEW_START);
        }
        if (radioButton!=null){
            LinearLayout.LayoutParams params2 = new LinearLayout.LayoutParams(
                    LinearLayout.LayoutParams.WRAP_CONTENT,
                    LinearLayout.LayoutParams.WRAP_CONTENT);
            params2.setMargins(20, 10, 20, 10);
            radioButton.setPadding(20, 15, 20, 15);
            radioButton.setTextColor(Color.parseColor("#515151"));
            radioButton.setLayoutParams(params2);
            radioButton.setTextAlignment(View.TEXT_ALIGNMENT_VIEW_START);
            radioButton.setButtonDrawable(R.drawable.nullselector);
            Typeface typeface2 = ResourcesCompat.getFont(mcontext, R.font.poppins_medium);
            Typeface typeface3 = ResourcesCompat.getFont(mcontext, R.font.droidkufiregular);
//            radioButton.setTypeface(typeface2);
            if (dashboardActivity.sessionManager.getlang().equals(Constants.ARABICKEY)){
                radioButton.setTypeface(typeface3);
            }
            else {
                radioButton.setTypeface(typeface2);
            }
            radioButton.setTextSize(12);
            radioButton.setTextColor(Color.parseColor("#515151"));
        }
    }
    private String getAge(int year, int month, int day){
        Calendar dob = Calendar.getInstance();
        Calendar today = Calendar.getInstance();
        dob.set(year, month, day);
        int age = today.get(Calendar.YEAR) - dob.get(Calendar.YEAR);
        if (today.get(Calendar.DAY_OF_YEAR) < dob.get(Calendar.DAY_OF_YEAR)){ age--; }
        Integer ageInt = new Integer(age);
        ageS = ageInt.toString();
        return ageS; }
    @Override
    public int getItemViewType(int position) { return position; }
    @Override
    public int getItemCount() { return dataSet.size(); }
    @Override
    public long getItemId(int position) { return position; }

}