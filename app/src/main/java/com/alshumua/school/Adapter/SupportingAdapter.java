package com.alshumua.school.Adapter;

import android.annotation.SuppressLint;
import android.app.DatePickerDialog;
import android.content.Context;
import android.content.res.Configuration;
import android.text.Editable;
import android.text.Html;
import android.text.TextWatcher;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.inputmethod.EditorInfo;
import android.webkit.WebView;
import android.webkit.WebViewClient;
import android.widget.DatePicker;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.cardview.widget.CardView;
import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.RecyclerView;

import com.alshumua.school.Activity.DashboardActivity;
import com.alshumua.school.Fragment.BriefFragment;
import com.alshumua.school.Model.InitialScreenModel;
import com.alshumua.school.Model.SupportingorgModel;
import com.alshumua.school.R;
import com.alshumua.school.Std.Constants;
import com.alshumua.school.Std.SessionManager;
import com.bumptech.glide.Glide;

import org.json.JSONArray;
import org.json.JSONException;

import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.List;
import java.util.Locale;

public class SupportingAdapter extends RecyclerView.Adapter<SupportingAdapter.MyViewHolder> {
    private Context mcontext;
    private List<SupportingorgModel> dataSet;
    private DashboardActivity dashboardActivity;

    Fragment fragment = null;
    SessionManager sessionManager;

    public SupportingAdapter(Context mcontext,List<SupportingorgModel> dataSet) {
        this.mcontext = mcontext;
        this.dataSet = dataSet;
        this.dashboardActivity = dashboardActivity;
    }

    public class MyViewHolder extends RecyclerView.ViewHolder {
        TextView textView;
        ImageView imageView;

        LinearLayout linearLayout,linearLayouthori;

        public MyViewHolder(@NonNull View itemView) {
            super(itemView);
            this.textView = (TextView) itemView.findViewById(R.id.supportingtext);
//            this.imageView = (ImageView) itemView.findViewById(R.id.img);
            this.linearLayout = (LinearLayout) itemView.findViewById(R.id.linearlayoutSupport);
            this.linearLayouthori = (LinearLayout) itemView.findViewById(R.id.imglinear);
//            this.linearLayouthori = (LinearLayout) itemView.findViewById(R.id.linearhori);

        }
    }
    @NonNull
    @Override
    public MyViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.supporting_items, parent, false);
        MyViewHolder  viewHolder  = new MyViewHolder(view);


        return viewHolder;
    }
    @Override
    public void onBindViewHolder(@NonNull MyViewHolder holder, @SuppressLint("RecyclerView") final int listPosition) {
        holder.textView.setText(Html.fromHtml(dataSet.get(listPosition).getTitle()));
        String input = dataSet.get(listPosition).getIcons();
        JSONArray iconArray = null;
        try {
            iconArray = new JSONArray(input);
        } catch (JSONException e) {
            e.printStackTrace();
        }
        Log.d("length", "length> "+iconArray.length());
        for (int i = 0; i <iconArray.length(); i++) {
            Log.d("i", "ii>> "+i);
                try {
                    LinearLayout parent = new LinearLayout(mcontext);
                    parent.setOrientation(LinearLayout.HORIZONTAL);
                    parent.setLayoutParams(new LinearLayout.LayoutParams
                            (100, 260));

                    ImageView imageView=new ImageView(mcontext);

                    parent.addView(imageView);
//                    imageView.getLayoutParams().height=100;
//                    imageView.getLayoutParams().width=100;//260
                    imageView.setPadding(20,20,20,20);

                    holder.linearLayouthori.addView(parent);
                    Glide.with(mcontext).load(iconArray.getJSONObject(i).getString("icon")).error(R.drawable.ic_physiotherapy).into(imageView);
                } catch (JSONException jsonException) {
                    jsonException.printStackTrace();
                }

//
        }
    }

    @Override
    public int getItemCount() {
        return dataSet.size();
    }
}

