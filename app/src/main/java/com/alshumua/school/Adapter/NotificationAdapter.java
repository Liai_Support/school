package com.alshumua.school.Adapter;

import android.annotation.SuppressLint;
import android.content.Context;
import android.text.Html;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.cardview.widget.CardView;
import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.RecyclerView;

import com.alshumua.school.Activity.DashboardActivity;
import com.alshumua.school.Fragment.FormFragment;
import com.alshumua.school.Fragment.NotifictionFragment;
import com.alshumua.school.Model.NotificationModel;
import com.alshumua.school.R;
import com.alshumua.school.Std.SessionManager;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.List;

public class NotificationAdapter extends RecyclerView.Adapter<NotificationAdapter.MyViewHolder> {
private Context mcontext;
private List<NotificationModel> dataSet;
    private DashboardActivity dashboardActivity;
    private NotifictionFragment notifictionFragment;


public NotificationAdapter(NotifictionFragment notifictionFragment,Context mcontext,List<NotificationModel> dataSet,DashboardActivity dashboardActivity) {
    this.notifictionFragment = notifictionFragment;
        this.mcontext = mcontext;
        this.dataSet = dataSet;
    this.dashboardActivity = dashboardActivity;
        }

public class MyViewHolder extends RecyclerView.ViewHolder {
    TextView title,message,createdat;
    LinearLayout linearLayout;
    CardView notificationcard;
    ImageView deleteicon;

    public MyViewHolder(@NonNull View itemView) {
        super(itemView);
        this.title = (TextView) itemView.findViewById(R.id.title);
        this.message = (TextView) itemView.findViewById(R.id.message);
        this.createdat = (TextView) itemView.findViewById(R.id.createdat);
        this.deleteicon = (ImageView) itemView.findViewById(R.id.deleteicon);
            this.notificationcard = (CardView) itemView.findViewById(R.id.notification_card);
    }
}
    @NonNull
    @Override
    public NotificationAdapter.MyViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.card_notification, parent, false);
        NotificationAdapter.MyViewHolder viewHolder  = new NotificationAdapter.MyViewHolder(view);


        return viewHolder;
    }
    @Override
    public void onBindViewHolder(@NonNull NotificationAdapter.MyViewHolder holder, @SuppressLint("RecyclerView") final int listPosition) {
//holder.deleteicon.setVisibility(View.GONE);
        holder.title.setText(dataSet.get(listPosition).getTitle());
        holder.message.setText(dataSet.get(listPosition).getMessage());
        holder.createdat.setText(dataSet.get(listPosition).getCreated_at());
        holder.deleteicon.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
//notifictionFragment.selectedid=dataSet.get(listPosition).getId();
                notifictionFragment.deleteSingleReqBody(dataSet.get(listPosition).getId());
            }
        });

    }

    @Override
    public int getItemCount() {
        return dataSet.size();
    }
}

