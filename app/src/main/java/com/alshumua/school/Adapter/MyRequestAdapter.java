package com.alshumua.school.Adapter;

import android.annotation.SuppressLint;
import android.content.Context;
import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.cardview.widget.CardView;
import androidx.core.content.ContextCompat;
import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.RecyclerView;

import com.alshumua.school.Fragment.MyRequestFragment;

import com.alshumua.school.Fragment.RequestDetailsFragment;
import com.alshumua.school.Model.MyRequestListModel;
import com.alshumua.school.R;
import com.alshumua.school.Std.SessionManager;
import com.squareup.picasso.Picasso;

import java.util.List;
public class MyRequestAdapter extends RecyclerView.Adapter<MyRequestAdapter.MyViewHolder> {
    private MyRequestFragment myRequestFragment;
    private Context mcontext;
    private List<MyRequestListModel> dataSet;


    Fragment fragment = null;
    SessionManager sessionManager;

    public MyRequestAdapter(MyRequestFragment myRequestFragment,
                            Context mcontext,
                            List<MyRequestListModel> dataSet) {
        this.myRequestFragment = myRequestFragment;
        this.mcontext = mcontext;
        this.dataSet = dataSet;
        this.fragment = fragment;
        this.sessionManager = sessionManager;
    }

    public class MyViewHolder extends RecyclerView.ViewHolder {
        TextView serviceName,requestdate;
        ImageView serviceImg;
        CardView card;
        public MyViewHolder(@NonNull View itemView) {
            super(itemView);
            this.serviceName = (TextView) itemView.findViewById(R.id.requestname);
            this.serviceImg = (ImageView) itemView.findViewById(R.id.requestimg);
            this.card = (CardView) itemView.findViewById(R.id.request_card);
            this.requestdate = (TextView) itemView.findViewById(R.id.requestdate);
        }
    }
    @NonNull
    @Override
    public MyViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.card_myrequest, parent, false);
        MyViewHolder  viewHolder  = new MyViewHolder(view);


        return viewHolder;
    }
    @Override
    public void onBindViewHolder(@NonNull MyViewHolder holder, @SuppressLint("RecyclerView") final int listPosition) {
        holder.serviceName.setText(""+dataSet.get(listPosition).getServiceName());
        holder.requestdate.setText(""+dataSet.get(listPosition).getDate());

       holder.serviceImg.setColorFilter(ContextCompat.getColor(mcontext, R.color.textcolor), android.graphics.PorterDuff.Mode.MULTIPLY);
        holder.serviceImg.getLayoutParams().height = 200;
        holder.serviceImg.getLayoutParams().width = 200;
        Picasso.get().load(dataSet.get(listPosition).getServiceIcon()).error(R.drawable.fullassesment).into(holder.serviceImg);

        holder.card.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                notifyDataSetChanged();
                Bundle args = new Bundle();
                args.putInt("requestId",dataSet.get(listPosition).getRequestId());
                Fragment fragment = new RequestDetailsFragment();
                fragment.setArguments(args);
                myRequestFragment.getActivity().getSupportFragmentManager().beginTransaction().replace(R.id.fragment_container, fragment).addToBackStack(null).commit();

            }
        });

    }

    @Override
    public int getItemCount() {
        return dataSet.size();
    }
}

