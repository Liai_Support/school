package com.alshumua.school.Adapter;

import android.annotation.SuppressLint;
import android.content.Context;
import android.graphics.Color;
import android.graphics.Typeface;
import android.os.Build;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.annotation.RequiresApi;
import androidx.cardview.widget.CardView;
import androidx.core.content.ContextCompat;
import androidx.core.content.res.ResourcesCompat;
import androidx.recyclerview.widget.RecyclerView;

import com.alshumua.school.Activity.DashboardActivity;
import com.alshumua.school.Fragment.RequestDetailsFragment;
import com.alshumua.school.Model.RequestDetailsModel;
import com.alshumua.school.R;
import com.alshumua.school.Std.Constants;

import org.json.JSONArray;
import org.json.JSONException;

import java.util.List;

import static android.content.ContentValues.TAG;

public class RequestDetailAdapter extends RecyclerView.Adapter<RequestDetailAdapter.MyViewHolder> {
    private Context mcontext;
    private List<RequestDetailsModel> dataSet;
    View view;
    private DashboardActivity dashboardActivity;
    public RequestDetailAdapter(Context mcontext, List<RequestDetailsModel> dataSet,DashboardActivity dashboardActivity) {
        this.mcontext = mcontext;
        this.dataSet = dataSet;
        this.dashboardActivity = dashboardActivity;
    }

    public class MyViewHolder extends RecyclerView.ViewHolder {
        LinearLayout linearLayout;

        public MyViewHolder(@NonNull View itemView) {
            super(itemView);
            this.linearLayout = (LinearLayout) itemView.findViewById(R.id.linearlayoutReqDetails);
        }
    }

    @NonNull
    @Override
    public MyViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        view = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.requestdetail_items, parent, false);
        MyViewHolder viewHolder = new MyViewHolder(view);
        viewHolder.setIsRecyclable(false);
        return viewHolder;
    }

    @RequiresApi(api = Build.VERSION_CODES.M)
    @Override
    public void onBindViewHolder(@NonNull MyViewHolder holder, @SuppressLint("RecyclerView") final int listPosition) {
        holder.setIsRecyclable(false);

        TextView textView = new TextView(mcontext);
        textView.setText(dataSet.get(listPosition).getServiceName());
        setTextViewAttributes1(textView);
//        setAttributes(textView,null,"text");
        holder.linearLayout.addView(textView);

        CardView card3 = new CardView(mcontext);
     cardAppearence(card3);
//        setAttributes(null,card3,"card");
        TextView remarks = new TextView(mcontext);


        CardView card2 = new CardView(mcontext);
        cardAppearence(card2);
        TextView status = new TextView(mcontext);
        status.setText(mcontext.getString(R.string.status)  +"   "+":"+" "+dataSet.get(listPosition).getStatus());
        setTextViewAttributes(status);
        card2.addView(status);
        holder.linearLayout.addView(card2);


if (!dataSet.get(listPosition).getRemarks().equals("")||!dataSet.get(listPosition).getRemarks().equals(null)){

    remarks.setText(mcontext.getString(R.string.Remark)+"   "+":"+" "+dataSet.get(listPosition).getRemarks());
    setTextViewAttributes(remarks);
    card3.addView(remarks);
    holder.linearLayout.addView(card3);
}
if (dataSet.get(listPosition).getRemarks().equals("")||dataSet.get(listPosition).getRemarks().equals(null)){
    card3.removeView(remarks);
    holder.linearLayout.removeView(card3);
}


        CardView card4 = new CardView(mcontext);
        cardAppearence(card4);
        TextView date = new TextView(mcontext);
        date.setText(mcontext.getString(R.string.Date)+"   "+":"+" "+dataSet.get(listPosition).getDate());
        setTextViewAttributes(date);
        card4.addView(date);
        holder.linearLayout.addView(card4);

        String input = dataSet.get(listPosition).getQuestionAnswers();
        JSONArray jsonArray = null;
        try {
            jsonArray = new JSONArray(input);
        } catch (JSONException e) {
            e.printStackTrace();
        }




        CardView cardd= new CardView(mcontext);
        cardAppearence(cardd);
            for (int i = 0; i < jsonArray.length(); i++) {

                try {
                    if (!jsonArray.getJSONObject(i).getString("answer").equals("default")){
                        try {
                            CardView card5 = new CardView(mcontext);
                            cardAppearence(card5);
                            LinearLayout parent = new LinearLayout(mcontext);
                            parent.setLayoutParams(new LinearLayout.LayoutParams
                                    (LinearLayout.LayoutParams.MATCH_PARENT, LinearLayout.LayoutParams.WRAP_CONTENT));
                            parent.setOrientation(LinearLayout.VERTICAL);
                            TextView tv6 = new TextView(mcontext);
                            TextView tv7 = new TextView(mcontext);
                            setTextViewAttributes2(tv7);
                            setTextViewAttributes2(tv6);
                            tv6.setText(jsonArray.getJSONObject(i).getString("question"));
                            tv7.setText(jsonArray.getJSONObject(i).getString("answer"));
                            parent.addView(tv6);
                            parent.addView(tv7);
                            card5.addView(parent);
                            holder.linearLayout.addView(card5);

                        } catch (JSONException e) {
                            e.printStackTrace();
                        }
                    }
                } catch (JSONException e) {
                    e.printStackTrace();
                }

                }




        }

    private void cardAppearence(CardView card) {
        LinearLayout.LayoutParams layoutParams = new LinearLayout.LayoutParams(
                LinearLayout.LayoutParams.MATCH_PARENT,
                LinearLayout.LayoutParams.WRAP_CONTENT
        );
        card.setLayoutParams(layoutParams);
       card.setUseCompatPadding(true);
        card.setCardBackgroundColor(Color.parseColor("#F5F4F4"));//gray
        card.requestLayout();
    }
    private void setTextViewAttributes(TextView textView) {
        LinearLayout.LayoutParams params = new LinearLayout.LayoutParams(
                LinearLayout.LayoutParams.MATCH_PARENT,
                LinearLayout.LayoutParams.WRAP_CONTENT);
        params.setMargins(0, 20, 0, 10);
        textView.setTextColor(ContextCompat.getColor(mcontext, R.color.textcolor));
        textView.setTextSize(12);
        textView.setTextAlignment(View.TEXT_ALIGNMENT_VIEW_START);
        textView.setTextAppearance(mcontext, R.style.poppin_medium);
        Typeface typefacear = ResourcesCompat.getFont(mcontext, R.font.droidkufiregular);
        Typeface typeface = ResourcesCompat.getFont(mcontext, R.font.poppins_medium);
        if (dashboardActivity.sessionManager.getlang().equals(Constants.ARABICKEY)){
            textView.setTypeface(typefacear);
        }
        else {
            textView.setTypeface(typeface);
        }

        textView.setTextColor(Color.parseColor("#515151"));
        textView.setPadding(5, 5, 5, 5);
//        textView.setTypeface(typeface);
        textView.setLayoutParams(params);
    }
    private void setTextViewAttributes1(TextView tv) {
        LinearLayout.LayoutParams params = new LinearLayout.LayoutParams(
                LinearLayout.LayoutParams.MATCH_PARENT,
                LinearLayout.LayoutParams.WRAP_CONTENT);
        params.setMargins(0, 10, 0, 10);
        tv.setTextColor(ContextCompat.getColor(mcontext, R.color.textcolor));
        tv.setTextSize(20);
        tv.setTextAlignment(View.TEXT_ALIGNMENT_CENTER);
        Typeface typeface = ResourcesCompat.getFont(mcontext, R.font.poppin_semibold);
        Typeface typefacear = ResourcesCompat.getFont(mcontext, R.font.droidkufiregular);
        if (dashboardActivity.sessionManager.getlang().equals(Constants.ARABICKEY)){
            tv.setTypeface(typefacear);
        }
        else {
            tv.setTypeface(typeface);
        }
        tv.setTextColor(Color.parseColor("#515151"));
         tv.setPadding(15, 15, 15, 15);
//        tv.setTypeface(typeface);
        tv.setLayoutParams(params);
    }
    private void setTextViewAttributes2(TextView textView) {
        LinearLayout.LayoutParams params = new LinearLayout.LayoutParams(
                LinearLayout.LayoutParams.MATCH_PARENT,
                LinearLayout.LayoutParams.WRAP_CONTENT);
        textView.setTextColor(ContextCompat.getColor(mcontext, R.color.textcolor));
        textView.setTextAlignment(View.TEXT_ALIGNMENT_VIEW_START);
        Typeface typeface = ResourcesCompat.getFont(mcontext, R.font.poppins_medium);
        Typeface typefacear = ResourcesCompat.getFont(mcontext, R.font.droidkufiregular);
        if (dashboardActivity.sessionManager.getlang().equals(Constants.ARABICKEY)){
            textView.setTypeface(typefacear);
        }
        else {
            textView.setTypeface(typeface);
        }
        textView.setTextColor(Color.parseColor("#707070"));
        textView.setPadding(5, 5, 5, 5);
        textView.setTextSize(12);
//        textView.setTypeface(typeface);
        textView.setLayoutParams(params);
    }
    private void setAttributes(TextView textView,CardView cardView,String type){
        LinearLayout.LayoutParams params = new LinearLayout.LayoutParams(
                LinearLayout.LayoutParams.MATCH_PARENT,
                LinearLayout.LayoutParams.WRAP_CONTENT);
        if (type.equals("text")){

            params.setMargins(0, 10, 0, 10);
            textView.setTextColor(ContextCompat.getColor(mcontext, R.color.textcolor));
            textView.setTextSize(20);
            textView.setTextAlignment(View.TEXT_ALIGNMENT_CENTER);
            Typeface typeface = ResourcesCompat.getFont(mcontext, R.font.poppin_semibold);
            Typeface typeface_ar = ResourcesCompat.getFont(mcontext, R.font.droidkufibold);
            textView.setTextColor(Color.parseColor("#515151"));
            textView.setPadding(15, 15, 15, 15);

            if (dashboardActivity.sessionManager.getlang().equals(Constants.ARABICKEY)){
                textView.setTypeface(typeface_ar);
            }
            else {
                textView.setTypeface(typeface);
            }
            textView.setLayoutParams(params);
        }
        if (cardView!=null){
            cardView.setLayoutParams(params);
            cardView.setUseCompatPadding(true);
            cardView.setCardBackgroundColor(Color.parseColor("#F5F4F4"));//gray
            cardView.requestLayout();
        }
    }


    @Override
    public int getItemCount() {
        return dataSet.size();
    }

    @Override
    public int getItemViewType(int position) {
        return position;
    }

    @Override
    public long getItemId(int position) {
        return position;
    }



}
