package com.alshumua.school.Adapter;

import static com.firebase.ui.auth.AuthUI.getApplicationContext;
import static com.google.firebase.messaging.Constants.MessageNotificationKeys.TAG;

import android.animation.ObjectAnimator;
import android.annotation.SuppressLint;
import android.app.Activity;
import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.os.Build;
import android.os.Bundle;
import android.util.Log;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.alshumua.school.Activity.DashboardActivity;
import com.alshumua.school.Activity.NewSignin;
//import com.alshumua.school.Activity.SupportingActivity;
import com.alshumua.school.Activity.ThreeBtnScreenActivity;
import com.alshumua.school.Std.Constants;
import com.alshumua.school.databinding.CardAllserviceBinding;
import androidx.annotation.NonNull;
import androidx.annotation.RequiresApi;
import androidx.cardview.widget.CardView;
import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.RecyclerView;

import com.alshumua.school.Fragment.HomeFragment;
import com.alshumua.school.Fragment.ServiceDetailsFragment;
import com.alshumua.school.Model.ServiceModel;
import com.alshumua.school.R;
import com.alshumua.school.Std.SessionManager;
import com.squareup.picasso.Picasso;

import java.util.ArrayList;
import java.util.List;

public class ServiceAdapter  extends RecyclerView.Adapter<ServiceAdapter.MyViewHolder> {
    private HomeFragment homeFragment;
    private Context mcontext;
    private List<ServiceModel> dataSet;
    Fragment fragment = null;
    private DashboardActivity dashboardActivity;
    int row_index=-1;


    public ServiceAdapter(HomeFragment homeFragment, Context mcontext, List<ServiceModel> dataSet,DashboardActivity dashboardActivity) {
        this.homeFragment = homeFragment;
        this.mcontext = mcontext;
        this.dataSet = dataSet;
        this.fragment = fragment;
        this.dashboardActivity = dashboardActivity;
    }
    static class MyViewHolder extends RecyclerView.ViewHolder {

        CardAllserviceBinding binding;

        public MyViewHolder(@NonNull View itemView) {
            super(itemView);
            binding = CardAllserviceBinding.bind(itemView);
        }

    }
    @NonNull
    @Override
    public MyViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        return new MyViewHolder(LayoutInflater.from(parent.getContext()).inflate(R.layout.card_allservice, parent, false));

//        return


    }
    @Override
    public void onBindViewHolder(@NonNull MyViewHolder holder, @SuppressLint("RecyclerView") final int listPosition) {
        holder.binding.serviceName.setText("" + dataSet.get(listPosition).getServiceName());
        Picasso.get().load(dataSet.get(listPosition).getServiceIcon()).error(R.drawable.fullassesment).into(holder.binding.serviceImg);
        if (dashboardActivity.sessionManager.getlang().equals(Constants.ARABICKEY)){
            dashboardActivity.sessionManager.arabicfont(holder.binding.serviceName,null, null,Constants.REGULAR,"textV");

        }
        holder.binding.serviceCard.setOnClickListener(new View.OnClickListener() {
            @RequiresApi(api = Build.VERSION_CODES.M)
            @Override
            public void onClick(View v) {
                row_index=listPosition;
                notifyDataSetChanged();
                Bundle args = new Bundle();
                args.putInt("serviceid",dataSet.get(listPosition).getId());
                args.putString("serviceName", dataSet.get(listPosition).getServiceName());
                args.putString("serviceIcon", dataSet.get(listPosition).getServiceIcon());
                args.putString("description",dataSet.get(listPosition).getDescription());
                args.putString("loginRequired",dataSet.get(listPosition).getLoginRequired());
                args.putString("initial",dataSet.get(listPosition).getInitial());
                args.putString("symptoms",dataSet.get(listPosition).getSymptoms());

                Fragment fragment = new ServiceDetailsFragment();
                fragment.setArguments(args);
                if (dataSet.get(listPosition).getLoginRequired().equals("yes")){
                    if (dashboardActivity.sessionManager.gettokenin().equals("")){
                        dashboardActivity.sessionManager.loginRequiredDialog(mcontext,"service");
                    }
                    else {
                        homeFragment.getActivity().getSupportFragmentManager().beginTransaction().replace(R.id.fragment_container, fragment).addToBackStack(null).commit();
                    }
                }
                else {
                    homeFragment.getActivity().getSupportFragmentManager().beginTransaction().replace(R.id.fragment_container, fragment).addToBackStack(null).commit();

                }
            }
        });
//  FOR CARD COLOR CHANGE ON CLICK
        if(row_index==listPosition){
            holder.binding.cardlinear.setBackgroundColor(Color.parseColor("#3BB54A")); }
        else
        {
            holder.binding.cardlinear.setBackgroundColor(Color.parseColor("#255FAD"));
        }

}
   @Override
    public int getItemCount() {
        return dataSet.size();
    }

    public void filterList(ArrayList<ServiceModel> filteredList) {
        dataSet = filteredList;
        notifyDataSetChanged();
    }

}

