package com.alshumua.school.Adapter;


import android.annotation.SuppressLint;
import android.content.Context;
import android.graphics.Color;
import android.graphics.Typeface;
import android.text.Html;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.CheckBox;
import android.widget.CompoundButton;
import android.widget.LinearLayout;

import androidx.annotation.NonNull;
import androidx.core.content.res.ResourcesCompat;
import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.RecyclerView;

import com.alshumua.school.Activity.DashboardActivity;
import com.alshumua.school.Fragment.FormFragment;
import com.alshumua.school.Fragment.InitialSymtomsFragment;
import com.alshumua.school.Model.ServiceModel;
import com.alshumua.school.Model.SymtomsModel;
import com.alshumua.school.R;
import com.alshumua.school.Std.Constants;
import com.alshumua.school.Std.SessionManager;

import org.json.JSONArray;
import org.json.JSONException;

import java.util.ArrayList;
import java.util.List;

public class SymtomsAdapter extends RecyclerView.Adapter<SymtomsAdapter.MyViewHolder> {
    private Context mcontext;
    private String symtomStr;

    private List<SymtomsModel> dataSet;
    private DashboardActivity dashboardActivity;

    private InitialSymtomsFragment fragment;
    SessionManager sessionManager;

    public SymtomsAdapter(InitialSymtomsFragment formFragment,Context mcontext,List<SymtomsModel> dataSet,String symtomStr) {
        this.fragment = formFragment;
        this.mcontext = mcontext;
        this.dataSet = dataSet;
        this.dashboardActivity = dashboardActivity;
        this.symtomStr =symtomStr;
    }

    public class MyViewHolder extends RecyclerView.ViewHolder {
        CheckBox checkBox;
        LinearLayout linearLayout;


        public MyViewHolder(@NonNull View itemView) {
            super(itemView);
//            this.checkBox = (CheckBox) itemView.findViewById(R.id.checkboxsym);
            this.linearLayout = itemView.findViewById(R.id.formlinear);
        }
    }
    @NonNull
    @Override
    public SymtomsAdapter.MyViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.checkbox_design, parent, false);
        SymtomsAdapter.MyViewHolder viewHolder  = new SymtomsAdapter.MyViewHolder(view);


        return viewHolder;
    }
    @Override
    public void onBindViewHolder(@NonNull SymtomsAdapter.MyViewHolder holder, @SuppressLint("RecyclerView") final int listPosition) {

//        String input = dataSet.get(listPosition).getSymptoms();
        Log.d("checkboxsym",""+symtomStr);
        JSONArray jsonArray = null;
        try {
            jsonArray = new JSONArray(symtomStr);
        } catch (JSONException e) {
            e.printStackTrace();
        }

        ArrayList<String> allitems = new ArrayList<String>();
        for (int i = 0; i < jsonArray.length(); i++) {
            LinearLayout checkBoxLayout = new LinearLayout(mcontext);
            checkBoxLayout.setOrientation(LinearLayout.HORIZONTAL);

            CheckBox checkBox = new CheckBox(mcontext);
            checkBox.setBackgroundResource(R.drawable.custom_checkbox_bg);
            try {

                checkBox.setText("   " + jsonArray.getJSONObject(i).getString("symName") + "   ");
//                checkBox.setText(jsonArray.getJSONObject(i).getString("symName"));
                checkBox.setId(Integer.parseInt(jsonArray.getJSONObject(i).getString("symId")));
                allitems.add(jsonArray.getJSONObject(i).getString("symName"));

                for (int i1 = 0; i1 < fragment.selectedStrings.size(); i1++) {
                    if (checkBox.getText().toString().equalsIgnoreCase( fragment.selectedStrings.get(i1))) {
                        checkBox.setChecked(true);
                    }
                }
            } catch (JSONException e) {
                e.printStackTrace();
            }
            checkBox.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
                @Override
                public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                    if (isChecked) {
                        checkBox.setTextColor(Color.parseColor("#FFFFFF"));
                    }
                    if (!isChecked) {
                        checkBox.setTextColor(Color.parseColor("#515151"));
                    }
                    int value = ischecked_checkbox(fragment.selectedStrings, checkBox.getText().toString());
                    int value2 = ischecked_checkbox(fragment.selectedSymId, String.valueOf(checkBox.getId()));
                    if (value != -1) {
                        fragment.selectedStrings.remove(value);

                    }
                    else {
                        fragment.selectedStrings.add(checkBox.getText().toString());
                    }
                    /*----------------------------------*/

                    if (value2 != -1) {
                        fragment.selectedSymId.remove(value2);

                    }
                    else {
                        fragment.selectedSymId.add(String.valueOf(checkBox.getId()));
                    }

                    if ( fragment.selectedStrings.size() == 0) {
//                        nothing selected
//
                    }
                    else {
//                        selectedStrings.toString()
                        Log.d("TAG", "selectedStrings: "+  fragment.selectedStrings.toString());
                        Log.d("TAG", "selectedSymId:1 "+  fragment.selectedSymId.toString());
//                        args.putString("symptom_ids", selectedSymId.toString());
                        sessionManager.setselectedSymtomId( fragment.selectedSymId.toString());
                    }
                }
            });

            checkBox.setTextColor(Color.parseColor("#515151"));
            LinearLayout.LayoutParams params = new LinearLayout.LayoutParams(LinearLayout.LayoutParams.MATCH_PARENT, LinearLayout.LayoutParams.WRAP_CONTENT);
            params.setMargins(50, 50, 0, 0);
            checkBox.setLayoutParams(params);
            checkBox.setTextAppearance(mcontext, R.style.poppin_medium);
            checkBox.setButtonDrawable(R.drawable.nullselector);
            Typeface typeface = ResourcesCompat.getFont(mcontext, R.font.poppins_medium);
            Typeface typeface_ar = ResourcesCompat.getFont(mcontext, R.font.droidkufiregular);
//            checkBox.setTypeface(typeface);
//            if (sessionManager.getlang().equals(Constants.ARABICKEY)){
//                checkBox.setTypeface(typeface_ar);
//            }
//            else {
//                checkBox.setTypeface(typeface);
//            }
            checkBox.setTextSize(12);
            checkBox.setTextAlignment(View.TEXT_ALIGNMENT_VIEW_START);

            checkBoxLayout.addView(checkBox);
           holder.linearLayout.addView(checkBoxLayout);
//
        }
    }

    @Override
    public int getItemCount() {
        return 1;
    }

    private int ischecked_checkbox(ArrayList<String> selectedstrings,String text){
        for (int j = 0; j < selectedstrings.size(); j++) {
            if (text.equalsIgnoreCase(selectedstrings.get(j))) { return j; } }
        return -1; }
}

