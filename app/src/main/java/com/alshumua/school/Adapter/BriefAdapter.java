package com.alshumua.school.Adapter;

import android.annotation.SuppressLint;
import android.content.Context;
import android.graphics.Color;
import android.graphics.Typeface;
import android.os.Build;
import android.os.Bundle;
import android.text.Html;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.webkit.WebView;
import android.webkit.WebViewClient;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.annotation.RequiresApi;
import androidx.cardview.widget.CardView;
import androidx.core.content.ContextCompat;
import androidx.core.content.res.ResourcesCompat;
import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.RecyclerView;

import com.alshumua.school.Fragment.MyRequestFragment;
import com.alshumua.school.Fragment.RequestDetailsFragment;
import com.alshumua.school.Model.BriefModel;
import com.alshumua.school.Model.InitialScreenModel;
import com.alshumua.school.Model.MyRequestListModel;
import com.alshumua.school.Model.RequestDetailsModel;
import com.alshumua.school.R;
import com.alshumua.school.Std.Constants;
import com.alshumua.school.Std.SessionManager;
import com.squareup.picasso.Picasso;

import org.json.JSONArray;
import org.json.JSONException;

import java.util.List;

public class BriefAdapter extends RecyclerView.Adapter<BriefAdapter.MyViewHolder> {
    private Context mcontext;
    private List<BriefModel> dataSet;


    Fragment fragment = null;
    SessionManager sessionManager;

    public BriefAdapter(Context mcontext,List<BriefModel> dataSet) {
        this.mcontext = mcontext;
        this.dataSet = dataSet;
    }

    public class MyViewHolder extends RecyclerView.ViewHolder {
        TextView textView;
//        LinearLayout linearLayout;

        public MyViewHolder(@NonNull View itemView) {
            super(itemView);
            this.textView = (TextView) itemView.findViewById(R.id.content);
//            this.linearLayout = (LinearLayout) itemView.findViewById(R.id.linearlayoutBrief);
        }
    }
    @NonNull
    @Override
    public MyViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.brief_items_new, parent, false);
        MyViewHolder  viewHolder  = new MyViewHolder(view);


        return viewHolder;
    }
    @Override
    public void onBindViewHolder(@NonNull MyViewHolder holder, @SuppressLint("RecyclerView") final int listPosition) {
//        holder.textView.setText(""+dataSet.get(listPosition).getBasic());
       holder.textView.setText(Html.fromHtml(dataSet.get(listPosition).getContent()));
//       if (sessionManager.getlang().equals(Constants.ARABICKEY)){
//           sessionManager.arabicfont(holder.textView, Constants.REGULAR);
//       }
       /* WebView webView = new WebView(mcontext);
        webView.getSettings().setJavaScriptEnabled(true);
        webView.setWebViewClient(new WebViewClient());
        webView.loadData(dataSet.get(listPosition).getBasic(),"text/html","UTF-8");
        holder.linearLayout.addView(webView);*/



    }

    @Override
    public int getItemCount() {
        return dataSet.size();
    }
}
