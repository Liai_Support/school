package com.alshumua.school.Adapter;

import android.annotation.SuppressLint;
import android.content.Context;
import android.text.Html;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.RecyclerView;

import com.alshumua.school.Model.FaqModel;
import com.alshumua.school.Model.InitialScreenModel;
import com.alshumua.school.R;
import com.alshumua.school.Std.SessionManager;

import java.util.List;

public class FaqAdapter extends RecyclerView.Adapter<FaqAdapter.MyViewHolder> {
    private Context mcontext;
    private List<FaqModel> dataSet;


    Fragment fragment = null;
    SessionManager sessionManager;

    public FaqAdapter(Context mcontext,List<FaqModel> dataSet) {
        this.mcontext = mcontext;
        this.dataSet = dataSet;
    }

    public class MyViewHolder extends RecyclerView.ViewHolder {
        TextView question,answer,id;
        LinearLayout linearLayout;

        public MyViewHolder(@NonNull View itemView) {
            super(itemView);
            this.question = (TextView) itemView.findViewById(R.id.question);
            this.answer = (TextView) itemView.findViewById(R.id.answer);
            this.id = (TextView) itemView.findViewById(R.id.id);
//            this.linearLayout = (LinearLayout) itemView.findViewById(R.id.linearlayoutBrief);
        }
    }
    @NonNull
    @Override
    public MyViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.card_faq, parent, false);
        MyViewHolder  viewHolder  = new MyViewHolder(view);


        return viewHolder;
    }
    @Override
    public void onBindViewHolder(@NonNull MyViewHolder holder, @SuppressLint("RecyclerView") final int listPosition) {
//        holder.textView.setText(""+dataSet.get(listPosition).getBasic());
        holder.question.setText(Html.fromHtml(dataSet.get(listPosition).getQuestion()));
        holder.answer.setText(Html.fromHtml(dataSet.get(listPosition).getAnswer()));
        holder.id.setText(Html.fromHtml(dataSet.get(listPosition).getId()));
//       if (sessionManager.getlang().equals(Constants.ARABICKEY)){
//           sessionManager.arabicfont(holder.textView, Constants.REGULAR);
//       }
       /* WebView webView = new WebView(mcontext);
        webView.getSettings().setJavaScriptEnabled(true);
        webView.setWebViewClient(new WebViewClient());
        webView.loadData(dataSet.get(listPosition).getBasic(),"text/html","UTF-8");
        holder.linearLayout.addView(webView);*/

    }

    @Override
    public int getItemCount() {
        return dataSet.size();
    }
}

