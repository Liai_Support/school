package com.alshumua.school.Std;

import android.app.Activity;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.pm.PackageManager;
import android.content.res.Configuration;
import android.content.res.Resources;
import android.graphics.Typeface;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.os.Build;
import android.util.DisplayMetrics;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.RadioButton;
import android.widget.TextView;

import androidx.appcompat.app.AlertDialog;
import androidx.core.app.ActivityCompat;
import androidx.core.content.ContextCompat;
import androidx.core.content.res.ResourcesCompat;

import com.alshumua.school.Activity.DashboardActivity;
import com.alshumua.school.Activity.NewSignin;
import com.android.volley.AuthFailureError;
import com.android.volley.NetworkError;
import com.android.volley.NoConnectionError;
import com.android.volley.ParseError;
import com.android.volley.ServerError;
import com.android.volley.TimeoutError;
import com.android.volley.VolleyError;
import com.alshumua.school.R;
import com.google.android.material.snackbar.BaseTransientBottomBar;
import com.google.android.material.snackbar.Snackbar;
import com.google.android.material.textfield.TextInputLayout;

import static android.Manifest.permission.ACCESS_FINE_LOCATION;
import static android.Manifest.permission.CAMERA;
import static android.Manifest.permission.READ_EXTERNAL_STORAGE;
import static android.Manifest.permission.WRITE_EXTERNAL_STORAGE;

import java.util.Locale;

public class SessionManager {

    private SharedPreferences pref;
    private static final String PREF_NAME = "SchoolPreference";
    SharedPreferences.Editor editor;
    Context context;
    ProgressDialog progressDialog;
    View view;
    Activity activity;




    // Shared pref mode
    int PRIVATE_MODE = 0;

    private static final String IS_NETWORK = "IsNetwork";

    private static final String IS_EXISTING_USER = "IsExistingUser";
    private static final String IS_GUEST_USER = "IsGuestUser";


    public SessionManager(Activity activity,Context context,View view) {
        this.context = context;
        this.view = view;
        this.activity = activity;
        pref = context.getSharedPreferences(PREF_NAME, PRIVATE_MODE);
        editor = pref.edit();
    }

    public void volleyerror(VolleyError error){
        if (error instanceof TimeoutError || error instanceof NoConnectionError) {
            //This indicates that the reuest has either time out or there is no connection
            snackbarToast(context.getString(R.string.timeouterror),view);
        } else if (error instanceof AuthFailureError) {
            // Error indicating that there was an Authentication Failure while performing the request
            snackbarToast(context.getString(R.string.authfailureerror),view);
        } else if (error instanceof ServerError) {
            //Indicates that the server responded with a error response
            snackbarToast(context.getString(R.string.servererror),view);
        } else if (error instanceof NetworkError) {
            //Indicates that there was network error while performing the request
            snackbarToast(context.getString(R.string.networkerror),view);
        } else if (error instanceof ParseError) {
            // Indicates that the server response could not be parsed
            snackbarToast(context.getString(R.string.parseerror),view);
        }
    }


    public void snackbarToast(String msg,View view){
        Snackbar.make(view, msg, Snackbar.LENGTH_SHORT)
                .setTextColor(ContextCompat.getColor(context,R.color.design_default_color_error))
                .setBackgroundTint(ContextCompat.getColor(context,R.color.gray))
                .setDuration(BaseTransientBottomBar.LENGTH_LONG)
                .setAnimationMode(BaseTransientBottomBar.ANIMATION_MODE_SLIDE)

                .show();

    }



    public void progressdialogshow(){
        if(progressDialog != null){
            if(progressDialog.isShowing()){
                progressDialog.dismiss();
            }
        }

        progressDialog = new ProgressDialog(context,R.style.MyDialogstyle);
        progressDialog.setTitle("");
        progressDialog.setMessage(context.getString(R.string.pleasewait));
        progressDialog.setCancelable(false);

        if(progressDialog.isShowing() == false){
            Log.d("dopencountt","1");
            progressDialog.show();
        }
    }

    public void progressdialogdismiss() {
        if (progressDialog != null) {
            if (progressDialog.isShowing() == true) {
                Log.d("dclosecountt", "1");
                progressDialog.dismiss();
            }
        }
    }

    public boolean isNetwork(){
        boolean connected = false;
        try {
            ConnectivityManager cm = (ConnectivityManager) context.getApplicationContext().getSystemService(Context.CONNECTIVITY_SERVICE);
            NetworkInfo nInfo = cm.getActiveNetworkInfo();
            connected = nInfo != null && nInfo.isAvailable() && nInfo.isConnected();
            return pref.getBoolean(IS_NETWORK,connected);
        }
        catch (Exception e) {
            Log.e("Connectivity Exception", e.getMessage());
        }
        return pref.getBoolean(IS_NETWORK,connected);
    }



//    existing user and guest user

    public void setselected(String selected) {
        pref.edit().putString("selected", selected).commit();
    }
    public String getselected() {
        String selected = pref.getString("selected","exist");
        return selected;
    }

    //    welcomeslidedone
    public void setwelcomeslidedone(int welcomeslidedone) {
        pref.edit().putInt("welcomeslidedone", welcomeslidedone).commit();
    }

    public int getwelcomeslidedone() {
        int welcomeslidedone = pref.getInt("welcomeslidedone", 0);
        return welcomeslidedone;
    }

    //    accesstoken
    public void settoken(String token) {
        pref.edit().putString("accesstoken", token).commit();
    }

    public String gettoken() {
        String token = pref.getString("accesstoken", "");
        return token;
    }


//    guest newpassword
public void setgunewpass(int gunewpass) {
    pref.edit().putInt("gunewpass", gunewpass).commit();
}

    public int getgunewpass() {
        int gunewpass = pref.getInt("gunewpass", 0);
        return gunewpass;
    }
    //    exist newpassword
    public void setexnewpass(int exnewpass) {
        pref.edit().putInt("exnewpass", exnewpass).commit();
    }

    public int getexnewpass() {
        int exnewpass = pref.getInt("exnewpass", 0);
        return exnewpass;
    }

    /*remember me exist start*/
    public void setremembermeExuname(String remembermeExuname) {
        pref.edit().putString("remembermeExuname", remembermeExuname).commit();
    }
    public String getremembermeExuname() {
        String remembermeExuname = pref.getString("remembermeExuname","");
        return remembermeExuname;
    }

    public void setremembermeExpass(String remembermeExpass) {
        pref.edit().putString("remembermeExpass", remembermeExpass).commit();
    }
    public String getremembermeExpass() {
        String remembermeExpass = pref.getString("remembermeExpass","");
        return remembermeExpass;
    }

    /*remember me exist end*/

    /*rememberme guest start*/
    public void setremembermeGuuname(String remembermeGuuname) {
        pref.edit().putString("remembermeGuuname", remembermeGuuname).commit();
    }
    public String getremembermeGuuname() {
        String remembermeGuuname = pref.getString("remembermeGuuname","");
        return remembermeGuuname;
    }

    public void setremembermeGupass(String remembermeGupass) {
        pref.edit().putString("remembermeGupass", remembermeGupass).commit();
    }
    public String getremembermeGupass() {
        String remembermeGupass = pref.getString("remembermeGupass","");
        return remembermeGupass;
    }
    /*rememberme guest end*/


    //    for guest user
    public void setguestuser(int guestuser) {
        pref.edit().putInt("guestuser", guestuser).commit();
    }

    public int getguestuser() {
        int guestuser = pref.getInt("guestuser", 0);
        return guestuser;
    }

    public void setguestuserin(int guestuser) {
        pref.edit().putInt("guestuser", guestuser).commit();
    }

    public int getguestuserin() {
        int guestuser = pref.getInt("guestuser", 0);
        return guestuser;
    }

    //    LOGIN TOK
    public void settokenin(String tokenin) {
        pref.edit().putString("accesstoken", tokenin).commit();
    }

    public String gettokenin() {
        String tokenin = pref.getString("accesstoken", "");
        return tokenin;
    }

    //    CHECKBOX REMEMBER ME
    public void setusernameR(String username) {
        pref.edit().putString("username", username).commit();
    }

    public String getusernameR() {
        String username = pref.getString("username", "");
        return username;
    }

    public void setpassword(String password) {
        pref.edit().putString("password", password).commit();
    }

    public String getpassword() {
        String password = pref.getString("password", "");
        return password;
    }

    //    username display
    public void setusernamedisplay(String usernamedisplay) {
        pref.edit().putString("username", usernamedisplay).commit();
    }

    public String getusernamedisplay() {
        String usernamedisplay = pref.getString("username", "User");
        return usernamedisplay;
    }

    //    ID
    public void setid(int id) {
        pref.edit().putInt("id", id).commit();
    }

    public int getid() {
        int id = pref.getInt("id", 0);
        return id;

    }

    //language
    public void setlang(String Lang) {
        pref.edit().putString("Lang", Lang).commit();
    }

    public String getlang() {
        String Lang = pref.getString("Lang", "en");
        return Lang;
    }
    //    Service ID
    public void setserviceid(int serviceid) {
        pref.edit().putInt("serviceid", serviceid).commit();
    }

    public int getserviceid() {
        int serviceid = pref.getInt("serviceid", 0);
        return serviceid;

    }

//profileimg
public void setprofile_img(String profile_img) {
    pref.edit().putString("profile_img", profile_img).commit();
}

    public String getprofile_img() {
        String profile_img = pref.getString("profile_img", "");
        return profile_img;
    }

//    existing parent details
//    parent name
public void setparent_name(String parent_name) {
    pref.edit().putString("parent_name", parent_name).commit();
}
public String getparent_name() {
        String parent_name = pref.getString("parent_name", "");
        return parent_name;
    }

    //    mobile_number
    public void setmobile_number(String mobile_number) {
        pref.edit().putString("mobile_number", mobile_number).commit();
    }
    public String getmobile_number() {
        String mobile_number = pref.getString("mobile_number", "");
        return mobile_number;
    }

    //    name
    public void setname(String name) {
        pref.edit().putString("name", name).commit();
    }
    public String getname() {
        String name = pref.getString("name", "");
        return name;
    }
    //   student_id
    public void setstudent_id(int student_id) {
        pref.edit().putInt("student_id", student_id).commit();
    }

    public int getstudent_id() {
        int student_id = pref.getInt("student_id", 0);
        return student_id;

    }
    //    verified
    public void setverified(String verified) {
        pref.edit().putString("verified", verified).commit();
    }
    public String getverified() {
        String verified = pref.getString("verified", "");
        return verified;
    }


    /*profile page details*/
    public void setexstudentname(String exstudentname) {
        pref.edit().putString("exstudentname", exstudentname).commit();
    }
    public String getexstudentname() {
        String exstudentname = pref.getString("exstudentname", "");
        return exstudentname;
    }

    public void setexparentname(String exparentname) {
        pref.edit().putString("exparentname", exparentname).commit();
    }
    public String getexparentname() {
        String exparentname = pref.getString("exparentname", "");
        return exparentname;
    }

    public void setexemail(String exemail) {
        pref.edit().putString("exemail", exemail).commit();
    }
    public String getexemail() {
        String exemail = pref.getString("exemail", "");
        return exemail;
    }

    public void setexmobile(String exmobile) {
        pref.edit().putString("exmobile", exmobile).commit();
    }
    public String getexmobile() {
        String exmobile = pref.getString("exmobile", "");
        return exmobile;
    }

    public void settype(String type) {
        pref.edit().putString("type", type).commit();
    }
    public String gettype() {
        String type = pref.getString("type", "");
        return type;
    }

    public void setexprofile_img(String exprofile_img) {
        pref.edit().putString("exprofile_img", exprofile_img).commit();
    }
    public String getexprofile_img() {
        String exprofile_img = pref.getString("exprofile_img", "");
        return exprofile_img;
    }

    public void setguprofile_img(String guprofile_img) {
        pref.edit().putString("guprofile_img", guprofile_img).commit();
    }
    public String getguprofile_img() {
        String guprofile_img = pref.getString("guprofile_img", "");
        return guprofile_img;
    }
/*               */

    public void setgufirstname(String gufirstname) {
        pref.edit().putString("gufirstname", gufirstname).commit();
    }
    public String getgufirstname() {
        String gufirstname = pref.getString("gufirstname", "");
        return gufirstname;
    }

    public void setgulastname(String gulastname) {
        pref.edit().putString("gulastname", gulastname).commit();
    }
    public String getgulastname() {
        String gulastname = pref.getString("gulastname", "");
        return gulastname;
    }



    public void setguemail(String guemail) {
        pref.edit().putString("guemail", guemail).commit();
    }
    public String getguemail() {
        String guemail = pref.getString("guemail", "");
        return guemail;
    }

    public void setgumobile(String gumobile) {
        pref.edit().putString("gumobile", gumobile).commit();
    }
    public String getgumobile() {
        String gumobile = pref.getString("gumobile", "");
        return gumobile;
    }

    public void setguparentname(String guparentname) {
        pref.edit().putString("guparentname", guparentname).commit();
    }
    public String getguparentname() {
        String guparentname = pref.getString("guparentname", "");
        return guparentname;
    }


//fcm tok
    public void setfcmtok(String fcmtok) {
        pref.edit().putString("fcmtok", fcmtok).commit();
    }
    public String getfcmtok() {
        String fcmtok = pref.getString("fcmtok", "");
        return fcmtok;
    }
//  COMMON DATA
public void setfirst_name_common(String first_name_common) {
    pref.edit().putString("first_name_common", first_name_common).commit();
}
    public String getfirst_name_common() {
        String first_name_common = pref.getString("first_name_common", "");
        return first_name_common;
    }

    public void setlast_name_common(String last_name_common) {
        pref.edit().putString("last_name_common", last_name_common).commit();
    }
    public String getlast_name_common() {
        String last_name_common = pref.getString("last_name_common", "");
        return last_name_common;
    }
    public void setemail_common(String email_common) {
        pref.edit().putString("email_common", email_common).commit();
    }
    public String getemail_common() {
        String email_common = pref.getString("email_common", "");
        return email_common;
    }
    public void setmobile_number_common(String mobile_number_common) {
        pref.edit().putString("mobile_number_common", mobile_number_common).commit();
    }
    public String getmobile_number_common() {
        String mobile_number_common = pref.getString("mobile_number_common", "");
        return mobile_number_common;
    }
    public void setparent_name_common(String parent_name_common) {
        pref.edit().putString("parent_name_common", parent_name_common).commit();
    }
    public String getparent_name_common() {
        String parent_name_common = pref.getString("parent_name_common", "");
        return parent_name_common;
    }


    public void setselectedSymtomId(String selectedSymtomId) {
        pref.edit().putString("selectedSymtomId", selectedSymtomId).commit();
    }
    public String getselectedSymtomId() {
        String selectedSymtomId = pref.getString("selectedSymtomId", "");
        return selectedSymtomId;
    }


    public boolean isPermissionsEnabled() {
        int result1 = ContextCompat.checkSelfPermission(context, ACCESS_FINE_LOCATION);
        int result2 = ContextCompat.checkSelfPermission(context, WRITE_EXTERNAL_STORAGE);
        int result3 = ContextCompat.checkSelfPermission(context, CAMERA);
        return result1 == PackageManager.PERMISSION_GRANTED && result2 == PackageManager.PERMISSION_GRANTED && result3 == PackageManager.PERMISSION_GRANTED;
    }

    public void permissionsEnableRequest() {
        ActivityCompat.requestPermissions(activity, new String[]{ACCESS_FINE_LOCATION,CAMERA,WRITE_EXTERNAL_STORAGE,READ_EXTERNAL_STORAGE}, Constants.PERMISSION_REQUEST_CODE);
    }

    //    change font for arabic
    public void arabicfont(TextView textview, EditText editText,Button button,String style, String type) {
        Typeface bold = ResourcesCompat.getFont(context, R.font.droidkufibold);
        Typeface regular = ResourcesCompat.getFont(context, R.font.droidkufiregular);
        if (type.equals("textV")) {
            if (style.equals(Constants.BOLD)){
                textview.setTypeface(bold);
            }
            else if (style.equals(Constants.REGULAR)){
                textview.setTypeface(regular);
            }

        }
        else if (type.equals("editV")){
            if (style.equals(Constants.BOLD)){
                editText.setTypeface(bold);
            }
            else if (style.equals(Constants.REGULAR)){
                editText.setTypeface(regular);
            }
        }
        else if (type.equals("btn")){
            if (style.equals(Constants.BOLD)){
                button.setTypeface(bold);
            }
            else if (style.equals(Constants.REGULAR)){
                button.setTypeface(regular);
            }
        }
    }

    public void arabicfontHint(TextInputLayout textview, String type) {
        Typeface typefacebold = ResourcesCompat.getFont(context, R.font.droidkufibold);
        Typeface typeface = ResourcesCompat.getFont(context, R.font.droidkufiregular);
        if (type.equals("bold")) {
            textview.setTypeface(typefacebold);
        }
        else if (type.equals("regular")){
            textview.setTypeface(typeface);
        }
    }
    public void arabicfontRadiobtn(RadioButton textview, String type) {
        Typeface typefacebold = ResourcesCompat.getFont(context, R.font.droidkufibold);
        Typeface typeface = ResourcesCompat.getFont(context, R.font.droidkufiregular);
        if (type.equals("bold")) {
            textview.setTypeface(typefacebold);
        }
        else if (type.equals("regular")){
            textview.setTypeface(typeface);
        }
    }

    public void setAppLocale(String localeCode) {
        Resources resources = activity.getResources();
        DisplayMetrics displayMetrics = resources.getDisplayMetrics();
        Configuration configuration = resources.getConfiguration();
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.JELLY_BEAN_MR1) {
            configuration.setLocale(new Locale(localeCode.toLowerCase()));
        }
        else {
            configuration.locale = new Locale(localeCode.toLowerCase());
        }
        resources.updateConfiguration(configuration, displayMetrics);
        setlang(localeCode);
    }
    public void loginRequiredDialog(final Context context,String from){
        AlertDialog.Builder alertDialog3 = new AlertDialog.Builder(context);
        // LayoutInflater inflater = getLayoutInflater();
        LayoutInflater inflater = (LayoutInflater)context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        View dialoglayout = inflater.inflate(R.layout.popup_loginrequired, null);
//        dialoglayout.setMinimumHeight(100);
//        dialoglayout.setMinimumWidth(100);
        dialoglayout.setBackgroundDrawable(null);
        alertDialog3.setView(R.layout.popup_loginrequired);
        final AlertDialog alertDialog = alertDialog3.create();
        alertDialog.show();
        alertDialog.setCancelable(true);
        alertDialog.setOnCancelListener(new DialogInterface.OnCancelListener() {
            @Override
            public void onCancel(DialogInterface dialog) {

            }
        });
        WindowManager.LayoutParams layoutParams = new WindowManager.LayoutParams();
        layoutParams.copyFrom(alertDialog.getWindow().getAttributes());
//        layoutParams.width = 500;
//        layoutParams.height = 500;
        alertDialog.getWindow().setBackgroundDrawable(null);
        alertDialog.getWindow().setAttributes(layoutParams);
//        ImageView btnClose = (ImageView)alertDialog.findViewById(R.id.btnIvClose);
        Button cancel = (Button) alertDialog.findViewById(R.id.buttonNo);
        Button login = (Button) alertDialog.findViewById(R.id.buttonYes);
        login.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
//                Intent ieventreport = new Intent(context, activity.getClass());
//                context.startActivity(ieventreport);

                Intent ieventreport = new Intent(context,NewSignin.class);
                context.startActivity(ieventreport);
            }
        });
        cancel.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
//                alertDialog.show();
                if (from.equals("service")){
                    alertDialog.dismiss();

                }
                else {
                    Intent ieventreport = new Intent(context, DashboardActivity.class);
                    context.startActivity(ieventreport);
                }


            }
        });

    }

    public void setcustphone(String custphone) {
        pref.edit().putString("custphone", custphone).commit();
    }
    public String getcustphone() {
        String custphone = pref.getString("custphone", "");
        return custphone;
    }
    public void setcustmail(String custmail) {
        pref.edit().putString("custmail", custmail).commit();
    }
    public String getcustmail() {
        String custmail = pref.getString("custmail", "");
        return custmail;
    }

}

