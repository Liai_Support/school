package com.alshumua.school.Std;

import android.app.AlertDialog;
import android.content.Context;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.DialogFragment;

import com.alshumua.school.R;

import static android.content.ContentValues.TAG;

public class Dialog extends DialogFragment {
    Context context;
    @NonNull
    @Override
    public android.app.Dialog onCreateDialog(@Nullable Bundle savedInstanceState) {



        AlertDialog.Builder builder=new AlertDialog.Builder(getActivity());
        LayoutInflater inflater=getActivity().getLayoutInflater();
        View view = inflater.inflate(R.layout.no_network, null);

//        sessionManager=new SessionManager(getActivity(),context,view);
        builder.setView(view);
        TextView tryagain = (TextView) (view.findViewById(R.id.tryagintxt));

        builder.setCancelable(false);
        tryagain.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Log.d(TAG, "onClick:no net loop check/ ");

                if (!isNetwork() == true){
                    Log.d(TAG, "onClick:no net loop check ");

                }
                else {
                }

            }
        });

        return builder.create();
    }

    public boolean isNetwork(){
        boolean connected = false;
        try {
            ConnectivityManager cm = (ConnectivityManager) context.getApplicationContext().getSystemService(Context.CONNECTIVITY_SERVICE);
            NetworkInfo nInfo = cm.getActiveNetworkInfo();
            connected = nInfo != null && nInfo.isAvailable() && nInfo.isConnected();

        }
        catch (Exception e) {
            Log.e("Connectivity Exception", e.getMessage());
        }

        return connected;
    }
}
