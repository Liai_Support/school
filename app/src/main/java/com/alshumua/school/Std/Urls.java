package com.alshumua.school.Std;

public class Urls {
    public static final int FRAGMENT_CODE = 26;
    public static String Baseurl="https://liastaging.com/shumua/";

//    public static String report="http://shada.shumua.edu.sa:8079/api/shumua/report/request";
    public static String report="http://shada.shumua.edu.sa/api/shumua/report/request";


    public  static String Signup=Baseurl+"customer/signup";
    public  static String Signin=Baseurl+"customer/login";
    public  static String Services=Baseurl+"customer/services";
    public  static String ServicesRequest=Baseurl+"customer/getformdet";
    public  static String SendRequest=Baseurl+"customer/sendrequest";
    public  static String Updateprofile=Baseurl+"customer/updateprofile";
    public  static String Viewprofile=Baseurl+"customer/viewprofile";
    public  static String imageuploadForm=Baseurl+"customer/imageupload";
    public  static String requestlist=Baseurl+"customer/requestlist";
    public  static String requestdetails=Baseurl+"customer/requestdetails";
    public  static String existingparentlogin=Baseurl+"customer/existingparentlogin";
    public  static String verifyOtp=Baseurl+"customer/verifyOtp";
    public  static String guestusersendotp=Baseurl+"customer/guestusersendotp";
    public  static String forgotpassword=Baseurl+"customer/forgotpassword";
    public  static String reportrequest=Baseurl+"customer/reportrequest";
    public  static String initialscreens=Baseurl+"customer/initialscreens";
    public  static String faq=Baseurl+"customer/faq";
    public  static String updateusertoken=Baseurl+"customer/updateusertoken";
    public  static String brief=Baseurl+"customer/brief";
    public  static String suport_org=Baseurl+"customer/suport_org";
    public  static String notifications=Baseurl+"customer/notifications";
    public  static String delete_notifications=Baseurl+"customer/delete_notifications";



}
