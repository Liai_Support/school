package com.alshumua.school.Std;

import android.graphics.Typeface;
import android.widget.TextView;

import androidx.core.content.res.ResourcesCompat;

import com.alshumua.school.R;

import org.json.JSONException;

public class Constants {
//    no network tag
    public static final String NONETWORK = "nonetwork";
//    back press time
    public static final int BACKPRESS_TIME = 2000;// 2 ses
    public static final String WHATSAPP_API = "com.lia.whatsappredirecting";
    public static final String WHAPTSAPP_NO = "9677218682";
    public static final String EMAIL_RECIPIENT = "srisnekhaammu@gmail.com";

    //    splash screen
    public static int SPLASHSCREENVISIBLE = 4000; //    4 sec
    //    language screen
    public static final String ENGLISHKEY = "eng";
    public static final String ARABICKEY = "ar";
//   guest and exist
public static final String GUEST ="guest" ;
public static final String EXIST ="exist" ;
public static final String REGULAR ="regular" ;
public static final String BOLD ="bold" ;

//  profile setting screen
public static final String PROFILEUPDATEIMG_EMPTY_URL ="https://liastaging.com/shumua/uploads/" ;

    public static final int PERMISSION_REQUEST_CODE = 200;



}
