package com.alshumua.school.LanguageSpinnerPackage;

public class CountryItem {
    private String mCountryName;

    public CountryItem(String countryName) {
        mCountryName = countryName;

    }
    public String getCountryName() {
        return mCountryName;
    }

}