package com.alshumua.school.Fragment;

import static android.content.ContentValues.TAG;

import android.os.Bundle;

import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.Toast;

import com.alshumua.school.Adapter.BriefAdapter;
import com.alshumua.school.Adapter.FaqAdapter;
import com.alshumua.school.Model.FaqModel;
import com.alshumua.school.Model.InitialScreenModel;
import com.alshumua.school.Model.ServiceRequestModel;
import com.alshumua.school.Std.Constants;
import com.alshumua.school.Std.SessionManager;
import com.alshumua.school.R;
import com.alshumua.school.Std.Urls;
import com.android.volley.AuthFailureError;
import com.android.volley.DefaultRetryPolicy;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.VolleyLog;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.UnsupportedEncodingException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;


public class FAQFragment extends Fragment {
View view;
 ImageView back_img;
 SessionManager sessionManager;
    private static ArrayList<FaqModel> faqArrayList;
    private static RecyclerView.Adapter faqAdapter;
    private static RecyclerView faqRecyclerView;
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
         view= inflater.inflate(R.layout.fragment_f_a_q, container, false);
        back_img = (ImageView) view.findViewById(R.id.back_from_FAQprofile);
        faqRecyclerView = (RecyclerView)view. findViewById(R.id.faq_recyclerview);
        faqRecyclerView.setLayoutManager(new LinearLayoutManager(getContext()));

        sessionManager = new SessionManager(getActivity(),getContext(),view);
        if (sessionManager.getlang().equals(Constants.ARABICKEY)){
            back_img.setRotationY(180);
        }
        back_img.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Fragment fragment = new ProfileFragment();
                getActivity().getSupportFragmentManager().beginTransaction().replace(R.id.fragment_container, fragment).addToBackStack(null).commit();
            }
        });

        sessionManager.progressdialogshow();
        JSONObject signinreqBody = new JSONObject();
        try {
            signinreqBody.put("", "");
            faqJSON(signinreqBody);
        } catch (JSONException e) {
            e.printStackTrace();
        }

        return view;

    }
    private void faqJSON(JSONObject response) {
        RequestQueue requestQueue = Volley.newRequestQueue(getContext());
        final String requestBody = response.toString();
        StringRequest stringRequest = new StringRequest(Request.Method.POST, Urls.faq, new Response.Listener<String>() {
            @Override
            public void onResponse(String response) {
                sessionManager.progressdialogdismiss();
                Log.d("faq_response", ">>" + response);
                try {
                    JSONObject obj = new JSONObject(response);
                    if (obj.getBoolean("error") == false) {
                       faqArrayList = new ArrayList<FaqModel>();
                        JSONArray obj1 = new JSONArray(obj.getString("data"));
                        Log.d(TAG, "obj1 len: "+obj.length());
                        for (int i = 0; i < obj1.length(); i++) {
                            JSONObject jsonObject = obj1.getJSONObject(i);
                            FaqModel datamodel = new FaqModel(
                                    getContext(),
                                    jsonObject.getString("id"),
                                    jsonObject.getString("question"),
                                    jsonObject.getString("answer"));
                            faqArrayList.add(datamodel);
                        }

                        faqAdapter = new FaqAdapter(getContext(),faqArrayList);
                        faqRecyclerView.setAdapter(faqAdapter);
                    } else {

                    }
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                Log.e("VOLLEY", error.toString());
                sessionManager.progressdialogdismiss();
                //  sessionManager.volleyerror(error);
                Toast.makeText(getContext(), error.toString(), Toast.LENGTH_SHORT).show();
            }
        }) {
            @Override
            public Map<String, String> getHeaders() throws AuthFailureError {
                Map<String, String> params = new HashMap<String, String>();
                params.put("lang",sessionManager.getlang());
                return params;
            }

            @Override
            public String getBodyContentType() {
                return "application/json; charset=utf-8";
            }

            @Override
            public byte[] getBody() throws AuthFailureError {
                try {
                    return requestBody == null ? null : requestBody.getBytes("utf-8");
                } catch (UnsupportedEncodingException uee) {
                    VolleyLog.wtf("Unsupported Encoding while trying to get the bytes of %s using %s", requestBody, "utf-8");
                    return null;
                }
            }
        };
        stringRequest.setRetryPolicy(new DefaultRetryPolicy(10000, DefaultRetryPolicy.DEFAULT_MAX_RETRIES, DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
        Log.d("asdsdjkxdk", "" + stringRequest);
        requestQueue.add(stringRequest);
    }
}