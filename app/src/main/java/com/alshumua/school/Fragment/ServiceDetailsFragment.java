package com.alshumua.school.Fragment;

import android.animation.ObjectAnimator;
import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;

import androidx.core.content.ContextCompat;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentTransaction;
import androidx.recyclerview.widget.RecyclerView;

import android.text.Html;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.ViewTreeObserver;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.alshumua.school.Activity.NewSignin;
//import com.alshumua.school.Activity.SupportingActivity;
import com.alshumua.school.Activity.ThreeBtnScreenActivity;
import com.alshumua.school.Model.ServiceRequestModel;
import com.alshumua.school.Std.Constants;
import com.alshumua.school.Std.Dialog;
import com.alshumua.school.Std.SessionManager;
import com.bumptech.glide.Glide;
import com.alshumua.school.Activity.DashboardActivity;
import com.alshumua.school.R;

import java.util.ArrayList;

public class ServiceDetailsFragment extends Fragment {
    //    VAR
    ImageView serviceImg, backtoservicedetailImg;
    TextView servicenameTv, sevicedescrpTv, readmoreTv;
    LinearLayout readmoreImg;
    Button sendrequestBtn;
    View view;
    SessionManager sessionManager;
    ImageView readarrow;

    //    BUNDLE
    Bundle args = new Bundle();
//    Bundle argservice = new Bundle();
    String serviceImgStr, servicenameTvStr, sevicedescrpTvStr,loginRequiredStr,initialStr,symtomsStr;
    int serviceId;

    //    READMORE
    private boolean expandable = false;
    private boolean expand = false;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        view = inflater.inflate(R.layout.newblue, container, false);

//     HOOKS
        serviceImg = (ImageView) view.findViewById(R.id.servicedetail_img);
        backtoservicedetailImg = (ImageView) view.findViewById(R.id.back_from_servicedetail);
        servicenameTv = (TextView) view.findViewById(R.id.servicedetail_name);
        sevicedescrpTv = (TextView) view.findViewById(R.id.servicedetail_descr);
        readmoreTv = (TextView) view.findViewById(R.id.readmoretxt);
        readmoreImg = (LinearLayout) view.findViewById(R.id.readmoreimg);
        sendrequestBtn = (Button) view.findViewById(R.id.sendrequest_btn);
        readarrow = (ImageView) view.findViewById(R.id.readrrow);

        sessionManager = new SessionManager(getActivity(), getContext(), view);
//    nonetwork check
        if (!sessionManager.isNetwork() == true) {
            Dialog dialog = new Dialog();
            dialog.show(getChildFragmentManager(), Constants.NONETWORK);
        }


//GET BUNDLE DATA
        args = getArguments();

        serviceImgStr = args.getString("serviceIcon");
        servicenameTvStr = args.getString("serviceName");
        sevicedescrpTvStr = args.getString("description");
        loginRequiredStr = args.getString("loginRequired");
        initialStr = args.getString("initial");
        symtomsStr = args.getString("symptoms");
        serviceId = args.getInt("serviceid");
        sessionManager.setserviceid(serviceId);

        args.putString("serviceName", servicenameTvStr);
        args.putString("serviceid", String.valueOf(serviceId));
        args.putString("symptoms", symtomsStr);
// SET DATA
        Glide.with(ServiceDetailsFragment.this).load(serviceImgStr).error(R.drawable.ic_physiotherapy).into(serviceImg);
        servicenameTv.setText(servicenameTvStr);

//  some descrp are begins with html tag so this line will help to remove <p>
        sevicedescrpTv.setText(Html.fromHtml(sevicedescrpTvStr));
        serviceImg.setColorFilter(ContextCompat.getColor(getContext(), R.color.nav_blue), android.graphics.PorterDuff.Mode.MULTIPLY);
        serviceImg.getLayoutParams().height = 380;
        serviceImg.getLayoutParams().width = 380;

//READ MORE TEXT
        sevicedescrpTv.getViewTreeObserver().addOnGlobalLayoutListener(new ViewTreeObserver.OnGlobalLayoutListener() {
            @Override
            public void onGlobalLayout() {
                if (sevicedescrpTv.getLineCount() <= 2) {
                    readmoreTv.setVisibility(View.GONE);
                    readmoreImg.setVisibility(View.GONE);
                }

                if (expandable) {
                    expandable = false;

                    if (sevicedescrpTv.getLineCount() > 2) {
                        readmoreImg.setVisibility(View.VISIBLE);
                        ObjectAnimator animation = ObjectAnimator.ofInt(sevicedescrpTv, "maxLines", 2);
                        animation.setDuration(0).start();
                    }

                }
            }
        });
        readmoreImg.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {

                if (!expand) {
                    expand = true;
                    ObjectAnimator animation = ObjectAnimator.ofInt(sevicedescrpTv, "maxLines", 90);
                    animation.setDuration(100).start();
                    readmoreTv.setText(R.string.readless);
                    readarrow.setImageResource(R.drawable.ic_baseline_keyboard_arrow_up_24);
                } else {
                    expand = false;
                    ObjectAnimator animation = ObjectAnimator.ofInt(sevicedescrpTv, "maxLines", 2);
                    animation.setDuration(100).start();
                    readmoreTv.setText(R.string.readmore);
                    readarrow.setImageResource(R.drawable.ic_baseline_keyboard_arrow_down_24);
                }

            }
        });
//SEND REQUEST BTN CLICK
        sendrequestBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                if (initialStr.equals("1") && serviceId==9){
                    Fragment fragment = new InitialSymtomsFragment();
                    fragment.setArguments(args);
                    FragmentTransaction transaction = getFragmentManager().beginTransaction();
                    transaction.replace(R.id.fragment_container, fragment);
                    transaction.addToBackStack(null);
                    transaction.commit();
                }
                else {
                    Fragment fragment = new FormFragment();
                    fragment.setArguments(args);
                    FragmentTransaction transaction = getFragmentManager().beginTransaction();
                    transaction.replace(R.id.fragment_container, fragment);
                    transaction.addToBackStack(null);
                    transaction.commit();
                }

            }
        });

//        Rotate arrow in arabic
        if (sessionManager.getlang().equals(Constants.ARABICKEY)) {
            backtoservicedetailImg.setRotationY(180);
            sessionManager.arabicfont(null,null, sendrequestBtn,Constants.BOLD,"btn");
            sessionManager.arabicfont(servicenameTv,null, null,Constants.BOLD,"textV");
            sessionManager.arabicfont(sevicedescrpTv,null, null,Constants.REGULAR,"textV");
            sessionManager.arabicfont(readmoreTv,null, null,Constants.REGULAR,"textV");

        }
//   Go back img btn
        backtoservicedetailImg.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent i = new Intent(getActivity(), DashboardActivity.class);
                i.putExtra("mypro", "mypro");
                startActivity(i);
                ((Activity) getActivity()).overridePendingTransition(0, 0);
                ((Activity) getActivity()).finish();
            }
        });
        return view;
    }


}