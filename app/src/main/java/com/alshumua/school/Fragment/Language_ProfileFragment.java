package com.alshumua.school.Fragment;

import android.app.Activity;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.res.Configuration;
import android.content.res.Resources;
import android.os.Build;
import android.os.Bundle;

import androidx.activity.OnBackPressedCallback;
import androidx.activity.OnBackPressedDispatcher;
import androidx.cardview.widget.CardView;
import androidx.fragment.app.Fragment;
import androidx.lifecycle.LifecycleOwner;

import android.util.DisplayMetrics;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;

import com.alshumua.school.Std.Constants;
import com.alshumua.school.Std.Dialog;
import com.alshumua.school.Std.SessionManager;
import com.alshumua.school.Activity.DashboardActivity;
import com.alshumua.school.R;
import com.alshumua.school.databinding.FragmentLanguageProfileBinding;
import com.alshumua.school.databinding.FragmentProfileSettingBinding;

import java.util.Locale;

import static android.content.Context.MODE_PRIVATE;

public class Language_ProfileFragment extends Fragment {
    View view;
    SessionManager sessionManager;
    String langcode = "";
FragmentLanguageProfileBinding binding;
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {

        View layout = LayoutInflater.from(requireContext()).inflate(R.layout.fragment_language__profile, container, false);
        binding = FragmentLanguageProfileBinding.bind(layout);
        sessionManager = new SessionManager(getActivity(), getContext(), view);

        if (sessionManager.getlang().equals(Constants.ARABICKEY) ) {
            binding.arabicbtn.setVisibility(View.VISIBLE);
            binding.englishbtn.setVisibility(View.GONE);
            binding.backFromProfileLanguage.setRotationY(180);
            sessionManager.arabicfont(binding.lantool,null,null,Constants.BOLD,"textV");



        }
        else {
            binding.englishbtn.setVisibility(View.VISIBLE);
            binding.arabicbtn.setVisibility(View.GONE);
        }

        binding.backFromProfileLanguage.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Fragment fragment = new ProfileFragment();
                getActivity().getSupportFragmentManager().beginTransaction().
                        replace(R.id.fragment_container, fragment).addToBackStack(null).commit();
            }
        });

        binding.arabicCard.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (!sessionManager.isNetwork() == true) {
                    Dialog dialog = new Dialog();
                    dialog.show(getChildFragmentManager(), Constants.NONETWORK);
                } else {
                    langcode = Constants.ARABICKEY;
                    sessionManager.setlang(Constants.ARABICKEY);
                    setAppLocale(langcode);
                    binding.arabicbtn.setVisibility(View.VISIBLE);
                    binding.englishbtn.setVisibility(View.GONE);
                    binding.arabicCard.setEnabled(false);

                }

            }
        });

        binding.englishCard.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                if (!sessionManager.isNetwork() == true) {
                    Dialog dialog = new Dialog();
                    dialog.show(getChildFragmentManager(), Constants.NONETWORK);
                } else {
                    langcode = Constants.ENGLISHKEY;
                    sessionManager.setlang(Constants.ENGLISHKEY);
                    setAppLocale(langcode);
                    binding.englishbtn.setVisibility(View.VISIBLE);
                    binding.arabicbtn.setVisibility(View.GONE);
                    binding.englishCard.setEnabled(false);

                }

            }
        });

        OnBackPressedDispatcher onBackPressedDispatcher = getActivity().getOnBackPressedDispatcher();
        if (onBackPressedDispatcher != null) {
            onBackPressedDispatcher.addCallback((LifecycleOwner)this, (OnBackPressedCallback)(new OnBackPressedCallback(true) {
                public void handleOnBackPressed() {
                    this.setEnabled(false);
                    Fragment fragment = new ProfileFragment();
                    getActivity().getSupportFragmentManager().beginTransaction().
                            replace(R.id.fragment_container, fragment).addToBackStack(null).commit();

                }
            }));
        }

        return binding.getRoot();
    }

    public void setAppLocale(String localeCode) {
        Resources res = getResources();
        DisplayMetrics dm = res.getDisplayMetrics();
        Configuration conf = res.getConfiguration();
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.JELLY_BEAN_MR1) {
            conf.setLocale(new Locale(localeCode.toLowerCase()));
        } else {
            conf.locale = new Locale(localeCode.toLowerCase());
        }
        SharedPreferences.Editor editor = getContext().getSharedPreferences("Settings", MODE_PRIVATE).edit();
        editor.putString("My_Lang", localeCode);
        editor.apply();
        res.updateConfiguration(conf, dm);
        Intent i = new Intent(getActivity(), DashboardActivity.class);
        i.putExtra("langfrom","profile");
        startActivity(i);
        ((Activity) getActivity()).overridePendingTransition(0, 0);
        ((Activity) getActivity()).finish();
    }
}