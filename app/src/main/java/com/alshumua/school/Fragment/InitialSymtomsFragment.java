package com.alshumua.school.Fragment;
import static android.content.ContentValues.TAG;


import android.app.Activity;
import android.content.Intent;
import android.graphics.Color;
import android.graphics.Typeface;
import android.os.Bundle;

import androidx.core.content.res.ResourcesCompat;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentTransaction;
import androidx.recyclerview.widget.GridLayoutManager;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.CompoundButton;
import android.widget.GridView;
import android.widget.HorizontalScrollView;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.alshumua.school.Activity.DashboardActivity;
import com.alshumua.school.Activity.RequestSentActivity;
import com.alshumua.school.Adapter.MyRequestAdapter;
import com.alshumua.school.Adapter.ServiceAdapter;
import com.alshumua.school.Adapter.ServiceRequestAdapter;
import com.alshumua.school.Adapter.SymtomsAdapter;
import com.alshumua.school.Fragment.FormFragment;
import com.alshumua.school.Model.MyRequestListModel;
import com.alshumua.school.Model.ServiceModel;
import com.alshumua.school.Model.ServiceRequestModel;
import com.alshumua.school.Model.SymtomsModel;
import com.alshumua.school.R;
import com.alshumua.school.Std.Constants;
import com.alshumua.school.Std.SessionManager;
import com.alshumua.school.Std.Urls;
import com.android.volley.AuthFailureError;
import com.android.volley.DefaultRetryPolicy;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.VolleyLog;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.UnsupportedEncodingException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

public class InitialSymtomsFragment extends Fragment {
    View view;
    //    BUNDLE
    Bundle args = new Bundle();
    //    Bundle arginitial = new Bundle();
    public String servicenameTvStr,loginRequiredStr,initialStr,symtomsStr,serviceidStr;
    TextView toolbartxt;
    Button nextBtn;
    ImageView backBtn;
    LinearLayout linearLayout;
    public ArrayList<String> selectedStrings = new ArrayList<String>();
    public ArrayList<String> selectedSymId = new ArrayList<String>();


//    private static ArrayList<SymtomsModel> requestedFormListModelArrayList;
//    private static RecyclerView.Adapter requestAdapter;
//    private static RecyclerView requestRecyclerView;
//    private RecyclerView recyclerView;



    SessionManager sessionManager;
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        view = inflater.inflate(R.layout.fragment_initial_symtoms, container, false);
        toolbartxt = (TextView) view.findViewById(R.id.servicenameformI);
        nextBtn = (Button) view.findViewById(R.id.nextInitial);
        backBtn = (ImageView) view.findViewById(R.id.back_from_symtoms);
        linearLayout = (LinearLayout) view.findViewById(R.id.formlinear);
        sessionManager = new SessionManager(getActivity(),getContext(),view);
//        recyclerView = view.findViewById(R.id.checkrecyc);
//        recyclerView.setHasFixedSize(true);
//        GridLayoutManager gridLayoutManager = new GridLayoutManager(view.getContext(),2);
//        gridLayoutManager.setOrientation(LinearLayoutManager.HORIZONTAL);
//        recyclerView.setLayoutManager(new LinearLayoutManager(getContext()));
        args = getArguments();
        serviceidStr = args.getString("serviceid");
        symtomsStr = args.getString("symptoms");

        Log.d(TAG, "onCreateView: "+ servicenameTvStr);
        Log.d(TAG, "onCreateView:symtomsStr "+ symtomsStr);
//        serviceReqBody();
//        recyclerView.setAdapter(new SymtomsAdapter(InitialSymtomsFragment.this,getContext(),requestedFormListModelArrayList,symtomsStr));
//------------------------------------------------------
//        requestRecyclerView = (RecyclerView) view.findViewById(R.id.symRecyclerView);
//        requestRecyclerView.setLayoutManager(new LinearLayoutManager(getContext()));
//GET BUNDLE DATA
        args = getArguments();
        servicenameTvStr = args.getString("serviceName");
        loginRequiredStr = args.getString("loginRequired");
        initialStr = args.getString("initial");
        symtomsStr = args.getString("symptoms");
//        requestedFormListModelArrayList = new ArrayList<SymtomsModel>();
//        requestedFormListModelArrayList.add(datamodel);

        toolbartxt.setText(servicenameTvStr);
//        serviceReqBody();
        String input = symtomsStr;
        JSONArray jsonArray = null;
        try {
            jsonArray = new JSONArray(input);
        } catch (JSONException e) {
            e.printStackTrace();
        }

        ArrayList<String> allitems = new ArrayList<String>();
        for (int i = 0; i < jsonArray.length(); i++) {
            LinearLayout checkBoxLayout = new LinearLayout(getContext());
            checkBoxLayout.setOrientation(LinearLayout.HORIZONTAL);

            CheckBox checkBox = new CheckBox(getContext());
            checkBox.setBackgroundResource(R.drawable.custom_checkbox_bg);
            try {

                checkBox.setText("   " + jsonArray.getJSONObject(i).getString("symName") + "   ");
//                checkBox.setText(jsonArray.getJSONObject(i).getString("symName"));
                checkBox.setId(Integer.parseInt(jsonArray.getJSONObject(i).getString("symId")));
                allitems.add(jsonArray.getJSONObject(i).getString("symName"));

                for (int i1 = 0; i1 < selectedStrings.size(); i1++) {
                    if (checkBox.getText().toString().equalsIgnoreCase(selectedStrings.get(i1))) {
                        checkBox.setChecked(true);
                    }
                }
            } catch (JSONException e) {
                e.printStackTrace();
            }
            checkBox.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
                @Override
                public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                    if (isChecked) {
                        checkBox.setTextColor(Color.parseColor("#FFFFFF"));
                    }
                    if (!isChecked) {
                        checkBox.setTextColor(Color.parseColor("#515151"));
                    }
                    int value = ischecked_checkbox(selectedStrings, checkBox.getText().toString());
                    int value2 = ischecked_checkbox(selectedSymId, String.valueOf(checkBox.getId()));
                    if (value != -1) {
                        selectedStrings.remove(value);

                    }
                    else {
                        selectedStrings.add(checkBox.getText().toString());
                    }
//----------------------------------

                    if (value2 != -1) {
                        selectedSymId.remove(value2);

                    }
                    else {
                        selectedSymId.add(String.valueOf(checkBox.getId()));
                    }

                    if (selectedStrings.size() == 0) {
//                        nothing selected
//
                    }
                    else {
//                        selectedStrings.toString()
                        Log.d("TAG", "selectedStrings: "+ selectedStrings.toString());
                        Log.d("TAG", "selectedSymId:1 "+ selectedSymId.toString());
//                        args.putString("symptom_ids", selectedSymId.toString());
                        sessionManager.setselectedSymtomId(selectedSymId.toString());
                        Log.d("TAG", "selectsession "+ selectedSymId.toString());
                    }
                }
            });

            checkBox.setTextColor(Color.parseColor("#515151"));
            LinearLayout.LayoutParams params = new LinearLayout.LayoutParams(LinearLayout.LayoutParams.WRAP_CONTENT, LinearLayout.LayoutParams.WRAP_CONTENT);
//            params.setMargins(50, 50, 0, 0);
            checkBox.setLayoutParams(params);
            checkBox.setTextAppearance(getContext(), R.style.poppin_medium);
            checkBox.setButtonDrawable(R.drawable.nullselector);
            Typeface typeface = ResourcesCompat.getFont(getContext(), R.font.poppins_medium);
            Typeface typeface_ar = ResourcesCompat.getFont(getContext(), R.font.droidkufiregular);
//            checkBox.setTypeface(typeface);
            if (sessionManager.getlang().equals(Constants.ARABICKEY)){
                checkBox.setTypeface(typeface_ar);
            }
            else {
                checkBox.setTypeface(typeface);
            }
            checkBox.setTextSize(12);
            checkBox.setTextAlignment(View.TEXT_ALIGNMENT_VIEW_START);

checkBoxLayout.addView(checkBox);
linearLayout.addView(checkBoxLayout);

        }

//        button click
        nextBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (selectedSymId.isEmpty()){
                    sessionManager.snackbarToast(getString(R.string.PleaseSelectSymtoms), nextBtn);
                }
                else {
                    Fragment fragment = new FormFragment();
                    fragment.setArguments(args);
                    FragmentTransaction transaction = getFragmentManager().beginTransaction();
                    transaction.replace(R.id.fragment_container, fragment);
                    transaction.addToBackStack(null);
                    transaction.commit();
                }


            }
        });

        backBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent i = new Intent(getActivity(), DashboardActivity.class);
                i.putExtra("pro","pro");
                startActivity(i);
                ((Activity) getActivity()).overridePendingTransition(0, 0);
                ((Activity) getActivity()).finish();
            }
        });
        return view;
    }

    private int ischecked_checkbox(ArrayList<String> selectedstrings,String text){
        for (int j = 0; j < selectedstrings.size(); j++) {
            if (text.equalsIgnoreCase(selectedstrings.get(j))) { return j; } }
        return -1; }
//    public void serviceReqBody() {
//        JSONObject requestbody = new JSONObject();
//        try {
//            requestbody.put("id", sessionManager.getid());
//            ServicerequestJSON(requestbody);
//
//            sessionManager.progressdialogshow();
//        } catch (JSONException e) {
//            e.printStackTrace();
//        }
//    }
//    private void ServicerequestJSON(JSONObject response) {
//        RequestQueue requestQueue = Volley.newRequestQueue(getContext());
//        final String requestBody = response.toString();
//        Log.d("Servicerequest", String.valueOf(response));
//        StringRequest stringRequest = new StringRequest(Request.Method.POST, Urls.Services, new Response.Listener<String>() {
//            @Override
//            public void onResponse(String response) {
//                sessionManager.progressdialogdismiss();
//                Log.d("Service_response", ">>" + response);
//                try {
//                    JSONObject obj = new JSONObject(response);
//                    if (obj.getBoolean("error") == false) {
//                        requestedFormListModelArrayList = new ArrayList<SymtomsModel>();
//                        JSONArray obj1 = new JSONArray(obj.getString("data"));
//                        for (int i = 0; i < obj1.length(); i++) {
//                            JSONObject jsonObject = obj1.getJSONObject(i);
//                            SymtomsModel datamodel = new SymtomsModel(getContext(),
//                                    jsonObject.getInt("id"),
//                                    jsonObject.getString("serviceName"),
//                                    jsonObject.getString("description"),
//                                    jsonObject.getString("serviceIcon"),
//                                    jsonObject.getString("loginRequired"),
//                                    jsonObject.getString("initial"),
//                                    jsonObject.getString("symptoms"));
//                            requestedFormListModelArrayList.add(datamodel);
//                        }
//                        requestAdapter = new SymtomsAdapter(InitialSymtomsFragment.this,getContext(),requestedFormListModelArrayList,symtomsStr);
//                        requestRecyclerView.setAdapter(requestAdapter);
//                    } else {
//                    }
//                } catch (Exception e) {
//                    e.printStackTrace();
//                }
//            }
//        }, new Response.ErrorListener() {
//            @Override
//            public void onErrorResponse(VolleyError error) {
//                sessionManager.progressdialogdismiss();
//                Toast.makeText(getContext(), error.getMessage(), Toast.LENGTH_SHORT).show();
//            }
//        }) {
//            @Override
//            public Map<String, String> getHeaders() throws AuthFailureError {
//                Map<String, String> params = new HashMap<String, String>();
//                params.put("lang",sessionManager.getlang());
//                return params;
//            }
//
//            @Override
//            public String getBodyContentType() {
//                return "application/json; charset=utf-8";
//            }
//
//            @Override
//            public byte[] getBody() throws AuthFailureError {
//                try {
//                    return requestBody == null ? null : requestBody.getBytes("utf-8");
//                } catch (UnsupportedEncodingException uee) {
//                    VolleyLog.wtf("Unsupported Encoding while trying to get the bytes of %s using %s", requestBody, "utf-8");
//                    return null;
//                }
//            }
//        };
//        stringRequest.setRetryPolicy(new DefaultRetryPolicy(10000, DefaultRetryPolicy.DEFAULT_MAX_RETRIES, DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
//        Log.d("asdsdjkxdk", "" + stringRequest);
//        requestQueue.add(stringRequest);
//    }


}