package com.alshumua.school.Fragment;

import static android.content.ContentValues.TAG;

import android.content.Intent;
import android.os.Bundle;

import androidx.activity.OnBackPressedCallback;
import androidx.activity.OnBackPressedDispatcher;
import androidx.fragment.app.Fragment;
import androidx.lifecycle.LifecycleOwner;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

//import com.alshumua.school.Activity.SupportingActivity;
import com.alshumua.school.Activity.ThreeBtnScreenActivity;
import com.alshumua.school.Adapter.FaqAdapter;
import com.alshumua.school.Adapter.SupportingAdapter;
import com.alshumua.school.Model.FaqModel;
import com.alshumua.school.Model.InitialScreenModel;
import com.alshumua.school.Model.SupportingorgModel;
import com.alshumua.school.R;
import com.alshumua.school.Std.Constants;
import com.alshumua.school.Std.SessionManager;
import com.alshumua.school.Std.Urls;
import com.android.volley.AuthFailureError;
import com.android.volley.DefaultRetryPolicy;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.VolleyLog;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;
import com.google.android.material.bottomnavigation.BottomNavigationView;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.UnsupportedEncodingException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;


public class SupportingFragment extends Fragment {
View view;
    ImageView back;
    SessionManager sessionManager;
    Button goback;

    private static ArrayList<SupportingorgModel> supportingorgModelslArrayList;
    private static RecyclerView.Adapter supportingAdapter;
    private static RecyclerView supportingRecyclerView;
    TextView toolbartxt;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        view=inflater.inflate(R.layout.activity_supporting, container, false);

        BottomNavigationView navBar = getActivity().findViewById(R.id.bottom_navigation);
        navBar.setVisibility(View.VISIBLE);
        back = (ImageView)view. findViewById(R.id.back_from_support);

        goback = (Button)view. findViewById(R.id.gobacksupport);
        toolbartxt = (TextView)view. findViewById(R.id.toolbartxt);
        sessionManager = new SessionManager(getActivity(),getContext(),view);

        supportingRecyclerView = (RecyclerView) view.findViewById(R.id.supportingrecyclerview);
        supportingRecyclerView.setLayoutManager(new LinearLayoutManager(getContext()));

        if (sessionManager.getlang().equals(Constants.ARABICKEY)){
            back.setRotationY(180);
            sessionManager.arabicfont(toolbartxt,null,null,Constants.BOLD,"textV");
        }
        back.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Fragment fragment = new ThreeBtnFragment();
                getActivity().getSupportFragmentManager().beginTransaction().replace(R.id.fragment_container, fragment).addToBackStack(null).commit();
            }
        });


        sessionManager.progressdialogshow();
        JSONObject signinreqBody = new JSONObject();
        try {
            signinreqBody.put("", "");

            supportingJSON(signinreqBody);
        } catch (
                JSONException e) {
            e.printStackTrace();
        }

        OnBackPressedDispatcher onBackPressedDispatcher = getActivity().getOnBackPressedDispatcher();
        if (onBackPressedDispatcher != null) {
            onBackPressedDispatcher.addCallback((LifecycleOwner)this, (OnBackPressedCallback)(new OnBackPressedCallback(true) {
                public void handleOnBackPressed() {
                    this.setEnabled(true);
                    Fragment fragment = new ThreeBtnFragment();
                    getActivity().getSupportFragmentManager().beginTransaction().replace(R.id.fragment_container, fragment).addToBackStack(null).commit();
                }
            }));
        }


        return view;
    }
    private void supportingJSON(JSONObject response) {
        RequestQueue requestQueue = Volley.newRequestQueue(getContext());
        final String requestBody = response.toString();
        StringRequest stringRequest = new StringRequest(Request.Method.POST, Urls.suport_org, new Response.Listener<String>() {
            @Override
            public void onResponse(String response) {
                sessionManager.progressdialogdismiss();
                Log.d("suport_org_response", ">>" + response);
                try {
                    JSONObject obj = new JSONObject(response);
                    if (obj.getBoolean("error") == false) {
                        supportingorgModelslArrayList = new ArrayList<SupportingorgModel>();
                        JSONArray obj1 = new JSONArray(obj.getString("data"));
                        Log.d(TAG, "obj1 len: "+obj.length());
                        for (int i = 0; i < obj1.length(); i++) {
                            JSONObject jsonObject = obj1.getJSONObject(i);
                            SupportingorgModel datamodel = new SupportingorgModel(
                                    getContext(),
                                    jsonObject.getString("title"),
                                    jsonObject.getString("icons"));
                            supportingorgModelslArrayList.add(datamodel);
                        }

                        supportingAdapter = new SupportingAdapter(getContext(),supportingorgModelslArrayList);
                        supportingRecyclerView.setAdapter(supportingAdapter);
                    } else {

                    }
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                Log.e("VOLLEY", error.toString());
                sessionManager.progressdialogdismiss();
                //  sessionManager.volleyerror(error);
                Toast.makeText(getContext(), error.toString(), Toast.LENGTH_SHORT).show();
            }
        }) {
            @Override
            public Map<String, String> getHeaders() throws AuthFailureError {
                Map<String, String> params = new HashMap<String, String>();
                params.put("lang",sessionManager.getlang());
                return params;
            }

            @Override
            public String getBodyContentType() {
                return "application/json; charset=utf-8";
            }

            @Override
            public byte[] getBody() throws AuthFailureError {
                try {
                    return requestBody == null ? null : requestBody.getBytes("utf-8");
                } catch (UnsupportedEncodingException uee) {
                    VolleyLog.wtf("Unsupported Encoding while trying to get the bytes of %s using %s", requestBody, "utf-8");
                    return null;
                }
            }
        };
        stringRequest.setRetryPolicy(new DefaultRetryPolicy(10000, DefaultRetryPolicy.DEFAULT_MAX_RETRIES, DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
        Log.d("asdsdjkxdk", "" + stringRequest);
        requestQueue.add(stringRequest);
    }
}