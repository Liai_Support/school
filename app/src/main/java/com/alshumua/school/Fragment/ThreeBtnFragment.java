package com.alshumua.school.Fragment;

import android.content.Intent;
import android.os.Build;
import android.os.Bundle;

import androidx.activity.OnBackPressedCallback;
import androidx.activity.OnBackPressedDispatcher;
import androidx.annotation.RequiresApi;
import androidx.core.content.ContextCompat;
import androidx.fragment.app.Fragment;
import androidx.lifecycle.LifecycleOwner;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.alshumua.school.Activity.DashboardActivity;
import com.alshumua.school.Activity.LanguageActivity;
import com.alshumua.school.Activity.NewSignin;
//import com.alshumua.school.Activity.SupportingActivity;
import com.alshumua.school.Activity.ThreeBtnScreenActivity;
import com.alshumua.school.R;
import com.alshumua.school.Std.Constants;
import com.alshumua.school.Std.Dialog;
import com.alshumua.school.Std.SessionManager;
import com.google.android.material.bottomnavigation.BottomNavigationView;

public class ThreeBtnFragment extends Fragment {
    View view;
    //    varialbes
    ImageView support,service,brief,back;
    LinearLayout servicelinear,brieflinear,supportlinear;
    SessionManager sessionManager;
    TextView toolbartxt,brieftxt,servicetxt,supporttxt;
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        view=inflater.inflate(R.layout.activity_three_btn_screen, container, false);
        BottomNavigationView navBar = getActivity().findViewById(R.id.bottom_navigation);
navBar.setVisibility(View.GONE);
        //        hooks
        support = (ImageView) view.findViewById(R.id.supporting);
        service = (ImageView) view.findViewById(R.id.service);
        brief = (ImageView) view.findViewById(R.id.brief);
        back = (ImageView) view.findViewById(R.id.back_from_threebtn);
        servicelinear = (LinearLayout)view. findViewById(R.id.servicelinear);
        supportlinear = (LinearLayout) view.findViewById(R.id.supportinglinear);
        brieflinear = (LinearLayout) view.findViewById(R.id.brieflinear);
        sessionManager = new SessionManager(getActivity(),getContext(),view);

        brieftxt = (TextView) view.findViewById(R.id.brieftxt);
        supporttxt = (TextView) view.findViewById(R.id.supporttxt);
        servicetxt = (TextView) view.findViewById(R.id.servicetxt);
        toolbartxt = (TextView) view.findViewById(R.id.toolbartxt);

//  ROTATE ARROWS ON ARABIC
        if (sessionManager.getlang().equals(Constants.ARABICKEY)){
            back.setRotationY(180);
            sessionManager.arabicfont(brieftxt,null,null,Constants.REGULAR,"textV");
            sessionManager.arabicfont(supporttxt,null,null,Constants.REGULAR,"textV");
            sessionManager.arabicfont(servicetxt,null,null,Constants.REGULAR,"textV");
            sessionManager.arabicfont(servicetxt,null,null,Constants.REGULAR,"textV");
            sessionManager.arabicfont(toolbartxt,null,null,Constants.BOLD,"textV");

            final int sdk = android.os.Build.VERSION.SDK_INT;
            if(sdk < android.os.Build.VERSION_CODES.JELLY_BEAN) {
                support.setBackgroundDrawable(ContextCompat.getDrawable(getContext(), R.drawable.onsidecorner_left) );
                brief.setBackgroundDrawable(ContextCompat.getDrawable(getContext(), R.drawable.onsidecorner_left) );
                service.setBackgroundDrawable(ContextCompat.getDrawable(getContext(), R.drawable.onsidecorner_left) );
            } else {
                support.setBackgroundDrawable(ContextCompat.getDrawable(getContext(), R.drawable.onsidecorner_left) );
                brief.setBackgroundDrawable(ContextCompat.getDrawable(getContext(), R.drawable.onsidecorner_left) );
                service.setBackgroundDrawable(ContextCompat.getDrawable(getContext(), R.drawable.onsidecorner_left) );
            }
        }

//        no network fragment
        if (!sessionManager.isNetwork() == true) {
            Dialog dialog=new Dialog();
            dialog.show(getChildFragmentManager(), Constants.NONETWORK); }

//        clicks
        supportlinear.setOnClickListener(new View.OnClickListener() {
            @RequiresApi(api = Build.VERSION_CODES.LOLLIPOP)
            @Override
            public void onClick(View v) {
                supportlinear.setBackgroundTintList(ContextCompat.getColorStateList(getContext(), R.color.white));
                Fragment fragment = new SupportingFragment();
                getActivity().getSupportFragmentManager().beginTransaction().replace(R.id.fragment_container, fragment).addToBackStack(null).commit();
            }
        });

        brieflinear.setOnClickListener(new View.OnClickListener() {
            @RequiresApi(api = Build.VERSION_CODES.LOLLIPOP)
            @Override
            public void onClick(View v) {
                brieflinear.setBackgroundTintList(ContextCompat.getColorStateList(getContext(), R.color.white));
                Fragment fragment = new BriefFragment();
//                Fragment fragment = new briefNewFragment();
                getActivity().getSupportFragmentManager().beginTransaction().replace(R.id.fragment_container, fragment).addToBackStack(null).commit();
            }
        });

        servicelinear.setOnClickListener(new View.OnClickListener() {
            @RequiresApi(api = Build.VERSION_CODES.LOLLIPOP)
            @Override
            public void onClick(View v) {
                servicelinear.setBackgroundTintList(ContextCompat.getColorStateList(getContext(), R.color.white));
                Fragment fragment = new HomeFragment();
                getActivity().getSupportFragmentManager().beginTransaction().replace(R.id.fragment_container, fragment).addToBackStack(null).commit();


            }
        });

        back.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(getContext(), LanguageActivity.class);
                startActivity(intent);
//                finish();
            }
        });
        OnBackPressedDispatcher onBackPressedDispatcher = getActivity().getOnBackPressedDispatcher();
        if (onBackPressedDispatcher != null) {
            onBackPressedDispatcher.addCallback((LifecycleOwner)this, (OnBackPressedCallback)(new OnBackPressedCallback(true) {
                public void handleOnBackPressed() {
                    this.setEnabled(true);
                   /* if (doubleBackToExitPressedOnce) {
                        Intent intent = new Intent(Intent.ACTION_MAIN);
                        intent.addCategory(Intent.CATEGORY_HOME);
                        startActivity(intent);
                        return;
                    }
                    doubleBackToExitPressedOnce = true;
                    Toast.makeText(getActivity(), getString(R.string.PressBackAgaintoExit), Toast.LENGTH_SHORT).show();
                    new Handler().postDelayed(() -> doubleBackToExitPressedOnce=false, 2000);*/
                    Intent ieventreport = new Intent(getContext(), LanguageActivity.class);
                    getContext().startActivity(ieventreport);
                }
            }));
        }
        return view;
    }

}