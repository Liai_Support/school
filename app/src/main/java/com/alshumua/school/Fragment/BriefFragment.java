package com.alshumua.school.Fragment;

import android.annotation.SuppressLint;
import android.content.Intent;
import android.content.IntentSender;
import android.content.pm.PackageManager;
import android.graphics.Color;
import android.location.Location;
import android.media.MediaPlayer;
import android.net.Uri;
import android.os.Bundle;

import androidx.activity.OnBackPressedCallback;
import androidx.activity.OnBackPressedDispatcher;
import androidx.annotation.NonNull;
import androidx.cardview.widget.CardView;
import androidx.fragment.app.Fragment;
import androidx.lifecycle.LifecycleOwner;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.text.Html;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.MediaController;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;
import android.widget.VideoView;

import com.alshumua.school.Activity.ThreeBtnScreenActivity;
//import com.alshumua.school.Adapter.BriefAdapter;
import com.alshumua.school.Adapter.BriefAdapter;
import com.alshumua.school.Model.BriefModel;
import com.alshumua.school.Model.InitialScreenModel;
import com.alshumua.school.R;
import com.alshumua.school.Std.Constants;
import com.alshumua.school.Std.SessionManager;
import com.alshumua.school.Std.Urls;
import com.android.volley.AuthFailureError;
import com.android.volley.DefaultRetryPolicy;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.VolleyLog;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;
import com.bumptech.glide.Glide;
import com.google.android.gms.common.api.ResolvableApiException;
import com.google.android.gms.location.FusedLocationProviderClient;
import com.google.android.gms.location.LocationCallback;
import com.google.android.gms.location.LocationRequest;
import com.google.android.gms.location.LocationServices;
import com.google.android.gms.location.LocationSettingsRequest;
import com.google.android.gms.location.LocationSettingsResponse;
import com.google.android.gms.location.SettingsClient;
import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.SupportMapFragment;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.MarkerOptions;
import com.google.android.gms.tasks.OnFailureListener;
import com.google.android.gms.tasks.OnSuccessListener;
import com.google.android.gms.tasks.Task;
import com.google.android.material.bottomnavigation.BottomNavigationView;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.UnsupportedEncodingException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class BriefFragment extends Fragment implements OnMapReadyCallback {
    ImageView back,pause;
    SessionManager sessionManager;
    View view;
    Button goback;
    RadioButton rb;
    TextView homeContent,hometitle,addresstxt;
    ImageView homeicon;
    LinearLayout homeLL,videoLL,addressLL;
    VideoView videoView;
    String videourl;
    FrameLayout video_frame;
    Double latStr,langStr;
    Fragment map;
    /*checkk*/
    private static ArrayList<BriefModel> briefArrayList;
    private static RecyclerView.Adapter briefAdapter;
    private static RecyclerView briefRecyclerView;

    View mapView;
    private GoogleMap mMap;
    FusedLocationProviderClient mFusedLocationProviderClient;
    private Location mLastKnownLocation;
    private LocationCallback locationCallback;
    private final float DEFAULT_ZOOM = 15;
ImageView whatsapp,mail;
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        view=inflater.inflate(R.layout.activity_brief_main, container, false);
        BottomNavigationView navBar = getActivity().findViewById(R.id.bottom_navigation);
        navBar.setVisibility(View.VISIBLE);


        whatsapp = (ImageView) view.findViewById(R.id.whatsapp);
        mail = (ImageView) view.findViewById(R.id.mail);

        back = (ImageView) view.findViewById(R.id.back_from_brief);
        videoView = (VideoView) view.findViewById(R.id.videoview);

        homeicon = (ImageView) view.findViewById(R.id.homeicon);
        pause = (ImageView) view.findViewById(R.id.pause);
        homeContent = (TextView) view.findViewById(R.id.homecontent);
        hometitle = (TextView) view.findViewById(R.id.hometitle);
        addresstxt = (TextView) view.findViewById(R.id.addresstxt);
        homeLL = (LinearLayout) view.findViewById(R.id.homelinear);
        videoLL = (LinearLayout) view.findViewById(R.id.videolinear);
        addressLL = (LinearLayout) view.findViewById(R.id.addresslinear);
        video_frame = (FrameLayout) view.findViewById(R.id.video_frame);

        sessionManager = new SessionManager(getActivity(),getContext(),view);
//        briefRecyclerView = (RecyclerView)view. findViewById(R.id.recyclerbrief);
//        briefRecyclerView.setLayoutManager(new LinearLayoutManager(getContext()));

        if (sessionManager.getlang().equals(Constants.ARABICKEY)){
            back.setRotationY(180);

        }

        SupportMapFragment mapFragment = (SupportMapFragment)getChildFragmentManager().findFragmentById(R.id.google_map);
        mapFragment.getMapAsync(this);
        mapView = mapFragment.getView();
        mapView.setVisibility(View.GONE);

        back.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Fragment fragment = new ThreeBtnFragment();
                getActivity().getSupportFragmentManager().beginTransaction().replace(R.id.fragment_container, fragment).addToBackStack(null).commit();
            }
        });
        homeLL.setVisibility(View.VISIBLE);
        videoLL.setVisibility(View.GONE);
        addressLL.setVisibility(View.GONE);
        briefBody();

        RadioGroup radioGroup= (RadioGroup)view.findViewById(R.id.radiogroup);
        RadioButton r0= (RadioButton)view.findViewById(R.id.radiobrief);
        RadioButton r1= (RadioButton)view.findViewById(R.id.radiovid);
        RadioButton r2= (RadioButton)view.findViewById(R.id.radioadd);
        RadioButton r3= (RadioButton)view.findViewById(R.id.radiolocation);
        radioGroup.setOnCheckedChangeListener(new RadioGroup.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(RadioGroup group, int checkedId) {
                rb = (RadioButton)view.findViewById(checkedId);
                if (sessionManager.getlang().equals(Constants.ARABICKEY)){
                    back.setRotationY(180);
                    sessionManager.arabicfontRadiobtn(rb,Constants.REGULAR);

                }
                if (rb.getText().equals("Location")||rb.getText().equals("موقع")){
                    r3.setTextColor(Color.WHITE);
                    r0.setTextColor(Color.BLACK);
                    r1.setTextColor(Color.BLACK);
                    r2.setTextColor(Color.BLACK);
                    homeLL.setVisibility(View.GONE);
                    addressLL.setVisibility(View.GONE);
                    videoLL.setVisibility(View.GONE);
                    mapView.setVisibility(View.VISIBLE);

                }
                else if (rb.getText().equals("Brief")||rb.getText().equals("نبذة")){
                    r0.setTextColor(Color.WHITE);
                    r1.setTextColor(Color.BLACK);
                    r2.setTextColor(Color.BLACK);
                    r3.setTextColor(Color.BLACK);
                    homeLL.setVisibility(View.VISIBLE);
                    addressLL.setVisibility(View.GONE);
                    videoLL.setVisibility(View.GONE);
                    mapView.setVisibility(View.GONE);

                }
                else if (rb.getText().equals("Video")||rb.getText().equals("فيديو")){
                    r1.setTextColor(Color.WHITE);
                    r2.setTextColor(Color.BLACK);
                    r3.setTextColor(Color.BLACK);
                    r0.setTextColor(Color.BLACK);
                    homeLL.setVisibility(View.GONE);
                    addressLL.setVisibility(View.GONE);
                    videoLL.setVisibility(View.VISIBLE);
                    mapView.setVisibility(View.GONE);
                    if (!videourl.equals("")) {
//                        videoView.setVideoPath("https://liastaging.com/shumua/uploads/video/sample.mp4");
                        videoView.setVideoPath(videourl);
                        videoView.start();

                      /*  MediaController mediaController = new MediaController(getContext());
                        mediaController.setAnchorView(videoView);
                        mediaController.setBackgroundColor(Color.TRANSPARENT);

                        videoView.setMediaController(mediaController);*/
                        videoView.setOnPreparedListener(new MediaPlayer.OnPreparedListener() {
                            @Override
                            public void onPrepared(MediaPlayer mp) {
                                mp.setLooping(true);
                            }
                        });
                    }


                }
                else if (rb.getText().equals("Address")||rb.getText().equals("عنوان")){
                    r2.setTextColor(Color.WHITE);
                    r0.setTextColor(Color.BLACK);
                    r1.setTextColor(Color.BLACK);
                    r3.setTextColor(Color.BLACK);
                    homeLL.setVisibility(View.GONE);
                    addressLL.setVisibility(View.VISIBLE);
                    videoLL.setVisibility(View.GONE);
                    mapView.setVisibility(View.GONE);

                }

                Log.d("TAG", "id: "+checkedId);

            }
        });

        whatsapp.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

//                check app installed or not
                boolean installed = appInstalledOrNot("com.alshumua.school");

                if (installed){
                    Intent intent=new Intent(Intent.ACTION_VIEW);
//                    intent.setData(Uri.parse("http://api.whatsapp.com/send?phone=" +"+966"+phn+"&text="+msg));
//                    intent.setData(Uri.parse("http://api.whatsapp.com/send?phone=" +"+966"+phn));
                    intent.setData(Uri.parse("http://api.whatsapp.com/send?phone=" +"+91"+"9677218682"));
                    startActivity(intent);
                }
                else{
                    Toast.makeText(getContext(),R.string.appnotinstalled,Toast.LENGTH_SHORT);
                }
            }
        });
        mail.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                sendmail();
            }
        });

        OnBackPressedDispatcher onBackPressedDispatcher = getActivity().getOnBackPressedDispatcher();
        if (onBackPressedDispatcher != null) {
            onBackPressedDispatcher.addCallback((LifecycleOwner)this, (OnBackPressedCallback)(new OnBackPressedCallback(true) {
                public void handleOnBackPressed() {
                    this.setEnabled(true);
                    Fragment fragment = new ThreeBtnFragment();
                    getActivity().getSupportFragmentManager().beginTransaction().replace(R.id.fragment_container, fragment).addToBackStack(null).commit();
                }
            }));
        }
        video_frame.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                setPause();
            }
        });
        pause.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                setPause();
            }
        });
        return view;

    }
private void briefBody(){
          sessionManager.progressdialogshow();
        JSONObject briefreqBody = new JSONObject();

        try {
            briefreqBody.put("tabvalue", "home");
            briefJSON(briefreqBody);
        } catch (JSONException e) {
            e.printStackTrace();
        }
}
    private void briefJSON(JSONObject response) {
        RequestQueue requestQueue = Volley.newRequestQueue(getContext());
        final String requestBody = response.toString();
        StringRequest stringRequest = new StringRequest(Request.Method.POST, Urls.brief, new Response.Listener<String>() {
            @Override
            public void onResponse(String response) {
                sessionManager.progressdialogdismiss();
                Log.d("brief_response", ">>" + response);
                try {
                    JSONObject obj = new JSONObject(response);
                    if (obj.getBoolean("error") == false) {
                        briefArrayList = new ArrayList<BriefModel>();
                        String data = obj.getString("data");
                        JSONObject maindata = new JSONObject(data);
                        JSONObject location = new JSONObject(maindata.getString("location"));
                        JSONObject home = new JSONObject(maindata.getString("home"));

                        BriefModel datamodel = new BriefModel(
                                getContext(),
                                maindata.getString("address"),
                                maindata.getString("video"),
                                location.getString("latitute"),
                                location.getString("longitute"),
                                home.getString("title"),
                                home.getString("icon"),
                                home.getString("content"),
                                maindata.getString("phone"),
                                maindata.getString("mail")
                                );
                        sessionManager.setcustphone(maindata.getString("phone"));
                        sessionManager.setcustmail(maindata.getString("mail"));
                        Log.d("TAG", "longitute: "+location.getString("longitute"));
                        briefArrayList.add(datamodel);
//                        briefAdapter = new BriefAdapter(getContext(),briefArrayList);
//                        briefRecyclerView.setAdapter(briefAdapter);
//                      homeContent.setText(home.getString("content"));
                        homeContent.setText(Html.fromHtml(home.getString("content")));
                        hometitle.setText(Html.fromHtml( home.getString("title")));
                        addresstxt.setText(Html.fromHtml( maindata.getString("address")));
                        Glide.with(BriefFragment.this).load(home.getString("icon")).error(R.drawable.ic_physiotherapy).into(homeicon);
                        videourl=  maindata.getString("video");
                        latStr=  Double.parseDouble(location.getString("latitute"));
                        langStr=  Double.parseDouble(location.getString("longitute"));

                        Log.d("TAG", "longitute: "+latStr);
                    } else {

                    }
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                Log.e("VOLLEY", error.toString());
                sessionManager.progressdialogdismiss();
                //  sessionManager.volleyerror(error);
                Toast.makeText(getContext(), error.toString(), Toast.LENGTH_SHORT).show();
            }
        }) {
            @Override
            public Map<String, String> getHeaders() throws AuthFailureError {
                Map<String, String> params = new HashMap<String, String>();
                params.put("lang",sessionManager.getlang());
                Log.d("TAG", "getHeaders: "+params);
                return params;
            }

            @Override
            public String getBodyContentType() {
                return "application/json; charset=utf-8";
            }

            @Override
            public byte[] getBody() throws AuthFailureError {
                try {
                    return requestBody == null ? null : requestBody.getBytes("utf-8");
                } catch (UnsupportedEncodingException uee) {
                    VolleyLog.wtf("Unsupported Encoding while trying to get the bytes of %s using %s", requestBody, "utf-8");
                    return null;
                }
            }
        };
        stringRequest.setRetryPolicy(new DefaultRetryPolicy(10000, DefaultRetryPolicy.DEFAULT_MAX_RETRIES, DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
        Log.d("asdsdjkxdk", "" + stringRequest);
        requestQueue.add(stringRequest);
    }

    @SuppressLint("MissingPermission")
    @Override
    public void onMapReady(GoogleMap googleMap) {
        mMap = googleMap;
        mMap.setMyLocationEnabled(false);
        mMap.getUiSettings().setMyLocationButtonEnabled(false);

//
//if (!langStr.equals(null)&&!latStr.equals(null)){
    LatLng userLocation = new LatLng(26.432143272323934,50.028188268624696);
//    LatLng userLocation = new LatLng(latStr,langStr);
    mMap.clear();
    mMap.addMarker(new MarkerOptions().position(userLocation).title("school"));
    mMap.moveCamera(CameraUpdateFactory.newLatLngZoom(userLocation,13));





       /* if (mapView != null && mapView.findViewById(Integer.parseInt("1")) != null) {
            View locationButton = ((View) mapView.findViewById(Integer.parseInt("1")).getParent()).findViewById(Integer.parseInt("2"));
            RelativeLayout.LayoutParams layoutParams = (RelativeLayout.LayoutParams) locationButton.getLayoutParams();
            layoutParams.addRule(RelativeLayout.ALIGN_PARENT_TOP, 0);
            layoutParams.addRule(RelativeLayout.ALIGN_PARENT_BOTTOM, RelativeLayout.TRUE);
            layoutParams.setMargins(0, 0, 40, 180);
        }

        //check if gps is enabled or not and then request user to enable it
        LocationRequest locationRequest = LocationRequest.create();
        locationRequest.setInterval(10000);
        locationRequest.setFastestInterval(5000);
        locationRequest.setPriority(LocationRequest.PRIORITY_HIGH_ACCURACY);

        LocationSettingsRequest.Builder builder = new LocationSettingsRequest.Builder().addLocationRequest(locationRequest);

        SettingsClient settingsClient = LocationServices.getSettingsClient(getContext());
        Task<LocationSettingsResponse> task = settingsClient.checkLocationSettings(builder.build());

        task.addOnSuccessListener(BriefFragment.this, new OnSuccessListener<LocationSettingsResponse>() {
            @Override
            public void onSuccess(LocationSettingsResponse locationSettingsResponse) {
//                getDeviceLocation();
            }
        });

        task.addOnFailureListener(BriefFragment.this, new OnFailureListener() {
            @Override
            public void onFailure(@NonNull Exception e) {
                if (e instanceof ResolvableApiException) {
                    ResolvableApiException resolvable = (ResolvableApiException) e;
                    try {
                        resolvable.startResolutionForResult(, 51);
                    } catch (IntentSender.SendIntentException e1) {
                        e1.printStackTrace();
                    }
                }
            }
        });

        mMap.setOnMyLocationButtonClickListener(new GoogleMap.OnMyLocationButtonClickListener() {
            @Override
            public boolean onMyLocationButtonClick() {
//                if (searchBar.isSuggestionsVisible())
//                    searchBar.clearSuggestions();
//                if (searchBar.isSearchEnabled())
//                    searchBar.disableSearch();
                return false;
            }
        });*/


    }

    private boolean appInstalledOrNot(String packageName) {
        PackageManager pm = requireContext().getPackageManager();
        boolean app_installed;
        try {
            pm.getPackageInfo(packageName, PackageManager.GET_ACTIVITIES);
            app_installed = true;
        }
        catch (PackageManager.NameNotFoundException e) {
            app_installed = false;
        }
        return app_installed;
    }
    private void sendmail() {
        String recipientList="info@shumua.edu.sa";
        String[] recipients=recipientList.split(",");

        Intent intent=new Intent(Intent.ACTION_SEND);
        intent.putExtra(intent.EXTRA_EMAIL,recipients);
        intent.putExtra(intent.EXTRA_SUBJECT,"Queries");
        intent.putExtra(intent.EXTRA_TEXT,"");
        intent.setType("message/rfc822");
        startActivity(Intent.createChooser(intent,"chooseanemailclient"));
    }

    public void setPause() {
                if (videoView.isPlaying()) {
                    videoView.pause();
                    pause.setImageResource(R.drawable.ic_baseline_play_circle_outline_24);
                } else {
                    videoView.start();
                    pause.setImageResource(R.drawable.ic_baseline_pause_circle_outline_24);

                }
            }
}
