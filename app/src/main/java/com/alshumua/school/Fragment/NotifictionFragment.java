package com.alshumua.school.Fragment;

import android.os.Bundle;

import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.alshumua.school.Activity.DashboardActivity;
import com.alshumua.school.Activity.NewSignin;
import com.alshumua.school.Adapter.MyRequestAdapter;
import com.alshumua.school.Adapter.NotificationAdapter;
import com.alshumua.school.Model.MyRequestListModel;
import com.alshumua.school.Model.NotificationModel;
import com.alshumua.school.Std.Constants;
import com.alshumua.school.Std.SessionManager;
import com.alshumua.school.R;
import com.alshumua.school.Std.Urls;
import com.android.volley.AuthFailureError;
import com.android.volley.DefaultRetryPolicy;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.VolleyLog;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.UnsupportedEncodingException;
import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.Map;

public class NotifictionFragment extends Fragment {

View view;
ImageView back;
TextView clear_notification,nodatatxt;
SessionManager sessionManager;
public String selectedid="prr";
    private static ArrayList<NotificationModel> requestedFormListModelArrayList;
    private static RecyclerView.Adapter requestAdapter;
    private static RecyclerView requestRecyclerView;
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        view=inflater.inflate(R.layout.fragment_notifiction, container, false);
        back = (ImageView) view.findViewById(R.id.back_from_notify);
        clear_notification = (TextView) view.findViewById(R.id.clear_notification);
        nodatatxt = (TextView) view.findViewById(R.id.nodatatxt);
        requestRecyclerView = (RecyclerView) view.findViewById(R.id.notification_recyclerview);
        requestRecyclerView.setLayoutManager(new LinearLayoutManager(getContext()));

        sessionManager = new SessionManager(getActivity(),getContext(),view);
        nodatatxt.setVisibility(View.GONE);
        Log.d("TAG", "selectwdid "+selectedid);
        notificationReqBody();
        if (sessionManager.getlang().equals(Constants.ARABICKEY)){
            back.setRotationY(180);
        }
        back.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Fragment fragment = new HomeFragment();
                getActivity().getSupportFragmentManager().beginTransaction().replace(R.id.fragment_container, fragment).addToBackStack(null).commit();
            }
        });

clear_notification.setVisibility(View.GONE);
        clear_notification.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                JSONObject requestbody = new JSONObject();
                try {

                    requestbody.put("custid", sessionManager.getid());
                    requestbody.put("notificationid", "all");
                    clearnotificationJSON(requestbody);
                    sessionManager.progressdialogshow();
                } catch (JSONException e) {
                    e.printStackTrace();
                }

            }
        });
        return view;
    }

    public void notificationReqBody() {
        JSONObject requestbody = new JSONObject();
        try {

            requestbody.put("custid", sessionManager.getid());
            notificationJSON(requestbody);
            sessionManager.progressdialogshow();
        } catch (JSONException e) {
            e.printStackTrace();
        }
    }

    public void deleteSingleReqBody(String id) {
        JSONObject requestbody = new JSONObject();
        try {
//if (!id.equals("")) {
            Log.d("TAG", "deleteSingleReqBody: "+id);
    requestbody.put("custid", sessionManager.getid());
    requestbody.put("notificationid", id);
    clearnotificationJSON(requestbody);
    sessionManager.progressdialogshow();
//}
//}
//else {
//    Toast.makeText(getContext(),"noselectedidcheckk",Toast.LENGTH_SHORT);
//}

        } catch (JSONException e) {
            e.printStackTrace();
        }
    }

    private void notificationJSON(JSONObject response) {
        RequestQueue requestQueue = Volley.newRequestQueue(getContext());
        final String requestBody = response.toString();
        Log.d("requestFormJSON", String.valueOf(response));
        StringRequest stringRequest = new StringRequest(Request.Method.POST, Urls.notifications, new Response.Listener<String>() {
            @Override
            public void onResponse(String response) {
                sessionManager.progressdialogdismiss();
                Log.d("notifications_response", ">>" + response);
                try {
                    JSONObject obj = new JSONObject(response);
                    String msg = obj.getString("message");

                    if (obj.getBoolean("error") == false) {
                        nodatatxt.setVisibility(View.GONE);
                        clear_notification.setVisibility(View.VISIBLE);
                        requestedFormListModelArrayList = new ArrayList<NotificationModel>();
                        JSONArray obj1 = new JSONArray(obj.getString("data"));
                        for (int i = 0; i < obj1.length(); i++) {
                            JSONObject jsonObject = obj1.getJSONObject(i);
                            NotificationModel datamodel = new NotificationModel(
                                    getContext(),
                                    jsonObject.getString("id"),
                                    jsonObject.getString("reqId"),
                                    jsonObject.getString("title"),
                                    jsonObject.getString("message"),
                                    jsonObject.getString("created_at"));
                            requestedFormListModelArrayList.add(datamodel);

                        }
//                        Collections.reverse(requestedFormListModelArrayList);
                        requestAdapter = new NotificationAdapter(NotifictionFragment.this,getContext(),requestedFormListModelArrayList,((DashboardActivity)getActivity()));
                        requestRecyclerView.setAdapter(requestAdapter);
                    } else {
                        clear_notification.setVisibility(View.GONE);
                        nodatatxt.setVisibility(View.VISIBLE);
                        nodatatxt.setText(msg);
//                        Toast.makeText(getContext(), msg, Toast.LENGTH_SHORT).show();
                    }
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                Log.e("VOLLEY", error.toString());
                sessionManager.progressdialogdismiss();
                Toast.makeText(getContext(), error.toString(), Toast.LENGTH_SHORT).show();
                //  sessionManager.volleyerror(error);
            }
        }) {
            @Override
            public Map<String, String> getHeaders() throws AuthFailureError {
                Map<String, String> params = new HashMap<String, String>();
                params.put("Authorization", "Bearer " + sessionManager.gettoken());
                params.put("lang",sessionManager.getlang());
//                    params.put("Authorization", "Bearer "+"eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJpZCI6IjEyIiwidXNlcm5hbWUiOiJTbmVoYSBzZWx2YW0ifQ.Lo7r-ebR4O89j-nYaDlr6W_Rweo4f07CWYCWktspd2o");
                return params;
            }

            @Override
            public String getBodyContentType() {
                return "application/json; charset=utf-8";
            }

            @Override
            public byte[] getBody() throws AuthFailureError {
                try {
                    return requestBody == null ? null : requestBody.getBytes("utf-8");
                } catch (UnsupportedEncodingException uee) {
                    VolleyLog.wtf("Unsupported Encoding while trying to get the bytes of %s using %s", requestBody, "utf-8");
                    return null;
                }
            }
        };
        stringRequest.setRetryPolicy(new DefaultRetryPolicy(10000, DefaultRetryPolicy.DEFAULT_MAX_RETRIES, DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
        Log.d("asdsdjkxdk", "" + stringRequest);
        requestQueue.add(stringRequest);
    }


    private void clearnotificationJSON(JSONObject response) {
        RequestQueue requestQueue = Volley.newRequestQueue(getContext());
        final String requestBody = response.toString();
        Log.d("clearnotiff",String.valueOf(response));
        StringRequest stringRequest = new StringRequest(Request.Method.POST, Urls.delete_notifications, new Response.Listener<String>() {
            @Override
            public void onResponse(String response) {
                sessionManager.progressdialogdismiss();
                Log.d("clearnotiff", ">>" + response);
                try {
                    JSONObject obj = new JSONObject(response);
                    String msg = obj.getString("message");
                    if (obj.getBoolean("error") == false) {
                        JSONObject requestbody = new JSONObject();
                        try {
                            requestbody.put("custid", sessionManager.getid());
                            requestbody.put("notificationid", "all");
                            notificationJSON(requestbody);
                        } catch (JSONException e) {
                            e.printStackTrace();
                        }
                    }
                    else {
//                        sessionManager.snackbarToast(getString(R.string.notificationnotcleared),view);
                        Toast.makeText(getContext(), msg, Toast.LENGTH_SHORT).show();
                    }

                } catch (JSONException e) {
                    e.printStackTrace();
                }
            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                Log.e("VOLLEY", error.toString());
                sessionManager.progressdialogdismiss();
                sessionManager.volleyerror(error);
            }
        }){
            @Override
            public String getBodyContentType() {
                return "application/json; charset=utf-8";
            }

            @Override
            public byte[] getBody() throws AuthFailureError {
                try {
                    return requestBody == null ? null : requestBody.getBytes("utf-8");
                } catch (UnsupportedEncodingException uee) {
                    VolleyLog.wtf("Unsupported Encoding while trying to get the bytes of %s using %s", requestBody, "utf-8");
                    return null;
                }
            }
        };
        stringRequest.setRetryPolicy(new DefaultRetryPolicy(10000, DefaultRetryPolicy.DEFAULT_MAX_RETRIES, DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
        Log.d("asdsdjkxdk",""+stringRequest);
        requestQueue.add(stringRequest);
    }
}