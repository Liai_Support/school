package com.alshumua.school.Fragment;

import android.annotation.SuppressLint;
import android.app.Activity;
import android.content.ComponentName;
import android.content.ContentResolver;
import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.content.pm.ResolveInfo;
import android.database.Cursor;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Color;
import android.net.Uri;
import android.os.Bundle;

import androidx.activity.result.ActivityResult;
import androidx.activity.result.ActivityResultCallback;
import androidx.activity.result.ActivityResultLauncher;
import androidx.activity.result.contract.ActivityResultContracts;
import androidx.core.content.FileProvider;
import androidx.fragment.app.Fragment;

import android.os.Environment;
import android.os.Parcelable;
import android.os.SystemClock;
import android.provider.DocumentsContract;
import android.provider.MediaStore;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.webkit.MimeTypeMap;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.Toast;

import com.alshumua.school.Std.Constants;
import com.alshumua.school.Std.Dialog;
import com.alshumua.school.Std.SessionManager;
import com.alshumua.school.Std.Urls;
import com.alshumua.school.Std.VolleyMultipartRequest;
import com.alshumua.school.databinding.FragmentProfileBinding;
import com.alshumua.school.databinding.FragmentProfileSettingBinding;
import com.android.volley.AuthFailureError;
import com.android.volley.NetworkResponse;
import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.Volley;
import com.alshumua.school.Activity.DashboardActivity;
import com.alshumua.school.R;
import com.bumptech.glide.Glide;
import com.squareup.picasso.Picasso;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.IOException;
import java.io.InputStream;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class ProfileSettingFragment extends Fragment {
//    var
    View view;
    SessionManager sessionManager;
    String gfname,glname,gmobile,gemail,guname,gparent;
    String msg;
    private long mLastClickTime = 0;
    Bitmap bitmap = null;
    VolleyMultipartRequest.DataPart imagedata;
    String currentPhotoPath = null;
    File currentPhotoFile = null;
    Uri currentPhotoPathFileUri = null;
    private final static int IMAGE_RESULT = 200;
FragmentProfileSettingBinding binding;
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View layout = LayoutInflater.from(requireContext()).inflate(R.layout.fragment_profile_setting, container, false);
        binding = FragmentProfileSettingBinding.bind(layout);
//         hooks
        /*Design for profile setting was 2 separate layouts- one for guest other for exixst*/
        sessionManager = new SessionManager(getActivity(), getContext(), view);


//   no network check
        if (!sessionManager.isNetwork() == true) {
            Dialog dialog = new Dialog();
            dialog.show(getChildFragmentManager(), Constants.NONETWORK);
        }
        if (sessionManager.getlang().equals(Constants.ARABICKEY)){
            sessionManager.arabicfont(null,null, binding.existupdatebtn,Constants.BOLD,"btn");
            sessionManager.arabicfont(null,null, binding.guestupdatebtn,Constants.BOLD,"btn");
            sessionManager.arabicfont(binding.profilesettingtool,null, null,Constants.BOLD,"textV");

            sessionManager.arabicfont(null,binding.usernameGuest, null,Constants.REGULAR,"editV");
            sessionManager.arabicfont(null,binding.emailGuest, null,Constants.REGULAR,"editV");
            sessionManager.arabicfont(null,binding.mobileGuest, null,Constants.REGULAR,"editV");
            sessionManager.arabicfont(null,binding.lastnameGuest, null,Constants.REGULAR,"editV");
            sessionManager.arabicfont(null,binding.firstnameGuest, null,Constants.REGULAR,"editV");
            sessionManager.arabicfont(null,binding.parentnameGuest, null,Constants.REGULAR,"editV");
            sessionManager.arabicfont(null,binding.usernameExist, null,Constants.REGULAR,"editV");
            sessionManager.arabicfont(null,binding.mobileExist, null,Constants.REGULAR,"editV");
            sessionManager.arabicfont(null,binding.emailExist, null,Constants.REGULAR,"editV");
            sessionManager.arabicfont(null,binding.stunameExist, null,Constants.REGULAR,"editV");
            sessionManager.arabicfont(null,binding.parentnameExist, null,Constants.REGULAR,"editV");
//for hint
            sessionManager.arabicfontHint(binding.unameguestlayout,Constants.REGULAR);
            sessionManager.arabicfontHint(binding.emailguestlayout,Constants.REGULAR);
            sessionManager.arabicfontHint(binding.mobileguestlayout,Constants.REGULAR);
            sessionManager.arabicfontHint(binding.lnameguestlayout,Constants.REGULAR);
            sessionManager.arabicfontHint(binding.firstnameGuestlay,Constants.REGULAR);
            sessionManager.arabicfontHint(binding.parentnameGuestlay,Constants.REGULAR);
            sessionManager.arabicfontHint(binding.usernameExistlayout,Constants.REGULAR);
            sessionManager.arabicfontHint(binding.mobileExistlayout,Constants.REGULAR);
            sessionManager.arabicfontHint(binding.emailExistlayout,Constants.REGULAR);
            sessionManager.arabicfontHint(binding.stunameExistlayout,Constants.REGULAR);
            sessionManager.arabicfontHint(binding.parentnameExistlayout,Constants.REGULAR);


        }

        if (sessionManager.gettype().equals("exist")){
            binding.existlinear.setVisibility(View.VISIBLE);
            binding.guestlinear.setVisibility(View.GONE);
            if (sessionManager.getexprofile_img().equals(Constants.PROFILEUPDATEIMG_EMPTY_URL)) {
                Glide.with(ProfileSettingFragment.this).load(R.drawable.profiledefault).error(R.drawable.profiledefault).into(binding.existprofileImg);
            }
            else {
                Glide.with(ProfileSettingFragment.this).load(sessionManager.getexprofile_img()).error(R.drawable.profiledefault).into(binding.existprofileImg);
            }
            binding.usernameExist.setText(sessionManager.getusernamedisplay());
            binding.mobileExist.setText(sessionManager.getexmobile());
            binding.emailExist.setText(sessionManager.getexemail());
            binding.stunameExist.setText(sessionManager.getexstudentname());
            binding.parentnameExist.setText(sessionManager.getexparentname());

//     add image button exist
            binding.existaddimgBtn.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    if (SystemClock.elapsedRealtime() - mLastClickTime < 1000) {
                        return;
                    }
                    mLastClickTime = SystemClock.elapsedRealtime();
                    showFileChooser1();
                }
            });
            binding.existprofileImg.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    if (SystemClock.elapsedRealtime() - mLastClickTime < 1000) {
                        return;
                    }
                    mLastClickTime = SystemClock.elapsedRealtime();
                    showFileChooser1();
                }
            });
//            update btn exist
            binding.existupdatebtn.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    if (SystemClock.elapsedRealtime() - mLastClickTime < 1000) {
                        return;
                    }
                    mLastClickTime = SystemClock.elapsedRealtime();
                    profilebodyExist();
                }
            });
        }


        if (sessionManager.gettype().equals("guest")){
            binding.guestlinear.setVisibility(View.VISIBLE);
            binding.existlinear.setVisibility(View.GONE);
            if (!sessionManager.getguprofile_img().equals(Constants.PROFILEUPDATEIMG_EMPTY_URL)) {
                Glide.with(ProfileSettingFragment.this).load(sessionManager.getguprofile_img()).error(R.drawable.profiledefault).into(binding.guestprofileImg);
            }
            else {
                Glide.with(ProfileSettingFragment.this).load(R.drawable.profiledefault).error(R.drawable.profiledefault).into(binding.guestprofileImg);
            }
            binding.guestaddimgBtn.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    if (SystemClock.elapsedRealtime() - mLastClickTime < 1000) {
                        return;
                    }
                    mLastClickTime = SystemClock.elapsedRealtime();
                    showFileChooser1();
                }
            });

            binding.guestprofileImg.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    if (SystemClock.elapsedRealtime() - mLastClickTime < 1000) {
                        return;
                    }
                    mLastClickTime = SystemClock.elapsedRealtime();
                    showFileChooser1();
                }
            });

            binding.usernameGuest.setText(sessionManager.getusernamedisplay());
            binding.firstnameGuest.setText(sessionManager.getgufirstname());
            binding.lastnameGuest.setText(sessionManager.getgulastname());
            binding.mobileGuest.setText(sessionManager.getgumobile());
            binding.emailGuest.setText(sessionManager.getguemail());
            binding.parentnameGuest.setText(sessionManager.getguparentname());

            binding.emailGuest.setEnabled(false);
            binding.mobileGuest.setEnabled(false);
            binding.usernameGuest.setEnabled(false);

            binding.emailGuest.setTextColor(Color.parseColor("#B8B8B8"));
            binding.mobileGuest.setTextColor(Color.parseColor("#B8B8B8"));
            binding.usernameGuest.setTextColor(Color.parseColor("#B8B8B8"));

            binding.guestupdatebtn.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    gfname=binding.firstnameGuest.getText().toString();
                    glname=binding.lastnameGuest.getText().toString();
                    gmobile=binding.mobileGuest.getText().toString();
                    gemail=binding.emailGuest.getText().toString();
                    guname=binding.usernameGuest.getText().toString();
                    gparent=binding.parentnameGuest.getText().toString();
                    profilebodyGuest();
                }
            });
        }
        if (sessionManager.getlang().equals(Constants.ARABICKEY)){
            binding.backFromProfilesetting.setRotationY(180);
        }

        binding.backFromProfilesetting.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Fragment fragment = new ProfileFragment();
                getActivity().getSupportFragmentManager().beginTransaction().replace(R.id.fragment_container, fragment).addToBackStack(null).commit();

            }
        });

        return binding.getRoot();
    }

    // Create this as a variable in your Fragment class
    ActivityResultLauncher<Intent> mLauncher = registerForActivityResult(
            new ActivityResultContracts.StartActivityForResult(),
            new ActivityResultCallback<ActivityResult>() {
                @Override
                public void onActivityResult(ActivityResult result) {
                    Log.d("result",""+result);
                    Log.d("IMAGE_RESULT",""+IMAGE_RESULT);
                    Log.d("RESULT_OK",""+Activity.RESULT_OK);


                    // Do your code from onActivityResult
                    if (result.getResultCode() == Activity.RESULT_OK) {

//                gal
                            if (result.getData() != null && result.getData().getData() != null) {
                                Uri contentUri = result.getData().getData();
                                String timeStamp = new SimpleDateFormat("yyyyMMdd_HHmmss").format(new Date());
                                String imageFilePath = "JPEG_" + timeStamp + "." + getFileExt(contentUri);
                                String RealimageFilePath = getImageFilePath(result.getData());
                                Bitmap image = BitmapFactory.decodeFile(RealimageFilePath);//loading the large bitmap is fine.
                                int w = image.getWidth();//get width
                                int h = image.getHeight();//get height
                                int aspRat = w / h;//get aspect ratio
                                int W = 100;//do whatever you want with width. Fixed, screen size, anything
                                int H = W * aspRat;//set the height based on width and aspect ratio
                                Log.d("kjkj", "onActivityResult: "+w+"..."+h+".."+W+".."+H);
//                                Bitmap b = Bitmap.createScaledBitmap(image, image.getWidth(), image.getHeight(), false);//scale the bitmap
                                Bitmap b = Bitmap.createScaledBitmap(image, 1920,1080 , false);//scale the bitmap
                                image = null;//save memory on the bitmap called 'image'
                                long imagename = System.currentTimeMillis();
                                imagedata = new VolleyMultipartRequest.DataPart(imagename+".jpg", getFileDataFromDrawable(b));

                                if (sessionManager.gettype().equals("exist")) {
                                    binding.existprofileImg.setImageBitmap(b);
                                } else {
                                    binding.guestprofileImg.setImageBitmap(b);
                                }


                               /* try {
                                    // bitmap = MediaStore.Images.Media.getBitmap(getActivity().getContentResolver(), contentUri);
                                    byte[] byteArray = getBytes(getActivity().getContentResolver().openInputStream(contentUri));
                                    bitmap = BitmapFactory.decodeByteArray(byteArray, 0, byteArray.length);
                                    long imagename = System.currentTimeMillis();
                                    imagedata = new VolleyMultipartRequest.DataPart(imagename + ".jpg", getFileDataFromDrawable(bitmap));

                                    Log.d("", "imagedata1>> " + imagedata);
                                } catch (IOException e) {
                                    e.printStackTrace();
                                }*/
                            }
                            else {
                                String filePath = getImageFilePath(result.getData());
                                if (filePath != null) {
                                    Log.d("filePath", String.valueOf(filePath));
                                    bitmap = BitmapFactory.decodeFile(filePath);
                                    if (sessionManager.gettype().equals("exist")) {
//                            existProfileImg.setImageURI(contentUri);
                                        binding.existprofileImg.setImageBitmap(bitmap);
                                    } else {
//                            guestProfileImg.setImageURI(contentUri);
                                        binding.guestprofileImg.setImageBitmap(bitmap);
                                    }

                                    long imagename = System.currentTimeMillis();
                                    imagedata = new VolleyMultipartRequest.DataPart(imagename + ".jpg", getFileDataFromDrawable(bitmap));
                                    Log.d("", "imagedata>> " + imagedata);
//                        profilebody();
                                }
                            }

                    }

                }
            });

    private void showFileChooser1() {

     mLauncher.launch(getPickImageChooserIntent());
//        startActivity(getPickImageChooserIntent());
    }

    public Intent getPickImageChooserIntent() {
        Uri outputFileUri = null;
        try {
            outputFileUri = createImageFile();
        } catch (IOException e) {
            e.printStackTrace();
        }
        List<Intent> allIntents = new ArrayList<>();
        PackageManager packageManager = getActivity().getPackageManager();
        Intent captureIntent = new Intent(android.provider.MediaStore.ACTION_IMAGE_CAPTURE);
        List<ResolveInfo> listCam = packageManager.queryIntentActivities(captureIntent, 0);
        for (ResolveInfo res : listCam) {
            Intent intent = new Intent(captureIntent);
            intent.setComponent(new ComponentName(res.activityInfo.packageName, res.activityInfo.name));
            intent.setPackage(res.activityInfo.packageName);
            if (outputFileUri != null) {
                intent.putExtra(MediaStore.EXTRA_OUTPUT, outputFileUri);
            }
            allIntents.add(intent);
        }

        Intent galleryIntent = new Intent(Intent.ACTION_GET_CONTENT);
        galleryIntent.setType("image/*");
        List<ResolveInfo> listGallery = packageManager.queryIntentActivities(galleryIntent, 0);
        for (ResolveInfo res : listGallery) {
            Intent intent = new Intent(galleryIntent);
            intent.setComponent(new ComponentName(res.activityInfo.packageName, res.activityInfo.name));
            intent.setPackage(res.activityInfo.packageName);
            Log.d("hjcb",""+intent);
            allIntents.add(intent);
        }

        Intent mainIntent = allIntents.get(allIntents.size() - 1);
        for (Intent intent : allIntents) {
            if (intent.getComponent().getClassName().equals("com.android.documentsui.DocumentsActivity")) {
                mainIntent = intent;
                break;
            }
            else {
            }
        }
        allIntents.remove(mainIntent);

        Intent chooserIntent = Intent.createChooser(mainIntent, getString(R.string.selectsource));
        chooserIntent.putExtra(Intent.EXTRA_INITIAL_INTENTS, allIntents.toArray(new Parcelable[allIntents.size()]));
        return chooserIntent;
    }
    private String getImageFilePath(Intent data) {
        boolean isCamera = data == null || data.getData() == null;
        if (isCamera) return currentPhotoPath;
        else return getPathFromURI(data.getData());
    }

    public byte[] getFileDataFromDrawable(Bitmap bitmap) {
        ByteArrayOutputStream byteArrayOutputStream = new ByteArrayOutputStream();
        bitmap.compress(Bitmap.CompressFormat.JPEG, 80, byteArrayOutputStream);
        return byteArrayOutputStream.toByteArray();
    }
    //gallery
    public String getPathFromURI(Uri contentUri) {
        String res = null;
        String[] proj = {MediaStore.Images.Media.DATA};
        Cursor cursor = getActivity().getContentResolver().query(contentUri, proj, null, null, null);
        if (cursor.moveToFirst()) {
            int column_index = cursor.getColumnIndexOrThrow(MediaStore.Images.Media.DATA);
            res = cursor.getString(column_index);
        }
        cursor.close();
        return res;
    }
    public byte[] getBytes(InputStream inputStream) throws IOException {
        ByteArrayOutputStream byteBuffer = new ByteArrayOutputStream();
        int bufferSize = 1024;
        byte[] buffer = new byte[bufferSize];

        int len = 0;
        while ((len = inputStream.read(buffer)) != -1) {
            byteBuffer.write(buffer, 0, len);
        }
        return byteBuffer.toByteArray();
    }
    private String getFileExt(Uri contentUri) {
        ContentResolver c = getActivity().getContentResolver();
        MimeTypeMap mime = MimeTypeMap.getSingleton();
        return mime.getExtensionFromMimeType(c.getType(contentUri));
    }
    //camera
    private Uri createImageFile() throws IOException {
        // Create an image file name
        String timeStamp = new SimpleDateFormat("yyyyMMdd_HHmmss").format(new Date());
        String imageFileName = "JPEG_" + timeStamp + "_";
        File storageDir = getActivity().getExternalFilesDir(Environment.DIRECTORY_PICTURES);
        File image = File.createTempFile(
                imageFileName,  /* prefix */
                ".jpg",         /* suffix */
                storageDir      /* directory */
        );

        // Save a file: path for use with ACTION_VIEW intents
        if (image != null) {
            currentPhotoFile = image;
            currentPhotoPath = image.getAbsolutePath();
            currentPhotoPathFileUri = FileProvider.getUriForFile(getContext(), "com.alshumua.school.android.fileprovider", image);
//            currentPhotoPathFileUri = FileProvider.getUriForFile(getContext(), "${applicationId}.fileprovider", image);
//            ${applicationId}.fileprovider
        }
        return currentPhotoPathFileUri;
    }
    private void galleryAddPic() {
        Intent mediaScanIntent = new Intent(Intent.ACTION_MEDIA_SCANNER_SCAN_FILE);
        File f = new File(currentPhotoPath);
        Uri contentUri = Uri.fromFile(f);
        mediaScanIntent.setData(contentUri);
        this.getActivity().sendBroadcast(mediaScanIntent);
        try {
            Bitmap bitmap = MediaStore.Images.Media.getBitmap(getActivity().getContentResolver(), contentUri);
        } catch (IOException e) {
            e.printStackTrace();
        }
    }
    //file manager and files
    @SuppressLint("NewApi")
    public String getPathFromURI(Context context, Uri uri) {
        Log.d("hyfhtf", "" + uri);
        String filePath = "";
        String fileId = DocumentsContract.getDocumentId(uri);
        // Split at colon, use second item in the array
        String id = fileId.split(":")[1];
        String[] column = {MediaStore.Images.Media.DATA};
        String selector = MediaStore.Images.Media._ID + "=?";
        Cursor cursor = context.getContentResolver().query(MediaStore.Images.Media.EXTERNAL_CONTENT_URI,
                column, selector, new String[]{id}, null);
        int columnIndex = cursor.getColumnIndex(column[0]);
        if (cursor.moveToFirst()) {
            filePath = cursor.getString(columnIndex);
        }
        cursor.close();
        return filePath;
    }


    private void profilebodyGuest() {
        JSONObject requestbody = new JSONObject();
        try {

            requestbody.put("id", sessionManager.getid());
            requestbody.put("username", sessionManager.getusernamedisplay());
            requestbody.put("image", imagedata);
            requestbody.put("first_name", gfname);
            requestbody.put("last_name", glname);
            requestbody.put("parent_name", gparent);
            requestbody.put("email", gemail);
            profileupdaterequestJSON(requestbody);
            sessionManager.progressdialogshow();
        } catch (JSONException e) {
            e.printStackTrace();
        }
    }
    private void profilebodyExist() {
        JSONObject requestbody = new JSONObject();
        try {

            requestbody.put("id", "" + sessionManager.getid());
            requestbody.put("username", "" + sessionManager.getusernamedisplay());
            requestbody.put("image", imagedata);
            requestbody.put("first_name", sessionManager.getexstudentname());
            requestbody.put("last_name", sessionManager.getexparentname());
            requestbody.put("parent_name",  sessionManager.getexparentname());
            requestbody.put("email", sessionManager.getexemail());

            profileupdaterequestJSON(requestbody);
            sessionManager.progressdialogshow();
        } catch (JSONException e) {
            e.printStackTrace();
        }
    }
    private void profileupdaterequestJSON(JSONObject response) {
        final String requestBody = response.toString();
        Log.d("profilereq", String.valueOf(response));
        VolleyMultipartRequest volleyMultipartRequest = new VolleyMultipartRequest(Request.Method.POST, Urls.Updateprofile,
                new Response.Listener<NetworkResponse>() {
                    @Override
                    public void onResponse(NetworkResponse response) {
                        sessionManager.progressdialogdismiss();
                        Log.d("updateprofileresponse", "" + response);
                        Log.d("updateprofileresponse>>", "" + response.data);

                        try {
                            JSONObject obj = new JSONObject(new String(response.data));
                            Log.d("", "obj: " + obj);
                            if (obj.getString("error").equals("true")) {
                                msg=obj.getString("message");
                                Toast.makeText(getContext(), msg, Toast.LENGTH_SHORT).show();

                            } else {
                                msg=obj.getString("message");
                                Toast.makeText(getContext(),msg, Toast.LENGTH_SHORT).show();
                                Intent i = new Intent(getActivity(), DashboardActivity.class);
                                i.putExtra("profilesetting","profilesetting");
                                startActivity(i);
                                ((Activity) getActivity()).overridePendingTransition(0, 0);
                                ((Activity) getActivity()).finish();
                            }
                        } catch (JSONException e) {
                            e.printStackTrace();
                        }

                    }
                },
                new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {
                        sessionManager.progressdialogdismiss();
                        Toast.makeText(getContext(), error.getMessage(), Toast.LENGTH_SHORT).show();
                    }
                }) {
            @Override
            public Map<String, String> getHeaders() throws AuthFailureError {
                Map<String, String> params = new HashMap<String, String>();
                return params;
            }

            @Override
            protected Map<String, String> getParams() throws AuthFailureError {
                Map<String, String> params = new HashMap<>();
if (sessionManager.gettype().equals("exist")){
    params.put("id", "" + sessionManager.getid());
    params.put("username", "" + sessionManager.getusernamedisplay());
    params.put("first_name", sessionManager.getexstudentname());
    params.put("last_name", sessionManager.getexparentname());
    params.put("parent_name",  sessionManager.getexparentname());
    params.put("email", sessionManager.getexemail());

}
else{
    params.put("id", "" + sessionManager.getid());
    params.put("username", "" + sessionManager.getusernamedisplay());
    params.put("first_name", gfname);
    params.put("last_name", glname);
    params.put("parent_name", gparent);
    params.put("email", gemail);
}
                Log.d("param orderbody", "" + params);
                return params;
            }

            @Override
            protected Map<String, DataPart> getByteData() {
                Map<String, DataPart> params = new HashMap<>();
                if (imagedata != null) {
                    params.put("image", imagedata);
                }
                Log.d("param img", "" + params);
                return params;
            }

        };
        //adding the request to volley
        Volley.newRequestQueue(getContext()).add(volleyMultipartRequest);
    }


}