package com.alshumua.school.Fragment;

import android.app.Activity;
import android.app.AlertDialog;
import android.app.FragmentManager;
import android.content.DialogInterface;
import android.content.Intent;
import android.graphics.drawable.ColorDrawable;
import android.os.Build;
import android.os.Bundle;

import androidx.activity.OnBackPressedCallback;
import androidx.activity.OnBackPressedDispatcher;
import androidx.annotation.RequiresApi;
import androidx.fragment.app.Fragment;
import androidx.lifecycle.LifecycleOwner;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.util.Log;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.alshumua.school.Activity.NewSignin;
import com.alshumua.school.Adapter.MyRequestAdapter;
import com.alshumua.school.Model.MyRequestListModel;
import com.alshumua.school.Std.Constants;
import com.alshumua.school.Std.Dialog;
import com.alshumua.school.Std.SessionManager;
import com.alshumua.school.Std.Urls;
import com.android.volley.AuthFailureError;
import com.android.volley.DefaultRetryPolicy;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.VolleyLog;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;
import com.alshumua.school.Activity.DashboardActivity;
import com.alshumua.school.R;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.UnsupportedEncodingException;
import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.Map;


public class MyRequestFragment extends Fragment {
        //    var
        SessionManager sessionManager;
        View view;
        ImageView back;
        private static ArrayList<MyRequestListModel> requestedFormListModelArrayList;
        private static RecyclerView.Adapter requestAdapter;
        private static RecyclerView requestRecyclerView;

    @RequiresApi(api = Build.VERSION_CODES.M)
    @Override
        public View onCreateView(LayoutInflater inflater, ViewGroup container,
                                 Bundle savedInstanceState) {
        view = inflater.inflate(R.layout.fragment_my_request, container, false);

        //        hooks
        sessionManager = new SessionManager(getActivity(), getContext(), view);
        back = (ImageView) view.findViewById(R.id.back_from_myrequest);
        requestRecyclerView = (RecyclerView) view.findViewById(R.id.myrequest_recyclerview);
        requestRecyclerView.setLayoutManager(new LinearLayoutManager(getContext()));

        if (!sessionManager.isNetwork() == true) {
            Dialog dialog = new Dialog();
            dialog.show(getChildFragmentManager(), Constants.NONETWORK);
        }
        if (sessionManager.getlang().equals(Constants.ARABICKEY)){
            back.setRotationY(180);
        }
        if (sessionManager.gettokenin().equals("")){

            sessionManager.loginRequiredDialog(getContext(),"myrequest");
        }

        else {
            ReqListBody();
        }




        OnBackPressedDispatcher onBackPressedDispatcher = getActivity().getOnBackPressedDispatcher();
        if (onBackPressedDispatcher != null) {
            onBackPressedDispatcher.addCallback((LifecycleOwner)this, (OnBackPressedCallback)(new OnBackPressedCallback(true) {
                public void handleOnBackPressed() {
                    this.setEnabled(false);
                    Intent i = new Intent(getActivity(), DashboardActivity.class);
                    i.putExtra("mypro","mypro");
                    startActivity(i);
                    ((Activity) getActivity()).overridePendingTransition(0, 0);
                    ((Activity) getActivity()).finish();

                }
            }));
        }
       back.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent i = new Intent(getActivity(), DashboardActivity.class);
                i.putExtra("mypro","mypro");
                startActivity(i);
                ((Activity) getActivity()).overridePendingTransition(0, 0);
                ((Activity) getActivity()).finish();
            }
        });


        return view;

    }



        public void ReqListBody() {
            JSONObject requestbody = new JSONObject();
            try {
                requestbody.put("custid", sessionManager.getid());
                requestFormJSON(requestbody);
                sessionManager.progressdialogshow();
            } catch (JSONException e) {
                e.printStackTrace();
            }
        }

        private void requestFormJSON(JSONObject response) {
            RequestQueue requestQueue = Volley.newRequestQueue(getContext());
            final String requestBody = response.toString();
            Log.d("requestFormJSON", String.valueOf(response));
            StringRequest stringRequest = new StringRequest(Request.Method.POST, Urls.requestlist, new Response.Listener<String>() {
                @Override
                public void onResponse(String response) {
                    sessionManager.progressdialogdismiss();
                    Log.d("_response", ">>" + response);
                    try {
                        JSONObject obj = new JSONObject(response);
                        if (obj.getBoolean("error") == false) {
                            requestedFormListModelArrayList = new ArrayList<MyRequestListModel>();
                            JSONArray obj1 = new JSONArray(obj.getString("data"));
                            for (int i = 0; i < obj1.length(); i++) {
                                JSONObject jsonObject = obj1.getJSONObject(i);
                                MyRequestListModel datamodel = new MyRequestListModel(
                                        getContext(),
                                        jsonObject.getString("serviceName"),
                                        jsonObject.getString("status"),
                                        jsonObject.getString("remarks"),
                                        jsonObject.getString("date"),
                                        jsonObject.getString("serviceIcon"),

                                        jsonObject.getInt("serviceId"),
                                        jsonObject.getInt("requestId")
                                       );
                                requestedFormListModelArrayList.add(datamodel);

                            }
                            Collections.reverse(requestedFormListModelArrayList);
                            requestAdapter = new MyRequestAdapter(MyRequestFragment.this,getContext(),requestedFormListModelArrayList);
                            requestRecyclerView.setAdapter(requestAdapter);
                        } else {
                        }
                    } catch (Exception e) {
                        e.printStackTrace();
                    }
                }
            }, new Response.ErrorListener() {
                @Override
                public void onErrorResponse(VolleyError error) {
                    Log.e("VOLLEY", error.toString());
                    sessionManager.progressdialogdismiss();
                    Toast.makeText(getContext(), error.getMessage(), Toast.LENGTH_SHORT).show();
                    //  sessionManager.volleyerror(error);
                }
            }) {
                @Override
                public Map<String, String> getHeaders() throws AuthFailureError {
                    Map<String, String> params = new HashMap<String, String>();
                    params.put("Authorization", "Bearer " + sessionManager.gettoken());
                    params.put("lang",sessionManager.getlang());
//                    params.put("Authorization", "Bearer "+"eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJpZCI6IjEyIiwidXNlcm5hbWUiOiJTbmVoYSBzZWx2YW0ifQ.Lo7r-ebR4O89j-nYaDlr6W_Rweo4f07CWYCWktspd2o");
                    return params;
                }

                @Override
                public String getBodyContentType() {
                    return "application/json; charset=utf-8";
                }

                @Override
                public byte[] getBody() throws AuthFailureError {
                    try {
                        return requestBody == null ? null : requestBody.getBytes("utf-8");
                    } catch (UnsupportedEncodingException uee) {
                        VolleyLog.wtf("Unsupported Encoding while trying to get the bytes of %s using %s", requestBody, "utf-8");
                        return null;
                    }
                }
            };
            stringRequest.setRetryPolicy(new DefaultRetryPolicy(10000, DefaultRetryPolicy.DEFAULT_MAX_RETRIES, DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
            Log.d("asdsdjkxdk", "" + stringRequest);
            requestQueue.add(stringRequest);
        }


    }