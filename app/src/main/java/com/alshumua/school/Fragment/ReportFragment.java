package com.alshumua.school.Fragment;

import android.content.Intent;
import android.os.Bundle;

import androidx.fragment.app.Fragment;

import android.os.SystemClock;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.TextView;
import android.widget.Toast;

import com.alshumua.school.Activity.DashboardActivity;
import com.alshumua.school.Activity.NewSignin;
import com.alshumua.school.Std.Constants;
import com.alshumua.school.Std.Dialog;
import com.alshumua.school.Std.SessionManager;
import com.alshumua.school.Std.Urls;
import com.alshumua.school.databinding.FragmentProfileSettingBinding;
import com.alshumua.school.databinding.FragmentReportBinding;
import com.android.volley.AuthFailureError;
import com.android.volley.DefaultRetryPolicy;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.VolleyLog;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;
import com.alshumua.school.Activity.ReportSendSucc;
import com.alshumua.school.R;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.UnsupportedEncodingException;
import java.nio.charset.StandardCharsets;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;
import java.util.Locale;
import java.util.Map;

public class ReportFragment extends Fragment {
    //    var
    private static final String TAG = "report";
    View view;
    SessionManager sessionManager;

    JSONObject requestStr = new JSONObject();
    JSONObject roportliaBody = new JSONObject();
    private long mLastClickTime = 0;
    String formattedDate;

    RadioButton rb;
FragmentReportBinding binding;
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View layout = LayoutInflater.from(requireContext()).inflate(R.layout.fragment_report, container, false);
        binding = FragmentReportBinding.bind(layout);
//        hooks
        sessionManager = new SessionManager(getActivity(), getContext(), view);

//    no network check
        if (!sessionManager.isNetwork() == true) {
            Dialog dialog = new Dialog();
            dialog.show(getChildFragmentManager(), Constants.NONETWORK);
        }

//        rotate arrow in arabic
        if (sessionManager.getlang().equals(Constants.ARABICKEY)) {
            binding.backFromReport.setRotationY(180);
            sessionManager.arabicfont(binding.initialfirst,null, null,Constants.REGULAR,"textV");
            sessionManager.arabicfont(binding.initialsecond,null, null,Constants.REGULAR,"textV");
            sessionManager.arabicfont(binding.initialboth,null, null,Constants.REGULAR,"textV");

            sessionManager.arabicfont(binding.periodic1,null, null,Constants.REGULAR,"textV");
            sessionManager.arabicfont(binding.periodic2,null, null,Constants.REGULAR,"textV");
            sessionManager.arabicfont(binding.periodicBoth,null, null,Constants.REGULAR,"textV");

            sessionManager.arabicfont(binding.integrate1,null, null,Constants.REGULAR,"textV");
            sessionManager.arabicfont(binding.integrate2,null, null,Constants.REGULAR,"textV");
            sessionManager.arabicfont(binding.integrateBoth,null, null,Constants.REGULAR,"textV");

            sessionManager.arabicfont(binding.initialtxt,null, null,Constants.REGULAR,"textV");
            sessionManager.arabicfont(binding.periodictxt,null, null,Constants.REGULAR,"textV");
            sessionManager.arabicfont(binding.integratedtxt,null, null,Constants.REGULAR,"textV");
            sessionManager.arabicfont(binding.report,null, null,Constants.BOLD,"textV");
            sessionManager.arabicfont(null,null, binding.reportsubmit,Constants.BOLD,"btn");

        }

//        Get current date
        Date c = Calendar.getInstance().getTime();
        SimpleDateFormat df = new SimpleDateFormat("yyyy-MM-dd", Locale.getDefault());
        formattedDate = df.format(c);

//        radio grp only visible only after  iep report (2nd row) click
        binding.radiogrpLinear.setVisibility(View.GONE);


//        back button click
        binding.backFromReport.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Fragment fragment = new ProfileFragment();
                getActivity().getSupportFragmentManager().beginTransaction().replace(R.id.fragment_container, fragment).addToBackStack(null).commit();
            }
        });

//        separate clicks for each report btns
        binding.initialfirst.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                binding.radiogrpLinear.setVisibility(View.GONE);
//       button click timer (it avoids multiple clicks )
                if (SystemClock.elapsedRealtime() - mLastClickTime < 1000) {
                    return;
                }
                mLastClickTime = SystemClock.elapsedRealtime();
                Toast.makeText(getContext(), getString(R.string.firststSemester) + " " + getString(R.string.wasselected), Toast.LENGTH_SHORT).show();
                if (sessionManager.gettype().equals("exist")) {
                    reportBody(sessionManager.getexstudentname(), sessionManager.getusernamedisplay(), sessionManager.getexparentname(), sessionManager.getexmobile(), sessionManager.getexemail(), getString(R.string.assesment), getString(R.string.firststSemester), getString(R.string.no), getString(R.string.no), getString(R.string.exist));
                    reportlia(getString(R.string.InitialAssessment));
                } else {
                    reportBody(sessionManager.getusernamedisplay(), sessionManager.getgufirstname(), sessionManager.getguparentname(), sessionManager.getgumobile(), sessionManager.getguemail(), getString(R.string.assesment), getString(R.string.firststSemester), getString(R.string.no), getString(R.string.no), getString(R.string.guest));
                    reportlia(getString(R.string.InitialAssessment));
                }

            }
        });
        binding.initialsecond.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                binding.radiogrpLinear.setVisibility(View.GONE);
                if (SystemClock.elapsedRealtime() - mLastClickTime < 1000) {
                    return;
                }
                mLastClickTime = SystemClock.elapsedRealtime();
                Toast.makeText(getContext(), getString(R.string.secondstSemester) + " " + getString(R.string.wasselected), Toast.LENGTH_SHORT).show();
                if (sessionManager.gettype().equals("exist")) {
                    reportBody(sessionManager.getexstudentname(), sessionManager.getusernamedisplay(), sessionManager.getexparentname(), sessionManager.getexmobile(), sessionManager.getexemail(), getString(R.string.assesment), getString(R.string.secondstSemester), getString(R.string.no), getString(R.string.no), getString(R.string.exist));
                    reportlia(getString(R.string.InitialAssessment));
                } else {
                    reportBody(sessionManager.getusernamedisplay(), sessionManager.getgufirstname(), sessionManager.getguparentname(), sessionManager.getgumobile(), sessionManager.getguemail(), getString(R.string.assesment), getString(R.string.secondstSemester), getString(R.string.no), getString(R.string.no), getString(R.string.guest));
                    reportlia(getString(R.string.InitialAssessment));
                }

            }
        });
        binding.initialboth.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                binding.radiogrpLinear.setVisibility(View.GONE);
                if (SystemClock.elapsedRealtime() - mLastClickTime < 1000) {
                    return;
                }
                mLastClickTime = SystemClock.elapsedRealtime();
                Toast.makeText(getContext(), getString(R.string.Both) + " " + getString(R.string.wasselected), Toast.LENGTH_SHORT).show();
                if (sessionManager.gettype().equals("exist")) {
                    reportBody(sessionManager.getexstudentname(), sessionManager.getusernamedisplay(), sessionManager.getexparentname(), sessionManager.getexmobile(), sessionManager.getexemail(), getString(R.string.assesment), getString(R.string.Both), getString(R.string.no), getString(R.string.no), getString(R.string.exist));
                    reportlia(getString(R.string.InitialAssessment));
                } else {
                    reportBody(sessionManager.getusernamedisplay(), sessionManager.getgufirstname(), sessionManager.getguparentname(), sessionManager.getgumobile(), sessionManager.getguemail(), getString(R.string.assesment), getString(R.string.Both), getString(R.string.no), getString(R.string.no), getString(R.string.guest));
                    reportlia(getString(R.string.InitialAssessment));
                }

            }
        });


        binding.periodic1.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (SystemClock.elapsedRealtime() - mLastClickTime < 1000) {
                    return;
                }
                mLastClickTime = SystemClock.elapsedRealtime();

                binding.radiogrpLinear.setVisibility(View.VISIBLE);

                binding.radiogrp.setOnCheckedChangeListener(new RadioGroup.OnCheckedChangeListener() {
                    @Override
                    public void onCheckedChanged(RadioGroup group, int checkedId) {
                        rb = (RadioButton) view.findViewById(checkedId);
                        if (sessionManager.gettype().equals("exist")) {

                            if (!rb.getText().toString().equals("") || !rb.getText().toString().equals(null)) {
                                Toast.makeText(getContext(), getString(R.string.firststSemester) + " " + "," + rb.getText() + " " + getString(R.string.wasselected), Toast.LENGTH_SHORT).show();
                                reportBody(sessionManager.getexstudentname(), sessionManager.getusernamedisplay(), sessionManager.getexparentname(), sessionManager.getexmobile(), sessionManager.getexemail(), getString(R.string.iep), getString(R.string.firststSemester), getString(R.string.yes), rb.getText().toString(), getString(R.string.exist));
                                reportlia(getString(R.string.PeriodicAssessment));
                            }
                        } else {
                            if (!rb.getText().toString().equals("") || !rb.getText().toString().equals(null)) {
                                Toast.makeText(getContext(), getString(R.string.firststSemester) + " " + "," + rb.getText() + " " + getString(R.string.wasselected), Toast.LENGTH_SHORT).show();
                                reportBody(sessionManager.getusernamedisplay(), sessionManager.getgufirstname(), sessionManager.getguparentname(), sessionManager.getgumobile(), sessionManager.getguemail(), getString(R.string.iep), getString(R.string.firststSemester), getString(R.string.yes), rb.getText().toString(), getString(R.string.guest));
                                reportlia(getString(R.string.PeriodicAssessment));
                            }
                        }
                    }
                });


            }
        });
        binding.periodic2.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (SystemClock.elapsedRealtime() - mLastClickTime < 1000) {
                    return;
                }
                mLastClickTime = SystemClock.elapsedRealtime();
                binding.radiogrpLinear.setVisibility(View.VISIBLE);


                binding.radiogrp.setOnCheckedChangeListener(new RadioGroup.OnCheckedChangeListener() {
                    @Override
                    public void onCheckedChanged(RadioGroup group, int checkedId) {
                        rb = (RadioButton) view.findViewById(checkedId);
                        if (sessionManager.gettype().equals("exist")) {

                            if (!rb.getText().toString().equals("") || !rb.getText().toString().equals(null)) {
                                Toast.makeText(getContext(), getString(R.string.secondstSemester) + " " + "," + rb.getText() + " " + getString(R.string.wasselected), Toast.LENGTH_SHORT).show();

                                reportBody(sessionManager.getexstudentname(), sessionManager.getusernamedisplay(), sessionManager.getexparentname(), sessionManager.getexmobile(), sessionManager.getexemail(), getString(R.string.iep), getString(R.string.secondstSemester), getString(R.string.yes), rb.getText().toString(), getString(R.string.exist));
                                reportlia(getString(R.string.PeriodicAssessment));
                            }
                        } else {
                            if (!rb.getText().toString().equals("") || !rb.getText().toString().equals(null)) {
                                Toast.makeText(getContext(), getString(R.string.secondstSemester) + " " + "," + rb.getText() + " " + getString(R.string.wasselected), Toast.LENGTH_SHORT).show();

                                reportBody(sessionManager.getusernamedisplay(), sessionManager.getgufirstname(), sessionManager.getguparentname(), sessionManager.getgumobile(), sessionManager.getguemail(), getString(R.string.iep), getString(R.string.secondstSemester), getString(R.string.yes), rb.getText().toString(), getString(R.string.guest));
                                reportlia(getString(R.string.PeriodicAssessment));
                            }
                        }
                    }
                });

            }
        });
        binding.periodicBoth.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (SystemClock.elapsedRealtime() - mLastClickTime < 1000) {
                    return;
                }
                mLastClickTime = SystemClock.elapsedRealtime();
                binding.radiogrpLinear.setVisibility(View.VISIBLE);

                binding.radiogrp.setOnCheckedChangeListener(new RadioGroup.OnCheckedChangeListener() {
                    @Override
                    public void onCheckedChanged(RadioGroup group, int checkedId) {
                        rb = (RadioButton) view.findViewById(checkedId);
                        if (sessionManager.gettype().equals("exist")) {

                            if (!rb.getText().toString().equals("") || !rb.getText().toString().equals(null)) {
                                Toast.makeText(getContext(), getString(R.string.Both) + " " + "," + rb.getText() + " " + getString(R.string.wasselected), Toast.LENGTH_SHORT).show();
                                reportBody(sessionManager.getexstudentname(), sessionManager.getusernamedisplay(), sessionManager.getexparentname(), sessionManager.getexmobile(), sessionManager.getexemail(), getString(R.string.iep), getString(R.string.firststSemester), getString(R.string.yes), rb.getText().toString(), getString(R.string.exist));
                                reportlia(getString(R.string.PeriodicAssessment));
                            }
                        } else {
                            if (!rb.getText().toString().equals("") || !rb.getText().toString().equals(null)) {
                                Toast.makeText(getContext(), getString(R.string.Both) + " " + "," + rb.getText() + " " + getString(R.string.wasselected), Toast.LENGTH_SHORT).show();
                                reportBody(sessionManager.getusernamedisplay(), sessionManager.getgufirstname(), sessionManager.getguparentname(), sessionManager.getgumobile(), sessionManager.getguemail(), getString(R.string.iep), getString(R.string.firststSemester), getString(R.string.yes), rb.getText().toString(), getString(R.string.guest));
                                reportlia(getString(R.string.PeriodicAssessment));
                            }
                        }
                    }
                });

            }
        });


        binding.integrate1.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                binding.radiogrpLinear.setVisibility(View.GONE);
                if (SystemClock.elapsedRealtime() - mLastClickTime < 1000) {
                    return;
                }
                mLastClickTime = SystemClock.elapsedRealtime();
                Toast.makeText(getContext(), getString(R.string.firststSemester) + " " + getString(R.string.wasselected), Toast.LENGTH_SHORT).show();

                if (sessionManager.gettype().equals("exist")) {
                    reportBody(sessionManager.getexstudentname(), sessionManager.getusernamedisplay(), sessionManager.getexparentname(), sessionManager.getexmobile(), sessionManager.getexemail(), getString(R.string.academic), getString(R.string.firststSemester), getString(R.string.no), getString(R.string.no), getString(R.string.exist));
                    reportlia(getString(R.string.IntegratedIEP));
                } else {
                    reportBody(sessionManager.getusernamedisplay(), sessionManager.getgufirstname(), sessionManager.getguparentname(), sessionManager.getgumobile(), sessionManager.getguemail(), getString(R.string.academic), getString(R.string.firststSemester), getString(R.string.no), getString(R.string.no), getString(R.string.guest));
                    reportlia(getString(R.string.IntegratedIEP));
                }

            }
        });
        binding.integrate2.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                binding.radiogrpLinear.setVisibility(View.GONE);
                if (SystemClock.elapsedRealtime() - mLastClickTime < 1000) {
                    return;
                }
                mLastClickTime = SystemClock.elapsedRealtime();
                Toast.makeText(getContext(), getString(R.string.secondstSemester) + " " + getString(R.string.wasselected), Toast.LENGTH_SHORT).show();

                if (sessionManager.gettype().equals("exist")) {
                    reportBody(sessionManager.getexstudentname(), sessionManager.getusernamedisplay(), sessionManager.getexparentname(), sessionManager.getexmobile(), sessionManager.getexemail(), getString(R.string.academic), getString(R.string.secondstSemester), getString(R.string.no), getString(R.string.no), getString(R.string.exist));
                    reportlia(getString(R.string.IntegratedIEP));
                } else {
                    reportBody(sessionManager.getusernamedisplay(), sessionManager.getgufirstname(), sessionManager.getguparentname(), sessionManager.getgumobile(), sessionManager.getguemail(), getString(R.string.academic), getString(R.string.secondstSemester), getString(R.string.no), getString(R.string.no), getString(R.string.guest));
                    reportlia(getString(R.string.IntegratedIEP));
                }

            }
        });
        binding.integrateBoth.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                binding.radiogrpLinear.setVisibility(View.GONE);
                if (SystemClock.elapsedRealtime() - mLastClickTime < 1000) {
                    return;
                }
                mLastClickTime = SystemClock.elapsedRealtime();
                Toast.makeText(getContext(), getString(R.string.Both) + " " + getString(R.string.wasselected), Toast.LENGTH_SHORT).show();

                if (sessionManager.gettype().equals("exist")) {
                    reportBody(sessionManager.getexstudentname(), sessionManager.getusernamedisplay(), sessionManager.getexparentname(), sessionManager.getexmobile(), sessionManager.getexemail(), getString(R.string.academic), getString(R.string.Both), getString(R.string.no), getString(R.string.no), getString(R.string.exist));
                    reportlia(getString(R.string.IntegratedIEP));
                } else {
                    reportBody(sessionManager.getusernamedisplay(), sessionManager.getgufirstname(), sessionManager.getguparentname(), sessionManager.getgumobile(), sessionManager.getguemail(), getString(R.string.academic), getString(R.string.Both), getString(R.string.no), getString(R.string.no), getString(R.string.guest));
                    reportlia(getString(R.string.IntegratedIEP));
                }

            }
        });

//  submit button click
        binding.reportsubmit.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (requestStr.length() != 0) {
                    sessionManager.progressdialogshow();
                    reportLiaJSON();
                    ReportJSON();
                } else {
                    Toast.makeText(getContext(), getString(R.string.pleaseselectreport), Toast.LENGTH_SHORT).show();
                }
            }
        });

        return binding.getRoot();
    }

    private void reportlia(String reportname) {

        try {
            roportliaBody.put("custid", sessionManager.getid());
            roportliaBody.put("reportname", reportname);

        } catch (
                JSONException e) {
            e.printStackTrace();
        }
    }

    private void reportBody(String name, String display, String pname, String mob, String email, String reportname, String sem, String type, String semperiod, String user) {
        try {
            requestStr.put("student_name", name);
            if (user.equals(getString(R.string.exist))) {
                requestStr.put("student_number", display);
            }
            requestStr.put("parent_name", pname);
            requestStr.put("mobile_number", mob);
            requestStr.put("mail", email);
            requestStr.put("report_name", reportname);
            if (type.equals(getText(R.string.yes))) {
                requestStr.put("sem_period", semperiod);
            }
            requestStr.put("sem1ster", sem);
            requestStr.put("date", formattedDate);
        } catch (JSONException e) {
            e.printStackTrace();
        }
        Log.d("reqqqqqq", requestStr.toString());

    }
//   this method for shumua server
    private void ReportJSON() {
        RequestQueue requestQueue = Volley.newRequestQueue(getContext());
        StringRequest stringRequest = new StringRequest(Request.Method.POST, Urls.report, new Response.Listener<String>() {
            @Override
            public void onResponse(String response) {
                Log.d("Reportresponse", ">>" + response);
                sessionManager.progressdialogdismiss();
                try {
                    JSONObject obj = new JSONObject(response);
                    JSONObject obj2 = new JSONObject(response);

                    String data = obj.getString("result");
                    JSONObject maindata = new JSONObject(data);
                    String msg = maindata.getString("message");


                    if (msg.equals("Success")) {
                        Intent intent = new Intent(getActivity(), ReportSendSucc.class);
                        startActivity(intent);
                        Toast.makeText(getActivity(), msg, Toast.LENGTH_SHORT).show();
                    } else {
                        String error = obj2.getString("error");
                        JSONObject errordata = new JSONObject(error);
                        String errormsg = errordata.getString("message");
                        if (errormsg.equals("Odoo Server Error")) {
                            Log.d(TAG, "errormsg" + errormsg);
                            Log.d(TAG, "obj2" + obj2);
                            Log.d(TAG, "errordata" + errordata);

                            if (errormsg.equals("Odoo Server Error")) {

                                Toast.makeText(getActivity(), errormsg, Toast.LENGTH_SHORT).show();
                            }

                            Log.d("erorr", "onResponse: ");

                            if (errormsg.equals("404: Not Found")) {
                                Toast.makeText(getContext(), errormsg, Toast.LENGTH_SHORT).show();
                            }
                        }


                       /* else if (errormsg.equals("404: Not Found")){
                            Toast.makeText(getContext(),errormsg, Toast.LENGTH_SHORT).show();
                        }*/
                    }
                } catch (Exception e) {
                    e.printStackTrace();
                }

            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                sessionManager.progressdialogdismiss();
                Log.e("VOLLEY", error.toString());
                Toast.makeText(getContext(), error.toString(), Toast.LENGTH_SHORT).show();
            }
        }) {
            @Override
            public String getBodyContentType() {
                return "application/json; charset=utf-8";
            }

            @Override
            public byte[] getBody() throws com.android.volley.AuthFailureError {
                Log.d("reqStr", "" + requestStr.toString());

                return requestStr.toString().getBytes(StandardCharsets.UTF_8);


            }

            ;
        };
        stringRequest.setRetryPolicy(new DefaultRetryPolicy(10000, DefaultRetryPolicy.DEFAULT_MAX_RETRIES, DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
        requestQueue.add(stringRequest);
    }

//    this method for listing report request on db, for that sending customer
//    id and report name
    private void reportLiaJSON() {
        RequestQueue requestQueue = Volley.newRequestQueue(getContext());
        StringRequest stringRequest = new StringRequest(Request.Method.POST, Urls.reportrequest, new Response.Listener<String>() {
            @Override
            public void onResponse(String response) {
                try {
                    sessionManager.progressdialogdismiss();
                    JSONObject obj = new JSONObject(response);
                    if (obj.getBoolean("error") == true) {
                        String msg = obj.getString("message");
//                        Toast.makeText(getContext(), msg, Toast.LENGTH_SHORT).show();
                    } else {
                        String msg1 = obj.getString("message");
//                        Toast.makeText(getContext(), msg1, Toast.LENGTH_SHORT).show();

                    }
                } catch (Exception e) {
                    e.printStackTrace();
                }

            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                sessionManager.progressdialogdismiss();
                Toast.makeText(getContext(), error.toString(), Toast.LENGTH_SHORT).show();
            }
        }) {
            @Override
            public String getBodyContentType() {
                return "application/json; charset=utf-8";
            }

            @Override
            public byte[] getBody() throws com.android.volley.AuthFailureError {
                Log.d("roportliaBody", "" + roportliaBody.toString());
                return roportliaBody.toString().getBytes(StandardCharsets.UTF_8);
            }

            ;
        };
        stringRequest.setRetryPolicy(new DefaultRetryPolicy(10000, DefaultRetryPolicy.DEFAULT_MAX_RETRIES, DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
        requestQueue.add(stringRequest);
    }
}