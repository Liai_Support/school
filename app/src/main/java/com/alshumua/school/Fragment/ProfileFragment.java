package com.alshumua.school.Fragment;

import android.annotation.SuppressLint;
import android.app.Activity;
import android.app.AlertDialog;
import android.content.ComponentName;
import android.content.ContentResolver;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.content.pm.ResolveInfo;
import android.database.Cursor;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Matrix;
import android.graphics.drawable.ColorDrawable;
import android.media.ExifInterface;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;

import androidx.activity.OnBackPressedCallback;
import androidx.activity.OnBackPressedDispatcher;
import androidx.annotation.RequiresApi;
import androidx.constraintlayout.widget.ConstraintLayout;
import androidx.core.content.FileProvider;
import androidx.databinding.DataBindingUtil;
import androidx.fragment.app.Fragment;
import androidx.lifecycle.LifecycleOwner;

import android.os.Environment;
import android.os.Parcelable;
import android.provider.DocumentsContract;
import android.provider.MediaStore;
import android.util.Log;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.webkit.MimeTypeMap;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

//import com.alshumua.school.Activity.BriefActivity;
import com.alshumua.school.Activity.NewSignin;
import com.alshumua.school.Std.Constants;

import com.alshumua.school.Std.Dialog;
import com.alshumua.school.Std.SessionManager;
import com.alshumua.school.Std.Urls;
import com.alshumua.school.Std.VolleyMultipartRequest;
import com.alshumua.school.databinding.FragmentProfileBinding;
import com.android.volley.AuthFailureError;
import com.android.volley.DefaultRetryPolicy;
import com.android.volley.NetworkResponse;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.VolleyLog;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;
import com.alshumua.school.Activity.DashboardActivity;
import com.alshumua.school.R;
import com.squareup.picasso.Picasso;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.IOException;
import java.io.InputStream;
import java.io.UnsupportedEncodingException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class ProfileFragment extends Fragment {
    //var
    SessionManager sessionManager;
    View view;
    FragmentProfileBinding binding;
    @RequiresApi(api = Build.VERSION_CODES.M)
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View layout = LayoutInflater.from(requireContext()).inflate(R.layout.fragment_profile, container, false);
        binding = FragmentProfileBinding.bind(layout);

        sessionManager = new SessionManager(getActivity(), getContext(), view);
        if (!sessionManager.isNetwork() == true) {
            Dialog dialog = new Dialog();
            dialog.show(getChildFragmentManager(), Constants.NONETWORK);
        }
        if (sessionManager.getlang().equals(Constants.ARABICKEY)){
            binding.langarrow.setRotationY(180);
            binding.aboutarrow.setRotationY(180);
            binding.privacyarrow.setRotationY(180);
            binding.faqarrow.setRotationY(180);
            binding.contactarrow.setRotationY(180);
            binding.reportarrow.setRotationY(180);
            binding.backFromProfile.setRotationY(180);
//public void arabicfont(TextView textview, EditText editText,Button button,String style, String type)
            sessionManager.arabicfont(binding.guestuname,null,null,Constants.BOLD,"textV");
            sessionManager.arabicfont(binding.existuname,null,null,Constants.BOLD,"textV");
            sessionManager.arabicfont(binding.guestemail,null,null,Constants.REGULAR,"textV");
            sessionManager.arabicfont(binding.existemail,null,null,Constants.REGULAR,"textV");
            sessionManager.arabicfont(binding.guestmobile,null,null,Constants.REGULAR,"textV");
            sessionManager.arabicfont(binding.existmobile,null,null,Constants.REGULAR,"textV");

            sessionManager.arabicfont(binding.myprofiletoolbar,null,null,Constants.BOLD,"textV");
            sessionManager.arabicfont(binding.language,null,null,Constants.REGULAR,"textV");
            sessionManager.arabicfont(binding.report,null,null,Constants.REGULAR,"textV");
            sessionManager.arabicfont(binding.privacypol,null,null,Constants.REGULAR,"textV");
            sessionManager.arabicfont(binding.aboutus,null,null,Constants.REGULAR,"textV");
            sessionManager.arabicfont(binding.contactus,null,null,Constants.REGULAR,"textV");
            sessionManager.arabicfont(binding.faq,null,null,Constants.REGULAR,"textV");
            sessionManager.arabicfont(null,null,binding.logoutbtn,Constants.BOLD,"btn");
        }
if (sessionManager.gettokenin().equals("")){
    sessionManager.loginRequiredDialog(getContext(),"profile");
}

            if (sessionManager.gettype().equals("exist")){
                Log.d("", "onCreateView:existt ");
                binding.guestprofilecard.setVisibility(View.GONE);
                binding.existprofilecard.setVisibility(View.VISIBLE);
                viewProfileExist();
            }
          else{
                Log.d("", "onCreateView:guestt ");
                binding.guestprofilecard.setVisibility(View.VISIBLE);
                binding.existprofilecard.setVisibility(View.GONE);
                viewProfileGuest();
            }




      /*  if (!sessionManager.getprofile_img().equals("https://liastaging.com/shumua/uploads/") || sessionManager.getprofile_img() != null) {
            Picasso.get().load(sessionManager.getprofile_img()).placeholder(R.drawable.profiledefault).error(R.drawable.profiledefault).into(profilepic);
        }*/
      /*  else {
            Picasso.get().load(R.drawable.ic_language_img).placeholder(R.drawable.ic_signin_icon).error(R.drawable.ic_language_img).into(profilepic);

        }*/




//back press
        OnBackPressedDispatcher onBackPressedDispatcher = getActivity().getOnBackPressedDispatcher();
        if (onBackPressedDispatcher != null) {
            onBackPressedDispatcher.addCallback((LifecycleOwner)this, (OnBackPressedCallback)(new OnBackPressedCallback(true) {
                public void handleOnBackPressed() {
                    this.setEnabled(false);
                    Intent i = new Intent(getActivity(), DashboardActivity.class);
                    i.putExtra("pro","pro");
                    startActivity(i);
                    ((Activity) getActivity()).overridePendingTransition(0, 0);
                    ((Activity) getActivity()).finish();

                }
            }));
        }
        binding.backFromProfile.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent i = new Intent(getActivity(), DashboardActivity.class);
                i.putExtra("pro","pro");
                startActivity(i);
                ((Activity) getActivity()).overridePendingTransition(0, 0);
                ((Activity) getActivity()).finish();
            }
        });



//        btn clicks
        binding.profilesetting.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (sessionManager.gettokenin().equals("")){
                    sessionManager.loginRequiredDialog(getContext(),"profile");
                }
                Fragment fragment = new ProfileSettingFragment();
//                Fragment fragment = new ProFragment();
                getActivity().getSupportFragmentManager().beginTransaction().replace(R.id.fragment_container, fragment).addToBackStack(null).commit();
            }
        });


        binding.profileAboutUsBackbtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
               /* Intent ieventreport = new Intent(getContext(), BriefActivity.class);
                getContext().startActivity(ieventreport);*/
                Intent i = new Intent(getActivity(), DashboardActivity.class);
                i.putExtra("aboutus","aboutus");
                startActivity(i);
                ((Activity) getActivity()).overridePendingTransition(0, 0);
                ((Activity) getActivity()).finish();
//                Fragment fragment = new BriefFragment();
//                getActivity().getSupportFragmentManager().beginTransaction().replace(R.id.fragment_container, fragment).addToBackStack(null).commit();
            }
        });
        binding.profileLangBackbtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Fragment fragment = new Language_ProfileFragment();
                getActivity().getSupportFragmentManager().beginTransaction().replace(R.id.fragment_container, fragment).addToBackStack(null).commit();
            }
        });
        binding.profilePrivacyPolicyBackbtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Fragment fragment = new PrivacyFragment();
                getActivity().getSupportFragmentManager().beginTransaction().replace(R.id.fragment_container, fragment).addToBackStack(null).commit();
            }
        });
        binding.profileFAQBackbtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Fragment fragment = new FAQFragment();
                getActivity().getSupportFragmentManager().beginTransaction().replace(R.id.fragment_container, fragment).addToBackStack(null).commit();
            }
        });
        binding.profileContactUsBackbtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (sessionManager.gettokenin().equals("")){
                    sessionManager.loginRequiredDialog(getContext(),"profile");
                }
                Fragment fragment = new ContactUsFragment();
                getActivity().getSupportFragmentManager().beginTransaction().replace(R.id.fragment_container, fragment).addToBackStack(null).commit();
            }
        });
        binding.profileReportBackbtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (sessionManager.gettokenin().equals("")){
                    sessionManager.loginRequiredDialog(getContext(),"profile");
                }
                Fragment fragment = new ReportFragment();
                getActivity().getSupportFragmentManager().beginTransaction().replace(R.id.fragment_container, fragment).addToBackStack(null).commit();
            }
        });
        binding.logoutbtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                sessionManager.settoken("");
                sessionManager.setguestuser(0);
                sessionManager.setexnewpass(0);
                sessionManager.setgunewpass(0);
                sessionManager.setusernamedisplay("");
                sessionManager.setid(0);
                sessionManager.setserviceid(0);
                sessionManager.settokenin("");
                sessionManager.setpassword("");
                sessionManager.setprofile_img("");
                sessionManager.setguprofile_img("");
                sessionManager.setexprofile_img("");
                sessionManager.settype("");
                sessionManager.setfcmtok("");
                sessionManager.setwelcomeslidedone(0);
                getActivity().finish();
                Intent intent = new Intent(getActivity(), NewSignin.class);
                intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
                startActivity(intent);
            }
        });
        return binding.getRoot();

    }

    private void viewProfileGuest() {
        JSONObject requestbody = new JSONObject();
        try {
            requestbody.put("id", sessionManager.getid());
            ViewProfileGuestJSON(requestbody);
            sessionManager.progressdialogshow();
        } catch (JSONException e) {
            e.printStackTrace();
        }
    }

    private void viewProfileExist() {
        JSONObject requestbody = new JSONObject();
        try {
            requestbody.put("id", sessionManager.getid());
            ViewProfileExistJSON(requestbody);
            sessionManager.progressdialogshow();
        } catch (JSONException e) {
            e.printStackTrace();
        }
    }

    private void ViewProfileGuestJSON(JSONObject response) {
        RequestQueue requestQueue = Volley.newRequestQueue(getContext());
        final String requestBody = response.toString();
        Log.d("profile", String.valueOf(response));
        StringRequest stringRequest = new StringRequest(Request.Method.POST, Urls.Viewprofile, new Response.Listener<String>() {
            @Override
            public void onResponse(String response) {
                sessionManager.progressdialogdismiss();
                Log.d("viewprofileguest", ">>" + response);
                try {
                    JSONObject obj = new JSONObject(response);
                    if (obj.getBoolean("error") == true) {
                        String msg = obj.getString("message");
                        Log.d("loginerror", "" + msg);
                    } else {
                        String msg = obj.getString("data");
                        JSONObject tok = new JSONObject(msg);

                        sessionManager.setusernamedisplay(tok.getString("username"));
                        sessionManager.setguprofile_img(tok.getString("profile_img"));

                        sessionManager.setgufirstname(tok.getString("first_name"));
                        sessionManager.setgulastname(tok.getString("last_name"));
                        sessionManager.setguemail(tok.getString("email"));
                        sessionManager.setgumobile(tok.getString("mobile_number"));
                        sessionManager.setguparentname(tok.getString("parent_name"));
                        sessionManager.settype(tok.getString("type"));

                        binding.guestuname.setText(sessionManager.getusernamedisplay());

                            binding.guestemail.setText(sessionManager.getguemail());
                        binding.guestmobile.setText(sessionManager.getgumobile());


                        if (!sessionManager.getguprofile_img().equals(Constants.PROFILEUPDATEIMG_EMPTY_URL)) {
                            Picasso.get().load(sessionManager.getguprofile_img()).placeholder(R.drawable.profiledefault).error(R.drawable.profiledefault).into(binding.guestprofileImg);
                        }
                        else{
                            Picasso.get().load(R.drawable.profiledefault).into(binding.guestprofileImg);

                        }
                    }
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                Log.e("VOLLEY", error.toString());
//                sessionManager.volleyerror(error);
                sessionManager.progressdialogdismiss();
                Toast.makeText(getContext(), error.getMessage(), Toast.LENGTH_SHORT).show();
            }
        }) {
            @Override
            public Map<String, String> getHeaders() throws AuthFailureError {
                int id = sessionManager.getid();
                Map<String, String> params = new HashMap<String, String>();
                params.put("lang",sessionManager.getlang() );
//                params.put("Authorization", "Bearer " + sessionManager.gettoken());
                Log.d("param", "" + params);
                Log.d("id", "" + id);
                return params;
            }

            @Override
            public String getBodyContentType() {
                return "application/json; charset=utf-8";
            }

            @Override
            public byte[] getBody() throws AuthFailureError {
                try {
                    return requestBody == null ? null : requestBody.getBytes("utf-8");
                } catch (UnsupportedEncodingException uee) {
                    VolleyLog.wtf("Unsupported Encoding while trying to get the bytes of %s using %s", requestBody, "utf-8");
                    return null;
                }
            }
        };
        stringRequest.setRetryPolicy(new DefaultRetryPolicy(10000, DefaultRetryPolicy.DEFAULT_MAX_RETRIES, DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
        Log.d("asdsdjkxdk", "" + stringRequest);
        requestQueue.add(stringRequest);
    }

    private void ViewProfileExistJSON(JSONObject response) {
        RequestQueue requestQueue = Volley.newRequestQueue(getContext());
        final String requestBody = response.toString();
        Log.d("profile", String.valueOf(response));
        StringRequest stringRequest = new StringRequest(Request.Method.POST, Urls.Viewprofile, new Response.Listener<String>() {
            @Override
            public void onResponse(String response) {
                sessionManager.progressdialogdismiss();
                Log.d("viewprofileguest", ">>" + response);
                try {
                    JSONObject obj = new JSONObject(response);
                    if (obj.getBoolean("error") == true) {
                        String msg = obj.getString("message");
                        Log.d("loginerror", "" + msg);
                    } else {
                        String msg = obj.getString("data");
                        JSONObject tok = new JSONObject(msg);

                        sessionManager.setusernamedisplay(tok.getString("username"));
                        sessionManager.setexprofile_img(tok.getString("profile_img"));

                        sessionManager.setexstudentname(tok.getString("first_name"));
//                        sessionManager.setexparentname(tok.getString("last_name"));
                        sessionManager.setexemail(tok.getString("email"));
                        sessionManager.setexmobile(tok.getString("mobile_number"));
                        sessionManager.setexparentname(tok.getString("parent_name"));
                        sessionManager.settype(tok.getString("type"));

                        binding.existuname.setText(sessionManager.getusernamedisplay());
                        binding.existemail.setText(sessionManager.getexemail());
                        binding.existmobile.setText(sessionManager.getexmobile());


                        if (!sessionManager.getexprofile_img().equals(Constants.PROFILEUPDATEIMG_EMPTY_URL)) {
                            Picasso.get().load(sessionManager.getexprofile_img()).placeholder(R.drawable.profiledefault).error(R.drawable.profiledefault).into(binding.existprofileImg);
                        }
                        else{
                            Picasso.get().load(R.drawable.profiledefault).into(binding.existprofileImg);

                        }

                    }
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                Log.e("VOLLEY", error.toString());
//                sessionManager.volleyerror(error);
                sessionManager.progressdialogdismiss();
                Toast.makeText(getContext(), error.getMessage(), Toast.LENGTH_SHORT).show();
            }
        }) {
            @Override
            public Map<String, String> getHeaders() throws AuthFailureError {
                int id = sessionManager.getid();
                Map<String, String> params = new HashMap<String, String>();
                params.put("lang",sessionManager.getlang() );
                Log.d("param", "" + params);
                return params;
            }

            @Override
            public String getBodyContentType() {
                return "application/json; charset=utf-8";
            }

            @Override
            public byte[] getBody() throws AuthFailureError {
                try {
                    return requestBody == null ? null : requestBody.getBytes("utf-8");
                } catch (UnsupportedEncodingException uee) {
                    VolleyLog.wtf("Unsupported Encoding while trying to get the bytes of %s using %s", requestBody, "utf-8");
                    return null;
                }
            }
        };
        stringRequest.setRetryPolicy(new DefaultRetryPolicy(10000, DefaultRetryPolicy.DEFAULT_MAX_RETRIES, DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
        Log.d("asdsdjkxdk", "" + stringRequest);
        requestQueue.add(stringRequest);
    }
    /*LayoutInflater inflater = (LayoutInflater)context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
    View dialoglayout = inflater.inflate(R.layout.popup_no_network, null);
        dialoglayout.setMinimumHeight(100);
        dialoglayout.setMinimumWidth(100);
        dialoglayout.setBackgroundDrawable(null);*/
    @RequiresApi(api = Build.VERSION_CODES.M)
    private void showWarningDialog(){
        AlertDialog.Builder builder =
                new AlertDialog.Builder
                        (getContext(), R.style.AppTheme);
       /* LayoutInflater inflater = (LayoutInflater)getContext().getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        View view = inflater.inflate(R.layout.custom_dig, null);*/
        View view = LayoutInflater.from(getContext()).inflate(R.layout.custom_dig,
               null
        );
view.setForegroundGravity(Gravity.CENTER_VERTICAL);
        builder.setView(view);
      /*  ((TextView) view.findViewById(R.id.textTitle))
                .setText(getResources().getString(R.string.warning_title));*/
        ((TextView) view.findViewById(R.id.textMessage))
                .setText(getResources().getString(R.string.PleaseLogintoAccesstheFeatures));
        ((Button) view.findViewById(R.id.buttonYes))
                .setText(getResources().getString(R.string.login));
        ((Button) view.findViewById(R.id.buttonNo))
                .setText(getResources().getString(R.string.cancel));
       /* ((ImageView) view.findViewById(R.id.imageIcon))
                .setImageResource(R.drawable.warning);*/
        final AlertDialog alertDialog = builder.create();
        view.findViewById(R.id.buttonYes).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
//                alertDialog.dismiss();
                Intent ieventreport = new Intent(getContext(),NewSignin.class);
                getContext().startActivity(ieventreport);
            }
        });
        view.findViewById(R.id.buttonNo).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
//                alertDialog.dismiss();
                Intent i = new Intent(getActivity(), DashboardActivity.class);
                i.putExtra("pro","pro");
                startActivity(i);
                ((Activity) getActivity()).overridePendingTransition(0, 0);
                ((Activity) getActivity()).finish();
            }
        });
        if (alertDialog.getWindow() != null){
            alertDialog.getWindow().setBackgroundDrawable(new ColorDrawable(0));
        }
        alertDialog.show();
    }

}