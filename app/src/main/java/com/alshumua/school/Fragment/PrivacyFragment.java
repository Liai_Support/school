package com.alshumua.school.Fragment;

import android.os.Bundle;

import androidx.fragment.app.Fragment;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;

import com.alshumua.school.Std.Constants;
import com.alshumua.school.Std.SessionManager;
import com.alshumua.school.R;

public class PrivacyFragment extends Fragment {
View view;
ImageView back_img;
SessionManager sessionManager;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        view= inflater.inflate(R.layout.fragment_privacy, container, false);
        back_img = (ImageView) view.findViewById(R.id.back_from_privacy);

        sessionManager = new SessionManager(getActivity(),getContext(),view);
        if (sessionManager.getlang().equals(Constants.ARABICKEY)){
            back_img.setRotationY(180);
        }
        back_img.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Fragment fragment = new ProfileFragment();
                getActivity().getSupportFragmentManager().beginTransaction().replace(R.id.fragment_container, fragment).addToBackStack(null).commit();
            }
        });
        return view;
    }
}