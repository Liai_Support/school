package com.alshumua.school.Fragment;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.database.Cursor;
import android.graphics.Color;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;

import androidx.activity.OnBackPressedCallback;
import androidx.activity.OnBackPressedDispatcher;
import androidx.annotation.Nullable;
import androidx.cardview.widget.CardView;
import androidx.fragment.app.Fragment;
import androidx.lifecycle.LifecycleOwner;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.provider.MediaStore;
import android.provider.OpenableColumns;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ProgressBar;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.alshumua.school.Activity.RequestSentActivity;
import com.alshumua.school.Adapter.ServiceRequestAdapter;
import com.alshumua.school.Model.ServiceRequestModel;

import com.alshumua.school.R;
import com.alshumua.school.Std.Constants;
import com.alshumua.school.Std.SessionManager;
import com.alshumua.school.Std.Urls;
import com.alshumua.school.Std.VolleyMultipartRequest;
import com.android.volley.AuthFailureError;
import com.android.volley.DefaultRetryPolicy;
import com.android.volley.NetworkResponse;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.VolleyLog;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;
import com.alshumua.school.Activity.DashboardActivity;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.ByteArrayOutputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStream;
import java.io.UnsupportedEncodingException;
import java.net.URISyntaxException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

import static android.app.Activity.RESULT_OK;
import static android.content.ContentValues.TAG;
import static java.lang.Double.parseDouble;

public class FormFragment extends Fragment {
//    VAR
    View view;
    SessionManager sessionManager;
    Button sendform;
    ImageView backBtn;
    public JSONArray emptyArr = new JSONArray();
    public JSONArray emptyArr2 = new JSONArray();
    public TextView uploadTxtt;
    public TextView Filename,servicenameTV,percentagetxt;
    private static ArrayList<ServiceRequestModel> requestModels;
    private static RecyclerView.Adapter requestAdapter;
    private static RecyclerView requestRecyclerview;
    private RequestQueue rQueue;
    public int imagevalueposition = 0;
    public double singleper =0.00;
CardView progresscard;
   public ProgressBar progressBar;
   RelativeLayout progresscardrelative;
    private int CurrentProgress=0;
    //    BUNDLE
    Bundle args = new Bundle();
//    Bundle arginitial = new Bundle();
    String  servicenameTvStr,initialassReqBodyStr,serviceIdStr;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        view = inflater.inflate(R.layout.fragment_form, container, false);
// HOOK
        sendform = (Button) view.findViewById(R.id.sendform);
        backBtn = (ImageView) view.findViewById(R.id.back_from_form);
        servicenameTV = (TextView) view.findViewById(R.id.servicenameform);
        percentagetxt = (TextView) view.findViewById(R.id.percentage);
        progressBar = (ProgressBar) view.findViewById(R.id.progressBar1);
        progresscard = (CardView) view.findViewById(R.id.progresscard);
        progresscardrelative = (RelativeLayout) view.findViewById(R.id.progresscardrelative);
        requestRecyclerview = (RecyclerView) view.findViewById(R.id.requestRecyclerView);
        requestRecyclerview.setLayoutManager(new LinearLayoutManager(getContext()));
        requestModels = new ArrayList<ServiceRequestModel>();
        sessionManager = new SessionManager(getActivity(),getContext(),view);

//  FETCH ALL QUESTIONS
        progresscardrelative.setVisibility(View.GONE);
        progresscard.setVisibility(View.GONE);

//  ROTATE ARROW ON ARABIC
        if (sessionManager.getlang().equals(Constants.ARABICKEY)){
            backBtn.setRotationY(180);
            sessionManager.arabicfont(null,null, sendform,Constants.BOLD,"btn");
            sessionManager.arabicfont(servicenameTV,null,null,Constants.BOLD,"textV");
        }
//GET BUNDLE DATA
        args = getArguments();
//        arginitial = getArguments();
        servicenameTvStr = args.getString("serviceName");
        serviceIdStr = args.getString("serviceid");


        Log.d(TAG, "servicenameTvStr "+servicenameTvStr);
        Log.d(TAG, "symptom_ids "+initialassReqBodyStr);
        if (!servicenameTvStr.isEmpty()){
            servicenameTV.setText(servicenameTvStr);
        }
        if (!sessionManager.getselectedSymtomId().equals("")&&serviceIdStr.equals("9")){
            Log.d(TAG, "loop check 111");
            initialassReqBodyStr = args.getString("symptom_ids");
            progresscardrelative.setVisibility(View.VISIBLE);
            progresscard.setVisibility(View.VISIBLE);

sendrequestFormbodyInitial();
            Log.d(TAG, "sendFormdata: "+emptyArr);
        }
        else {
            progresscardrelative.setVisibility(View.GONE);
            progresscard.setVisibility(View.GONE);
            sendrequestFormbody();
        }
//  BUTTON CLICK
        backBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent i = new Intent(getActivity(), DashboardActivity.class);
                i.putExtra("pro","pro");
                startActivity(i);
                ((Activity) getActivity()).overridePendingTransition(0, 0);
                ((Activity) getActivity()).finish();
            }
        });
        sendform.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                    if (isMandatory() == true) {

                    } else {
                        if (!sessionManager.getselectedSymtomId().equals("")&&serviceIdStr.equals("9")){
                            sendFormdataInitial();
                        }
                        else {
                            sendFormdata();
                        }

                    }
                }
        });
        OnBackPressedDispatcher onBackPressedDispatcher = getActivity().getOnBackPressedDispatcher();
        if (onBackPressedDispatcher != null) {
            onBackPressedDispatcher.addCallback((LifecycleOwner) this, (OnBackPressedCallback) (new OnBackPressedCallback(true) {
                public void handleOnBackPressed() {
                    this.setEnabled(false);
                    Intent i = new Intent(getActivity(), DashboardActivity.class);
                    i.putExtra("mypro", "mypro");
                    startActivity(i);
                    ((Activity) getActivity()).overridePendingTransition(0, 0);
                    ((Activity) getActivity()).finish();

                }
            }));
        }

 /*       if (Build.VERSION.SDK_INT >= 11) {
            requestRecyclerview.addOnLayoutChangeListener(new View.OnLayoutChangeListener() {
                @Override
                public void onLayoutChange(View v,
                                           int left, int top, int right, int bottom,
                                           int oldLeft, int oldTop, int oldRight, int oldBottom) {
                    if (bottom < oldBottom) {
                        requestRecyclerview.postDelayed(new Runnable() {
                            @Override
                            public void run() {
                                requestRecyclerview.smoothScrollToPosition(
                                        requestRecyclerview.getAdapter().getItemCount() - 1);
                            }
                        }, 100);
                    }
                }
            });
        }
        if (bottom < oldBottom) {
            requestRecyclerview.postDelayed(new Runnable() {
                @Override
                public void run() {
                    requestRecyclerview.smoothScrollToPosition(
                            requestRecyclerview.getAdapter().getItemCount() - 1);
                }
            }, 100);
        }*/

        return view;
    }

    private boolean isMandatory(){
        for(int i=0;i<emptyArr2.length();i++){
            try {
                JSONObject jsonObject = emptyArr2.getJSONObject(i);
                if(jsonObject.getString("mandatory").equalsIgnoreCase("on") &&
                        jsonObject.getString("manval").equalsIgnoreCase("default")){
                    sessionManager.snackbarToast(getString(R.string.pleaseenter)+" "+jsonObject.getString("question"), sendform);
                    return true;

                }
                else if (jsonObject.getString("mandatory").equalsIgnoreCase("on") &&
                        !jsonObject.getString("manval").equalsIgnoreCase("default")){
                    if (jsonObject.getString("inputType").equals("email")){
                        String email=jsonObject.getString("value").trim();
                        if (isValidEmailId(email)){
                        }
                        else {
                            sessionManager.snackbarToast(getString(R.string.pleaseentervalidemail), sendform);
                            return true;
                        }

                    }
                    else if (jsonObject.getString("inputType").equals("phone")&&
                            !jsonObject.getString("manval").equalsIgnoreCase("default")) {
                        String phone = jsonObject.getString("value").trim();
                        if (isValidPhone(phone)) {
                        } else {
                            sessionManager.snackbarToast(getString(R.string.pleaseentervalidmobilenumber), sendform);
                            return true;
                        }
                    }

                }

            } catch (JSONException e) {
                e.printStackTrace();
            }

        }
        return false;
    }

    private boolean isValidEmailId(String email) {
        return android.util.Patterns.EMAIL_ADDRESS.matcher(email).matches();
    }
    private boolean isValidPhone(String phone) {
        return android.util.Patterns.PHONE.matcher(phone).matches();
    }
//symptom_id
private void sendFormdataInitial() {
    sessionManager.progressdialogshow();
    JSONObject formBody = new JSONObject();
    try {
        formBody.put("userid",sessionManager.getid());
        formBody.put("service_id",sessionManager.getserviceid());
        formBody.put("data",emptyArr.toString());
        formBody.put("symptom_id",sessionManager.getselectedSymtomId());
        formBody.put("entry_from","android");
        FormJSON(formBody);
    } catch (
            JSONException e) {
        e.printStackTrace();
    }
}
    private void sendFormdata() {
        sessionManager.progressdialogshow();
        JSONObject formBody = new JSONObject();
        Log.d(TAG, "sendFormdata: "+emptyArr);
        try {
            formBody.put("userid",sessionManager.getid());
            formBody.put("service_id",sessionManager.getserviceid());
            formBody.put("data",emptyArr.toString());
            formBody.put("symptom_id","");
            formBody.put("entry_from","android");
            FormJSON(formBody);
        } catch (
                JSONException e) {
            e.printStackTrace();
        }
    }

    private void FormJSON(JSONObject response) {
        RequestQueue requestQueue = Volley.newRequestQueue(getContext());
        final String requestBody = response.toString();
        Log.d("form>>",String.valueOf(response));
        StringRequest stringRequest = new StringRequest(Request.Method.POST, Urls.SendRequest, new Response.Listener<String>() {
            @Override
            public void onResponse(String response) {
                Log.d("response", ">>" + response);
                sessionManager.progressdialogdismiss();
                try {
                    JSONObject obj = new JSONObject(response);
                    if (obj.getBoolean("error") == false) {
                        getActivity().finish();
                        Intent intent = new Intent(getActivity(), RequestSentActivity.class);
                        startActivity(intent);

                        sessionManager.snackbarToast(getString( R.string.Requestcreatedsuccessfully), sendform);

                    }
                    else {

                        Log.d("erorr", "onResponse: ");
                        sessionManager.snackbarToast(getString( R.string.error), sendform);
                    }
                } catch (Exception e) {
                    e.printStackTrace();
                }

            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                sessionManager.progressdialogdismiss();
                Log.e("VOLLEY", error.toString());
//                sessionManager.volleyerror(error);
                Toast.makeText(getContext(), error.getMessage(), Toast.LENGTH_SHORT).show();
            }
        }){

            @Override
            public Map<String, String> getHeaders() throws AuthFailureError {
                Map<String, String> params = new HashMap<String, String>();
                params.put("lang",sessionManager.getlang() );
                Log.d("param", "" + params);
                return params;
            }
            @Override
            public String getBodyContentType() {
                return "application/json; charset=utf-8";
            }

            @Override
            public byte[] getBody() throws AuthFailureError {
                try {
                    return requestBody == null ? null : requestBody.getBytes("utf-8");
                } catch (UnsupportedEncodingException uee) {
                    VolleyLog.wtf("Unsupported Encoding while trying to get the bytes of %s using %s", requestBody, "utf-8");
                    return null;
                }
            }
        };
        stringRequest.setRetryPolicy(new DefaultRetryPolicy(10000, DefaultRetryPolicy.DEFAULT_MAX_RETRIES, DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
        Log.d("asdsdjkxdk",""+stringRequest);
        requestQueue.add(stringRequest);
    }
    private void sendrequestFormbody() {
        sessionManager.progressdialogshow();
        JSONObject servicereqBody = new JSONObject();
        try {
            servicereqBody.put("service_id",sessionManager.getserviceid());
            servicerequestFormJSON(servicereqBody);
        } catch (
                JSONException e) {
            e.printStackTrace();
        }
    }
    private void sendrequestFormbodyInitial() {
        sessionManager.progressdialogshow();
        JSONObject servicereqBody = new JSONObject();
        try {
            servicereqBody.put("service_id",sessionManager.getserviceid());
            servicereqBody.put("symptom_ids",sessionManager.getselectedSymtomId());
            Log.d(TAG, "sendrequestFormbodyInitial: "+sessionManager.getselectedSymtomId());
            servicerequestFormJSON(servicereqBody);
        } catch (
                JSONException e) {
            e.printStackTrace();
        }
    }
    private void servicerequestFormJSON(JSONObject response) {
        RequestQueue requestQueue = Volley.newRequestQueue(getContext());
        final String requestBody = response.toString();
        Log.d("servicerequestid",String.valueOf(response));
        StringRequest stringRequest = new StringRequest(Request.Method.POST,Urls.ServicesRequest, new Response.Listener<String>() {
            @Override
            public void onResponse(String response) {
                Log.d("serviceRequestresponse", ">>" + response);
                sessionManager.progressdialogdismiss();
                try {
                    JSONObject obj = new JSONObject(response);
                    if (obj.getBoolean("error") == false) {
                        singleper = parseDouble(String.valueOf(obj.getInt("noOfQuestions"))) / 100.00 ;
                        Log.d("singleper",""+obj.getInt("noOfQuestions")+"/"+"100="+singleper);
                        requestModels = new ArrayList<ServiceRequestModel>();
                        JSONArray obj1 = new JSONArray(obj.getString("data"));
                        Log.d(TAG, "obj1 len: "+obj1.length());
                        for (int i = 0; i < obj1.length(); i++) {
                            JSONObject jsonObject = obj1.getJSONObject(i);
                            ServiceRequestModel datamodel = new ServiceRequestModel(
                                    getContext(),
                                    jsonObject.getInt("id"),
                                    jsonObject.getInt("noOfFields"),
                                    obj.getInt("noOfQuestions"),
                                    jsonObject.getString("subHead"),
                                    jsonObject.getString("question"),
                                    jsonObject.getString("fieldType"),
                                    jsonObject.getString("answerFields"),
                                    jsonObject.getString("mandatory"),
                                    jsonObject.getString("inputType"),
                                    jsonObject.getString("description"),
                                    jsonObject.getString("autofill"),
                                    jsonObject.getString("ifAnswered")
                                    );
                            requestModels.add(datamodel);
                            Log.d(TAG, "noOfQuestions "+obj.getInt("noOfQuestions"));
                        }

                        for (int j = 0; j < obj1.length() ; j++)
                        {
                            JSONObject emptyobj = new JSONObject();
                            JSONObject emptyobj2 = new JSONObject();
                            if(obj1.getJSONObject(j).getString("fieldType").equalsIgnoreCase("textbox") ||
                                    obj1.getJSONObject(j).getString("fieldType").equalsIgnoreCase("textarea") ||
                                    obj1.getJSONObject(j).getString("fieldType").equalsIgnoreCase("radio") ||
                                    obj1.getJSONObject(j).getString("fieldType").equalsIgnoreCase("checkbox") ||
                                    obj1.getJSONObject(j).getString("fieldType").equalsIgnoreCase("dropdown")||
                                    obj1.getJSONObject(j).getString("fieldType").equalsIgnoreCase("file")){
                                emptyobj.put("question", obj1.getJSONObject(j).getString("question"));
                                emptyobj.put("value", "default");

                                emptyobj.put("description", obj1.getJSONObject(j).getString("description"));
                                emptyobj.put("descvalue", "default");

                                emptyobj2.put("question", obj1.getJSONObject(j).getString("question"));
                                emptyobj2.put("value", "default");

                                emptyobj2.put("mandatory", obj1.getJSONObject(j).getString("mandatory"));
                                emptyobj2.put("manval", "default");

                                emptyobj2.put("inputType",obj1.getJSONObject(j).getString("inputType"));
                                emptyobj2.put("inputval",obj1.getJSONObject(j).getString("inputType"));

                                emptyobj2.put("description", obj1.getJSONObject(j).getString("description"));
                                emptyobj2.put("descvalue", "default");

                                emptyobj2.put("ifAnswered", obj1.getJSONObject(j).getString("ifAnswered"));
                                emptyobj2.put("answered", "default");

                                emptyArr.put(emptyobj);
                                emptyArr2.put(emptyobj2);
                            }
                            else {
                                emptyobj.put("question", obj1.getJSONObject(j).getString("question"));
                                emptyobj.put("value", "");
                                emptyobj.put("description", obj1.getJSONObject(j).getString("description"));
                                emptyobj.put("descvalue", "");

                                emptyobj2.put("question", obj1.getJSONObject(j).getString("question"));
                                emptyobj2.put("mandatory", obj1.getJSONObject(j).getString("mandatory"));
                                emptyobj2.put("value", "");
                                Log.d(TAG, "mandatory> "+obj1.getJSONObject(j).getString("mandatory"));
                                Log.d(TAG, "mandatory1> "+ obj1.getJSONObject(j).getString("question"));
                                emptyArr.put(emptyobj);
                                emptyArr2.put(emptyobj2);
                            }
                        }
                        requestAdapter  = new ServiceRequestAdapter(FormFragment.this,getContext(),requestModels,((DashboardActivity)getActivity()));
                        requestRecyclerview.setAdapter(requestAdapter);

                    } else {
                        Log.d("erorrrrr", "onResponse: ");
                    }
                } catch (Exception e) {
                    e.printStackTrace();
                }

            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                sessionManager.progressdialogdismiss();
                Log.e("VOLLEY", error.toString());
//                sessionManager.volleyerror(error);
                Toast.makeText(getContext(), error.getMessage(), Toast.LENGTH_SHORT).show();
            }
        }){

            @Override
            public Map<String, String> getHeaders() throws AuthFailureError {
                Map<String, String> params = new HashMap<String, String>();
                params.put("lang",sessionManager.getlang() );
                Log.d("param", "" + params);
                return params;
            }
            @Override
            public String getBodyContentType() {
                return "application/json; charset=utf-8";
            }

            @Override
            public byte[] getBody() throws AuthFailureError {
                try {
                    return requestBody == null ? null : requestBody.getBytes("utf-8");
                } catch (UnsupportedEncodingException uee) {
                    VolleyLog.wtf("Unsupported Encoding while trying to get the bytes of %s using %s", requestBody, "utf-8");
                    return null;
                }
            }
        };
        stringRequest.setRetryPolicy(new DefaultRetryPolicy(10000, DefaultRetryPolicy.DEFAULT_MAX_RETRIES, DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
        Log.d("asdsdjkxdk",""+stringRequest);
        requestQueue.add(stringRequest);
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, @Nullable Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        switch (requestCode) {
            case 19:
                if (resultCode == RESULT_OK) {
                    // Get the Uri of the selected file
                    Uri uri = data.getData();
                    Log.d(TAG, "File Uri: " + uri.toString());
                    Log.d(TAG, "File Uri:> " + uri);
                    // Get the path
                    String path = "";
                    try {
                        path = getPath(getContext(), uri);
                    } catch (URISyntaxException e) {
                        e.printStackTrace();
                    }
                   /* Log.d(TAG, "File Path: " + path);
                    String filename1 = path.substring(path.lastIndexOf("/")+1);
                    Log.d(TAG, "File Path1: " + filename1);
                    File file = new File(path);
                    String strFileName = file.getName();
                    Log.d(TAG, "File Path2: " + strFileName);*/

                    String filename = getfilename(uri);
                    Log.d(TAG, "File Path3: " + filename);
                    uploadfile(filename,uri);
                    // Get the file instance
                    // File file = new File(path);
                    // Initiate the upload
                }
                break;
        }
    }

    public String getfilename(Uri uri) {

        // The query, because it only applies to a single document, returns only
        // one row. There's no need to filter, sort, or select fields,
        // because we want all fields for one document.
        Cursor cursor = getActivity().getContentResolver()
                .query(uri, null, null, null, null, null);

        try {
            // moveToFirst() returns false if the cursor has 0 rows. Very handy for
            // "if there's anything to look at, look at it" conditionals.
            if (cursor != null && cursor.moveToFirst()) {

                // Note it's called "Display Name". This is
                // provider-specific, and might not necessarily be the file name.
                String displayName = cursor.getString(
                        cursor.getColumnIndex(OpenableColumns.DISPLAY_NAME));
                Log.i(TAG, "Display Name: " + displayName);

                int sizeIndex = cursor.getColumnIndex(OpenableColumns.SIZE);
                // If the size is unknown, the value stored is null. But because an
                // int can't be null, the behavior is implementation-specific,
                // and unpredictable. So as
                // a rule, check if it's null before assigning to an int. This will
                // happen often: The storage API allows for remote files, whose
                // size might not be locally known.
                String size = null;
                if (!cursor.isNull(sizeIndex)) {
                    // Technically the column stores an int, but cursor.getString()
                    // will do the conversion automatically.
                    size = cursor.getString(sizeIndex);
                } else {
                    size = "Unknown";
                }
                Log.i(TAG, "Size: " + size);
                return displayName;
            }
        } finally {
            cursor.close();
        }
        return null;
    }

    private String getFileName(Uri uri) throws IllegalArgumentException {
        // Obtain a cursor with information regarding this uri
        Cursor cursor = getActivity().getContentResolver().query(uri, null, null, null, null);

        if (cursor.getCount() <= 0) {
            cursor.close();
            throw new IllegalArgumentException("Can't obtain file name, cursor is empty");
        }

        cursor.moveToFirst();

        String fileName = cursor.getString(cursor.getColumnIndexOrThrow(OpenableColumns.DISPLAY_NAME));

        cursor.close();

        return fileName;
    }

    public String getPath(Context context, Uri uri) throws URISyntaxException {
        if ("content".equalsIgnoreCase(uri.getScheme())) {
            String[] projection = { "_data" };
            Cursor cursor = null;

            try {
                cursor = context.getContentResolver().query(uri, projection, null, null, null);
                int column_index = cursor.getColumnIndexOrThrow("_data");
                if (cursor.moveToFirst()) {
                    return cursor.getString(column_index);
                }
            } catch (Exception e) {
                // Eat it
            }
        }
        else if ("file".equalsIgnoreCase(uri.getScheme())) {
            return uri.getPath();
        }

        return null;
    }

    public String getPath1(Uri uri) {

        String path = null;
        String[] projection = { MediaStore.Files.FileColumns.DATA };
        Cursor cursor = getActivity().getContentResolver().query(uri, projection, null, null, null);

        if(cursor == null){
            path = uri.getPath();
        }
        else{
            cursor.moveToFirst();
            int column_index = cursor.getColumnIndexOrThrow(projection[0]);
            path = cursor.getString(column_index);
            cursor.close();
        }

        return ((path == null || path.isEmpty()) ? (uri.getPath()) : path);
    }


    private void uploadfile(final String filename, Uri file){
        sessionManager.progressdialogshow();
        InputStream iStream = null;
        try {

            iStream = getActivity().getContentResolver().openInputStream(file);
            final byte[] inputData = getBytes(iStream);

            VolleyMultipartRequest volleyMultipartRequest = new VolleyMultipartRequest(Request.Method.POST, Urls.imageuploadForm,
                    new Response.Listener<NetworkResponse>() {
                        @Override
                        public void onResponse(NetworkResponse response) {
                            Log.d("ressssssoo",new String(response.data));
                            rQueue.getCache().clear();
                            try {
                                JSONObject jsonObject = new JSONObject(new String(response.data));
                                jsonObject.toString().replace("\\\\","");
                                sessionManager.progressdialogdismiss();
                                if (jsonObject.getString("error").equals("false")) {
                                    Log.d("come::: >>>  ","yessssss");
                                    JSONObject jsonObject1 = jsonObject.getJSONObject("data");
                                    String imagvalue = jsonObject1.getString("imageName");
                                    emptyArr.getJSONObject(imagevalueposition).put("value", imagvalue);

                                    emptyArr2.getJSONObject(imagevalueposition).put("value", imagvalue);
                                    emptyArr2.getJSONObject(imagevalueposition).put("manval", null);

                                    uploadTxtt.setVisibility(View.VISIBLE);
                                    uploadTxtt.setText(R.string.UploadedSuccessfully);

                                    Filename.setVisibility(View.VISIBLE);

//
                                }
                                else {
                                    Filename.setVisibility(View.GONE);
                                    uploadTxtt.setVisibility(View.VISIBLE);
                                    uploadTxtt.setText(R.string.Tryagain);
                                    uploadTxtt.setTextColor(Color.RED);

                                }
                            } catch (JSONException e) {
                                e.printStackTrace();
                            }
                        }
                    },
                    new Response.ErrorListener() {
                        @Override
                        public void onErrorResponse(VolleyError error) {
                            Toast.makeText(getContext(), error.getMessage(), Toast.LENGTH_SHORT).show();
                        }
                    }) {

                /*
                 * If you want to add more parameters with the image
                 * you can do it here
                 * here we have only one parameter with the image
                 * which is tags
                 * */
                @Override
                protected Map<String, String> getParams() throws AuthFailureError {
                    Map<String, String> params = new HashMap<>();
                    // params.put("tags", "ccccc");  add string parameters
                    return params;
                }

                /*
                 *pass files using below method
                 * */
                @Override
                protected Map<String, DataPart> getByteData() {
                    Map<String, DataPart> params = new HashMap<>();
                    params.put("image", new DataPart(filename ,inputData));
                    return params;
                }
            };


            volleyMultipartRequest.setRetryPolicy(new DefaultRetryPolicy(
                    0,
                    DefaultRetryPolicy.DEFAULT_MAX_RETRIES,
                    DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
            rQueue = Volley.newRequestQueue(getActivity());
            rQueue.add(volleyMultipartRequest);



        } catch (FileNotFoundException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }


    }

    public byte[] getBytes(InputStream inputStream) throws IOException {
        ByteArrayOutputStream byteBuffer = new ByteArrayOutputStream();
        int bufferSize = 1024;
        byte[] buffer = new byte[bufferSize];

        int len = 0;
        while ((len = inputStream.read(buffer)) != -1) {
            byteBuffer.write(buffer, 0, len);
        }
        return byteBuffer.toByteArray();
    }

}
