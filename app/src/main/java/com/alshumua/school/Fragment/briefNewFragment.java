package com.alshumua.school.Fragment;

import static android.content.ContentValues.TAG;

import android.graphics.Color;
import android.media.MediaPlayer;
import android.os.Bundle;

import androidx.activity.OnBackPressedCallback;
import androidx.activity.OnBackPressedDispatcher;
import androidx.fragment.app.Fragment;
import androidx.lifecycle.LifecycleOwner;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.text.Html;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.TextView;
import android.widget.Toast;

import com.alshumua.school.Adapter.BriefNewAdapter;
import com.alshumua.school.Adapter.SupportingAdapter;
import com.alshumua.school.Model.BriefModel;
import com.alshumua.school.Model.BriefNewModel;
import com.alshumua.school.Model.SupportingorgModel;
import com.alshumua.school.R;
import com.alshumua.school.Std.Constants;
import com.alshumua.school.Std.SessionManager;
import com.alshumua.school.Std.Urls;
import com.android.volley.AuthFailureError;
import com.android.volley.DefaultRetryPolicy;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.VolleyLog;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;
import com.bumptech.glide.Glide;
import com.google.android.material.bottomnavigation.BottomNavigationView;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.UnsupportedEncodingException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

public class briefNewFragment extends Fragment {
    View view;
    ImageView back;
    SessionManager sessionManager;
    Button goback;

    private static ArrayList<BriefNewModel> briefArrayList;
    private static RecyclerView.Adapter briefAdapter;
    private static RecyclerView briefRecyclerView;
    TextView toolbartxt;
    RadioButton rb;
    public  String tab="";
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        view = inflater.inflate(R.layout.fragment_briefnew_new, container, false);


//        back = (ImageView) view.findViewById(R.id.back_from_support);
//
//        goback = (Button) view.findViewById(R.id.gobacksupport);
//        toolbartxt = (TextView) view.findViewById(R.id.toolbartxt);
        sessionManager = new SessionManager(getActivity(), getContext(), view);

        briefRecyclerView = (RecyclerView) view.findViewById(R.id.recyclerbrief);
        briefRecyclerView.setLayoutManager(new LinearLayoutManager(getContext()));

//        if (sessionManager.getlang().equals(Constants.ARABICKEY)) {
//            back.setRotationY(180);
//            sessionManager.arabicfont(toolbartxt, null, null, Constants.BOLD, "textV");
//        }
//        back.setOnClickListener(new View.OnClickListener() {
//            @Override
//            public void onClick(View v) {
//                Fragment fragment = new ThreeBtnFragment();
//                getActivity().getSupportFragmentManager().beginTransaction().replace(R.id.fragment_container, fragment).addToBackStack(null).commit();
//            }
//        });

briefBody();

        RadioGroup radioGroup= (RadioGroup)view.findViewById(R.id.radiogroup);
        RadioButton r0= (RadioButton)view.findViewById(R.id.radiobrief);
        RadioButton r1= (RadioButton)view.findViewById(R.id.radiovid);
        RadioButton r2= (RadioButton)view.findViewById(R.id.radioadd);
        RadioButton r3= (RadioButton)view.findViewById(R.id.radiolocation);
        radioGroup.setOnCheckedChangeListener(new RadioGroup.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(RadioGroup group, int checkedId) {
                rb = (RadioButton)view.findViewById(checkedId);
//                if (sessionManager.getlang().equals(Constants.ARABICKEY)){
//                    back.setRotationY(180);
//                    sessionManager.arabicfontRadiobtn(rb,Constants.REGULAR);
//
//                }
                if (rb.getText().equals("Location")||rb.getText().equals("موقع")){
                    r3.setTextColor(Color.WHITE);
                    r0.setTextColor(Color.BLACK);
                    r1.setTextColor(Color.BLACK);
                    r2.setTextColor(Color.BLACK);
                    tab="Location";
//                    homeLL.setVisibility(View.GONE);
//                    addressLL.setVisibility(View.GONE);
//                    videoLL.setVisibility(View.GONE);
//                    mapView.setVisibility(View.VISIBLE);

                }
                else if (rb.getText().equals("Brief")||rb.getText().equals("نبذة")){
                    r0.setTextColor(Color.WHITE);
                    r1.setTextColor(Color.BLACK);
                    r2.setTextColor(Color.BLACK);
                    r3.setTextColor(Color.BLACK);tab="Brief";
//                    homeLL.setVisibility(View.VISIBLE);
//                    addressLL.setVisibility(View.GONE);
//                    videoLL.setVisibility(View.GONE);
//                    mapView.setVisibility(View.GONE);

                }
                else if (rb.getText().equals("Video")||rb.getText().equals("فيديو")){
                    r1.setTextColor(Color.WHITE);
                    r2.setTextColor(Color.BLACK);
                    r3.setTextColor(Color.BLACK);
                    r0.setTextColor(Color.BLACK);
                    tab="Video";
//                    homeLL.setVisibility(View.GONE);
//                    addressLL.setVisibility(View.GONE);
//                    videoLL.setVisibility(View.VISIBLE);
//                    mapView.setVisibility(View.GONE);


                }
                else if (rb.getText().equals("Address")||rb.getText().equals("عنوان")){
                    r2.setTextColor(Color.WHITE);
                    r0.setTextColor(Color.BLACK);
                    r1.setTextColor(Color.BLACK);
                    r3.setTextColor(Color.BLACK);tab="Address";
//                    homeLL.setVisibility(View.GONE);
//                    addressLL.setVisibility(View.VISIBLE);
//                    videoLL.setVisibility(View.GONE);
//                    mapView.setVisibility(View.GONE);

                }

                Log.d("TAG", "id: "+checkedId);

            }
        });
        OnBackPressedDispatcher onBackPressedDispatcher = getActivity().getOnBackPressedDispatcher();
        if (onBackPressedDispatcher != null) {
            onBackPressedDispatcher.addCallback((LifecycleOwner) this, (OnBackPressedCallback) (new OnBackPressedCallback(true) {
                public void handleOnBackPressed() {
                    this.setEnabled(true);
                    Fragment fragment = new ThreeBtnFragment();
                    getActivity().getSupportFragmentManager().beginTransaction().replace(R.id.fragment_container, fragment).addToBackStack(null).commit();
                }
            }));
        }


        return view;
    }




    private void briefBody(){
        sessionManager.progressdialogshow();
        JSONObject briefreqBody = new JSONObject();

        try {
            briefreqBody.put("tabvalue", "home");
            briefJSON(briefreqBody);
        } catch (JSONException e) {
            e.printStackTrace();
        }
    }
    private void briefJSON(JSONObject response) {
        RequestQueue requestQueue = Volley.newRequestQueue(getContext());
        final String requestBody = response.toString();
        StringRequest stringRequest = new StringRequest(Request.Method.POST, Urls.brief, new Response.Listener<String>() {
            @Override
            public void onResponse(String response) {
                sessionManager.progressdialogdismiss();
                Log.d("brief_response", ">>" + response);
                try {
                    JSONObject obj = new JSONObject(response);
                    String message = obj.getString("message");
                    if (obj.getBoolean("error") == false) {
                        briefArrayList = new ArrayList<BriefNewModel>();
                        String data = obj.getString("data");
                        JSONObject maindata = new JSONObject(data);
                        JSONObject location = new JSONObject(maindata.getString("location"));
                        JSONObject home = new JSONObject(maindata.getString("home"));

                        BriefNewModel datamodel = new BriefNewModel(
                                getContext(),
                                maindata.getString("address"),
                                maindata.getString("video"),
                                location.getString("latitute"),
                                location.getString("longitute"),
                                home.getString("title"),
                                home.getString("icon"),
                                home.getString("content"));
                        Log.d("TAG", "longitute: "+location.getString("longitute"));
                        briefArrayList.add(datamodel);
                        briefAdapter = new BriefNewAdapter(getContext(),briefArrayList,briefNewFragment.this);
                        briefRecyclerView.setAdapter(briefAdapter);
                    } else {
                        Toast.makeText(getContext(), message.toString(), Toast.LENGTH_SHORT).show();
                    }
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                Log.e("VOLLEY", error.toString());
                sessionManager.progressdialogdismiss();
                //  sessionManager.volleyerror(error);
                Toast.makeText(getContext(), error.toString(), Toast.LENGTH_SHORT).show();
            }
        }) {
            @Override
            public Map<String, String> getHeaders() throws AuthFailureError {
                Map<String, String> params = new HashMap<String, String>();
                params.put("lang",sessionManager.getlang());
                Log.d("TAG", "getHeaders: "+params);
                return params;
            }

            @Override
            public String getBodyContentType() {
                return "application/json; charset=utf-8";
            }

            @Override
            public byte[] getBody() throws AuthFailureError {
                try {
                    return requestBody == null ? null : requestBody.getBytes("utf-8");
                } catch (UnsupportedEncodingException uee) {
                    VolleyLog.wtf("Unsupported Encoding while trying to get the bytes of %s using %s", requestBody, "utf-8");
                    return null;
                }
            }
        };
        stringRequest.setRetryPolicy(new DefaultRetryPolicy(10000, DefaultRetryPolicy.DEFAULT_MAX_RETRIES, DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
        Log.d("asdsdjkxdk", "" + stringRequest);
        requestQueue.add(stringRequest);
    }

}