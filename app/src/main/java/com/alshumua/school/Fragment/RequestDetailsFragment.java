package com.alshumua.school.Fragment;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;

import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.Toast;

import com.alshumua.school.Activity.DashboardActivity;
import com.alshumua.school.Activity.RequestSentActivity;
import com.alshumua.school.Adapter.RequestDetailAdapter;
import com.alshumua.school.Model.RequestDetailsModel;
import com.alshumua.school.Std.Constants;
import com.alshumua.school.Std.Dialog;
import com.alshumua.school.Std.SessionManager;
import com.alshumua.school.Std.Urls;
import com.android.volley.AuthFailureError;
import com.android.volley.DefaultRetryPolicy;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.VolleyLog;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;
import com.alshumua.school.R;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.UnsupportedEncodingException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

public class RequestDetailsFragment extends Fragment {
    //    var
    SessionManager sessionManager;
    View view;
    private static ArrayList<RequestDetailsModel> requestDetailArrayList;
    private static RecyclerView.Adapter detailsAdapter;
    private static RecyclerView detailsRecyclerView;
    Bundle args = new Bundle();
    int requestId;
    ImageView back;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        view = inflater.inflate(R.layout.fragment_request_details, container, false);

//        hooks
        sessionManager = new SessionManager(getActivity(), getContext(), view);
        detailsRecyclerView = (RecyclerView) view.findViewById(R.id.requestDetailsRecyclerView);
        back = (ImageView) view.findViewById(R.id.back_from_myrequestdetail);
        detailsRecyclerView.setLayoutManager(new LinearLayoutManager(getContext()));

//        no network check
        if (!sessionManager.isNetwork() == true) {
            Dialog dialog = new Dialog();
            dialog.show(getChildFragmentManager(), Constants.NONETWORK);
        }

//        get arguments
        args = getArguments();
        requestId = args.getInt("requestId");

//        Rotate arrow in arabic
        if (sessionManager.getlang().equals(Constants.ARABICKEY)) {
            back.setRotationY(180);
        }

//        get data
        ReqDetailsBody();

//        back btn click
        back.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Fragment fragment = new MyRequestFragment();
                getActivity().getSupportFragmentManager().beginTransaction().replace(R.id.fragment_container, fragment).addToBackStack(null).commit();
            }
        });
        return view;

    }

    public void ReqDetailsBody() {
        JSONObject requestbody = new JSONObject();
        try {
            requestbody.put("requestid", requestId);
            requestDetailsJSON(requestbody);
            sessionManager.progressdialogshow();
        } catch (JSONException e) {
            e.printStackTrace();
        }
    }

    private void requestDetailsJSON(JSONObject response) {
        RequestQueue requestQueue = Volley.newRequestQueue(getContext());
        final String requestBody = response.toString();
        StringRequest stringRequest = new StringRequest(Request.Method.POST, Urls.requestdetails, new Response.Listener<String>() {
            @Override
            public void onResponse(String response) {
                sessionManager.progressdialogdismiss();
                try {
                    JSONObject obj = new JSONObject(response);
                    if (obj.getBoolean("error") == false) {
                        requestDetailArrayList = new ArrayList<RequestDetailsModel>();
                        JSONArray obj1 = new JSONArray(obj.getString("data"));
                        for (int i = 0; i < obj1.length(); i++) {
                            JSONObject jsonObject = obj1.getJSONObject(i);
                            RequestDetailsModel datamodel = new RequestDetailsModel(
                                    jsonObject.getString("serviceName"),
                                    jsonObject.getString("status"),
                                    jsonObject.getString("remarks"),
                                    jsonObject.getString("date"),
                                    jsonObject.getString("questionAnswers"));
                            requestDetailArrayList.add(datamodel);
                        }
                        detailsAdapter = new RequestDetailAdapter(getContext(), requestDetailArrayList,((DashboardActivity)getActivity()));
                        detailsRecyclerView.setAdapter(detailsAdapter);
                    } else {
                        String msg = obj.getString("message");
                        Toast.makeText(getContext(), msg, Toast.LENGTH_SHORT).show();
                    }
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                sessionManager.progressdialogdismiss();
                Toast.makeText(getContext(), error.getMessage(), Toast.LENGTH_SHORT).show();
            }
        }) {
            @Override
            public Map<String, String> getHeaders() throws AuthFailureError {
                Map<String, String> params = new HashMap<String, String>();
                params.put("lang", sessionManager.getlang());
                return params;
            }

            @Override
            public String getBodyContentType() {
                return "application/json; charset=utf-8";
            }

            @Override
            public byte[] getBody() throws AuthFailureError {
                try {
                    return requestBody == null ? null : requestBody.getBytes("utf-8");
                } catch (UnsupportedEncodingException uee) {
                    VolleyLog.wtf("Unsupported Encoding while trying to get the bytes of %s using %s", requestBody, "utf-8");
                    return null;
                }
            }
        };
        stringRequest.setRetryPolicy(new DefaultRetryPolicy(10000, DefaultRetryPolicy.DEFAULT_MAX_RETRIES, DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
        requestQueue.add(stringRequest);
    }

}