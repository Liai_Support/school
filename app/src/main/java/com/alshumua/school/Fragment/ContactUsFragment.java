package com.alshumua.school.Fragment;

import android.content.Intent;
import android.content.pm.PackageManager;
import android.net.Uri;
import android.os.Bundle;

import androidx.cardview.widget.CardView;
import androidx.fragment.app.Fragment;

import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.Toast;

import com.alshumua.school.Std.Constants;
import com.alshumua.school.Std.SessionManager;
import com.alshumua.school.R;

import java.util.Objects;


public class ContactUsFragment extends Fragment {
View view;
CardView call,message,whatsapp,mail;
SessionManager sessionManager;
ImageView back;
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        view=inflater.inflate(R.layout.fragment_contact_us, container, false);
        call = (CardView) view.findViewById(R.id.callus);
        message = (CardView) view.findViewById(R.id.messagewithus);

        whatsapp = (CardView) view.findViewById(R.id.whatsapp);
        mail = (CardView) view.findViewById(R.id.email);
        back = (ImageView) view.findViewById(R.id.back_from_contact);

        sessionManager = new SessionManager(getActivity(),getContext(),view);
        if (sessionManager.getlang().equals(Constants.ARABICKEY)){
            back.setRotationY(180);
        }
        back.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Fragment fragment = new ProfileFragment();
                getActivity().getSupportFragmentManager().beginTransaction().replace(R.id.fragment_container, fragment).addToBackStack(null).commit();
            }
        });

        call.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

            }
        });

        message.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

            }
        });

        whatsapp.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

//                check app installed or not
                boolean installed = appInstalledOrNot("com.alshumua.school");

                if (installed){
                    Intent intent=new Intent(Intent.ACTION_VIEW);
//                    intent.setData(Uri.parse("http://api.whatsapp.com/send?phone=" +"+966"+phn+"&text="+msg));
//                    intent.setData(Uri.parse("http://api.whatsapp.com/send?phone=" +"+966"+phn));
                    intent.setData(Uri.parse("http://api.whatsapp.com/send?phone=" +"+91"+"9677218682"));
                    startActivity(intent);
                }
                else{
                    Toast.makeText(getContext(),R.string.appnotinstalled,Toast.LENGTH_SHORT);
                }
            }
        });
        mail.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                sendmail();
            }
        });

        return  view;
    }
    private boolean appInstalledOrNot(String packageName) {
        PackageManager pm = requireContext().getPackageManager();
        boolean app_installed;
        try {
            pm.getPackageInfo(packageName, PackageManager.GET_ACTIVITIES);
            app_installed = true;
        }
        catch (PackageManager.NameNotFoundException e) {
            app_installed = false;
        }
        return app_installed;
    }
    private void sendmail() {
        String recipientList="info@shumua.edu.sa";
        String[] recipients=recipientList.split(",");
        Log.d("TAG", "sendmail: "+sessionManager.getcustmail());
        Intent intent=new Intent(Intent.ACTION_SEND);
        intent.putExtra(intent.EXTRA_EMAIL,recipients);
        intent.putExtra(intent.EXTRA_SUBJECT,"Queries");
        intent.putExtra(intent.EXTRA_TEXT,"");

   /*     Intent emailIntent = new Intent(Intent.ACTION_SEND);
//        emailIntent.setType("text/plain");
        emailIntent.setType("message/rfc822");
        startActivity(emailIntent);*/

        intent.setType("message/rfc822");

        startActivity(Intent.createChooser(intent,"chooseanemailclient"));
    }
}