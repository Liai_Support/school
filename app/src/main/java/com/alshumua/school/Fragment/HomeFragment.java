package com.alshumua.school.Fragment;

import android.content.Intent;
import android.os.Bundle;

import androidx.activity.OnBackPressedCallback;
import androidx.activity.OnBackPressedDispatcher;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentTransaction;
import androidx.lifecycle.LifecycleOwner;
import androidx.recyclerview.widget.GridLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.os.Handler;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.inputmethod.EditorInfo;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.alshumua.school.Activity.DashboardActivity;
import com.alshumua.school.Activity.LanguageActivity;
import com.alshumua.school.Activity.ThreeBtnScreenActivity;
import com.alshumua.school.Adapter.ServiceAdapter;
import com.alshumua.school.Model.ServiceModel;
import com.alshumua.school.Std.Constants;
import com.alshumua.school.Std.Dialog;
import com.alshumua.school.Std.SessionManager;
import com.alshumua.school.Std.Urls;
import com.alshumua.school.databinding.FragmentHomeBinding;
import com.alshumua.school.databinding.FragmentProfileSettingBinding;
import com.android.volley.AuthFailureError;
import com.android.volley.DefaultRetryPolicy;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.VolleyLog;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;
import com.alshumua.school.R;
import com.google.android.material.bottomnavigation.BottomNavigationView;
import com.squareup.picasso.Picasso;


import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.UnsupportedEncodingException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

public class HomeFragment extends Fragment {
// VAR

    SessionManager sessionManager;
    View view;
    private static ArrayList<ServiceModel> serviceModelArrayList;
    private ServiceAdapter serviceAdapter;

    boolean doubleBackToExitPressedOnce = false;
    Integer id;



FragmentHomeBinding binding;
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View layout = LayoutInflater.from(requireContext()).inflate(R.layout.fragment_home, container, false);
        binding = FragmentHomeBinding.bind(layout);
// HOOKS
        BottomNavigationView navBar = getActivity().findViewById(R.id.bottom_navigation);
        navBar.setVisibility(View.VISIBLE);

        sessionManager = new SessionManager(getActivity(),getContext(),view);
        binding.serviceRecyclerview.setLayoutManager(new GridLayoutManager(getContext(), 3));

// NETWORK CHECK
        if (!sessionManager.isNetwork() == true) {
            Dialog dialog = new Dialog();
            dialog.show(getChildFragmentManager(), Constants.NONETWORK);
        }
        if (sessionManager.getlang().equals(Constants.ARABICKEY)){
            sessionManager.arabicfont(binding.home,null,null,Constants.BOLD,"textV");
            sessionManager.arabicfontHint(binding.outlinedTextField1,Constants.REGULAR);
            sessionManager.arabicfont(binding.usernameHome,null,null,Constants.BOLD,"textV");
            sessionManager.arabicfont(binding.hloText,null,null,Constants.REGULAR,"textV");
            sessionManager.arabicfont(binding.servicelist,null,null,Constants.REGULAR,"textV");

        }
// METHODS
        serviceReqBody();
if (sessionManager.gettokenin()!=null|| !sessionManager.gettokenin().equals("")){
    viewProfile();
}


//        viewProfile();


// SET USERNAME ON HOME
/*if (!sessionManager.getparent_name().equals("")||!sessionManager.getparent_name().equals(null)) {
    binding.usernameHome.setText(String.valueOf(sessionManager.getstudent_id()));
}*/
//search filter

{
    binding.edittext.setImeOptions(EditorInfo.IME_ACTION_GO);
}
        binding.edittext.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {
            }
            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
            }
            @Override
            public void afterTextChanged(Editable s) {
                filter(s.toString());
            }
        });


// BUTTON CLICK
        binding.notification.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Fragment fragment = new NotifictionFragment();
                getActivity().getSupportFragmentManager().beginTransaction().replace(R.id.fragment_container, fragment).addToBackStack(null).commit();
            }
        });

// back press-HARDWARE
        OnBackPressedDispatcher onBackPressedDispatcher = getActivity().getOnBackPressedDispatcher();
        if (onBackPressedDispatcher != null) {
            onBackPressedDispatcher.addCallback((LifecycleOwner)this, (OnBackPressedCallback)(new OnBackPressedCallback(true) {
                public void handleOnBackPressed() {
                    this.setEnabled(true);
                   /* if (doubleBackToExitPressedOnce) {
                        Intent intent = new Intent(Intent.ACTION_MAIN);
                        intent.addCategory(Intent.CATEGORY_HOME);
                        startActivity(intent);
                        return;
                    }
                    doubleBackToExitPressedOnce = true;
                    Toast.makeText(getActivity(), getString(R.string.PressBackAgaintoExit), Toast.LENGTH_SHORT).show();
                    new Handler().postDelayed(() -> doubleBackToExitPressedOnce=false, 2000);*/
                    Fragment fragment = new ThreeBtnFragment();
                    getActivity().getSupportFragmentManager().beginTransaction().replace(R.id.fragment_container, fragment).addToBackStack(null).commit();
                }
            }));
        }
        return binding.getRoot();

    }

    private void viewProfile() {
        JSONObject requestbody = new JSONObject();
        try {
            requestbody.put("id", sessionManager.getid());
            ViewProfilerequestJSON(requestbody);
            sessionManager.progressdialogshow();
        } catch (JSONException e) {
            e.printStackTrace();
        }
    }
    public void serviceReqBody() {
        JSONObject requestbody = new JSONObject();
        try {
            requestbody.put("id", sessionManager.getid());
            ServicerequestJSON(requestbody);
           
            sessionManager.progressdialogshow();
        } catch (JSONException e) {
            e.printStackTrace();
        }
    }
    private void ServicerequestJSON(JSONObject response) {
        RequestQueue requestQueue = Volley.newRequestQueue(getContext());
        final String requestBody = response.toString();
        Log.d("Servicerequest", String.valueOf(response));
        StringRequest stringRequest = new StringRequest(Request.Method.POST, Urls.Services, new Response.Listener<String>() {
            @Override
            public void onResponse(String response) {
                sessionManager.progressdialogdismiss();
                Log.d("Service_response", ">>" + response);
                try {
                    JSONObject obj = new JSONObject(response);
                    if (obj.getBoolean("error") == false) {
                        serviceModelArrayList = new ArrayList<ServiceModel>();
                        JSONArray obj1 = new JSONArray(obj.getString("data"));
                        for (int i = 0; i < obj1.length(); i++) {
                            JSONObject jsonObject = obj1.getJSONObject(i);
                            ServiceModel datamodel = new ServiceModel(getContext(),
                                    jsonObject.getInt("id"),
                                    jsonObject.getString("serviceName"),
                                    jsonObject.getString("description"),
                                    jsonObject.getString("serviceIcon"),
                                    jsonObject.getString("loginRequired"),
                                    jsonObject.getString("initial"),
                                    jsonObject.getString("symptoms")
                                    );
                            serviceModelArrayList.add(datamodel);
                        }
                        serviceAdapter = new ServiceAdapter(HomeFragment.this,getContext(),serviceModelArrayList,((DashboardActivity)getActivity()));
                        binding.serviceRecyclerview.setAdapter(serviceAdapter);
                    } else {
                    }
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                sessionManager.progressdialogdismiss();
                Toast.makeText(getContext(), error.getMessage(), Toast.LENGTH_SHORT).show();
            }
        }) {
            @Override
            public Map<String, String> getHeaders() throws AuthFailureError {
                Map<String, String> params = new HashMap<String, String>();
                params.put("lang",sessionManager.getlang());
                return params;
            }

            @Override
            public String getBodyContentType() {
                return "application/json; charset=utf-8";
            }

            @Override
            public byte[] getBody() throws AuthFailureError {
                try {
                    return requestBody == null ? null : requestBody.getBytes("utf-8");
                } catch (UnsupportedEncodingException uee) {
                    VolleyLog.wtf("Unsupported Encoding while trying to get the bytes of %s using %s", requestBody, "utf-8");
                    return null;
                }
            }
        };
        stringRequest.setRetryPolicy(new DefaultRetryPolicy(10000, DefaultRetryPolicy.DEFAULT_MAX_RETRIES, DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
        Log.d("asdsdjkxdk", "" + stringRequest);
        requestQueue.add(stringRequest);
    }
    private void ViewProfilerequestJSON(JSONObject response) {
        RequestQueue requestQueue = Volley.newRequestQueue(getContext());
        final String requestBody = response.toString();
        Log.d("profile", String.valueOf(response));
        StringRequest stringRequest = new StringRequest(Request.Method.POST, Urls.Viewprofile, new Response.Listener<String>() {
            @Override
            public void onResponse(String response) {
                sessionManager.progressdialogdismiss();
                Log.d("viewprofileresponse", ">>" + response);
                try {
                    JSONObject obj = new JSONObject(response);
                    if (obj.getBoolean("error") == true) {
                        String msg = obj.getString("message");
                        Toast.makeText(getContext(), msg, Toast.LENGTH_SHORT).show();
                    } else {
                        String msg = obj.getString("data");
                        JSONObject tok = new JSONObject(msg);

                        sessionManager.setusernamedisplay(tok.getString("username"));
                        sessionManager.setprofile_img(tok.getString("profile_img"));
                        binding.usernameHome.setText(sessionManager.getusernamedisplay());
                        sessionManager.setfirst_name_common(tok.getString("first_name"));
                        sessionManager.setlast_name_common(tok.getString("last_name"));
                        sessionManager.setemail_common(tok.getString("email"));
                        sessionManager.setmobile_number_common(tok.getString("mobile_number"));
                        sessionManager.setparent_name_common(tok.getString("parent_name"));
                        Picasso.get().load(sessionManager.getprofile_img()).placeholder(R.drawable.profiledefault).error(R.drawable.profiledefault).into(binding.profileImghome);

                    }
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {//                sessionManager.volleyerror(error);
                Toast.makeText(getContext(), error.getMessage(), Toast.LENGTH_SHORT).show();
                sessionManager.progressdialogdismiss();
            }
        }) {
            @Override
            public Map<String, String> getHeaders() throws AuthFailureError {
                int id = sessionManager.getid();
                Map<String, String> params = new HashMap<String, String>();
                params.put("lang",sessionManager.getlang() );
                return params;
            }

            @Override
            public String getBodyContentType() {
                return "application/json; charset=utf-8";
            }

            @Override
            public byte[] getBody() throws AuthFailureError {
                try {
                    return requestBody == null ? null : requestBody.getBytes("utf-8");
                } catch (UnsupportedEncodingException uee) {
                    VolleyLog.wtf("Unsupported Encoding while trying to get the bytes of %s using %s", requestBody, "utf-8");
                    return null;
                }
            }
        };
        stringRequest.setRetryPolicy(new DefaultRetryPolicy(10000, DefaultRetryPolicy.DEFAULT_MAX_RETRIES, DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
        Log.d("asdsdjkxdk", "" + stringRequest);
        requestQueue.add(stringRequest);
    }
    private void filter(String text) {
        ArrayList<ServiceModel> filteredList = new ArrayList<>();
        for (ServiceModel item : serviceModelArrayList) {
            if (item.getServiceName().toLowerCase().contains(text.toLowerCase())) {
                filteredList.add(item);
            }
        }
        serviceAdapter.filterList(filteredList);
    }


}